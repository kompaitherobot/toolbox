import { Component, Input, OnInit } from "@angular/core";
import { PoiPage } from "../../pages/poi/poi";
import { TutoPage } from "../../pages/tuto/tuto";
import { NavController } from "ionic-angular";
import { ApiService } from "../../services/api.service";
import { AlertService } from "../../services/alert.service";
import { PopupService } from "../../services/popup.service";
import { SpeechService } from "../../services/speech.service";
import { ParamService } from "../../services/param.service";
import { ParamPage } from "../../pages/param/param";
import { ToastController } from "ionic-angular";

@Component({
  selector: "headpage",
  templateUrl: "headpage.html",
})
export class HeadpageComponent implements OnInit {
  poiPage = PoiPage;
  tutoPage = TutoPage;
  paramPage = ParamPage;

  cptduration: any;
  lowbattery: any;
  cpt_battery: number;
  cpt_open: number;
  microon: boolean;
  cabinet_close: boolean = true;
  update: any;
  optiondateofday: any;
  textBoutonHeader: string; // bouton 's text (Return / Exit), to the left of the header, depends on the page you are on
  textHeadBand: string;
  volumeState: number; //0 mute,  1 low,  2 hight
  source: any;

  @Input() pageName: string;
  constructor(
    public speech: SpeechService,
    public toastCtrl: ToastController,
    public navCtrl: NavController,
    public param: ParamService,
    public api: ApiService,
    public alert: AlertService,
    public popup: PopupService
  ) {
    this.optiondateofday = { weekday: "long", day: "numeric", month: "long" };
    this.microon = false;

    this.cpt_battery = 0;
    this.cpt_open = 0;

    if (this.param.allowspeech == 1) {
      this.volumeState = 1;
    } else {
      this.volumeState = 0;
    }
  }

  ionViewWillLeave() {
    clearInterval(this.update);
    clearInterval(this.lowbattery);
    clearInterval(this.source);
    clearInterval(this.cptduration);
  }

  ngOnDestroy(): void {
    clearInterval(this.update);
    clearInterval(this.lowbattery);
    clearInterval(this.source);
    clearInterval(this.cptduration);
  }

  updateBattery() {
    if (this.api.statusRobot === 2) {
      // battery logo will not apppear
      this.api.batteryState = 10;
    } else if (this.api.battery_status.status < 2) {
      //charging
      this.api.batteryState = 4;
    } else if (
      this.api.battery_status.status === 3 ||
      this.api.battery_status.remaining <= this.param.battery.critical
    ) {
      //critical
      this.api.batteryState = 0;
    } else if (this.api.battery_status.remaining > this.param.battery.high) {
      //hight
      this.api.batteryState = 3;
    } else if (this.api.battery_status.remaining > this.param.battery.low) {
      //mean
      this.api.batteryState = 2;
    } else if (this.api.battery_status.remaining <= this.param.battery.low) {
      //low
      this.api.batteryState = 1;
    } else {
      // battery logo will not apppear
      this.api.batteryState = 10;
    }
  }

  onTouchWifi(ev) {
    ev.preventDefault();
    if (!this.api.wifiok) {
      this.okToast(this.param.datatext.nointernet);
    } else {
      this.okToast(this.param.datatext.robotConnectedToI);
    }
  }

  mail() {

    this.param.cptDuration(); //update duration
    if (this.param.durationNS.length > 1) {
      if (this.api.wifiok) {
        //this.param.updateDurationNS(this.param.durationNS[0].id_duration);
        this.alert.duration(this.param.durationNS[0], this.api.mailAddInformationBasic());
        //this.param.getDurationNS();
      }
    }
  }

  ngOnInit() {
    this.popup.onSomethingHappened5(this.accessparam.bind(this));
    // bouton 's text (Return / Exit), to the left of the header, depends on the page you are on
    if (this.pageName === "poi") {
      this.update = setInterval(() => this.getUpdate(), 1000); //update hour, battery every 1/2 secondes
      this.lowbattery = setInterval(() => this.monitoringBattery(), 60000);
      this.source = setInterval(() => this.api.jetsonOK(), 2000);
      this.cptduration = setInterval(() => this.mail(), 120000);
      this.textBoutonHeader = this.param.datatext.quit;
      this.api.textHeadBand = this.param.datatext.toolbox;
    } else if (this.pageName === "rounddisplay") {
      this.textBoutonHeader = this.param.datatext.return;
      this.api.textHeadBand = this.param.datatext.headBand_rounds;
    } else if (this.pageName === "roundcreation") {
      this.textBoutonHeader = this.param.datatext.return;
      this.api.textHeadBand = this.param.datatext.roundcreation;
    } else if (this.pageName === "param") {
      this.textBoutonHeader = this.param.datatext.return;
      this.api.textHeadBand = this.param.datatext.param;
    } else if (this.pageName === "roundedit") {
      this.textBoutonHeader = this.param.datatext.return;
      this.api.textHeadBand = this.param.datatext.rounds_edit;
    } else if (this.pageName === "paramuv") {
      this.textBoutonHeader = this.param.datatext.return;
      this.api.textHeadBand = this.param.datatext.uvsettings.toUpperCase();
    } else if (this.pageName === "qrcode") {
      this.textBoutonHeader = this.param.datatext.return;
      this.api.textHeadBand = this.param.datatext.qrcodes;
    } else if (this.pageName === "lift") {
      this.textBoutonHeader = this.param.datatext.return;
      this.api.textHeadBand = "BELHARRA";
    }
    else if (this.pageName === "langue") {
      this.textBoutonHeader = this.param.datatext.return;
      this.api.textHeadBand = this.param.datatext.changelangage;
    }
    else if (this.pageName === "password") {
      this.textBoutonHeader = this.param.datatext.return;
      this.api.textHeadBand = this.param.datatext.editpswd;
    } else if (this.pageName === "docking") {
      this.textBoutonHeader = this.param.datatext.return;
      this.api.textHeadBand = this.param.datatext.configDocking.toUpperCase();
    } else {
      this.textBoutonHeader = this.param.datatext.return;
      this.api.textHeadBand = this.param.datatext.tutorial;
    }
  }
  accessparam() {
    this.navCtrl.push(this.paramPage);
  }
  onTouchParam(ev) {
    ev.preventDefault();
    if (!this.api.roundActive && !this.api.towardDocking) {
      //robot is not moving
      if (
        this.pageName === "param" ||
        this.pageName === "sms" ||
        this.pageName === "mail" ||
        this.pageName === "paramuv" ||
        this.pageName === "langue" ||
        this.pageName === "password"
      ) {
        console.log("already");
      } else {
        this.popup.askpswd();
      }
    }
  }

  onTouchStatus(ev) {
    ev.preventDefault();
    if (!this.api.roundActive && !this.api.towardDocking) {
      if (this.api.statusRobot === 2) {
        // if status red
        this.popup.statusRedPresent();
      } else {
        this.okToast(this.param.datatext.statusGreenPresent_message);
      }
    }
  }

  okToast(m: string) {
    const toast = this.toastCtrl.create({
      message: m,
      duration: 3000,
      position: "middle",
      cssClass: "toastok",
    });
    toast.present();
  }

  monitoringBattery() {
    if (this.api.batteryState === 0) {
      this.alert.lowBattery(this.api.mailAddInformationBasic());

      if (!this.api.towardDocking) {
        this.api.towardDocking = true;
        setTimeout(() => {
          this.api.reachHttp("docking");
        }, 2000);
      }
      clearInterval(this.lowbattery);
    }
  }

  onTouchVolume() {
    if (this.volumeState === 0) {
      this.volumeState = 1;
      this.param.allowspeech = 1;
    } else {
      this.volumeState = 0;
      this.param.allowspeech = 0;
    }
  }

  onTouchBattery(ev) {
    ev.preventDefault();
    if (this.api.batteryState == 0) {

      this.popup.showToastRed(
        this.param.datatext.lowBattery_title +
        " : " +
        this.api.battery_status.remaining +
        "%", 3000, "top"
      );
    } else {
      this.okToast(
        this.param.datatext.battery +
        " : " +
        this.api.battery_status.remaining +
        "%"
      );
    }
  }

  getUpdate() {
    //update hour, battery
    this.updateBattery();
    this.updateBand();
    var now = new Date().toLocaleString("fr-FR", { hour: "numeric", minute: "numeric" });

    this.api.hour = now;
    //console.log(this.batteryState);
    if (this.api.statusRobot === 0 && this.api.batteryState === 10) {
      // importante to know if the application has run correctly
      this.cpt_battery += 1;
    } else {
      this.cpt_battery = 0;
    }
    if (this.cpt_battery > 10) {
      this.api.statusRobot = 2;
    }
  }

  updateBand() {
    if (this.api.is_blocked || this.api.is_high) {
      this.textHeadBand = this.param.datatext.errorBlocked_title.toUpperCase();
    } else if (this.pageName === "poi") {
      this.textHeadBand = this.param.datatext.toolbox;
    }

  }

  onTouchHelp(ev) {
    ev.preventDefault();
    if (!this.api.roundActive && !this.api.towardDocking) {
      //robot is not moving
      if (!(this.pageName === "tuto")) {
        this.alert.displayTuto(this.api.mailAddInformationBasic());
        this.navCtrl.push(this.tutoPage);
      }
    }
  }

  onTouchLocked(ev) {

    ev.preventDefault();
    if (this.param.robot.num_of_cabinet != 0) {
      this.cabinet_close = false;
      this.popup.showToastGreen(this.param.datatext.cabinet_open, 5000, "middle");
      for (let i = 0; i < this.param.robot.num_of_cabinet; i++) {
        console.log
        this.api.cabinethttp(i, true);
      }

      setTimeout(() => {
        this.cabinet_close = true;
      }, 5000);
    }


  }

  clicOnMenu(ev) {
    ev.preventDefault();
    if (
      this.pageName === "tuto" ||
      this.pageName === "param" ||
      this.pageName === "rounddisplay" ||
      this.pageName === "qrcode" ||
      this.pageName === "lift" ||
      this.pageName === "docking"
    ) {
      this.navCtrl.popToRoot();
    } else if (
      this.pageName === "paramuv" ||
      this.pageName === "langue" ||
      this.pageName === "password" ||
      this.pageName === "roundcreation" ||
      this.pageName === "roundedit"
    ) {
      this.navCtrl.pop();
    } else {
      this.api.close_app = true;
      this.alert.appClosed(this.api.mailAddInformationBasic());
      //stop the round and quit the app

      setTimeout(() => {
        window.close();
      }, 1000);
    }
  }
}
