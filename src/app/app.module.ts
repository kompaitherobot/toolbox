import { BrowserModule } from "@angular/platform-browser";
import { ErrorHandler, NgModule } from "@angular/core";
import { IonicApp, IonicErrorHandler, IonicModule } from "ionic-angular";
import { SplashScreen } from "@ionic-native/splash-screen";
import { StatusBar } from "@ionic-native/status-bar";
import { ComponentsModule } from "../components/components.module";
import { MyApp } from "./app.component";
import { PoiPage } from "../pages/poi/poi";
import { HeadpageComponent } from "../components/headpage/headpage";
import { ApiService } from "../services/api.service";
import { AlertService } from "../services/alert.service";
import { HttpClientModule } from "@angular/common/http";
import { BackgroundMode } from "@ionic-native/background-mode";
import { TutoPage } from "../pages/tuto/tuto";
import { PopupService } from "../services/popup.service";
import { ParamService } from "../services/param.service";
import { SpeechService } from "../services/speech.service";
import { ParamPage } from "../pages/param/param";
import { LanguePage } from "../pages/langue/langue";
import { QRCodePage } from "../pages/qrcode/qrcode";
import { ParamUVPage } from "../pages/paramuv/paramuv";
import { FirstPage } from "../pages/first/first";
import { RoundDisplayPage } from "../pages/rounddisplay/rounddisplay";
import { RoundCreationPage } from "../pages/roundcreation/roundcreation";
import { RoundEditPage } from "../pages/roundedit/roundedit";
//import { File } from "@ionic-native/file";
import { PasswordPage } from "../pages/password/password";
import { DockingPage } from "../pages/docking/docking";
import { LiftPage } from "../pages/lift/lift";

@NgModule({
  declarations: [
    MyApp,
    FirstPage,
    PoiPage,
    TutoPage,
    LanguePage,
    RoundDisplayPage,
    RoundCreationPage,
    RoundEditPage,
    QRCodePage,
    LiftPage,
    PasswordPage,
    ParamUVPage,
    ParamPage,
    DockingPage
  ],
  imports: [
    BrowserModule,
    ComponentsModule,
    HttpClientModule,
    IonicModule.forRoot(MyApp),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    FirstPage,
    PoiPage,
    TutoPage,
    LanguePage,
    RoundDisplayPage,
    RoundCreationPage,
    RoundEditPage,
    QRCodePage,
    LiftPage,
    PasswordPage,
    ParamUVPage,
    ParamPage,
    DockingPage,
    HeadpageComponent,
  ],
  providers: [
    BackgroundMode,
    ApiService,
    AlertService,
    PopupService,
    ParamService,
    StatusBar,
    SplashScreen,
    SpeechService,
    //File,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    { provide: Window, useValue: window },
  ],
})
export class AppModule {}
