import { Component, OnInit, ElementRef, ViewChild } from "@angular/core";
import { ParamService } from "../../services/param.service";
import { ItemSliding, LoadingController, ToastController } from "ionic-angular";
import { ApiService } from "../../services/api.service";
import { NavController } from "ionic-angular";
import { Select, Content } from "ionic-angular"
import { AlertService } from "../../services/alert.service";

@Component({
  selector: "page-roundcreation",
  templateUrl: "roundcreation.html",
})
export class RoundCreationPage implements OnInit {
  @ViewChild("canvas3") canvasEl: ElementRef;
  @ViewChild("imgmap3") imgmapElement: ElementRef;
  @ViewChild("content1") content1: Content;
  private _CANVAS: any;
  initialzoom: any;
  heightcol: any;
  widthcol: any;
  private _CONTEXT: any;
  poiId: number;
  poiName: string;
  poiWait: number;
  poiData: string;
  poiAnticollisionProfile:number;
  poiWay: number;
  poiDisableAvoidance: boolean;
  poiSpeed: number;
  nameRound: string;
  roundCreationPage = RoundCreationPage;
  loading: any;
  imageToShow: any;
  previousimage: any;
  isImageLoading: boolean;
  rawImageToShow: any;
  creation: any;
  loc: any;
  interv: any;
  offsetX: any;
  offsetY: any;
  resomap: any;
  map_url="http://" + this.param.localhost + "/api/maps/current/image?t=" + new Date().getTime();
  imge:any;
  moving: any;
  start: any = {
    x: 0,
    y: 0,
  };

  ngOnInit() {
    this.creation = "carte";
    this._CANVAS = this.canvasEl.nativeElement;   
    this._CONTEXT = this._CANVAS.getContext("2d");
    this._CANVAS.style.width ="100%";
    this._CANVAS.style.height="100%";
 
    this.heightcol = document.getElementById("body3").clientHeight - 100;
    this.widthcol = document.getElementById("testcol3").clientWidth;
  }

  ionViewDidEnter() {
   
    if (this.api.editRound) {
      this.api.textHeadBand = this.param.datatext.rounds_edit;
    }
    this.getImageFromService();
    
    
  }

  @ViewChild("select1") select1 :Select;
  @ViewChild("select2") select2 :Select;
  @ViewChild("select3") select3 :Select;
  @ViewChild("select4") select4 :Select;

  constructor(
    public param: ParamService,
    public navCtrl: NavController,
    public toastCtrl: ToastController,
    public loadingCtrl: LoadingController,
    public api: ApiService,
    public alert: AlertService 
  ) {
    this.poiWait = 0;
    this.poiWay = 1;
    this.poiSpeed = 80;
    this.poiData = "none";
    this.nameRound = "";
    this.poiAnticollisionProfile=0;
    this.poiDisableAvoidance = false; //avoidance activated

    if (this.api.copyRound) {
      this.nameRound = this.api.copy_round.Name + "-copy";
      this.displayCopy();
    }
    if (this.api.editRound) {
      this.api.textHeadBand = this.param.datatext.rounds_edit;
      this.nameRound = this.api.copy_round.Name;
    }
    this.api.all_locations = this.api.all_locations.filter(
      (x) => x.Label !== "qr" && x.Name !=="docking" && x.Name!=="docked"
    );
    
  }

  public open(ev, itemSlide: ItemSliding) {
    ev.preventDefault();
    if (itemSlide.getSlidingPercent() == 0) {
      // Two calls intentional after examination of vendor.js function
      itemSlide.moveSliding(-170);
      itemSlide.moveSliding(-170);
    } else {
      itemSlide.close();
    }
  }

  on_zoom_out(ev) {
    ev.preventDefault();
    if (!(this.resomap >= this.initialzoom)) {
      this.resomap = this.resomap + 150 / 100000;
    } else {
      this.offsetX = 0;
      this.offsetY = 0;
    }
    this.createImageFromBlob();
  }

  on_zoom_in(ev) {
    ev.preventDefault();
    if (this.resomap > this.initialzoom / 4) {
      this.resomap = this.resomap - 150 / 100000;
    }
    this.createImageFromBlob();
  }
  ionViewWillLeave() {
    //clearInterval(this.interv);
  }

  displayCopy() {
    this.api.copy_round.Locations.forEach((element) => {
      this.api.createRoundList(
        element.Location.Id,
        element.Location.Name,
        element.Wait,
        element.Speed,
        element.Data,
        element.AnticollisionProfile,
        element.DisableAvoidance
      );
    });
  }

  onWayChange(event) {
    console.log(event);
    this.poiWay = parseInt(event);
    console.log(this.poiWay);
  }

  onGabaritChange(){
    console.log(this.poiAnticollisionProfile);
    
  }

  onOptionChange(event) {
    console.log(event);
    this.poiData = event;
    console.log(this.poiData);
  }

  onToggleChange() {
    this.poiDisableAvoidance = !this.poiDisableAvoidance;
    console.log(this.poiDisableAvoidance);
  }

  onPOIChange(event) {
    this.poiId = event;
    this.poiName = this.api.all_locations.filter(
      (poi) => poi.Id == event
    )[0].Name;
    console.log(this.poiName);
    console.log(this.poiId);
  }

  addPOIRound(ev) {
    ev.preventDefault();
    this.poiWait = Math.abs(this.poiWait);
    if (Math.abs(this.poiSpeed) > 100) {
      this.poiSpeed = 100;
    }
    console.log(this.poiWait);
    console.log(this.poiSpeed);
    console.log(this.poiWay);
    this.api.createRoundList(
      this.poiId,
      this.poiName,
      this.poiWait,
      (Math.abs(this.poiSpeed) * this.poiWay) / 100,
      this.poiData,
      this.poiAnticollisionProfile,
      this.poiDisableAvoidance
    );
    setTimeout(() => {
      this.content1.scrollToBottom();
    }, 300
    );
  }

  cancelCreation(ev) {
    ev.preventDefault();
    this.navCtrl.pop();
    this.api.roundpoi_list = [];
    if (this.api.editRound) {
      this.displayCopy();
    }
  }

  reorderItems(indexes) {
    let element = this.api.roundpoi_list[indexes.from];
    this.api.roundpoi_list.splice(indexes.from, 1);
    this.api.roundpoi_list.splice(indexes.to, 0, element);
  }

  createRound(ev) {
    this.alert.addround(this.api.mailAddInformationBasic());
    ev.preventDefault();
    this.nameRound = this.nameRound.replace(
      new RegExp("[<>&$§µ£#*_|`¤~(){}°²@%/]", "g"),
      ""
    );
    this.nameRound = this.nameRound.replace(new RegExp('["]', "g"), "");
    this.nameRound = this.nameRound.replace(new RegExp(/\\/, "g"), "");
    this.nameRound = this.nameRound.replace(new RegExp(/[\[\]]/, "g"), "");
    if (this.nameRound != "") {
      
      this.showLoading();
      if (!this.api.editRound && this.api.roundpoi_list.length > 1) {
        this.api.postRound(this.nameRound);
        console.log("post");
      } else {
        this.api.copy_round.Name = this.nameRound;
        this.api.putRound(this.nameRound, this.api.copy_round.Id);
      }
      console.log(this.nameRound);
      setTimeout(() => {
        this.api.getRoundList();
      }, 2000);
      setTimeout(() => {
        if(this.api.editRound){
          this.api.copy_round=this.api.round_current_map.filter( (x) => x.Id == this.api.copy_round.Id
        )[0];
        }
        
        this.dismissLoading();
        this.api.smgthCreated = true;
        this.navCtrl.pop();
        
      }, 3500);
      
    }
  }

  deletePOI(ev, index: number) {
    ev.preventDefault();
    this.api.roundpoi_list.splice(index, 1);
  }



  on_mouse_down(evt) {
    this.createImageFromBlob();
    //console.log(evt);

    if (evt.button == 0) {
      evt.preventDefault();
      this.moving = true;
      this.start.x = evt.x;
      this.start.y = evt.y;

      //console.log(evt.x + " " + this.start.x);
    }
  }

  on_mouse_up(evt) {
    this.createImageFromBlob();
    //console.log(evt);
    if (this.moving) {
      var offset = {
        x: this.offsetX,
        y: this.offsetY,
        t: 0,
      };

      offset.x += evt.x - this.start.x;
      offset.y += evt.y - this.start.y;

      this.start.x = evt.x;
      this.start.y = evt.y;

      this.offsetX = offset.x;
      this.offsetY = offset.y;
    }
    this.moving = false;
  }

  on_mouse_move(evt) {

    if (this.moving) { 
      this.createImageFromBlob();
      evt.preventDefault();

      var offset = {
        x: this.offsetX,
        y: this.offsetY,
      };

      //console.log(evt.x + " " + this.start.x);

      offset.x += evt.x - this.start.x;
      offset.y += evt.y - this.start.y;

      this.start.x = evt.x;
      this.start.y = evt.y;

      this.offsetX = offset.x;
      this.offsetY = offset.y;
    }
  }

  on_wheel(evt) {
    this.createImageFromBlob();
    evt.preventDefault();

    //console.log(evt.deltaY);
    if (evt.deltaY > 0) {
      if (!(this.resomap >= this.initialzoom)) {
        this.resomap = this.resomap + 150 / 100000;
      } else {
        this.offsetX = 0;
        this.offsetY = 0;
      }
    } else {
      if (this.resomap > this.initialzoom / 4) {
        this.resomap = this.resomap - 150 / 100000;
      }
    }
    console.log(this.resomap);
  }

  showLoading() {
    if (!this.loading) {
      this.loading = this.loadingCtrl.create({
        content: this.param.datatext.wait,
      });
      this.loading.present();
    }
  }
  dismissLoading() {
    if (this.loading) {
      this.loading.dismiss();
      this.loading = null;
    }
  }

  createImageFromBlob() {
  

      if (this._CANVAS.getContext) {
       
       
        this._CANVAS.width = this.imge.width;
        this._CANVAS.height = this.imge.height;

    
        var w = this._CANVAS.width;
        var h = this._CANVAS.height;
        var r = this.resomap;
        var x = this.offsetX;
        var y = this.offsetY;

        var xm = this.api.mapdata.Offset.X;
        var ym = this.api.mapdata.Offset.Y;
        var rm = this.api.mapdata.Resolution;
        var wm = this.api.mapdata.Width;
        var hm = this.api.mapdata.Height;

        var dx = xm / r + x;
        var dy = h - (ym + hm * rm) * (1 / r) + y;

        var dw = (wm * rm) / r;
        var dh = (hm * rm) / r;

        this._CONTEXT.drawImage(this.imge, dx, dy, dw, dh);
        //console.log('dessin image');

        if (this.api.all_locations != undefined) {
          this.api.all_locations.forEach((evenement) => {
            if (evenement.Label !== "qr") {
              this._CONTEXT.beginPath();
              this._CONTEXT.font =
                "" + Math.round(this._CANVAS.width * 0.025) + "px Arial";
              //console.log(evenement);
              this._CONTEXT.fillStyle = "rgba(26, 156, 195,0.7)";
              this._CONTEXT.fillText(
                evenement.Id, //+ "- " + evenement.Name,
                evenement.Pose.X / this.resomap + this.offsetX - 20,
                this._CANVAS.height -
                  (evenement.Pose.Y / this.resomap - this.offsetY) -
                  10
              );

              if (evenement.Name === "docking" || evenement.Name === "docked") {
                this._CONTEXT.fillText(
                  evenement.Id + "- " + evenement.Name,
                  evenement.Pose.X / this.resomap + this.offsetX - 20,
                  this._CANVAS.height -
                    (evenement.Pose.Y / this.resomap - this.offsetY) -
                    10
                );
              }

              this._CONTEXT.beginPath();
              //console.log(evenement.Pose)
              this._CONTEXT.arc(
                evenement.Pose.X / this.resomap + this.offsetX,
                this._CANVAS.height -
                  (evenement.Pose.Y / this.resomap - this.offsetY),
                this._CANVAS.width * 0.005,
                0,
                2 * Math.PI
              );
              this._CONTEXT.lineWidth = 1;
              this._CONTEXT.fillStyle = "rgb(26, 156, 195)";
              this._CONTEXT.fill();
              this._CONTEXT.beginPath();
              this._CONTEXT.save();

              this._CONTEXT.translate(
                evenement.Pose.X / this.resomap + this.offsetX,
                this._CANVAS.height -
                  (evenement.Pose.Y / this.resomap - this.offsetY)
              );
              this._CONTEXT.rotate(-evenement.Pose.T);
              this._CONTEXT.moveTo(0, 0);
              this._CONTEXT.lineWidth = 2;
              this._CONTEXT.lineTo(2 * this._CANVAS.width * 0.009, 0);
              this._CONTEXT.strokeStyle = "rgb(26, 156, 195)";
              this._CONTEXT.stroke();

              this._CONTEXT.restore();
            }
          });
        }
      }
    
  }

  onSliderRelease(ev, id: Select){
    ev.preventDefault();
    id.open();
  }

  
  getImageFromService() {
    this.imge = document.getElementById("imgmap3") as HTMLImageElement;
    if (this.api.id_current_map != undefined) {
      this.offsetX = this.api.mapdata.Offset.X;
      this.offsetY = this.api.mapdata.Offset.Y;
      this.resomap = this.api.mapdata.Resolution;
      this.initialzoom = this.resomap;
    }
    console.log("GetImageFromService");
    this.api.getImgMapHttp().subscribe(
      (data) => {
        this.api.imgMap = data;
        this.createImageFromBlob();
        console.log("GetImageFromService");
      },
      (error) => {
        console.log(error);
      }
    );
  }
}
