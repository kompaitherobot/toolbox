import { Component, OnInit, ElementRef, ViewChild } from "@angular/core";
import {
  ItemSliding,
  LoadingController,
  NavController,
  ToastController,
} from "ionic-angular";
import { AlertService } from "../../services/alert.service";
import { ApiService } from "../../services/api.service";
import { ParamService } from "../../services/param.service";
import { PopupService } from "../../services/popup.service";
import nipplejs from "nipplejs";
import { Select, Content } from "ionic-angular";
import { SpeechService } from "../../services/speech.service";


@Component({
  selector: "page-lift",
  templateUrl: "lift.html",
})
export class LiftPage implements OnInit {
  @ViewChild("canvas7") canvasEl: ElementRef;
  @ViewChild("imgmap7") imgmapElement: ElementRef;
  @ViewChild("content1") content1: Content;
  current_map: number;
  loading: any;
  private _CANVAS: any;
  initialzoom: any;
  heightcol: any;
  widthcol: any;
  private _CONTEXT: any;
  time_blocked: number;
  imageToShow: any;
  previousimage: any;
  isImageLoading: boolean;
  rawImageToShow: any;
  loc: any;
  intervmap: any;
  offsetX: any;
  offsetY: any;
  resomap: any;

  moving: any;
  start: any = {
    x: 0,
    y: 0,
  };

  clickpose: any = {
    x: 0,
    y: 0,
  };

  xtest: number;
  ytest: number;
  reloc: boolean;
  round: number;

  px: number;
  py: number;
  pt: number;
  map_url="http://" + this.param.localhost + "/api/maps/current/image?t=" + new Date().getTime();
  imge:any;
  source:number;
  destination:number;

  enterlift:any;
  can_leave_lift:boolean=false;

 
  chimiostate:number=0;
  pharmastate:number=0;
  chargemap:boolean=false;
  cpt_locked: number=0;
  date_blocked: Date;
  countjoystick: number;
  checkjoystick: boolean;
  stopjoystick: boolean;
  manager:any
  size: number = 40;
  maxrad: number = 0.08;
  maxlin: number = 0.12;
  maxback: number = 0.08;
  currentx: number;
  currenty: number;
  lastx: number;
  lasty: number;
  vrad: number;
  vlin: number;
  gamepadinterv: any;
  options:any;
  imgRos : any;
  cam:boolean=false;

  ngAfterViewInit() {
    console.log(document.getElementById("zone_joystick"));
    this.createjoystick();
   
  }

  change_cam(){
    this.cam=!this.cam;
  }
  joystickmove() {
    this.api.joystickHttp(this.vlin, this.vrad);
  }

  joystickstop() {
    this.api.joystickHttp(0, 0);
    
  }

  createjoystick(){
    this.manager = nipplejs.create({
      zone: document.getElementById("zone_joystick"),
      size: 3 * this.size
    });

    this.manager.on("move", (evt, nipple) => {
      this.currentx = nipple.raw.position.x ;
      this.currenty = nipple.raw.position.y ;
      this.checkjoystick = true;
      if(!this.stopjoystick){
        if (nipple.direction && nipple.direction.angle && nipple.angle) {
          //console.log(nipple.direction.angle);
          if (nipple.direction.angle === "left") {
            this.vlin = 0;
            this.vrad = (0.3 * nipple.distance) / 50;
          } else if (nipple.direction.angle === "right") {
            this.vlin = 0;
            this.vrad = -(0.3 * nipple.distance) / 50;
          } else if (nipple.direction.angle === "up") {
            this.vlin = (this.maxlin * nipple.distance) / 50;
            this.vrad = 0;
            // if (Math.abs((nipple.angle.radian - Math.PI / 2) / 2) > 0.18) {
            //   this.vrad = (nipple.angle.radian - Math.PI / 2) / 2 - 0.09;
            // } else {
            // this.vrad = 0;
            // }
          } else if (nipple.direction.angle === "down") {
            this.vlin = -(this.maxback * nipple.distance) / 50;
            this.vrad = 0;
          }
        }
      }else{
        this.vlin = 0;
        this.vrad = 0;
      }
    });

    this.manager.on("added", (evt, nipple) => {
      console.log("added");
      this.gamepadinterv = setInterval(() => {if(!this.stopjoystick){this.joystickmove();}}, 500);
    });
    this.manager.on("end", (evt, nipple) => {
      console.log("end");
      this.vlin = 0;
      this.vrad = 0;
      clearInterval(this.gamepadinterv);
      this.checkjoystick = false;
      this.stopjoystick = false;
      this.joystickstop();
    });
  }

  ngOnInit() {
    this.api.is_blocked=false;
    this._CANVAS = this.canvasEl.nativeElement;
    this._CANVAS.style.width = "100%";
    this._CANVAS.style.height = "100%";
    this._CONTEXT = this._CANVAS.getContext("2d");
    this.heightcol =
      document.getElementById("body7").clientHeight -
      document.getElementById("firstrow7").clientHeight * 2;
    this.widthcol = document.getElementById("testcol7").clientWidth;
  }

  calcDiffMin(one: Date, two: Date) {
    var DateBegMin = one.getMinutes();
    var DateBegHours = one.getHours() * 60;
    var DateBeg = DateBegMin + DateBegHours;
    var DateEndMin = two.getMinutes();
    var DateEndHours = two.getHours() * 60;
    return DateEndMin + DateEndHours - DateBeg;
  }

  watchIfLocked() {
    if (this.api.moovingchimio || this.api.moovingpharma) {
      
      if (
        !this.api.anticollision_status.locked
      ) {
        
        if(this.cpt_locked>0){
          this.cpt_locked =0 ;
        }
        
        if (this.api.is_blocked) {
            this.time_blocked = this.calcDiffMin(this.date_blocked, new Date());
            this.alert.noLongerBlocked(
              "<br> Blocked for : " +
              this.time_blocked +
              "<br>" +
              this.api.mailAddInformationBasic()
            );
            this.api.is_blocked = false;
            if (this.popup.alert_blocked) {
              this.popup.alert_blocked.dismiss();
            }
            this.time_blocked = 0;
          }
        
      } else if (
        this.api.anticollision_status.locked
      ) {
        
        this.cpt_locked += 1;
      }
      
      if (this.cpt_locked > 10 && !this.api.is_blocked) {
          this.speech.speak("Un obstacle m'empêche de passer. Veuillez m'aider s'il vous plait. J'ai une mission importante à terminer")
          this.date_blocked = new Date();
          this.popup.blockedAlert();
          this.api.is_blocked = true;
          this.alert.blocking(this.api.mailAddInformationBasic());
      }
    }
  }

  ionViewDidEnter() {
   
    this.current_map = this.api.id_current_map;
    if (this.api.id_current_map != undefined) {
      this.api.getCurrentLocations();
    }
    this.api.getRoundList();
    this.getImageFromService();
    this.intervmap = setInterval(() => this.updatemap(), 400);
    this.stoprobot();
  }

  @ViewChild("select1") select1 :Select;
  @ViewChild("select2") select2 :Select;

  constructor(
 
    public navCtrl: NavController,
    public loadingCtrl: LoadingController,
    public toastCtrl: ToastController,
    public popup: PopupService,
    public api: ApiService,
    public param: ParamService,
    public speech: SpeechService,
    public alert: AlertService
  ) {
    this.currentx = 0 ;
    this.currenty = 0 ;
    this.lastx = 0 ;
    this.lasty = 0 ;
    this.checkjoystick = false;
    this.stopjoystick = false;
    this.reloc = false;
     // CAMERA ROS
    // if( this.param.robot.cam_USB==0 ){



    this.api.listener_cameraf.subscribe((message) => {

      //console.log('Received message on ' + listener_camera.name + ': ');
      this.imgRos = document.getElementById('imgcpsd');
      //console.log(message);

      var bufferdata = this.convertDataURIToBinary2(message.data);

      var blob = new Blob([bufferdata], {type: 'image/jpeg'});
      var blobUrl = URL.createObjectURL(blob);

      this.imgRos.src = blobUrl;


    });

  //}

// CAMERA ROS FIN
  }

  convertDataURIToBinary2(dataURI) {
    var raw = window.atob(dataURI);
    var rawLength = raw.length;
    var array = new Uint8Array(new ArrayBuffer(rawLength));


    for(var i = 0; i < rawLength; i++) {
      array[i] = raw.charCodeAt(i);
    }
    return array;
  }


  public open(ev, itemSlide: ItemSliding) {
    ev.preventDefault();
    if (itemSlide.getSlidingPercent() == 0) {
      // Two calls intentional after examination of vendor.js function
      itemSlide.moveSliding(-370);
      itemSlide.moveSliding(-370);
    } else {
      itemSlide.close();
    }
  }


    updatemap(){
      if(this.reloc || this.api.differential_status.CurrentAngularSpeed>0.005 || this.api.differential_status.CurrentLinearSpeed>0.01 || this._CANVAS.width==0){
        this.createImageFromBlob();
      }

      if(this.checkjoystick){

    
        if(this.currentx == this.lastx ){
          if(this.currenty == this.lasty)
          {
            this.countjoystick = this.countjoystick + 1;
            if(this.countjoystick >= 5)
            {
              console.log("stopjoystick");
              this.joystickstop();
              this.stopjoystick = true;
            }
            if(this.countjoystick >= 15)
            {
              clearInterval(this.gamepadinterv);
              this.checkjoystick = false;
              this.manager.destroy();
              this.createjoystick();
              this.checkjoystick=false;
            }
          }
          else
          {
            this.countjoystick = 0;
            this.stopjoystick = false;
            this.checkjoystick = true;
            this.lastx = this.currentx;
            this.lasty = this.currenty;
          }
        }
        else
        {
          this.countjoystick = 0;
          this.stopjoystick = false;
          this.checkjoystick = true;
          this.lastx = this.currentx;
          this.lasty = this.currenty;
        }
      
      }

      if(this.api.navigation_status.status==5){
        this.stoprobot();
        this.alert.naverror(this.api.mailAddInformationBasic());
        this.popup.errorNavAlert();
        this.speech.speak("Erreur. Impossible de calculer mon trajet.")
      }


      if(this.api.moovingchimio){
        this.watchIfLocked();
        if(this.api.b_pressed){
          this.stoprobot();
        }
        if(this.chimiostate==3){
          // code pour attendre la fin de la mission et dire "mission terminé" quand robot sur poi chimio
          if(this.api.trajectory_status.status==1 && this.api.navigation_status.status == 0){
            this.speech.speak("Mission terminée ! vous pouvez récuperer les bacs de chimio.")
            this.popup.showToastGreen("Mission terminée !",3000,"middle")
            this.api.moovingchimio=false;
            this.chimiostate=0;
            this.stoprobot();
          }
          
        }else if(this.chimiostate==2){
          if(!this.chargemap && this.api.trajectory_status.status==1 && this.api.navigation_status.status == 0 && this.getDistance()<0.8 ){
            //this.speech.speak("Chargement de la carte chimio")
            this.popup.showToastGreen("Changement de carte",15000,"middle")
            this.reloc_chimio();
            this.chargemap=true;
            setTimeout(() => {
              if(this.api.moovingchimio){
                this.can_leave_lift=true;
              }
              
              
            }, 15000);
            
          }
          if(this.api.trajectory_status.status==0 && this.chargemap && this.can_leave_lift){
            this.chimiostate=3
            this.speech.speak("En route vers le service chimio");
            this.api.reachHttp("chimio");
          }
          
        }else if(this.chimiostate==1){
          if((this.api.trajectory_status.status==1 && this.api.navigation_status.status == 0)|| (this.api.trajectory_status.status==0 && this.api.navigation_status.status == 0)){
            this.chimiostate=2;
            this.enter_lift();
          }
        }
      }
      else if(this.api.moovingpharma){
        if(this.api.b_pressed){
          this.stoprobot();
        }
        this.watchIfLocked();
          if(this.pharmastate==3){
          // code pour attendre la fin de la mission et dire "mission terminé" quand robot sur poi pharma
          if(this.api.trajectory_status.status==1 && this.api.navigation_status.status == 0){
            this.speech.speak("Mission terminée ! Robot sur position PHARMA")
            this.popup.showToastGreen("Mission terminée !",3000,"middle")
            this.api.moovingpharma=false;
            this.pharmastate=0;
            this.stoprobot();
          }
          
        }else if(this.pharmastate==2){
          if(!this.chargemap && this.api.trajectory_status.status==1 && this.api.navigation_status.status == 0 && this.getDistance()<0.8){
            this.popup.showToastGreen("Changement de carte",15000,"middle")
            this.reloc_pharma();
            this.chargemap=true;
            setTimeout(() => {
              if(this.api.moovingpharma){
                this.can_leave_lift=true;
              }
              
              
            }, 15000);
            
          }
          if(this.api.trajectory_status.status==0 && this.chargemap && this.can_leave_lift){
            this.pharmastate=3
            this.can_leave_lift=false;
            this.speech.speak("En route vers le service pharma");
            this.api.reachHttp("pharma");
          }
          
        }else if(this.pharmastate==1){
          if(this.api.trajectory_status.status<2 && this.api.navigation_status.status == 0){
            this.pharmastate=2;
            this.enter_lift();
          }
        }
      }
    }


  on_click_relocate(ev) {
    ev.preventDefault();
    if (!this.reloc) {
      this.reloc = true;
      console.log(this.clickpose);
      this.api.putLocalization(this.xtest, this.ytest, 0);
    } else {
      this.reloc = false;
    }

  }

  stoprobot(){
    this.api.moovingchimio=false;
    this.api.moovingpharma=false;
    this.chargemap=false;
    this.can_leave_lift=false;
    this.chimiostate=0
    this.pharmastate=0
    console.log("stoprobot")
    this.api.abortNavHttp()
  }

  on_click_relocate_left(ev) {
    ev.preventDefault();
    this.api.putLocalization(
      this.api.localization_status.positionx,
      this.api.localization_status.positiony,
      (this.api.localization_status.positiont + 0.46) % (2 * Math.PI)
    );

  }

  on_click_relocate_right(ev) {
    ev.preventDefault();
    this.api.putLocalization(
      this.api.localization_status.positionx,
      this.api.localization_status.positiony,
      (this.api.localization_status.positiont - 0.46) % (2 * Math.PI)
    );

  }

 
  ionViewWillLeave() {
    if(this.param.robot.cam_USB==0){
      this.api.listener_camera.removeAllListeners();
      this.api.startQrDetection(false);
    }

    clearInterval(this.intervmap);
    this.api.listener_cameraf.unsubscribe()
  }

  enter_lift(){
   
      this.speech.speak("Attention, j'entre dans l'ascenseur");
      this.api.reachHttp("enterlift");
      
  }

  getDistance(){
   
    var x2=this.api.localization_status.positionx;
    var y2=this.api.localization_status.positiony;
    var x1=this.enterlift[0].Pose.X
    var y1=this.enterlift[0].Pose.Y
    let y = x2 - x1;
    let x = y2 - y1;
    let res = Math.sqrt(x * x + y * y);
    console.log(res);
    return res;
  }

  leave_lift(ev){
    ev.preventDefault();
    this.speech.speak("Attention, je sors de l'ascenseur");
    this.api.reachHttp("leavelift");
    
  }


  go_pharma(ev){
     
     ev.preventDefault();
     this.enterlift= this.api.all_locations.filter((poi) => poi.Name == "enterlift");
     this.stoprobot();
     this.chargemap=false;
     this.api.moovingchimio=false;
     var index= this.api.mapdata.Name.toLowerCase().indexOf("pharma"); //savoir si on est sur map chimio
     this.alert.gopoi(this.api.mailAddInformationBasic());
     if(index !== -1){
      
       console.log("on est sur map parma");
       this.speech.speak("Direction le service pharma");
       this.api.moovingpharma=true;
       setTimeout(() => {
        if(this.api.moovingpharma)
        this.api.reachHttp("pharma"); 
        
      }, 1000);
      setTimeout(() => {
        if(this.api.moovingpharma)
        this.pharmastate=3
        
      }, 2000);
   } else{
       
       console.log("on est pas sur map pharma");
       this.speech.speak("Direction l'ascenseur logistique propre");
       this.api.moovingpharma=true;
       setTimeout(() => {
        if(this.api.moovingpharma){
          this.api.reachHttp("waitlift");
        }
       
      }, 1000);
      setTimeout(() => {
    
        if(this.api.moovingpharma)
        this.pharmastate=1
      }, 2000);
   }
  }

  go_chimio(ev){
    
    ev.preventDefault();
    this.enterlift= this.api.all_locations.filter((poi) => poi.Name == "enterlift");
    this.stoprobot();
    this.chargemap=false;
    this.api.moovingpharma=false;
    var index= this.api.mapdata.Name.toLowerCase().indexOf("chimio"); //savoir si on est sur map chimio
    this.alert.gopoi(this.api.mailAddInformationBasic());
    if(index !== -1){
      
      console.log("on est sur map chimio");
      this.api.moovingchimio=true;
      this.speech.speak("Direction le service chimio"); 
      setTimeout(() => {
        if(this.api.moovingchimio)
        this.api.reachHttp("chimio"); 
       
      }, 1000);
      setTimeout(() => {
        if(this.api.moovingchimio)
        this.chimiostate=3
      }, 2000);
      

  } else{
     
      console.log("on est pas sur map chimio");
      this.speech.speak("Direction l'ascenseur logistique propre");
      this.api.moovingchimio=true;
      setTimeout(() => {
        this.api.reachHttp("waitlift");
        
      }, 1000);
      setTimeout(() => {
        
        this.chimiostate=1
      }, 2000);
  }

 }
  showLoading() {
    if (!this.loading) {
      this.loading = this.loadingCtrl.create({
        content: this.param.datatext.wait,
      });
      this.loading.present();
    }
  }
  dismissLoading() {
    if (this.loading) {
      this.loading.dismiss();
      this.loading = null;
    }
  }

 

  onMapChange(event) {
    this.showLoading();
    this.api.changeMapbyIdHttp(event);
    setTimeout(() => {
      this.imge.src="http://" + this.param.localhost + "/api/maps/current/image?t=" + new Date().getTime()
    }, 4500);
    setTimeout(() => {
      this.api.getCurrentLocations();
      this.api.getCurrentMap();

    }, 5500);
    setTimeout(() => {
      this.api.getRoundList();
      this.getImageFromService();
    }, 6500);
    setTimeout(() => {
      if (this.current_map != this.api.id_current_map) {
        this.current_map = this.api.id_current_map;
      }

      this.dismissLoading();
      this.api.current_map = this.api.id_current_map;
      
    }, 8000);
  }


  on_mouse_down(evt) {
    this.createImageFromBlob();
    //console.log(evt);

    if (evt.button == 0) {
      evt.preventDefault();
      this.moving = true;
      this.start.x = evt.x;
      this.start.y = evt.y;

      //console.log(evt.x + " " + this.start.x);

      //var t = evt.changedTouches[0];
      //var x = evt.x;
      //var y = evt.y;

      var x = (evt.offsetX * this._CANVAS.width) / this._CANVAS.clientWidth;
      //var y = evt.offsetY;
      var y = (evt.offsetY * this._CANVAS.height) / this._CANVAS.clientHeight;
      this.clickpose.x =
        (x - this.offsetX - this._CANVAS.width / 2) * this.resomap;
      this.clickpose.y =
        -(y - this.offsetY - this._CANVAS.height / 2) * this.resomap;

      console.log(this.clickpose);
      console.log(evt.x + " " + evt.y);
    }
    if (this.reloc) {
      console.log(this.clickpose);
      this.api.putLocalization(this.xtest, this.ytest, 0);
    }
  }

  on_mouse_up(evt) {
    this.createImageFromBlob();
    //console.log(evt);
    if (this.moving) {
      var offset = {
        x: this.offsetX,
        y: this.offsetY,
        t: 0,
      };

      offset.x += evt.x - this.start.x;
      offset.y += evt.y - this.start.y;

      this.start.x = evt.x;
      this.start.y = evt.y;

      this.offsetX = offset.x;
      this.offsetY = offset.y;
    }
    this.moving = false;
    if (this.reloc) {
      console.log(this.clickpose);
      this.api.putLocalization(this.xtest, this.ytest, 0);
    }
  }

  on_mouse_move(evt) {

    if (this.moving) {
      this.createImageFromBlob();
      evt.preventDefault();

      var offset = {
        x: this.offsetX,
        y: this.offsetY,
      };

      offset.x += evt.x - this.start.x;
      offset.y += evt.y - this.start.y;

      this.start.x = evt.x;
      this.start.y = evt.y;

      this.offsetX = offset.x;
      this.offsetY = offset.y;
    }
  }

  on_wheel(evt) {
    this.createImageFromBlob();
    evt.preventDefault();

    //console.log(evt.deltaY);
    if (evt.deltaY > 0) {
      if (!(this.resomap >= this.initialzoom)) {
        this.resomap = this.resomap + 150 / 100000;
      } else {
        this.offsetX = 0;
        this.offsetY = 0;
      }
    } else {
      if (this.resomap > this.initialzoom / 4) {
        this.resomap = this.resomap - 150 / 100000;
      }
    }
    console.log(this.resomap);
  }

  getname(id_round: number) {
    return this.api.round_current_map.filter((x) => x.Id == id_round)[0].Name;
  }



  createImageFromBlob() {
    console.log("createImageFromBlob");
      if (this._CANVAS.getContext) {

        this._CANVAS.width = this.imge.width;
        this._CANVAS.height = this.imge.height;


        var w = this._CANVAS.width;
        var h = this._CANVAS.height;
        var r = this.resomap;
        var x = this.offsetX;
        var y = this.offsetY;

        var xm = this.api.mapdata.Offset.X;
        var ym = this.api.mapdata.Offset.Y;
        var rm = this.api.mapdata.Resolution;
        var wm = this.api.mapdata.Width;
        var hm = this.api.mapdata.Height;

        var dx = xm / r + x;
        var dy = h - (ym + hm * rm) * (1 / r) + y;

        var dw = (wm * rm) / r;
        var dh = (hm * rm) / r;

        this._CONTEXT.drawImage(this.imge, dx, dy, dw, dh);
        //console.log('dessin image');

        if (this.api.all_locations != undefined) {
          this.api.all_locations.forEach((evenement) => {
       
              this._CONTEXT.beginPath();
              this._CONTEXT.font =
                "" + Math.round(this._CANVAS.width * 0.025) + "px Arial";
              //console.log(evenement);
              this._CONTEXT.fillStyle = "rgba(26, 156, 195,0.7)";
              this._CONTEXT.fillText(
                evenement.Id + "- " + evenement.Name,
                evenement.Pose.X / this.resomap + this.offsetX - 20,
                this._CANVAS.height -
                  (evenement.Pose.Y / this.resomap - this.offsetY) -
                  10
              );

              this._CONTEXT.beginPath();
              //console.log(evenement.Pose)
              this._CONTEXT.arc(
                evenement.Pose.X / this.resomap + this.offsetX,
                this._CANVAS.height -
                  (evenement.Pose.Y / this.resomap - this.offsetY),
                this._CANVAS.width * 0.005,
                0,
                2 * Math.PI
              );
              this._CONTEXT.lineWidth = 1;
              this._CONTEXT.fillStyle = "rgb(26, 156, 195)";
              this._CONTEXT.fill();
              this._CONTEXT.beginPath();
              this._CONTEXT.save();

              this._CONTEXT.translate(
                evenement.Pose.X / this.resomap + this.offsetX,
                this._CANVAS.height -
                  (evenement.Pose.Y / this.resomap - this.offsetY)
              );
              this._CONTEXT.rotate(-evenement.Pose.T);
              this._CONTEXT.moveTo(0, 0);
              this._CONTEXT.lineWidth = 2;
              this._CONTEXT.lineTo(2 * this._CANVAS.width * 0.009, 0);
              this._CONTEXT.strokeStyle = "rgb(26, 156, 195)";
              this._CONTEXT.stroke();

              this._CONTEXT.restore();
            
          });
        }

        var xc = w / 2 + this.clickpose.x / r + x;
        var yc = h / 2 - this.clickpose.y / r + y;


        this.xtest = (xc - x) * r;
        this.ytest = -((yc - y - h) * r);

        this._CONTEXT.beginPath();

        this._CONTEXT.arc(xc, yc, this._CANVAS.width * 0.009, 0, 2 * Math.PI);
        this._CONTEXT.lineWidth = 1;
        this._CONTEXT.fillStyle = "#32CD32";
        this._CONTEXT.fill();

        this._CONTEXT.beginPath();

        this._CONTEXT.save();

        this._CONTEXT.translate(
          this.api.localization_status.positionx / this.resomap + this.offsetX,
          this._CANVAS.height -
            this.api.localization_status.positiony / this.resomap +
            this.offsetY
        );
        this._CONTEXT.rotate(-this.api.localization_status.positiont);
        this._CONTEXT.moveTo(0, -15);
        this._CONTEXT.lineTo(15, 0);
        this._CONTEXT.lineTo(0, 15);
        this._CONTEXT.lineTo(-10, 15);
        this._CONTEXT.lineTo(-10, -15);
        this._CONTEXT.lineTo(0, -15);

        this._CONTEXT.lineWidth = 2;
        this._CONTEXT.fill("nonzero");
        this._CONTEXT.stroke();

        this._CONTEXT.restore();

        if (this.api.navigation_status) {

          if (this.api.navigation_status.status == 1) {
            
            var traj = this.api.navigation_status.trajectory;
            traj.forEach((evenement) => {
              this._CONTEXT.beginPath();
              this._CONTEXT.moveTo(
                evenement.Start.X / this.resomap + this.offsetX,
                this._CANVAS.height -
                  (evenement.Start.Y / this.resomap - this.offsetY)
              );
              this._CONTEXT.lineTo(
                evenement.End.X / this.resomap + this.offsetX,
                this._CANVAS.height -
                  (evenement.End.Y / this.resomap - this.offsetY)
              );
              this._CONTEXT.strokeStyle = "#32CD32";
              this._CONTEXT.stroke();
            });
          }
        }
      }
    };



  on_zoom_out(ev) {
    ev.preventDefault();
    if (!(this.resomap >= this.initialzoom)) {
      this.resomap = this.resomap + 150 / 100000;
    } else {
      this.offsetX = 0;
      this.offsetY = 0;
    }
    this.createImageFromBlob();
  }

  on_zoom_in(ev) {
    ev.preventDefault();
    if (this.resomap > this.initialzoom / 4) {
      this.resomap = this.resomap - 150 / 100000;
    }
    this.createImageFromBlob();
  }

  getImageFromService() {
    this.imge = document.getElementById("imgmap7") as HTMLImageElement;
    if (this.api.id_current_map != undefined) {
      this.offsetX = this.api.mapdata.Offset.X;
      this.offsetY = this.api.mapdata.Offset.Y;
      this.resomap = this.api.mapdata.Resolution;
      this.initialzoom = this.resomap;
    }
    console.log("GetImageFromService");
    this.api.getImgMapHttp().subscribe(
      (data) => {
        this.api.imgMap = data;
        this.createImageFromBlob();
        console.log("GetImageFromService");
      },
      (error) => {
        console.log(error);
      }
    );
  }

  onSliderRelease(ev, id: Select){
    ev.preventDefault();
    id.open();
  }

  chimiotopharma(ev){
    ev.preventDefault();
    this.api.call_lift(2,0);
  }

  pharmatochimio(ev){
    ev.preventDefault();
    this.api.call_lift(0,2);
  }

  reloc_pharma(){
   
    console.log(this.param.qrcodes)
    this.api.QRselected = this.param.qrcodes.filter(
      (x) => x.qr == "relocpharma"
    )[0];

    this.onReloc();
  }

  onReloc() {
    console.log("onreloc");
    this.showLoading();
    this.speech.speak(this.param.datatext.alertReloc);
    //this.api.getCurrentMap();
    console.log(this.api.id_current_map)
    console.log(this.api.QRselected.id_map)
    if (!(this.api.id_current_map == this.api.QRselected.id_map)) {
        this.api.current_map=this.api.QRselected.id_map;
        console.log("onchangemap");
        this.onMapChange(this.api.QRselected.id_map);
    }
    setTimeout(() => {
      this.api.getFirstLocationHttp();
    }, 4000);
    setTimeout(() => {
      this.api.resetLocationHttp();
    }, 6000);
    setTimeout(() => {
      this.imge.src="http://" + this.param.localhost + "/api/maps/current/image?t=" + new Date().getTime()
    }, 8000);
    setTimeout(() => {
      this.createImageFromBlob();
      this.dismissLoading();
      this.popup.showToastBlue(
        this.param.datatext.localizationDone + this.api.mapdata.Name,
        4000
      );
      this.speech.speak(this.param.datatext.localizationDone + this.api.mapdata.Name);
      this.api.abortNavHttp()
    }, 9000);
  }

  reloc_chimio(){

    console.log(this.param.qrcodes)
   
    this.api.QRselected = this.param.qrcodes.filter(
      (x) => x.qr == "relocchimio"
    )[0];

    this.onReloc();
  }
}
