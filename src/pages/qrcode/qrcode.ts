import { Component, OnInit, ElementRef, ViewChild,Renderer2 } from "@angular/core";
import {
  ItemSliding,
  LoadingController,
  NavController,
  ToastController,
} from "ionic-angular";
import { AlertService } from "../../services/alert.service";
import { ApiService } from "../../services/api.service";
import { ParamService } from "../../services/param.service";
import { PopupService } from "../../services/popup.service";
import jsQR from "jsqr";
import { Select, Content } from "ionic-angular";
import { SpeechService } from "../../services/speech.service";

@Component({
  selector: "page-qrcode",
  templateUrl: "qrcode.html",
})
export class QRCodePage implements OnInit {
  @ViewChild("canvas5") canvasEl: ElementRef;
  @ViewChild("imgmap5") imgmapElement: ElementRef;
  @ViewChild("content1") content1: Content;
  @ViewChild("videoCanvas2") videoCanvas2: ElementRef;
  @ViewChild("video2") videoElement2: ElementRef;
  current_map: number;
  loading: any;
  private _CANVAS: any;
  initialzoom: any;
  heightcol: any;
  widthcol: any;
  private _CONTEXT: any;

  imageToShow: any;
  previousimage: any;
  isImageLoading: boolean;
  rawImageToShow: any;
  loc: any;
  intervmap: any;
  offsetX: any;
  offsetY: any;
  resomap: any;

  moving: any;
  start: any = {
    x: 0,
    y: 0,
  };

  clickpose: any = {
    x: 0,
    y: 0,
  };

  xtest: number;
  ytest: number;
  reloc: boolean;
  round: number;
  addqr: boolean;
  qr_read: string;
  link_to_round: boolean;
  qrdetected: boolean;
  qrdetection:boolean;
  px: number;
  py: number;
  pt: number;
  map_url="http://" + this.param.localhost + "/api/maps/current/image?t=" + new Date().getTime();
  imge:any;
  imgRos : any;
  videoWidth = 0;
  videoHeight = 0;

  ngOnInit() {
    this.popup.onSomethingHappened4(this.deleteqr.bind(this));
    this._CANVAS = this.canvasEl.nativeElement;
    this._CANVAS.style.width = "100%";
    this._CANVAS.style.height = "100%";
    this._CONTEXT = this._CANVAS.getContext("2d");
    this.heightcol =
      document.getElementById("body5").clientHeight -
      document.getElementById("firstrow5").clientHeight * 2;
    this.widthcol = document.getElementById("testcol5").clientWidth;

   this.qrdetection=false;
  }


  ionViewDidEnter() {
    this.qrdetection=false;
    this.current_map = this.api.id_current_map;
    if (this.api.id_current_map != undefined) {
      this.api.getCurrentLocations();
    }
    this.api.getRoundList();
    this.getImageFromService();
    this.intervmap = setInterval(() => this.updatemap(), 400);
    
  // CAMERA ROS
  if( this.param.robot.cam_USB==0 ){

    this.api.listener_camera.subscribe((message) => {

      //console.log('Received message on ' + listener_compressed.name + ': ');
      this.imgRos = document.getElementById('imgcompressed2');
      //console.log(message);

      var bufferdata = this.convertDataURIToBinary2(message.data);

      var blob = new Blob([bufferdata], {type: 'image/jpeg'});
      var blobUrl = URL.createObjectURL(blob);

      this.imgRos.src = blobUrl;


    });

}

// CAMERA ROS FIN
  }

  @ViewChild("select1") select1 :Select;
  @ViewChild("select2") select2 :Select;

  constructor(
    public navCtrl: NavController,
    public loadingCtrl: LoadingController,
    private renderer: Renderer2,
    public toastCtrl: ToastController,
    public popup: PopupService,
    public api: ApiService,
    public param: ParamService,
    public speech: SpeechService,
    public alert: AlertService
  ) {
    this.addqr = false;
    this.qrdetected = false;
    this.link_to_round = false;
    this.reloc = false;

    if(this.param.robot.cam_USB==1){
      this.setupconstraint();
    }
    
  }

  constraints = {
    //audio: false,
    //video: {deviceId: "f9860b260e18d7fd1687d8567e21b49c4bb26f87606020741717e11f6179f409"},
    video: true,

    facingMode: "environment",
    width: { ideal: 1920 },
    height: { ideal: 1080 },
  };
  
  setupconstraint() {
    // demande de permission camera
    navigator.mediaDevices.getUserMedia(this.constraints);

    // verification des permissions video et de la disponibilité de devices
    if (!!(navigator.mediaDevices && navigator.mediaDevices.getUserMedia)) {
      // enumeration des appareils connectes pour filtrer
      navigator.mediaDevices.enumerateDevices().then((result) => {
        var constraints;
        console.log(result);

        result.forEach(function (device) {
          // filtrer pour garder les flux video
          if (device.kind.includes("videoinput")) {
            // filtrer le label de la camera
            if (device.label.includes("USB")) {
              constraints = {
                audio: false,
                //video: {deviceId: "f9860b260e18d7fd1687d8567e21b49c4bb26f87606020741717e11f6179f409"},
                // affecter le deviceid filtré
                video: { deviceId: device.deviceId },

                facingMode: "environment",
                width: { ideal: 1920 },
                height: { ideal: 1080 },
              };
            }
          }
        });
        // on lance la connexion de la vidéo
        if (constraints) {
          this.constraints = constraints;
          this.startCamera(constraints);
          console.log("startcam");
        }
        //this.startCamera(constraints);
      });
    } else {
      console.log("Sorry, camera not available.");
    }
  }
  startCamera(cs) {
    if (!!(navigator.mediaDevices && navigator.mediaDevices.getUserMedia)) {
      // recupération du flux avec les constraints configurés puis on attache le flux à la balise video
      navigator.mediaDevices
        .getUserMedia(cs)
        .then(this.attachVideo.bind(this))
        .catch(this.handleError);
    } else {
      alert("Sorry, camera not available.");
    }
  }

  handleError(error) {
    console.log("Error: ", error);
  }

  attachVideo(stream) {
    this.renderer.setProperty(
      this.videoElement2.nativeElement,
      "srcObject",
      stream
    );
    this.renderer.listen(this.videoElement2.nativeElement, "play", (event) => {
      this.videoHeight = this.videoElement2.nativeElement.videoHeight;
      this.videoWidth = this.videoElement2.nativeElement.videoWidth;
    });
  }
  convertDataURIToBinary2(dataURI) {
    var raw = window.atob(dataURI);
    var rawLength = raw.length;
    var array = new Uint8Array(new ArrayBuffer(rawLength));


    for(var i = 0; i < rawLength; i++) {
      array[i] = raw.charCodeAt(i);
    }
    return array;
  }


  public open(ev, itemSlide: ItemSliding) {
    ev.preventDefault();
    if (itemSlide.getSlidingPercent() == 0) {
      // Two calls intentional after examination of vendor.js function
      itemSlide.moveSliding(-370);
      itemSlide.moveSliding(-370);
    } else {
      itemSlide.close();
    }
  }


    updatemap(){
      if(this.reloc || this.api.differential_status.CurrentAngularSpeed>0.005 || this.api.differential_status.CurrentLinearSpeed>0.01 || this._CANVAS.width==0){
        this.createImageFromBlob();
      }

    }



  createqr(ev) {
    ev.preventDefault();
    var poi: number;
    this.showLoading();
    this.api.postqr(this.qr_read, this.px, this.py, this.pt);
    setTimeout(() => {
      this.api.changeMapbyIdHttp(this.api.id_current_map);
      this.api.getCurrentLocations();
    }, 3000);
    setTimeout(() => {
      poi = this.api.all_locations[this.api.all_locations.length - 1].Id;
    }, 5000);
    setTimeout(() => {
      if (this.link_to_round) {
        this.param.addQR(
          this.qr_read,
          this.api.id_current_map,
          poi,
          this.round
        );
      } else {
        this.param.addQR(this.qr_read, this.api.id_current_map, poi, -1);
      }

      this.createImageFromBlob();
    }, 6000);

    setTimeout(() => {
      this.param.getQR();
      this.dismissLoading();
      this.addqr = false;
      this.qrdetected = false;
      this.content1.scrollToBottom(1000);
    }, 8000);
  }

  on_click_relocate(ev) {
    ev.preventDefault();
    if (!this.reloc) {
      this.reloc = true;
      console.log(this.clickpose);
      this.api.putLocalization(this.xtest, this.ytest, 0);
    } else {
      this.reloc = false;
    }

  }

  on_click_relocate_left(ev) {
    ev.preventDefault();
    this.api.putLocalization(
      this.api.localization_status.positionx,
      this.api.localization_status.positiony,
      (this.api.localization_status.positiont + 0.46) % (2 * Math.PI)
    );

  }

  on_click_relocate_right(ev) {
    ev.preventDefault();
    this.api.putLocalization(
      this.api.localization_status.positionx,
      this.api.localization_status.positiony,
      (this.api.localization_status.positiont - 0.46) % (2 * Math.PI)
    );

  }

  scan(ev) {
    ev.preventDefault();
    this.alert.scanqr(this.api.mailAddInformationBasic());
    //console.log("enter scan");
    if(this.param.robot.cam_USB==1){
      console.log("enter scan usb");
      this.qrdetection=true;
      setTimeout(() => {
        if (this.qrdetection) {
          this.qrdetection = false;
        }
      }, 3000);
      try{
        this.videoCanvas2.nativeElement.height = this.videoElement2.nativeElement.videoHeight;
        this.videoCanvas2.nativeElement.width = this.videoElement2.nativeElement.videoWidth;
    
        const g = this.videoCanvas2.nativeElement.getContext("2d");
        g.drawImage(this.videoElement2.nativeElement, 0, 0, this.videoCanvas2.nativeElement.width, this.videoCanvas2.nativeElement.height);
        const imageData = g.getImageData(0, 0, this.videoCanvas2.nativeElement.width, this.videoCanvas2.nativeElement.height);
        const code = jsQR(imageData.data, imageData.width, imageData.height, {
          inversionAttempts: "dontInvert",
        });
    
        if (code){
          console.log(code.data.trim());
          if (
            code.data !== null &&
            this.param.qrcodes.filter((x) => x.qr == code.data.trim())
              .length > 0
          ) {
            this.popup.showToastRed(
              this.param.datatext.qrcodeUsed1 +
              code.data.trim() +
                this.param.datatext.qrcodeUsed2,
                4000,
                "middle"
  
            );
            this.speech.speak(this.param.datatext.error + this.param.datatext.qrcodeUsed1 +
            this.param.datatext.qrcodeUsed2);
          } else if (
            code.data !== null &&
            this.param.qrcodes.filter((x) => x.qr == code.data.trim())
              .length === 0
          ) {
            this.qr_read = code.data.trim();
            this.addqr = false;
            this.qrdetected = true;
  
            this.px = this.api.localization_status.positionx;
            this.py = this.api.localization_status.positiony;
            this.pt = this.api.localization_status.positiont;
          }
          
        }else{
          this.popup.showToastRed(
            this.param.datatext.noqrdetected,
            5000,
            "middle"
          );
          this.speech.speak(this.param.datatext.noqrdetected);
        }
      } catch(err){
        console.log("Error", err);
      }

      
    }else if(this.param.robot.cam_USB==0){
      console.log("scan qr ros");
      this.qrdetection=true;
      this.api.startQrDetection(true);
      setTimeout(() => {
        if(this.qrdetection){
          this.qrdetection=false;
          this.api.startQrDetection(false);
          this.popup.showToastRed(
            this.param.datatext.noqrdetected,
            3000,
            "middle"

          );
          this.speech.speak(this.param.datatext.noqrdetected)
        }
      }, 10000);
      this.api.listener_qr.subscribe((message)=> {
        console.log('Received message on ' + this.api.listener_qr.name + ': ' + message.data);

        if (
          this.qrdetection &&
          this.param.qrcodes.filter((x) => x.qr == message.data.trim())
            .length > 0
        ) {
          this.popup.showToastRed(
            this.param.datatext.qrcodeUsed1 +
            message.data.trim() +
              this.param.datatext.qrcodeUsed2,
              3000,
              "middle"

          );
          this.speech.speak(this.param.datatext.error + this.param.datatext.qrcodeUsed1 +
            this.param.datatext.qrcodeUsed2);
        } else{
          this.qr_read = message.data.trim();
          this.addqr = false;
          this.qrdetected = true;

          this.px = this.api.localization_status.positionx;
          this.py = this.api.localization_status.positiony;
          this.pt = this.api.localization_status.positiont;
        }
        this.qrdetection=false;
        this.api.startQrDetection(false);
      });
    }

  }

  popupDelete(ev, qr: string, poi: number) {
    ev.preventDefault();
    this.popup.DeleteMessage(qr, poi);
  }

  deleteqr(qr: string, poi: number) {
    this.showLoading();
    this.api.deletePOI(poi);
    this.param.deleteQR(qr);
    setTimeout(() => {
      this.param.getQR();
      this.api.getCurrentLocations();
    }, 2000);
    setTimeout(() => {
      this.createImageFromBlob();
      this.dismissLoading();
    }, 3000);
  }

  ionViewWillLeave() {
    if(this.param.robot.cam_USB==0){
      this.api.listener_camera.removeAllListeners();
      this.api.startQrDetection(false);
    }

    clearInterval(this.intervmap);
  }

  canceladdqr(ev) {
    ev.preventDefault();
    this.addqr = false;
    this.qrdetected = false;
    this.qrdetection=false;
    if(this.param.robot.cam_USB==0){
      this.api.startQrDetection(false);
    }

  }

  onClickAddQR(ev) {
    ev.preventDefault();
    this.addqr = true;
  }

  showLoading() {
    if (!this.loading) {
      this.loading = this.loadingCtrl.create({
        content: this.param.datatext.wait,
      });
      this.loading.present();
    }
  }
  dismissLoading() {
    if (this.loading) {
      this.loading.dismiss();
      this.loading = null;
    }
  }


  onMapChange(event) {
    this.showLoading();
    this.api.changeMapbyIdHttp(event);
    setTimeout(() => {
      this.imge.src="http://" + this.param.localhost + "/api/maps/current/image?t=" + new Date().getTime()
    }, 4500);
    setTimeout(() => {
      this.api.getCurrentLocations();
      this.api.getCurrentMap();

    }, 5500);
    setTimeout(() => {
      this.api.getRoundList();
      this.getImageFromService();
    }, 6500);
    setTimeout(() => {
      if (this.current_map != this.api.id_current_map) {
        this.current_map = this.api.id_current_map;
      }

      this.dismissLoading();
    }, 8000);
  }


  on_mouse_down(evt) {
    this.createImageFromBlob();
    //console.log(evt);

    if (evt.button == 0) {
      evt.preventDefault();
      this.moving = true;
      this.start.x = evt.x;
      this.start.y = evt.y;

      //console.log(evt.x + " " + this.start.x);

      //var t = evt.changedTouches[0];
      //var x = evt.x;
      //var y = evt.y;

      var x = (evt.offsetX * this._CANVAS.width) / this._CANVAS.clientWidth;
      //var y = evt.offsetY;
      var y = (evt.offsetY * this._CANVAS.height) / this._CANVAS.clientHeight;
      this.clickpose.x =
        (x - this.offsetX - this._CANVAS.width / 2) * this.resomap;
      this.clickpose.y =
        -(y - this.offsetY - this._CANVAS.height / 2) * this.resomap;

      console.log(this.clickpose);
      console.log(evt.x + " " + evt.y);
    }
    if (this.reloc) {
      console.log(this.clickpose);
      this.api.putLocalization(this.xtest, this.ytest, 0);
    }
  }

  on_mouse_up(evt) {
    this.createImageFromBlob();
    //console.log(evt);
    if (this.moving) {
      var offset = {
        x: this.offsetX,
        y: this.offsetY,
        t: 0,
      };

      offset.x += evt.x - this.start.x;
      offset.y += evt.y - this.start.y;

      this.start.x = evt.x;
      this.start.y = evt.y;

      this.offsetX = offset.x;
      this.offsetY = offset.y;
    }
    this.moving = false;
    if (this.reloc) {
      console.log(this.clickpose);
      this.api.putLocalization(this.xtest, this.ytest, 0);
    }
  }

  on_mouse_move(evt) {

    if (this.moving) {
      this.createImageFromBlob();
      evt.preventDefault();

      var offset = {
        x: this.offsetX,
        y: this.offsetY,
      };

      offset.x += evt.x - this.start.x;
      offset.y += evt.y - this.start.y;

      this.start.x = evt.x;
      this.start.y = evt.y;

      this.offsetX = offset.x;
      this.offsetY = offset.y;
    }
  }

  on_wheel(evt) {
    this.createImageFromBlob();
    evt.preventDefault();

    //console.log(evt.deltaY);
    if (evt.deltaY > 0) {
      if (!(this.resomap >= this.initialzoom)) {
        this.resomap = this.resomap + 150 / 100000;
      } else {
        this.offsetX = 0;
        this.offsetY = 0;
      }
    } else {
      if (this.resomap > this.initialzoom / 4) {
        this.resomap = this.resomap - 150 / 100000;
      }
    }
    console.log(this.resomap);
  }

  getname(id_round: number) {
    return this.api.round_current_map.filter((x) => x.Id == id_round)[0].Name;
  }



  createImageFromBlob() {
    console.log("createImageFromBlob");



      if (this._CANVAS.getContext) {


        this._CANVAS.width = this.imge.width;
        this._CANVAS.height = this.imge.height;


        var w = this._CANVAS.width;
        var h = this._CANVAS.height;
        var r = this.resomap;
        var x = this.offsetX;
        var y = this.offsetY;

        var xm = this.api.mapdata.Offset.X;
        var ym = this.api.mapdata.Offset.Y;
        var rm = this.api.mapdata.Resolution;
        var wm = this.api.mapdata.Width;
        var hm = this.api.mapdata.Height;

        var dx = xm / r + x;
        var dy = h - (ym + hm * rm) * (1 / r) + y;

        var dw = (wm * rm) / r;
        var dh = (hm * rm) / r;

        this._CONTEXT.drawImage(this.imge, dx, dy, dw, dh);
        //console.log('dessin image');

        if (this.api.all_locations != undefined) {
          this.api.all_locations.forEach((evenement) => {
            if (evenement.Label === "qr") {
              this._CONTEXT.beginPath();
              this._CONTEXT.font =
                "" + Math.round(this._CANVAS.width * 0.025) + "px Arial";
              //console.log(evenement);
              this._CONTEXT.fillStyle = "rgba(26, 156, 195,0.7)";
              this._CONTEXT.fillText(
                evenement.Id + "- " + evenement.Name,
                evenement.Pose.X / this.resomap + this.offsetX - 20,
                this._CANVAS.height -
                  (evenement.Pose.Y / this.resomap - this.offsetY) -
                  10
              );

              this._CONTEXT.beginPath();
              //console.log(evenement.Pose)
              this._CONTEXT.arc(
                evenement.Pose.X / this.resomap + this.offsetX,
                this._CANVAS.height -
                  (evenement.Pose.Y / this.resomap - this.offsetY),
                this._CANVAS.width * 0.005,
                0,
                2 * Math.PI
              );
              this._CONTEXT.lineWidth = 1;
              this._CONTEXT.fillStyle = "rgb(26, 156, 195)";
              this._CONTEXT.fill();
              this._CONTEXT.beginPath();
              this._CONTEXT.save();

              this._CONTEXT.translate(
                evenement.Pose.X / this.resomap + this.offsetX,
                this._CANVAS.height -
                  (evenement.Pose.Y / this.resomap - this.offsetY)
              );
              this._CONTEXT.rotate(-evenement.Pose.T);
              this._CONTEXT.moveTo(0, 0);
              this._CONTEXT.lineWidth = 2;
              this._CONTEXT.lineTo(2 * this._CANVAS.width * 0.009, 0);
              this._CONTEXT.strokeStyle = "rgb(26, 156, 195)";
              this._CONTEXT.stroke();

              this._CONTEXT.restore();
            }
          });
        }

        var xc = w / 2 + this.clickpose.x / r + x;
        var yc = h / 2 - this.clickpose.y / r + y;


        this.xtest = (xc - x) * r;
        this.ytest = -((yc - y - h) * r);

        this._CONTEXT.beginPath();

        this._CONTEXT.arc(xc, yc, this._CANVAS.width * 0.009, 0, 2 * Math.PI);
        this._CONTEXT.lineWidth = 1;
        this._CONTEXT.fillStyle = "#32CD32";
        this._CONTEXT.fill();

        this._CONTEXT.beginPath();

        this._CONTEXT.save();

        this._CONTEXT.translate(
          this.api.localization_status.positionx / this.resomap + this.offsetX,
          this._CANVAS.height -
            this.api.localization_status.positiony / this.resomap +
            this.offsetY
        );
        this._CONTEXT.rotate(-this.api.localization_status.positiont);
        this._CONTEXT.moveTo(0, -15);
        this._CONTEXT.lineTo(15, 0);
        this._CONTEXT.lineTo(0, 15);
        this._CONTEXT.lineTo(-10, 15);
        this._CONTEXT.lineTo(-10, -15);
        this._CONTEXT.lineTo(0, -15);

        this._CONTEXT.lineWidth = 2;
        this._CONTEXT.fill("nonzero");
        this._CONTEXT.stroke();

        this._CONTEXT.restore();
      }
    };



  on_zoom_out(ev) {
    ev.preventDefault();
    if (!(this.resomap >= this.initialzoom)) {
      this.resomap = this.resomap + 150 / 100000;
    } else {
      this.offsetX = 0;
      this.offsetY = 0;
    }
    this.createImageFromBlob();
  }

  on_zoom_in(ev) {
    ev.preventDefault();
    if (this.resomap > this.initialzoom / 4) {
      this.resomap = this.resomap - 150 / 100000;
    }
    this.createImageFromBlob();
  }

  getImageFromService() {
    this.imge = document.getElementById("imgmap5") as HTMLImageElement;
    if (this.api.id_current_map != undefined) {
      this.offsetX = this.api.mapdata.Offset.X;
      this.offsetY = this.api.mapdata.Offset.Y;
      this.resomap = this.api.mapdata.Resolution;
      this.initialzoom = this.resomap;
    }
    console.log("GetImageFromService");
    this.api.getImgMapHttp().subscribe(
      (data) => {
        this.api.imgMap = data;
        this.createImageFromBlob();
        console.log("GetImageFromService");
      },
      (error) => {
        console.log(error);
      }
    );
  }

  onSliderRelease(ev, id: Select){
    ev.preventDefault();
    id.open();
  }
}
