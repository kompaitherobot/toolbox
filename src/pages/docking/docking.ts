import { Component, OnInit, ElementRef, ViewChild } from "@angular/core";
import {
  LoadingController,
  NavController,
  ToastController,
} from "ionic-angular";
import { AlertService } from "../../services/alert.service";
import { ApiService } from "../../services/api.service";
import { ParamService } from "../../services/param.service";
import { PopupService } from "../../services/popup.service";
import { Select, Content } from "ionic-angular";


@Component({
  selector: "page-docking",
  templateUrl: "docking.html",
})
export class DockingPage implements OnInit {
  @ViewChild("canvas8") canvasEl: ElementRef;
  @ViewChild("content1") content1: Content;
  @ViewChild("imgmap8") imgmapElement: ElementRef;
  current_map: number;
  loading: any;
  private _CANVAS: any;
  initialzoom: any;
  heightcol: any;
  widthcol: any;
  private _CONTEXT: any;

  imageToShow: any;
  previousimage: any;
  isImageLoading: boolean;
  rawImageToShow: any;
  loc: any;
  intervmap: any;
  offsetX: any;
  offsetY: any;
  resomap: any;

  moving: any;
  start: any = {
    x: 0,
    y: 0,
  };

  clickpose: any = {
    x: 0,
    y: 0,
  };
  map_url="http://" + this.param.localhost + "/api/maps/current/image?t=" + new Date().getTime();
  imge:any;
  xtest: number;
  ytest: number;
  reloc: boolean;
  px: number;
  py: number;
  pt: number;
  config: boolean;

  ngOnInit() {
    this._CANVAS = this.canvasEl.nativeElement;
    this._CONTEXT = this._CANVAS.getContext("2d");
    this._CANVAS.style.width = "100%";
    this._CANVAS.style.height = "100%";
    
    this.heightcol =
      document.getElementById("body8").clientHeight -
      document.getElementById("firstrow8").clientHeight * 2;
    this.widthcol = document.getElementById("testcol8").clientWidth;
    this.popup.onSomethingHappened6(this.removePOIdocking.bind(this));
    this.popup.onSomethingHappened7(this.configDocking.bind(this));
  }

  ionViewWillEnter(){
    this.config = false;
  }

  ionViewDidEnter() {
    this.current_map = this.api.id_current_map;
    if (this.api.id_current_map != undefined) {
      this.api.getCurrentLocations();
    }
    this.api.getRoundList();
    this.getImageFromService();
    this.intervmap = setInterval(() => this.updatemap(), 400);
  }

  @ViewChild("select1") select1 :Select;
  
  constructor(
    public navCtrl: NavController,
    public loadingCtrl: LoadingController,
    public toastCtrl: ToastController,
    public popup: PopupService,
    public api: ApiService,
    public param: ParamService,
    public alert: AlertService,
  ) {
    this.reloc = false;
    this.config = false;
  }

  updatemap(){
    if(this.reloc || this.api.differential_status.CurrentAngularSpeed>0.005 || this.api.differential_status.CurrentLinearSpeed>0.01 || this._CANVAS.width==0){
      this.createImageFromBlob();
    }
    
  }
  okToast(m: string, n: number) {
    const toast = this.toastCtrl.create({
      message: m,
      duration: n,
      position: "middle",
      cssClass: "toastam",
    });
    toast.present();
  }

  on_click_relocate(ev) {
    ev.preventDefault();
    if (!this.reloc) {
      this.reloc = true;
      console.log(this.clickpose);
      this.api.putLocalization(this.xtest, this.ytest, 0);
    } else {
      this.reloc = false;
    }
    
  }

  on_click_relocate_left(ev) {
    ev.preventDefault();
    this.api.putLocalization(
      this.api.localization_status.positionx,
      this.api.localization_status.positiony,
      (this.api.localization_status.positiont + 0.46) % (2 * Math.PI)
    );
    
  }

  on_click_relocate_right(ev) {
    ev.preventDefault();
    this.api.putLocalization(
      this.api.localization_status.positionx,
      this.api.localization_status.positiony,
      (this.api.localization_status.positiont - 0.46) % (2 * Math.PI)
    );
    
  }

  ionViewWillLeave() {
    clearInterval(this.intervmap);
  }

  showLoading() {
    if (!this.loading) {
      this.loading = this.loadingCtrl.create({
        content: this.param.datatext.wait,
      });
      this.loading.present();
    }
  }
  dismissLoading() {
    if (this.loading) {
      this.loading.dismiss();
      this.loading = null;
    }
  }


  onMapChange(event) {
    this.showLoading();
    this.api.changeMapbyIdHttp(event);
    setTimeout(() => {
      this.imge.src="http://" + this.param.localhost + "/api/maps/current/image?t=" + new Date().getTime()
    }, 4500);
    setTimeout(() => {
      this.api.getCurrentLocations();
      this.api.getCurrentMap();
      
    }, 5500);
    setTimeout(() => {
      this.api.getRoundList();
      this.getImageFromService();
    }, 6500);
    setTimeout(() => {
      if (this.current_map != this.api.id_current_map) {
        this.current_map = this.api.id_current_map;
      }
      
      this.dismissLoading();
    }, 8000);
  }
  

  on_mouse_down(evt) {
    this.createImageFromBlob();
    //console.log(evt);

    if (evt.button == 0) {
      evt.preventDefault();
      this.moving = true;
      this.start.x = evt.x;
      this.start.y = evt.y;

      //console.log(evt.x + " " + this.start.x);

      //var t = evt.changedTouches[0];
      //var x = evt.x;
      //var y = evt.y;

      var x = (evt.offsetX * this._CANVAS.width) / this._CANVAS.clientWidth;
      //var y = evt.offsetY;
      var y = (evt.offsetY * this._CANVAS.height) / this._CANVAS.clientHeight;
      this.clickpose.x =
        (x - this.offsetX - this._CANVAS.width / 2) * this.resomap;
      this.clickpose.y =
        -(y - this.offsetY - this._CANVAS.height / 2) * this.resomap;

      console.log(this.clickpose);
      console.log(evt.x + " " + evt.y);
    }
    if (this.reloc) {
      console.log(this.clickpose);
      this.api.putLocalization(this.xtest, this.ytest, 0);
    }
  }

  on_mouse_up(evt) {
    this.createImageFromBlob();
    //console.log(evt);
    if (this.moving) {
      var offset = {
        x: this.offsetX,
        y: this.offsetY,
        t: 0,
      };

      offset.x += evt.x - this.start.x;
      offset.y += evt.y - this.start.y;

      this.start.x = evt.x;
      this.start.y = evt.y;

      this.offsetX = offset.x;
      this.offsetY = offset.y;
    }
    this.moving = false;
    if (this.reloc) {
      console.log(this.clickpose);
      this.api.putLocalization(this.xtest, this.ytest, 0);
    }
  }

  on_mouse_move(evt) {
    
    if (this.moving) {
      this.createImageFromBlob();
      evt.preventDefault();

      var offset = {
        x: this.offsetX,
        y: this.offsetY,
      };

      offset.x += evt.x - this.start.x;
      offset.y += evt.y - this.start.y;

      this.start.x = evt.x;
      this.start.y = evt.y;

      this.offsetX = offset.x;
      this.offsetY = offset.y;
    }
  }

  on_wheel(evt) {
    this.createImageFromBlob();
    evt.preventDefault();

    //console.log(evt.deltaY);
    if (evt.deltaY > 0) {
      if (!(this.resomap >= this.initialzoom)) {
        this.resomap = this.resomap + 150 / 100000;
      } else {
        this.offsetX = 0;
        this.offsetY = 0;
      }
    } else {
      if (this.resomap > this.initialzoom / 4) {
        this.resomap = this.resomap - 150 / 100000;
      }
    }
    console.log(this.resomap);
  }

  getname(id_round: number) {
    return this.api.round_current_map.filter((x) => x.Id == id_round)[0].Name;
  }



  createImageFromBlob() {
    console.log("createImageFromBlob");


      if (this.current_map && this._CANVAS.getContext) {
      
 
        this._CANVAS.width = this.imge.width;
        this._CANVAS.height = this.imge.height;


        var w = this._CANVAS.width;
        var h = this._CANVAS.height;
        var r = this.resomap;
        var x = this.offsetX;
        var y = this.offsetY;

        var xm = this.api.mapdata.Offset.X;
        var ym = this.api.mapdata.Offset.Y;
        var rm = this.api.mapdata.Resolution;
        var wm = this.api.mapdata.Width;
        var hm = this.api.mapdata.Height;

        var dx = xm / r + x;
        var dy = h - (ym + hm * rm) * (1 / r) + y;

        var dw = (wm * rm) / r;
        var dh = (hm * rm) / r;

        this._CONTEXT.drawImage(this.imge, dx, dy, dw, dh);
        //console.log('dessin image');

        if (this.api.all_locations != undefined) {
          this.api.all_locations.forEach((evenement) => {
            if (evenement.Name === "docking" || evenement.Name === "docked") {
              this._CONTEXT.beginPath();
              this._CONTEXT.font =
                "" + Math.round(this._CANVAS.width * 0.025) + "px Arial";
              //console.log(evenement);
              this._CONTEXT.fillStyle = "rgba(26, 156, 195,0.7)";
              this._CONTEXT.fillText(
                evenement.Id + "- " + evenement.Name,
                evenement.Pose.X / this.resomap + this.offsetX - 20,
                this._CANVAS.height -
                  (evenement.Pose.Y / this.resomap - this.offsetY) -
                  10
              );

              this._CONTEXT.beginPath();
              //console.log(evenement.Pose)
              this._CONTEXT.arc(
                evenement.Pose.X / this.resomap + this.offsetX,
                this._CANVAS.height -
                  (evenement.Pose.Y / this.resomap - this.offsetY),
                this._CANVAS.width * 0.005,
                0,
                2 * Math.PI
              );
              this._CONTEXT.lineWidth = 1;
              this._CONTEXT.fillStyle = "rgb(26, 156, 195)";
              this._CONTEXT.fill();
              this._CONTEXT.beginPath();
              this._CONTEXT.save();

              this._CONTEXT.translate(
                evenement.Pose.X / this.resomap + this.offsetX,
                this._CANVAS.height -
                  (evenement.Pose.Y / this.resomap - this.offsetY)
              );
              this._CONTEXT.rotate(-evenement.Pose.T);
              this._CONTEXT.moveTo(0, 0);
              this._CONTEXT.lineWidth = 2;
              this._CONTEXT.lineTo(2 * this._CANVAS.width * 0.009, 0);
              this._CONTEXT.strokeStyle = "rgb(26, 156, 195)";
              this._CONTEXT.stroke();

              this._CONTEXT.restore();
            }
          });
        }

        var xc = w / 2 + this.clickpose.x / r + x;
        var yc = h / 2 - this.clickpose.y / r + y;

    
        this.xtest = (xc - x) * r;
        this.ytest = -((yc - y - h) * r);

        this._CONTEXT.beginPath();
        
        this._CONTEXT.arc(xc, yc, this._CANVAS.width * 0.009, 0, 2 * Math.PI);
        this._CONTEXT.lineWidth = 1;
        this._CONTEXT.fillStyle = "#32CD32";
        this._CONTEXT.fill();

        this._CONTEXT.beginPath();
        
        this._CONTEXT.save();

        this._CONTEXT.translate(
          this.api.localization_status.positionx / this.resomap + this.offsetX,
          this._CANVAS.height -
            this.api.localization_status.positiony / this.resomap +
            this.offsetY
        );
        this._CONTEXT.rotate(-this.api.localization_status.positiont);
        this._CONTEXT.moveTo(0, -15);
        this._CONTEXT.lineTo(15, 0);
        this._CONTEXT.lineTo(0, 15);
        this._CONTEXT.lineTo(-10, 15);
        this._CONTEXT.lineTo(-10, -15);
        this._CONTEXT.lineTo(0, -15);

        this._CONTEXT.lineWidth = 2;
        this._CONTEXT.fill("nonzero");
        this._CONTEXT.stroke();

        this._CONTEXT.restore();
      }
   
   
  }

  on_zoom_out(ev) {
    ev.preventDefault();
    if (!(this.resomap >= this.initialzoom)) {
      this.resomap = this.resomap + 150 / 100000;
    } else {
      this.offsetX = 0;
      this.offsetY = 0;
    }
    this.createImageFromBlob();
  }

  on_zoom_in(ev) {
    ev.preventDefault();
    if (this.resomap > this.initialzoom / 4) {
      this.resomap = this.resomap - 150 / 100000;
    }
    this.createImageFromBlob();
  }

  getImageFromService() {
    this.imge = document.getElementById("imgmap8") as HTMLImageElement;
    if (this.api.id_current_map != undefined) {
      this.offsetX = this.api.mapdata.Offset.X;
      this.offsetY = this.api.mapdata.Offset.Y;
      this.resomap = this.api.mapdata.Resolution;
      this.initialzoom = this.resomap;
    }
    console.log("GetImageFromService");
    this.api.getImgMapHttp().subscribe(
      (data) => {
        this.api.imgMap = data;
        this.createImageFromBlob();
        console.log("GetImageFromService");
      },
      (error) => {
        console.log(error);
      }
    );
  }

  onSliderRelease(ev, id: Select){
    ev.preventDefault();
    id.open();
  }

  configDocking(){
    this.showLoading()
    this.api.postPoi("docked","none");
    this.api.configPOIDocking();
    setTimeout(() => {
        this.api.changeMapbyIdHttp(this.api.id_current_map);
        this.api.getCurrentLocations();
      }, 3000);
      setTimeout(() => {
        this.createImageFromBlob();
        this.dismissLoading();
        this.config = !this.config;
      }, 4000);
      setTimeout(() => {
        this.createImageFromBlob();
        this.createImageFromBlob();
      }, 5000);
  }

  removePOIdocking(){
    this.showLoading();
    var docking = this.api.all_locations.filter(
      (poi) => poi.Name == "docking" || poi.Name == "docked"
    );
    docking.forEach(poi => {
      this.api.deletePOI(poi.Id)
    });
    setTimeout(() => {
      this.api.getCurrentLocations();
    }, 2000);
    setTimeout(() => {
      this.createImageFromBlob();
      this.dismissLoading();
      this.config = !this.config;
    }, 3000);
  }

  onChangeConfig(ev){
    ev.preventDefault();
    var docking = this.api.all_locations.filter(
      (poi) => poi.Name == "docking" || poi.Name == "docked"
    );
    if(docking.length > 0)
      this.popup.askRemoveDataDocking();
    else
      this.config = !this.config;
  }

  canceldocking(ev){
    ev.preventDefault();
    this.navCtrl.pop();
  }
}
