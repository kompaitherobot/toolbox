import { Component, ElementRef, ViewChild, OnInit } from "@angular/core";
import { ParamService } from "../../services/param.service";
import {
  Content,
  ItemSliding,
  LoadingController,
  NavController,
  ToastController,
} from "ionic-angular";
import { ApiService } from "../../services/api.service";
import { RoundCreationPage } from "../roundcreation/roundcreation";
import { RoundEditPage } from "../roundedit/roundedit";
import { PopupService } from "../../services/popup.service";
import { Select } from "ionic-angular";
import { AlertService } from "../../services/alert.service";

@Component({
  selector: "page-rounddisplay",
  templateUrl: "rounddisplay.html",
})
export class RoundDisplayPage implements OnInit {
  roundCreationPage = RoundCreationPage;
  @ViewChild("canvas") canvasEl: ElementRef;
  @ViewChild("imgmap") imgmapElement: ElementRef;
  loading: any;
  private _CANVAS: any;
  heightcol: any;
  widthcol: any;
  private _CONTEXT: any;
  initialzoom: any;
  imageToShow: any;
  previousimage: any;
  isImageLoading: boolean;
  rawImageToShow: any;
  roundEditPage = RoundEditPage;
  current_map: number;
  loc: any;
  interv: any;
  offsetX: any;
  offsetY: any;
  resomap: any;
  map_url="http://" + this.param.localhost + "/api/maps/current/image?t=" + new Date().getTime();
  imge:any;
  moving: any;
  start: any = {
    x: 0,
    y: 0,
  };

  ngOnInit() {
    this.popup.onSomethingHappened3(this.deleteRound.bind(this));
    this._CANVAS = this.canvasEl.nativeElement;
    this._CANVAS.style.width = "100%";
    this._CANVAS.style.height = "100%";
    this._CONTEXT = this._CANVAS.getContext("2d");
    this.heightcol =
      document.getElementById("body").clientHeight -
      document.getElementById("firstrow").clientHeight;
    this.widthcol = document.getElementById("testcol").clientWidth;
    this.api.getRoundList();
  }

  ionViewWillEnter(){
    setTimeout(() => {
      if(this.api.smgthCreated)
        this.content1.scrollToBottom();
      this.api.smgthCreated = false;
    },300
    );
  }

  @ViewChild("select1") select1 :Select;
  @ViewChild("content1") content1 : Content;

  constructor(
    public loadingCtrl: LoadingController,
    public navCtrl: NavController,
    public param: ParamService,
    public toastCtrl: ToastController,
    public api: ApiService,
    public popup: PopupService,
    public alert: AlertService 
  ) {
    this.current_map = this.api.id_current_map;
  }

  public open(ev, itemSlide: ItemSliding) {
    ev.preventDefault();
    if (itemSlide.getSlidingPercent() == 0) {
      // Two calls intentional after examination of vendor.js function
      itemSlide.moveSliding(-390);
      itemSlide.moveSliding(-390);
    } else {
      itemSlide.close();
    }
  }

  on_zoom_out(ev) {
    ev.preventDefault();
    if (!(this.resomap >= this.initialzoom)) {
      this.resomap = this.resomap + 150 / 100000;
    } else {
      this.offsetX = 0;
      this.offsetY = 0;
    }
    this.createImageFromBlob();
  }

  on_zoom_in(ev) {
    ev.preventDefault();
    if (this.resomap > this.initialzoom / 4) {
      this.resomap = this.resomap - 150 / 100000;
    }
    this.createImageFromBlob();
  }

  showLoading() {
    if (!this.loading) {
      this.loading = this.loadingCtrl.create({
        content: this.param.datatext.wait,
      });
      this.loading.present();
    }
  }

  dismissLoading() {
    if (this.loading) {
      this.loading.dismiss();
      this.loading = null;
    }
  }

  ionViewDidEnter() {
    this.getImageFromService();
    this.api.textHeadBand = this.param.datatext.headBand_rounds;
  }

  ionViewDidLeave() {
    //clearInterval(this.interv);
  }



  onMapChange(event) {
    this.showLoading();
    this.api.changeMapbyIdHttp(event);
    setTimeout(() => {
      this.imge.src="http://" + this.param.localhost + "/api/maps/current/image?t=" + new Date().getTime()
    }, 4500);
    setTimeout(() => {
      this.api.getCurrentLocations();
      this.api.getCurrentMap();
    }, 5500);
    setTimeout(() => {
      this.api.getRoundList();
      this.getImageFromService();
    }, 6500);
    setTimeout(() => {
      if (this.current_map != this.api.id_current_map) {
        this.current_map = this.api.id_current_map;
      }
      this.dismissLoading();
    }, 8000);
  }

  goCopyRound(ev, index: number) {
    ev.preventDefault();
    this.api.roundpoi_list = [];
    this.api.editRound = false;
    this.api.copyRound = true;
    this.api.copy_round = this.api.round_current_map[index];
    console.log(this.api.copy_round);
    //this.api.getCurrentLocations();
    this.navCtrl.push(this.roundCreationPage);
  }

  popupDelete(ev, type: string, id: number) {
    ev.preventDefault();
    this.popup.DeleteMessage(type, id);
  }

  deleteRound(id: number) {
    this.alert.deleteround(this.api.mailAddInformationBasic())
    this.showLoading();
    //console.log(id);
    this.api.deleteRound(id);
    this.param.qrcodes.forEach((qr) => {
      if (qr.id_round == id) {
        this.api.deletePOI(qr.id_poi);
        this.param.deleteQR(qr.qr);
      }
    });
    setTimeout(() => {
      this.api.getRoundList();
      this.dismissLoading();
    }, 3000);
  }

  goEditRound(ev, index: number) {
    ev.preventDefault();
    this.api.roundpoi_list = [];
    this.api.editRound = true;
    this.api.copyRound = false;
    this.api.copy_round = this.api.round_current_map[index];
    console.log(this.api.copy_round);
    //this.api.getCurrentLocations();
    this.navCtrl.push(this.roundEditPage);
  }

  goCreateRound(ev) {
    ev.preventDefault();
    if (this.api.id_current_map != undefined) {
      this.api.editRound = false;
      this.api.copyRound = false;
      this.api.roundpoi_list = [];
      //this.api.getCurrentLocations();
      this.navCtrl.push(this.roundCreationPage);
    }
  }


  on_mouse_down(evt) {
    this.createImageFromBlob();
    //console.log(evt);

    if (evt.button == 0) {
      evt.preventDefault();
      this.moving = true;
      this.start.x = evt.x;
      this.start.y = evt.y;

      //console.log(evt.x + " " + this.start.x);
    }
  }

  on_mouse_up(evt) {
    this.createImageFromBlob();
    //console.log(evt);
    if (this.moving) {
      var offset = {
        x: this.offsetX,
        y: this.offsetY,
        t: 0,
      };

      offset.x += evt.x - this.start.x;
      offset.y += evt.y - this.start.y;

      this.start.x = evt.x;
      this.start.y = evt.y;

      this.offsetX = offset.x;
      this.offsetY = offset.y;
    }
    this.moving = false;
  }

  on_mouse_move(evt) {
    
    //console.log(evt);

    if (this.moving) {
      this.createImageFromBlob();
      evt.preventDefault();

      var offset = {
        x: this.offsetX,
        y: this.offsetY,
      };

      //console.log(evt.x + " " + this.start.x);

      offset.x += evt.x - this.start.x;
      offset.y += evt.y - this.start.y;

      this.start.x = evt.x;
      this.start.y = evt.y;

      this.offsetX = offset.x;
      this.offsetY = offset.y;
    }
  }

  on_wheel(evt) {
    this.createImageFromBlob();
    evt.preventDefault();

    //console.log(evt.deltaY);
    if (evt.deltaY > 0) {
      if (!(this.resomap >= this.initialzoom)) {
        this.resomap = this.resomap + 150 / 100000;
      } else {
        this.offsetX = 0;
        this.offsetY = 0;
      }
    } else {
      if (this.resomap > this.initialzoom / 4) {
        this.resomap = this.resomap - 150 / 100000;
      }
    }
    console.log(this.resomap);
  }

 

  createImageFromBlob() {
   

      if (this._CANVAS.getContext) {
  
        this._CANVAS.width = this.imge.width;
        this._CANVAS.height = this.imge.height;


        if(this._CANVAS.width==0 && this.current_map){
          console.log("width 0")
          
       }
       

        var w = this._CANVAS.width;
        var h = this._CANVAS.height;
        var r = this.resomap;
        var x = this.offsetX;
        var y = this.offsetY;

        var xm = this.api.mapdata.Offset.X;
        var ym = this.api.mapdata.Offset.Y;
        var rm = this.api.mapdata.Resolution;
        var wm = this.api.mapdata.Width;
        var hm = this.api.mapdata.Height;

        var dx = xm / r + x;
        var dy = h - (ym + hm * rm) * (1 / r) + y;

        var dw = (wm * rm) / r;
        var dh = (hm * rm) / r;


          if (this.current_map) {
          this._CONTEXT.drawImage(this.imge, dx, dy, dw, dh);
          //console.log('dessin image');
        }

        if (this.api.all_locations != undefined) {
          this.api.all_locations.forEach((evenement) => {
            if (evenement.Label !== "qr") {
              this._CONTEXT.beginPath();
              this._CONTEXT.font =
                "" + Math.round(this._CANVAS.width * 0.025) + "px Arial";
              //console.log(evenement);
              this._CONTEXT.fillStyle = "rgba(26, 156, 195,0.7)";
              this._CONTEXT.fillText(
                evenement.Id, // + "- " + evenement.Name,
                evenement.Pose.X / this.resomap + this.offsetX - 20,
                this._CANVAS.height -
                  (evenement.Pose.Y / this.resomap - this.offsetY) -
                  10
              );

              if (evenement.Name === "docking" || evenement.Name === "docked") {
                this._CONTEXT.fillText(
                  evenement.Id + "- " + evenement.Name,
                  evenement.Pose.X / this.resomap + this.offsetX - 20,
                  this._CANVAS.height -
                    (evenement.Pose.Y / this.resomap - this.offsetY) -
                    10
                );
              }

              this._CONTEXT.beginPath();
              //console.log(evenement.Pose)
              this._CONTEXT.arc(
                evenement.Pose.X / this.resomap + this.offsetX,
                this._CANVAS.height -
                  (evenement.Pose.Y / this.resomap - this.offsetY),
                this._CANVAS.width * 0.005,
                0,
                2 * Math.PI
              );
              this._CONTEXT.lineWidth = 1;
              this._CONTEXT.fillStyle = "rgb(26, 156, 195)";
              this._CONTEXT.fill();
              this._CONTEXT.beginPath();
              this._CONTEXT.save();

              this._CONTEXT.translate(
                evenement.Pose.X / this.resomap + this.offsetX,
                this._CANVAS.height -
                  (evenement.Pose.Y / this.resomap - this.offsetY)
              );
              this._CONTEXT.rotate(-evenement.Pose.T);
              this._CONTEXT.moveTo(0, 0);
              this._CONTEXT.lineWidth = 2;
              this._CONTEXT.lineTo(2 * this._CANVAS.width * 0.009, 0);
              this._CONTEXT.strokeStyle = "rgb(26, 156, 195)";
              this._CONTEXT.stroke();

              this._CONTEXT.restore();
            }
          });
        }
      }
    }
 

  getImageFromService() {
    this.imge = document.getElementById("imgmap") as HTMLImageElement;
    if (this.api.id_current_map != undefined) {
      this.offsetX = this.api.mapdata.Offset.X;
      this.offsetY = this.api.mapdata.Offset.Y;
      this.resomap = this.api.mapdata.Resolution;
      this.initialzoom = this.resomap;
    }
    console.log("GetImageFromService");
    this.api.getImgMapHttp().subscribe(
      (data) => {
        //console.log(data);
        this.api.imgMap = data; 
        
        console.log("EndcreateImageFromBlob");
        this.createImageFromBlob();
        
      },
      (error) => {
        console.log(error);
      }
    );
  }

  onSliderRelease(ev, id: Select){
    ev.preventDefault();
    id.open();
  }
}
