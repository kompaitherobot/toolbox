import { Component, OnInit } from "@angular/core";
import { AlertController, NavController } from "ionic-angular";
import { ApiService } from "../../services/api.service";
import { ParamService } from "../../services/param.service";
import { ToastController } from "ionic-angular";

@Component({
  selector: "page-paramuv",
  templateUrl: "paramuv.html",
})
export class ParamUVPage implements OnInit {
  t_evac: number;
  t_preheat: number;
  threshold_lamp: number;
  threshold_uv: number;
  threshold_battery: number;
  detection: boolean;

  ngOnInit() {}

  constructor(
    public navCtrl: NavController,
    public toastCtrl: ToastController,
    public api: ApiService,
    public param: ParamService,
    private alertCtrl: AlertController
  ) {
    this.t_evac = this.param.paramuv.evacuation_time;
    this.t_preheat = this.param.paramuv.preheating_time;
    this.threshold_lamp = this.param.paramuv.lamp_threshold;
    this.threshold_uv = this.param.paramuv.uv_threshold;
    this.threshold_battery = this.param.paramuv.battery_threshold;
    this.detection = this.param.paramuv.person_detection == 1;
    console.log(this.detection);
  }

  editparamuv() {
    if (Math.abs(this.threshold_battery) > 90) {
      this.threshold_battery = 90;
    }
    this.param.paramuv.evacuation_time = Math.abs(this.t_evac);
    this.param.paramuv.preheating_time = Math.abs(this.t_preheat);
    this.param.paramuv.lamp_threshold = Math.abs(this.threshold_lamp);
    this.param.paramuv.uv_threshold = Math.abs(this.threshold_uv);
    this.param.paramuv.battery_threshold = Math.abs(this.threshold_battery);
    if (this.detection) {
      this.param.paramuv.person_detection = 1;
    } else {
      this.param.paramuv.person_detection = 0;
    }
    this.okToast(this.param.datatext.saveDone);
    this.param.updateUVparam();
    this.navCtrl.pop();
  }

  savePassword(ev) {
    ev.preventDefault();
    let alert = this.alertCtrl.create({
      title: this.param.datatext.password,
      message: this.param.datatext.enterPassword,
      cssClass: "alertstyle",
      enableBackdropDismiss: true,
      inputs: [
        {
          name: "password",
          placeholder: this.param.datatext.password,
          type: "password",
        },
      ],
      buttons: [
        {
          text: this.param.datatext.btn_cancel,
          role: "cancel",
          handler: (data) => {
            console.log("Cancel clicked");
          },
        },
        {
          text: this.param.datatext.btn_save,
          role: "backdrop",
          handler: (data) => {
            if (data.password === "Kompai64") {
              this.editparamuv();
            } else {
              this.wrongPassword();
            }
          },
        },
      ],
    });
    alert.present();
  }

  wrongPassword() {
    let alert = this.alertCtrl.create({
      title: this.param.datatext.password,
      message: this.param.datatext.wrongPassword,
      cssClass: "alertstyle_wrongpass",
      enableBackdropDismiss: true,
      inputs: [
        {
          name: "password",
          placeholder: this.param.datatext.password,
          type: "password",
        },
      ],
      buttons: [
        {
          text: this.param.datatext.btn_cancel,
          role: "cancel",
          handler: (data) => {
            console.log("Cancel clicked");
          },
        },
        {
          text: "Save",
          role: "backdrop",
          handler: (data) => {
            if (data.password === "Kompai64") {
              this.editparamuv();
            } else {
              this.wrongPassword();
            }
          },
        },
      ],
    });
    alert.present();
  }

  btnsave() {
    return (
      this.param.paramuv.evacuation_time != Math.abs(this.t_evac) ||
      this.param.paramuv.preheating_time != Math.abs(this.t_preheat) ||
      this.param.paramuv.lamp_threshold != Math.abs(this.threshold_lamp) ||
      this.param.paramuv.uv_threshold != Math.abs(this.threshold_uv) ||
      this.param.paramuv.battery_threshold !=
        Math.abs(this.threshold_battery) ||
      (this.param.paramuv.person_detection == 1) != this.detection
    );
  }

  onCancel(ev) {
    ev.preventDefault();
    this.navCtrl.pop();
  }

  onToggleChange() {
    this.detection = !this.detection;
  }

  okToast(m: string) {
    const toast = this.toastCtrl.create({
      message: m,
      duration: 3000,
      position: "middle",
      cssClass: "toastok",
    });
    toast.present();
  }
}
