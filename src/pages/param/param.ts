import { Component, OnInit } from "@angular/core";
import { NavController } from "ionic-angular";
import { ApiService } from "../../services/api.service";
import { ParamUVPage } from "../paramuv/paramuv";
import { LanguePage } from "../langue/langue";
import { ParamService } from "../../services/param.service";
import { PasswordPage } from "../password/password";

@Component({
  selector: "page-param",
  templateUrl: "param.html",
})
export class ParamPage implements OnInit {
  languePage = LanguePage;
  paramuvPage = ParamUVPage;
  passwordPage=PasswordPage;

  ngOnInit() {}

  constructor(
    public navCtrl: NavController,
    public api: ApiService,
    public param: ParamService
  ) {}

  ionViewDidEnter() {
    this.api.textHeadBand = this.param.datatext.param;
  }

  goLangue(ev) {
    ev.preventDefault();
    this.navCtrl.push(this.languePage);
  }

  goParamUV() {
    this.param.getUVparam();
    this.navCtrl.push(this.paramuvPage);
  }

  goPassword(ev){
    ev.preventDefault();
    this.navCtrl.push(this.passwordPage);
  }

  activateAdminMode(){
   
    this.api.adminMode=!this.api.adminMode;
  }
}
