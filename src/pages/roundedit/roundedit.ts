import { Component, OnInit, ElementRef, ViewChild } from "@angular/core";
import { ParamService } from "../../services/param.service";
import { ToastController } from "ionic-angular";
import { LoadingController } from "ionic-angular";
import { ApiService } from "../../services/api.service";
import { RoundCreationPage } from "../roundcreation/roundcreation";
import { NavController } from "ionic-angular";
import { AlertService } from "../../services/alert.service";

@Component({
  selector: "page-roundedit",
  templateUrl: "roundedit.html",
})
export class RoundEditPage implements OnInit {
  @ViewChild("canvas2") canvasEl: ElementRef;
  @ViewChild("imgmap2") imgmapElement: ElementRef;
  loading: any;
  private _CANVAS: any;
  initialzoom: any;
  heightcol: any;
  widthcol: any;
  private _CONTEXT: any;

  imageToShow: any;
  previousimage: any;
  isImageLoading: boolean;
  rawImageToShow: any;
  roundEditPage = RoundEditPage;
  roundCreationPage = RoundCreationPage;
  loc: any;
  interv: any;
  offsetX: any;
  offsetY: any;
  resomap: any;
  map_url="http://" + this.param.localhost + "/api/maps/current/image?t=" + new Date().getTime();
  imge:any;
  moving: any;
  start: any = {
    x: 0,
    y: 0,
  };

  ngOnInit() {
    this._CANVAS = this.canvasEl.nativeElement; 
    this._CONTEXT = this._CANVAS.getContext("2d");
    this._CANVAS.style.width = "100%";
    this._CANVAS.style.height = "100%";

    this.heightcol =
      document.getElementById("body2").clientHeight -
      document.getElementById("firstrow2").clientHeight;
    this.widthcol = document.getElementById("testcol2").clientWidth;
  }

  constructor(
    public api: ApiService,
    public navCtrl: NavController,
    public loadingCtrl: LoadingController,
    public param: ParamService,
    public toastCtrl: ToastController,
    public alert: AlertService 
  ) {
    
  }

  ionViewDidEnter() {
    this.api.roundpoi_list = [];
    this.displayCopy();
    this.getImageFromService();
    
  }

  ionViewWillLeave() {
    
  }

  on_zoom_out(ev) {
    ev.preventDefault();
    if (!(this.resomap >= this.initialzoom)) {
      this.resomap = this.resomap + 150 / 100000;
    } else {
      this.offsetX = 0;
      this.offsetY = 0;
    }
    this.createImageFromBlob();
  }

  on_zoom_in(ev) {
    ev.preventDefault();
    if (this.resomap > this.initialzoom / 4) {
      this.resomap = this.resomap - 150 / 100000;
    }
    this.createImageFromBlob();
  }

  displayCopy() {
    console.log(this.api.copy_round.Locations);
    this.api.copy_round.Locations.forEach((element) => {
      this.api.createRoundList(
        element.Location.Id,
        element.Location.Name,
        element.Wait,
        element.Speed,
        element.Data,
        element.AnticollisionProfile,
        element.DisableAvoidance
      );
    });
   
  }

  goEditRound(ev) {
    this.alert.editround(this.api.mailAddInformationBasic())
    ev.preventDefault();
    this.navCtrl.push(this.roundCreationPage);
  }

 

  on_mouse_down(evt) {
    this.createImageFromBlob();
    //console.log(evt);

    if (evt.button == 0) {
      evt.preventDefault();
      this.moving = true;
      this.start.x = evt.x;
      this.start.y = evt.y;

      //console.log(evt.x + " " + this.start.x);
    }
  }

  on_mouse_up(evt) {
    this.createImageFromBlob();
    //console.log(evt);
    if (this.moving) {
      var offset = {
        x: this.offsetX,
        y: this.offsetY,
        t: 0,
      };

      offset.x += evt.x - this.start.x;
      offset.y += evt.y - this.start.y;

      this.start.x = evt.x;
      this.start.y = evt.y;

      this.offsetX = offset.x;
      this.offsetY = offset.y;
    }
    this.moving = false;
  }

  on_mouse_move(evt) {
    
    //console.log(evt);

    if (this.moving) {
      this.createImageFromBlob();
      evt.preventDefault();

      var offset = {
        x: this.offsetX,
        y: this.offsetY,
      };

      //console.log(evt.x + " " + this.start.x);

      offset.x += evt.x - this.start.x;
      offset.y += evt.y - this.start.y;

      this.start.x = evt.x;
      this.start.y = evt.y;

      this.offsetX = offset.x;
      this.offsetY = offset.y;
    }
  }

  on_wheel(evt) {
    this.createImageFromBlob();
    evt.preventDefault();

    //console.log(evt.deltaY);
    if (evt.deltaY > 0) {
      if (!(this.resomap >= this.initialzoom)) {
        this.resomap = this.resomap + 150 / 100000;
      } else {
        this.offsetX = 0;
        this.offsetY = 0;
      }
    } else {
      if (this.resomap > this.initialzoom / 4) {
        this.resomap = this.resomap - 150 / 100000;
      }
    }
    console.log(this.resomap);
  }



  createImageFromBlob() {
    //console.log("createImageFromBlob");
  
     

      if (this._CANVAS.getContext) {
       

        this._CANVAS.width = this.imge.width;
        this._CANVAS.height = this.imge.height;

        var w = this._CANVAS.width;
        var h = this._CANVAS.height;
        var r = this.resomap;
        var x = this.offsetX;
        var y = this.offsetY;

        var xm = this.api.mapdata.Offset.X;
        var ym = this.api.mapdata.Offset.Y;
        var rm = this.api.mapdata.Resolution;
        var wm = this.api.mapdata.Width;
        var hm = this.api.mapdata.Height;

        var dx = xm / r + x;
        var dy = h - (ym + hm * rm) * (1 / r) + y;

        var dw = (wm * rm) / r;
        var dh = (hm * rm) / r;

        this._CONTEXT.drawImage(this.imge, dx, dy, dw, dh);
        //console.log('dessin image');

        this.api.copy_round.Locations.forEach((evenement) => {
          if (evenement.Label !== "qr") {
            this._CONTEXT.beginPath();
            this._CONTEXT.font =
              "" + Math.round(this._CANVAS.width * 0.025) + "px Arial";
            //console.log(evenement);
            this._CONTEXT.fillStyle = "rgba(26, 156, 195,0.7)";
            this._CONTEXT.fillText(
              evenement.Location.Id, // + "- " + evenement.Location.Name,
              evenement.Location.Pose.X / this.resomap + this.offsetX - 20,
              this._CANVAS.height -
                (evenement.Location.Pose.Y / this.resomap - this.offsetY) -
                10
            );

           
            this._CONTEXT.beginPath();
            //console.log(evenement.Pose)
            this._CONTEXT.arc(
              evenement.Location.Pose.X / this.resomap + this.offsetX,
              this._CANVAS.height -
                (evenement.Location.Pose.Y / this.resomap - this.offsetY),
              this._CANVAS.width * 0.005,
              0,
              2 * Math.PI
            );
            this._CONTEXT.lineWidth = 1;

                  if(this.api.copy_round.Locations[0].Location.Id == evenement.Location.Id){
              this._CONTEXT.fillStyle = "rgb(145,221,145)";
            }
            else{
              if(this.api.copy_round.Locations[this.api.copy_round.Locations.length-1].Location.Id == evenement.Location.Id){
                this._CONTEXT.fillStyle = "rgb(221,145,145)";
              }
              else{
                this._CONTEXT.fillStyle = "rgb(26, 156, 195)";
              }
            }
            this._CONTEXT.fill();
            this._CONTEXT.beginPath();
            this._CONTEXT.save();

            this._CONTEXT.translate(
              evenement.Location.Pose.X / this.resomap + this.offsetX,
              this._CANVAS.height -
                (evenement.Location.Pose.Y / this.resomap - this.offsetY)
            );
            this._CONTEXT.rotate(-evenement.Location.Pose.T);
            this._CONTEXT.moveTo(0, 0);
            this._CONTEXT.lineWidth = 2;
            this._CONTEXT.lineTo(2 * this._CANVAS.width * 0.009, 0);
            this._CONTEXT.strokeStyle = "rgb(26, 156, 195)";
            this._CONTEXT.stroke();

            this._CONTEXT.restore();
          }
        });

     for(let i = 0; i<this.api.copy_round.Locations.length-1; i++){
          this._CONTEXT.beginPath();
          if(this.api.copy_round.Locations[i+1].Speed > 0)
          {
            this._CONTEXT.setLineDash([]);
          }else
          {
            this._CONTEXT.setLineDash([5]);
          }

          this._CONTEXT.moveTo(
            this.api.copy_round.Locations[i].Location.Pose.X / this.resomap + this.offsetX,
            this._CANVAS.height -
              (this.api.copy_round.Locations[i].Location.Pose.Y / this.resomap - this.offsetY)
          );

          this._CONTEXT.lineTo(
            this.api.copy_round.Locations[i+1].Location.Pose.X / this.resomap + this.offsetX,
            this._CANVAS.height -
              (this.api.copy_round.Locations[i+1].Location.Pose.Y / this.resomap - this.offsetY)
          );

          this._CONTEXT.stroke();

        }

        //this._CONTEXT.stroke();
        this._CONTEXT.restore();

      }
    };
    
    getImageFromService() {
      this.imge = document.getElementById("imgmap2") as HTMLImageElement;
      if (this.api.id_current_map != undefined) {
        this.offsetX = this.api.mapdata.Offset.X;
        this.offsetY = this.api.mapdata.Offset.Y;
        this.resomap = this.api.mapdata.Resolution;
        this.initialzoom = this.resomap;
      }
      console.log("GetImageFromService");
      this.api.getImgMapHttp().subscribe(
        (data) => {
          this.api.imgMap = data;
          this.createImageFromBlob();
          console.log("GetImageFromService");
        },
        (error) => {
          console.log(error);
        }
      );
    }
  
  
}
