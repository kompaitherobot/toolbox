import { Component, OnInit, ElementRef, ViewChild ,Renderer2} from "@angular/core";
import { ApiService } from "../../services/api.service";
import { AlertService } from "../../services/alert.service";
import { PopupService } from "../../services/popup.service";
import { ParamService } from "../../services/param.service";
import { SpeechService } from "../../services/speech.service";
//import { File } from "@ionic-native/file";
import { Select, Content } from "ionic-angular";
import jsQR from "jsqr";

import {
  ItemSliding,
  LoadingController,
  NavController,
  ToastController,
} from "ionic-angular";
import { RoundDisplayPage } from "../rounddisplay/rounddisplay";
import { QRCodePage } from "../qrcode/qrcode";
import { DockingPage } from "../docking/docking";
import { LiftPage } from "../lift/lift";

@Component({
  selector: "page-poi",
  templateUrl: "poi.html",
})
export class PoiPage implements OnInit {
  @ViewChild("canvas4") canvasEl: ElementRef;
  @ViewChild("imgmap4") imgmapElement: ElementRef;
  @ViewChild("content1") content1: Content;
  @ViewChild("video") videoElement: ElementRef;
  @ViewChild("videoCanvas") videoCanvas: ElementRef;
  rdPage = RoundDisplayPage;
  qrcPage = QRCodePage;
  dockPage = DockingPage;
  liftPage = LiftPage;
  loading: any;
  private _CANVAS: any;
  initialzoom: any;
  heightcol: any;
  widthcol: any;
  private _CONTEXT: any;

  imageToShow: any;
  previousimage: any;
  isImageLoading: boolean;
  rawImageToShow: any;
  loc: any;
  interv: any;
  intervgodocking: any;
  intervgopoi: any;
  intervpoi: boolean;
  intervmap: any;
  offsetX: any;
  offsetY: any;
  resomap: any;

  moving: any;
  pose: any;
  start: any = {
    x: 0,
    y: 0,
  };

  clickpose: any = {
    x: 0,
    y: 0,
  };

  xtest: number;
  ytest: number;

  idpoi_to_edit: number;
  namepoi: string;
  editnamepoi: string;
  newpoi: boolean;
  editpoi: boolean;
  resetposition: boolean;
  gopoi: boolean;
  waygo: number;
  avoidance: boolean;
  speed: number;
  newmap: boolean;
  namemap: string;
  editmap: boolean;
  nameeditmap: string;
  loadimg: boolean;
  reloc: boolean;
  filen: string;
  b: any;
  map_url = "http://" + this.param.localhost + "/api/maps/current/image?t=" + new Date().getTime();
  imge: any;
  @ViewChild("select1") select1: Select;
  @ViewChild("select2") select2: Select;
  qrdetection: boolean;
  imgRos: any;
  selectLabel: any;
  SelectLabel: string = "none";
  videoWidth = 0;
  videoHeight = 0;

  constructor(
    private renderer: Renderer2,
    public navCtrl: NavController,
    public loadingCtrl: LoadingController,
    public toastCtrl: ToastController,
    public popup: PopupService,
    public api: ApiService,
    public speech: SpeechService,
    public param: ParamService,
    public alert: AlertService //private alertCtrl: AlertController
  ) {
    this.qrdetection = false;
    this.newpoi = false;
    this.namepoi = "";
    this.namemap = "";
    this.editnamepoi = "";
    this.editpoi = false;
    this.resetposition = false;
    this.gopoi = false;
    this.waygo = 1;
    this.avoidance = true;
    this.speed = 70;
    this.newmap = false;
    this.editmap = false;
    this.loadimg = false;
    this.reloc = false;

    this.selectLabel = {
      title: "Label",
    };

    if(this.param.robot.cam_USB==1){
      this.setupconstraint();
    }
    

   
  }

  constraints = {
    //audio: false,
    //video: {deviceId: "f9860b260e18d7fd1687d8567e21b49c4bb26f87606020741717e11f6179f409"},
    video: true,

    facingMode: "environment",
    width: { ideal: 1920 },
    height: { ideal: 1080 },
  };
  
  setupconstraint() {
    // demande de permission camera
    navigator.mediaDevices.getUserMedia(this.constraints);

    // verification des permissions video et de la disponibilité de devices
    if (!!(navigator.mediaDevices && navigator.mediaDevices.getUserMedia)) {
      // enumeration des appareils connectes pour filtrer
      navigator.mediaDevices.enumerateDevices().then((result) => {
        var constraints;
        console.log(result);

        result.forEach(function (device) {
          // filtrer pour garder les flux video
          if (device.kind.includes("videoinput")) {
            // filtrer le label de la camera
            if (device.label.includes("USB")) {
              constraints = {
                audio: false,
                //video: {deviceId: "f9860b260e18d7fd1687d8567e21b49c4bb26f87606020741717e11f6179f409"},
                // affecter le deviceid filtré
                video: { deviceId: device.deviceId },

                facingMode: "environment",
                width: { ideal: 1920 },
                height: { ideal: 1080 },
              };
            }
          }
        });
        // on lance la connexion de la vidéo
        if (constraints) {
          this.constraints = constraints;
          this.startCamera(constraints);
          console.log("startcam");
        }
        //this.startCamera(constraints);
      });
    } else {
      console.log("Sorry, camera not available.");
    }
  }

  onLabelChange() {
    console.log("change label")
  }


  convertDataURIToBinary2(dataURI) {
    var raw = window.atob(dataURI);
    var rawLength = raw.length;
    var array = new Uint8Array(new ArrayBuffer(rawLength));


    for (var i = 0; i < rawLength; i++) {
      array[i] = raw.charCodeAt(i);
    }
    return array;
  }

  ngOnInit() {
    //this.current_map=this.api.id_current_map;

    this._CANVAS = this.canvasEl.nativeElement;
    this._CONTEXT = this._CANVAS.getContext("2d");
    this._CANVAS.style.width = "100%";
    this._CANVAS.style.height = "100%";
    this.heightcol =
      document.getElementById("body4").clientHeight -
      document.getElementById("firstrow4").clientHeight * 2;
    this.widthcol = document.getElementById("testcol4").clientWidth;



    //this.speech.getVoice();
    this.popup.onSomethingHappened1(this.deleteMap.bind(this));
    this.popup.onSomethingHappened2(this.deletePOI.bind(this));
    this.getImageFromService();
  }

  abort(ev) {
    ev.preventDefault();
    if (this.intervpoi) {
      clearInterval(this.intervgopoi);
    }
    this.api.abortNavHttp();
  }


  scan(ev) {
    ev.preventDefault();
    ///si cam usb
    this.alert.scanqr(this.api.mailAddInformationBasic());
    if (this.param.robot.cam_USB == 1) {
      this.qrdetection=true;
      console.log("enter scan usb");
      setTimeout(() => {
        if (this.qrdetection) {
          this.qrdetection = false;
        }
      }, 3000);
      try {
        this.videoCanvas.nativeElement.height = this.videoElement.nativeElement.videoHeight;
        this.videoCanvas.nativeElement.width = this.videoElement.nativeElement.videoWidth;

        const g = this.videoCanvas.nativeElement.getContext("2d");
        g.drawImage(this.videoElement.nativeElement, 0, 0, this.videoCanvas.nativeElement.width, this.videoCanvas.nativeElement.height);
        const imageData = g.getImageData(0, 0, this.videoCanvas.nativeElement.width, this.videoCanvas.nativeElement.height);
        const code = jsQR(imageData.data, imageData.width, imageData.height, {
          inversionAttempts: "dontInvert",
        });

        if (code) {
          console.log(code.data.trim());
          if (
            code.data !== null &&
            this.param.qrcodes.filter((x) => x.qr == code.data.trim())
              .length > 0
          ) {
            this.api.QRselected = this.param.qrcodes.filter(
              (x) => x.qr == code.data.trim()
            )[0];
            this.onReloc();
          } else if (
            code.data !== null &&
            this.param.qrcodes.filter((x) => x.qr == code.data.trim())
              .length === 0
          ) {
            this.popup.showToastRed(this.param.datatext.unknownqrcode, 4000, "middle");
            this.speech.speak(this.param.datatext.unknownqrcode);
          }
        } else {
          setTimeout(() => {
            this.popup.showToastRed(
              this.param.datatext.noqrdetected,
              4000,
              "middle"
            );
            this.speech.speak(this.param.datatext.noqrdetected);
          }, 1000);
          
        }
      } catch (err) {
        console.log("Error", err);
      }

      /////si cam ros
    } else {
      console.log("scan qr ros");
      this.qrdetection = true;
      this.api.startQrDetection(true);
      setTimeout(() => {
        if (this.qrdetection) {
          this.qrdetection = false;
          this.api.startQrDetection(false);
          this.popup.showToastRed(
            this.param.datatext.noqrdetected,
            5000,
            "middle"
          );
          this.speech.speak(this.param.datatext.noqrdetected);
        }
      }, 5000);
      this.api.listener_qr.subscribe((message) => {
        console.log('Received message on ' + this.api.listener_qr.name + ': ' + message.data);
        if (this.qrdetection) {
          this.qrdetection = false;
          this.api.startQrDetection(false);
          if (
            message.data !== null &&
            this.param.qrcodes.filter((x) => x.qr == message.data.trim())
              .length > 0
          ) {
            this.api.QRselected = this.param.qrcodes.filter(
              (x) => x.qr == message.data.trim()
            )[0];
            this.onReloc();
          } else if (
            message.data !== null &&
            this.param.qrcodes.filter((x) => x.qr == message.data.trim())
              .length === 0
          ) {
            this.popup.showToastRed(this.param.datatext.unknownqrcode, 4000, "middle");
            this.speech.speak(this.param.datatext.unknownqrcode);
          }
        }

      })

    }



  }

  onReloc() {
    console.log("onreloc");
    this.showLoading();
    this.speech.speak(this.param.datatext.alertReloc);
    //this.api.getCurrentMap();
    console.log(this.api.id_current_map)
    console.log(this.api.QRselected.id_map)
    if (!(this.api.id_current_map == this.api.QRselected.id_map)) {
      this.api.current_map = this.api.QRselected.id_map;
      console.log("onchangemap");
      this.onMapChange(this.api.QRselected.id_map);
    }
    setTimeout(() => {
      this.api.getFirstLocationHttp();
    }, 4000);
    setTimeout(() => {
      this.api.resetLocationHttp();
    }, 6000);
    setTimeout(() => {
      this.imge.src = "http://" + this.param.localhost + "/api/maps/current/image?t=" + new Date().getTime()
    }, 8000);
    setTimeout(() => {
      this.createImageFromBlob();
      this.dismissLoading();
      this.popup.showToastBlue(
        this.param.datatext.localizationDone + this.api.mapdata.Name,
        4000
      );
      this.speech.speak(this.param.datatext.localizationDone + this.api.mapdata.Name);
    }, 9000);
  }

  ionViewDidEnter() {


    this.filen = null;

    this.api.textHeadBand = this.param.datatext.toolbox;

    if (this.api.id_current_map && this.api.firstopen) {

      this.api.current_map = this.api.mapdata.Id;
      this.api.firstopen = false;
      this.getImageFromService();
    }
    this.api.getCurrentLocations();
    this.api.getRoundList();

    this.intervmap = setInterval(() => this.updatemap(), 400);
    // CAMERA ROS
    if (this.param.robot.cam_USB == 0) {

      this.api.listener_camera.subscribe((message) => {

        //console.log('Received message on ' + listener_compressed.name + ': ');
        this.imgRos = document.getElementById('imgcompressed');
        //console.log(message);

        var bufferdata = this.convertDataURIToBinary2(message.data);

        var blob = new Blob([bufferdata], { type: 'image/jpeg' });
        var blobUrl = URL.createObjectURL(blob);

        this.imgRos.src = blobUrl;


      });

    }
  }

  updatemap() {
    if (this.reloc || this.api.differential_status.CurrentAngularSpeed > 0.005 || this.api.differential_status.CurrentLinearSpeed > 0.01 || this._CANVAS.width == 0) {
      this.createImageFromBlob();
    }


  }

  ionViewWillLeave() {
    clearInterval(this.intervmap);
    if (this.param.robot.cam_USB == 0) {
      this.api.listener_camera.removeAllListeners();
    }
  }

  onClickNewPOI(ev) {
    ev.preventDefault();
    this.newpoi = true;
    this.SelectLabel = "none";
    this.namepoi = "";
  }


  onClickNewMap(ev) {
    ev.preventDefault();
    this.filen = null;
    this.newmap = true;
    this.namemap = "";
  }

  onClickEditMap(ev) {
    ev.preventDefault();
    this.filen = null;
    this.editmap = true;
    this.nameeditmap = this.api.mapdata.Name;
  }

  onClickGoPOI(ev, id: number, n: string, p: any) {
    ev.preventDefault();
    this.pose = p;
    this.idpoi_to_edit = id;
    this.namepoi = n;
    if (this.namepoi === "docked" || this.namepoi === "docking") {
      this.api.towardDocking = true;
      this.api.reachHttp("docking");
      this.intervgodocking = setInterval(() => this.updateTrajectory(), 500);
    } else {
      this.intervpoi = true;
      this.gopoi = true;
      this.intervgopoi = setInterval(() => this.updateGoPOI(), 500);
    }

  }

  updateGoPOI() {
    if (this.api.navigation_status.status === 2) {
      this.intervpoi = false;
      this.gopoi = false;
      clearInterval(this.intervgopoi);
    }
  }

  onClickEditPOI(ev, n: string, l: string, poi: number) {
    ev.preventDefault();
    this.editpoi = true;
    this.SelectLabel = l;
    this.editnamepoi = n;
    this.idpoi_to_edit = poi;
  }

  ReachPoi(ev) {
    ev.preventDefault();
    this.alert.gopoi(this.api.mailAddInformationBasic());
    this.speed = Math.abs(this.speed);
    if (Math.abs(this.speed) > 100) {
      this.speed = 100;
    }

    this.api.putDestination(
      this.pose,
      (this.waygo * this.speed) / 100,
      this.avoidance
    );
  }

  updateTrajectory() {
    // listen the round status to allow the robot to go to the next
    //// go docking or go poi in progress
    if (this.api.towardDocking) {
      if (this.api.docking_status.status === 3) {
        this.api.towardDocking = false;

      } else if (
        this.api.towardDocking &&
        this.api.navigation_status.status === 0 &&
        this.api.docking_status.detected
      ) {
        //connect to the docking when he detect it
        this.api.connectHttp();
      }
    } else {
      clearInterval(this.intervgodocking);
    }
  }

  cancelpoi(ev) {
    ev.preventDefault();
    this.newpoi = false;
  }

  cancelmap(ev) {
    ev.preventDefault();
    this.api.mapok = true;
    this.newmap = false;
    this.editmap = false;
    this.filen = null;
  }
  cancelgopoi(ev) {
    ev.preventDefault();
    this.gopoi = false;
    this.api.towardDocking = false;
  }

  canceleditpoi(ev) {
    ev.preventDefault();
    this.editpoi = false;
    this.resetposition = false;
  }

  editPoi(ev) {
    ev.preventDefault();
    this.alert.editpoi(this.api.mailAddInformationBasic());
    this.editnamepoi = this.editnamepoi.replace(
      new RegExp("[<>&$§µ£#*_|`¤~(){}°²@%/]", "g"),
      ""
    );
    this.editnamepoi = this.editnamepoi.replace(new RegExp('["]', "g"), "");
    this.editnamepoi = this.editnamepoi.replace(new RegExp(/\\/, "g"), "");
    this.editnamepoi = this.editnamepoi.replace(new RegExp(/[\[\]]/, "g"), "");
    if (this.editnamepoi != "") {
      var x: number;
      var y: number;
      var t: number;

      this.showLoading();
      if (this.resetposition) {
        x = this.api.localization_status.positionx;
        y = this.api.localization_status.positiony;
        t = this.api.localization_status.positiont;
      } else {
        x = this.api.all_locations.filter(
          (poi) => poi.Id == this.idpoi_to_edit
        )[0].Pose.X;
        y = this.api.all_locations.filter(
          (poi) => poi.Id == this.idpoi_to_edit
        )[0].Pose.Y;
        t = this.api.all_locations.filter(
          (poi) => poi.Id == this.idpoi_to_edit
        )[0].Pose.T;
      }

      this.api.putPOI(this.editnamepoi, this.SelectLabel, this.idpoi_to_edit, x, y, t);
      setTimeout(() => {
        this.editpoi = false;
        this.api.getCurrentLocations();
      }, 2000);
      setTimeout(() => {
        this.createImageFromBlob();
        this.dismissLoading();
      }, 4000);
    }
  }

  createpoi(ev) {
    ev.preventDefault();
    this.alert.createpoi(this.api.mailAddInformationBasic());
    this.namepoi = this.namepoi.replace(
      new RegExp("[<>&$§µ£#*_|`¤~(){}°²@%/]", "g"),
      ""
    );
    this.namepoi = this.namepoi.replace(new RegExp('["]', "g"), "");
    this.namepoi = this.namepoi.replace(new RegExp(/\\/, "g"), "");
    this.namepoi = this.namepoi.replace(new RegExp(/[\[\]]/, "g"), "");
    if (this.namepoi != "") {
      this.showLoading();
      this.api.postPoi(this.namepoi, this.SelectLabel);
      setTimeout(() => {
        this.newpoi = false;
        this.api.changeMapbyIdHttp(this.api.id_current_map);
        this.api.getCurrentLocations();
      }, 2000);
      setTimeout(() => {
        this.createImageFromBlob();
        this.dismissLoading();
        this.content1.scrollToBottom();
      }, 4000);
    }
  }

  addMap(ev) {
    ev.preventDefault();
    this.alert.addmap(this.api.mailAddInformationBasic());
    this.namemap = this.namemap.replace(
      new RegExp("[<>&$§µ£#*_|`¤~(){}°²@%/]", "g"),
      ""
    );
    this.namemap = this.namemap.replace(new RegExp('["]', "g"), "");
    this.namemap = this.namemap.replace(new RegExp(/\\/, "g"), "");
    this.namemap = this.namemap.replace(new RegExp(/[\[\]]/, "g"), "");
    if (this.namemap != "" && this.filen) {
      this.showLoading();
      var formul = <HTMLFormElement>document.getElementById("update_form");
      let formData = new FormData(formul);
      formData.append("resolution", "0.04");
      formData.append("offset_x", "0.0");
      formData.append("offset_y", "0.0");
      formData.append("filename", this.b);


      console.log(formData.get("filename"));
      setTimeout(() => {
        this.api.postMap(formData);
      }, 500);
      setTimeout(() => {
        if (this.api.mapok) {
          this.api.getAllMapsHttp();
        }
      }, 3000);
      setTimeout(() => {
        if (this.api.mapok) {
          this.newmap = false;
          this.filen = null;
          this.dismissLoading();
          this.onMapChange(this.api.all_maps[this.api.all_maps.length - 1].Id);
        } else {
          this.dismissLoading();
        }
      }, 4000);
    }
  }

  editMap(ev) {
    ev.preventDefault();
    this.alert.editmap(this.api.mailAddInformationBasic());
    this.nameeditmap = this.nameeditmap.replace(
      new RegExp("[<>&$§µ£#*_|`¤~(){}°²@%/]", "g"),
      ""
    );
    this.nameeditmap = this.nameeditmap.replace(new RegExp('["]', "g"), "");
    this.nameeditmap = this.nameeditmap.replace(new RegExp(/\\/, "g"), "");
    this.nameeditmap = this.nameeditmap.replace(new RegExp(/[\[\]]/, "g"), "");
    if (this.nameeditmap != "") {
      this.showLoading();
      var formul = <HTMLFormElement>document.getElementById("update_formul");
      let formData = new FormData(formul);
      formData.append("resolution", "0.04");
      formData.append("offset_x", "0.0");
      formData.append("offset_y", "0.0");
      if (this.filen) {
        formData.append("filename", this.b);
      } else {
        formData.append("filename", null);
      }

      console.log(formData.get("filename"));
      setTimeout(() => {
        this.api.putMap(formData);
      }, 500);
      setTimeout(() => {
        if (this.api.mapok) {
          this.api.getAllMapsHttp();
        }
      }, 4000);
      setTimeout(() => {
        if (this.api.mapok) {
          this.editmap = false;

          this.dismissLoading();
          if (this.filen) {
            this.onMapChange(this.api.id_current_map);
          }
          this.filen = null;
        } else {
          this.dismissLoading();
        }
      }, 5000);
    }
  }

  showLoading() {
    if (!this.loading) {
      this.loading = this.loadingCtrl.create({
        content: this.param.datatext.wait,
      });
      this.loading.present();
    }
  }

  popupDelete(ev, type: string, id: number) {
    ev.preventDefault();
    this.popup.DeleteMessage(type, id);
  }

  deleteMap() {
    this.alert.deletemap(this.api.mailAddInformationBasic());
    this.showLoading();
    this.api.deleteMap(this.api.id_current_map);
    this.api.all_locations = [];
    this.param.qrcodes.forEach((qr) => {
      if (qr.id_map == this.api.id_current_map) {
        this.api.deletePOI(qr.id_poi);
        this.param.deleteQR(qr.qr);
      }
    });
    // this.current_map = undefined;
    // this.api.id_current_map = undefined;
    // this.api.imgMap = undefined;
    setTimeout(() => {
      this.api.getAllMapsHttp();
      this.api.current_map = undefined;
      this.api.id_current_map = undefined;
      this.api.imgMap = undefined;
      this.createImageFromBlob();
      this.dismissLoading();
    }, 3000);
  }

  roundPage(ev) {
    ev.preventDefault();
    if (this.api.all_locations.length > 1) {
      this.api.getRoundList();
      this.navCtrl.push(this.rdPage);
    }
  }

  dockingPage(ev) {
    ev.preventDefault();
    if (this.api.current_map) {
      this.showLoading();
      setTimeout(() => {
        this.api.changeMapbyIdHttp(this.api.id_current_map);
        this.api.getCurrentLocations();
      }, 3000);
      setTimeout(() => {
        this.dismissLoading();
      }, 4000);
      this.navCtrl.push(this.dockPage);
    }

  }

  goLiftPage(ev) {
    ev.preventDefault();

    this.navCtrl.push(this.liftPage);


  }

  qrPage(ev) {
    ev.preventDefault();
    this.api.getRoundList();
    this.param.getQR();
    this.navCtrl.push(this.qrcPage);
  }

  deletePOI(id: number) {
    this.alert.deletepoi(this.api.mailAddInformationBasic());
    this.showLoading();
    this.api.deletePOI(id);
    setTimeout(() => {
      this.api.getCurrentLocations();
    }, 2000);
    setTimeout(() => {
      this.createImageFromBlob();
      this.dismissLoading();
    }, 3000);
  }

  dismissLoading() {
    if (this.loading) {
      this.loading.dismiss();
      this.loading = null;
    }
  }

  onMapChange(event: number) {
    this.alert.loadmap(this.api.mailAddInformationBasic());
    this.showLoading();
    this.api.changeMapbyIdHttp(event);
    setTimeout(() => {
      this.imge.src = "http://" + this.param.localhost + "/api/maps/current/image?t=" + new Date().getTime()
    }, 4500);
    setTimeout(() => {
      this.api.getCurrentLocations();
      this.api.getCurrentMap();
    }, 5500);
    setTimeout(() => {
      if (this.api.current_map != this.api.id_current_map) {
        this.api.current_map = this.api.id_current_map;
      }
    }, 6500);
    setTimeout(() => {
      this.getImageFromService();
      this.dismissLoading();
    }, 7000);
  }



  on_mouse_down(evt) {

    this.createImageFromBlob();
    if (evt.button == 0) {
      evt.preventDefault();
      this.moving = true;
      this.start.x = evt.x;
      this.start.y = evt.y;


      var x = (evt.offsetX * this._CANVAS.width) / this._CANVAS.clientWidth;

      var y = (evt.offsetY * this._CANVAS.height) / this._CANVAS.clientHeight;
      this.clickpose.x =
        (x - this.offsetX - this._CANVAS.width / 2) * this.resomap;
      this.clickpose.y =
        -(y - this.offsetY - this._CANVAS.height / 2) * this.resomap;


    }
    if (this.reloc) {

      this.api.putLocalization(this.xtest, this.ytest, 0);
    }
  }

  on_mouse_up(evt) {
    this.createImageFromBlob();
    if (this.moving) {
      var offset = {
        x: this.offsetX,
        y: this.offsetY,
        t: 0,
      };

      offset.x += evt.x - this.start.x;
      offset.y += evt.y - this.start.y;

      this.start.x = evt.x;
      this.start.y = evt.y;

      this.offsetX = offset.x;
      this.offsetY = offset.y;
    }
    this.moving = false;
    if (this.reloc) {

      this.api.putLocalization(this.xtest, this.ytest, 0);
    }
  }

  on_mouse_move(evt) {


    if (this.moving) {
      this.createImageFromBlob();
      evt.preventDefault();

      var offset = {
        x: this.offsetX,
        y: this.offsetY,
      };



      offset.x += evt.x - this.start.x;
      offset.y += evt.y - this.start.y;

      this.start.x = evt.x;
      this.start.y = evt.y;

      this.offsetX = offset.x;
      this.offsetY = offset.y;
    }
  }

  on_wheel(evt) {
    this.createImageFromBlob();
    evt.preventDefault();


    if (evt.deltaY > 0) {
      if (!(this.resomap >= this.initialzoom)) {
        this.resomap = this.resomap + 150 / 100000;
      } else {
        this.offsetX = 0;
        this.offsetY = 0;
      }
    } else {
      if (this.resomap > this.initialzoom / 4) {
        this.resomap = this.resomap - 150 / 100000;
      }
    }

  }

  public open(ev, itemSlide: ItemSliding) {
    ev.preventDefault();
    if (itemSlide.getSlidingPercent() == 0) {
      // Two calls intentional after examination of vendor.js function
      itemSlide.moveSliding(-390);
      itemSlide.moveSliding(-390);
    } else {
      itemSlide.close();
    }
  }

  on_zoom_out(ev) {
    ev.preventDefault();
    //zoom -
    if (!(this.resomap >= this.initialzoom)) {
      this.resomap = this.resomap + 150 / 100000;
    } else {
      this.offsetX = 0;
      this.offsetY = 0;
    }

    this.createImageFromBlob();
  }

  on_zoom_in(ev) {
    ev.preventDefault();
    //zoom +
    if (this.resomap > this.initialzoom / 4) {
      this.resomap = this.resomap - 150 / 100000;
    }

    this.createImageFromBlob();
  }

  createImageFromBlob() {
    console.log("createImageFromBlob4");

    if (!this.api.current_map) {
      console.log("no current map");
    }
    if (this._CONTEXT !== undefined && !this.api.current_map) {
      console.log("no map");
      this._CONTEXT.clearRect(0, 0, this._CANVAS.width, this._CANVAS.height);
      this._CONTEXT.beginPath();
      this._CONTEXT.fillStyle = "rgba(0, 0, 0, 0.0)";
      this._CONTEXT.fillRect(0, 0, this._CANVAS.width, this._CANVAS.height);

    }


    if (this.api.current_map && this._CANVAS.getContext) {
      //console.log(this._CANVAS);

      //console.log(i);
      this._CANVAS.width = this.imge.width;
      this._CANVAS.height = this.imge.height;

      if (this._CANVAS.width == 0 && this.api.current_map) {
        console.log("width 0")

      }


      var w = this._CANVAS.width;
      var h = this._CANVAS.height;
      var r = this.resomap;
      var x = this.offsetX;
      var y = this.offsetY;

      var xm = this.api.mapdata.Offset.X;
      var ym = this.api.mapdata.Offset.Y;
      var rm = this.api.mapdata.Resolution;
      var wm = this.api.mapdata.Width;
      var hm = this.api.mapdata.Height;

      var dx = xm / r + x;
      var dy = h - (ym + hm * rm) * (1 / r) + y;

      var dw = (wm * rm) / r;
      var dh = (hm * rm) / r;

      if (this.api.current_map) {
        this._CONTEXT.drawImage(this.imge, dx, dy, dw, dh);
        //console.log('dessin image');
      }


      if (this.api.all_locations) {
        this.api.all_locations.forEach((evenement) => {
          if (evenement.Label !== "qr") {
            this._CONTEXT.beginPath();
            this._CONTEXT.font =
              "" + Math.round(this._CANVAS.width * 0.025) + "px Arial";

            this._CONTEXT.fillStyle = "rgba(26, 156, 195,0.7)";
            this._CONTEXT.fillText(
              evenement.Id, //+ "- " + evenement.Name,
              evenement.Pose.X / this.resomap + this.offsetX - 20,
              this._CANVAS.height -
              (evenement.Pose.Y / this.resomap - this.offsetY) -
              10
            );

            if (evenement.Name === "docking" || evenement.Name === "docked") {
              this._CONTEXT.fillText(
                evenement.Id + "- " + evenement.Name,
                evenement.Pose.X / this.resomap + this.offsetX - 20,
                this._CANVAS.height -
                (evenement.Pose.Y / this.resomap - this.offsetY) -
                10
              );
            }

            this._CONTEXT.beginPath();
            this._CONTEXT.arc(
              evenement.Pose.X / this.resomap + this.offsetX,
              this._CANVAS.height -
              (evenement.Pose.Y / this.resomap - this.offsetY),
              this._CANVAS.width * 0.005,
              0,
              2 * Math.PI
            );
            this._CONTEXT.lineWidth = 1;
            this._CONTEXT.fillStyle = "rgb(26, 156, 195)";
            this._CONTEXT.fill();

            this._CONTEXT.beginPath();
            this._CONTEXT.save();

            this._CONTEXT.translate(
              evenement.Pose.X / this.resomap + this.offsetX,
              this._CANVAS.height -
              (evenement.Pose.Y / this.resomap - this.offsetY)
            );
            this._CONTEXT.rotate(-evenement.Pose.T);
            this._CONTEXT.moveTo(0, 0);
            this._CONTEXT.lineWidth = 2;
            this._CONTEXT.lineTo(2 * this._CANVAS.width * 0.009, 0);
            this._CONTEXT.strokeStyle = "rgb(26, 156, 195)";
            this._CONTEXT.stroke();

            this._CONTEXT.restore();
          }
        });
      }

      var xc = w / 2 + this.clickpose.x / r + x;
      var yc = h / 2 - this.clickpose.y / r + y;

      this.xtest = (xc - x) * r;
      this.ytest = -((yc - y - h) * r);

      this._CONTEXT.beginPath();

      this._CONTEXT.arc(xc, yc, this._CANVAS.width * 0.009, 0, 2 * Math.PI);
      this._CONTEXT.lineWidth = 1;
      this._CONTEXT.fillStyle = "#32CD32";
      this._CONTEXT.fill();

      this._CONTEXT.beginPath();

      this._CONTEXT.save();

      this._CONTEXT.translate(
        this.api.localization_status.positionx / this.resomap + this.offsetX,
        this._CANVAS.height -
        this.api.localization_status.positiony / this.resomap +
        this.offsetY
      );
      this._CONTEXT.rotate(-this.api.localization_status.positiont);
      this._CONTEXT.moveTo(0, -15);
      this._CONTEXT.lineTo(15, 0);
      this._CONTEXT.lineTo(0, 15);
      this._CONTEXT.lineTo(-10, 15);
      this._CONTEXT.lineTo(-10, -15);
      this._CONTEXT.lineTo(0, -15);

      this._CONTEXT.closePath();

      this._CONTEXT.lineWidth = 2;
      this._CONTEXT.fill("nonzero");
      this._CONTEXT.stroke();

      this._CONTEXT.restore();

      if (this.api.navigation_status) {

        if (this.api.navigation_status.status == 1) {
          console.log("trajectoire reach");
          var traj = this.api.navigation_status.trajectory;
          traj.forEach((evenement) => {
            this._CONTEXT.beginPath();
            this._CONTEXT.moveTo(
              evenement.Start.X / this.resomap + this.offsetX,
              this._CANVAS.height -
              (evenement.Start.Y / this.resomap - this.offsetY)
            );
            this._CONTEXT.lineTo(
              evenement.End.X / this.resomap + this.offsetX,
              this._CANVAS.height -
              (evenement.End.Y / this.resomap - this.offsetY)
            );
            this._CONTEXT.strokeStyle = "#32CD32";
            this._CONTEXT.stroke();
          });
        }
      }

    }

  }

  on_click_relocate(ev) {
    ev.preventDefault();
    if (!this.reloc) {
      this.alert.localisemanually(this.api.mailAddInformationBasic());
      this.reloc = true;
      this.api.putLocalization(this.xtest, this.ytest, 0);
    } else {
      this.reloc = false;
    }

  }

  on_click_relocate_left(ev) {
    ev.preventDefault();
    this.api.putLocalization(
      this.api.localization_status.positionx,
      this.api.localization_status.positiony,
      (this.api.localization_status.positiont + 0.46) % (2 * Math.PI)
    );

  }

  on_click_relocate_right(ev) {
    ev.preventDefault();
    this.api.putLocalization(
      this.api.localization_status.positionx,
      this.api.localization_status.positiony,
      (this.api.localization_status.positiont - 0.46) % (2 * Math.PI)
    );

  }

  getImageFromService() {
    this.imge = document.getElementById("imgmap4") as HTMLImageElement;
    if (this.api.current_map) {
      this.offsetX = this.api.mapdata.Offset.X;
      this.offsetY = this.api.mapdata.Offset.Y;
      this.resomap = this.api.mapdata.Resolution;
      this.initialzoom = this.resomap;
    }

    this.api.getImgMapHttp().subscribe(
      (data) => {
        //console.log(data);
        this.api.imgMap = data;
        console.log("GetImageFromService");
        this.createImageFromBlob();
      },
      (error) => {
        this.getImageFromService();
        console.log(error);
      }
    );
  }

  on_click_goToPose(ev) {
    ev.preventDefault();
    this.alert.reachgreenpoint(this.api.mailAddInformationBasic());
    var p = {
      X: this.xtest,
      Y: this.ytest,
      T: 0,
    };
    this.api.reach_location(p, false, 0.8, false);
  }

  openfileBrowser(ev) {
    document.getElementById('mapBrowserFileInput').click();
  }

  openfile(ev: Event) {
    ev.preventDefault();
    this.api.mapok = true;
  
    const file = (ev.target as HTMLInputElement).files[0];
    const reader = new FileReader();
  
    reader.onloadend = (evt) => {
      try {
        
        const result = evt.target;
        const byteString = atob((<string>result["result"]).split(",")[1]);
        const ab = new ArrayBuffer(byteString.length);
        const ia = new Uint8Array(ab);
  
        for (let i = 0; i < byteString.length; i++) {
          ia[i] = byteString.charCodeAt(i);
        }
  
        this.b = new Blob([ab], { type: "image/png" });
        this.loadimg = true;
      } catch (error) {
        console.error("Error reading file:", error);
      }
    };
  
    if (file) {
      this.filen = file.name;
      reader.readAsDataURL(file);
    }
  }

  onSliderRelease(ev, id: Select) {
    ev.preventDefault();
    id.open();
  }

  startCamera(cs) {
    if (!!(navigator.mediaDevices && navigator.mediaDevices.getUserMedia)) {
      // recupération du flux avec les constraints configurés puis on attache le flux à la balise video
      navigator.mediaDevices
        .getUserMedia(cs)
        .then(this.attachVideo.bind(this))
        .catch(this.handleError);
    } else {
      alert("Sorry, camera not available.");
    }
  }

  handleError(error) {
    console.log("Error: ", error);
  }

  // function qui attache le flux video à la balise video
  attachVideo(stream) {
    this.renderer.setProperty(
      this.videoElement.nativeElement,
      "srcObject",
      stream
    );
    this.renderer.listen(this.videoElement.nativeElement, "play", (event) => {
      this.videoHeight = this.videoElement.nativeElement.videoHeight;
      this.videoWidth = this.videoElement.nativeElement.videoWidth;
    });
  }

}
