// the parameter script
//import paramdata from "../parameter/robot_parameter.json";
import frtext from "../langage/fr-FR.json";
import entext from "../langage/en-GB.json";
import estext from "../langage/es-ES.json";
import detext from "../langage/de-DE.json";
import ittext from "../langage/it-IT.json";
import grtext from "../langage/el-GR.json";
import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { DomSanitizer } from "@angular/platform-browser";

@Injectable()
export class ParamService {
  localhost: string;
  robotmail: string;
  datamail: string;
  langage: string;
  datamaillist: string[];
  maillist: string[];
  serialnumber: string;
  mailrobotpassw: string;
  maildatapassw: string;
  allowspeech: number;
  name: string;
  allowskip: boolean;
  datatext: any;
  robot: any;
  duration: any;
  tableduration: any = {};
  tablerobot: any = {};
  qrcodes: any = {};
  tableqr: any = {};
  date: any;
  battery: any;
  currentduration: any = {};
  cpt: number;
  sendduration: boolean;
  tableparamuv: any = {};
  paramuv: any;
  source: any;
  durationNS:any={};
  tabledurationNS: any = {};
  liftip ="192.168.1.2:8000";

  constructor(private httpClient: HttpClient, public sanitizer: DomSanitizer) {
    this.cpt = 0;
    this.sendduration = false;
  }

  getDataRobot() {
    this.httpClient.get("http://localhost/ionicDB/getrobot.php").subscribe(
      (data) => {
        this.robot = data[0];
        this.langage = this.robot.langage;
        if (this.langage === "fr-FR") {
          this.datatext = frtext;
        } else if (this.langage === "en-GB") {
          this.datatext = entext;
        } else if (this.langage === "es-ES") {
          this.datatext = estext;
        }
        else if (this.langage === "de-DE") {
          this.datatext = detext;
        }else if(this.langage === "el-GR"){
          this.datatext = grtext;
        }else if (this.langage === "it-IT") {
          this.datatext = ittext;
        }
        this.source = this.sanitizer.bypassSecurityTrustResourceUrl(this.datatext.URL_toolbox);
      },
      (err) => {
        console.log(err);
      }
    );
  }

  getQR() {
    this.httpClient.get("http://localhost/ionicDB/getqrcodes.php").subscribe(
      (data) => {
        this.qrcodes = data;
      },
      (err) => {
        console.log(err);
      }
    );
  }

  addQR(qr: string, map: number, poi: number, round: number) {
    this.tableqr.action = "insert";
    this.tableqr.qr = qr;
    this.tableqr.id_map = map;
    this.tableqr.id_poi = poi;
    this.tableqr.id_round = round;

    this.httpClient
      .post(
        "http://localhost/ionicDB/addqrcode.php",
        JSON.stringify(this.tableqr)
      )
      .subscribe(
        (data) => {
          console.log(data);
        },
        (err) => {
          console.log(err);
        }
      );
  }

  deleteQR(qr: string) {
    this.tableqr.action = "delete";
    this.tableqr.qr = qr;
    this.httpClient
      .post(
        "http://localhost/ionicDB/deleteqrcode.php",
        JSON.stringify(this.tableqr)
      )
      .subscribe(
        (data) => {
          console.log(data);
        },
        (err) => {
          console.log(err);
        }
      );
  }



  addDuration() {
    this.tableduration.action = "insert";
    this.tableduration.serialnumber = this.serialnumber;
    this.tableduration.round = this.currentduration.round;
    this.tableduration.battery = this.currentduration.battery;
    this.tableduration.walk = this.currentduration.walk;
    this.tableduration.patrol = this.currentduration.patrol;
    this.tableduration.date = this.currentduration.date;
    this.httpClient
      .post(
        "http://localhost/ionicDB/addduration.php",
        JSON.stringify(this.tableduration)
      )
      .subscribe(
        (data) => {
          //console.log(data);
        },
        (err) => {
          console.log(err);
        }
      );
  }

  getDurationNS(){
    this.httpClient.get("http://localhost/ionicDB/duration/getduration.php").subscribe(
      (data) => {
        
        this.duration=data;
        this.durationNS = data;
        //console.log(data);
        //console.log(this.durationNS.length);
      },
      (err) => {
        console.log(err);
      }
    );
  }

  updateDurationNS(id:number){
    

    this.tabledurationNS.action="update";
    this.tabledurationNS.id_duration=id;
    this.httpClient
      .post(
        "http://localhost/ionicDB/duration/updateduration.php",
        JSON.stringify( this.tabledurationNS)
      )
      .subscribe(
        (data) => {
          console.log(data);
        },
        (err) => {
          console.log(err);
        }
      );
  }

  getBattery() {
    this.httpClient.get("http://localhost/ionicDB/getbattery.php").subscribe(
      (data) => {
        this.battery = data[0];
      },
      (err) => {
        console.log(err);
      }
    );
  }
  updateRobot() {
    this.tablerobot.action = "update";
    this.tablerobot.langage = this.robot.langage;
    this.tablerobot.serialnumber = this.serialnumber;
    this.tablerobot.name = this.name;
    this.tablerobot.send_pic = this.robot.send_pic;
    this.tablerobot.password = this.robot.password;
    this.httpClient
      .post(
        "http://localhost/ionicDB/updaterobotpassword.php",
        JSON.stringify(this.tablerobot)
      )
      .subscribe(
        (data) => {
          //console.log(data);
        },
        (err) => {
          console.log(err);
        }
      );
  }

  updateDuration() {
    this.tableduration.action = "update";
    this.tableduration.toolbox = this.currentduration.toolbox;
    this.tableduration.date = this.currentduration.date;
    this.httpClient
      .post(
        "http://localhost/ionicDB/duration/updatedurationtoolbox.php",
        JSON.stringify(this.tableduration)
      )
      .subscribe(
        (data) => {
          console.log(data);
        },
        (err) => {
          console.log(err);
        }
      );
  }


  updateUVparam() {
    this.tableparamuv.action = "update";
    this.tableparamuv.preheating_time = this.paramuv.preheating_time;
    this.tableparamuv.evacuation_time = this.paramuv.evacuation_time;
    this.tableparamuv.battery_threshold = this.paramuv.battery_threshold;
    this.tableparamuv.uv_threshold = this.paramuv.uv_threshold;
    this.tableparamuv.lamp_threshold = this.paramuv.lamp_threshold;
    this.tableparamuv.person_detection = this.paramuv.person_detection;
    this.httpClient
      .post(
        "http://localhost/ionicDB/updateparamuv.php",
        JSON.stringify(this.tableparamuv)
      )
      .subscribe(
        (data) => {
          console.log(data);
        },
        (err) => {
          console.log(err);
        }
      );
  }

  getUVparam() {
    this.httpClient.get("http://localhost/ionicDB/getparamuv.php").subscribe(
      (data) => {
        this.paramuv = data[0];
        //console.log(this.paramuv);
      },
      (err) => {
        console.log(err);
      }
    );
  }

  fillData() {
    this.allowspeech = this.robot.allowspeech;
    this.datamail = this.robot.maildata;
    this.maildatapassw = atob(this.robot.maildatapass);
    this.mailrobotpassw = atob(this.robot.mailrobotpass);
    this.localhost = this.robot.localhost;
    this.serialnumber = this.robot.serialnumber;
    this.robotmail = this.robot.mailrobot;
    this.datamail = this.robot.maildata;
    this.datamaillist = ["data@kompai.com"];
    this.allowskip = this.robot.allowskip;


    if (this.duration.length > 0) {
      if (
        this.duration[this.duration.length - 1].date ===
        new Date().toLocaleDateString("fr-CA")
      ) {
        this.currentduration.date = this.duration[this.duration.length - 1].date;
        this.currentduration.round = this.duration[
          this.duration.length - 1
        ].round;
        this.currentduration.battery = this.duration[
          this.duration.length - 1
        ].battery;
        this.currentduration.patrol = this.duration[
          this.duration.length - 1
        ].patrol;
        this.currentduration.walk = this.duration[this.duration.length - 1].walk;
        this.currentduration.toolbox = this.duration[this.duration.length - 1].toolbox;
        this.currentduration.logistic = this.duration[this.duration.length - 1].logistic;
      } else {
        if(!this.sendduration){
          this.sendduration = true;
          this.init_currentduration();
          this.addDuration();
        }
        
      }
    } else {

      if(!this.sendduration){
        this.sendduration = true;
        this.init_currentduration();
        this.addDuration();
      }

    }
  }

  cptDuration() {
    if (this.currentduration.date === new Date().toLocaleDateString("fr-CA")) {
      this.cpt = parseInt(this.currentduration.toolbox);
      this.currentduration.toolbox = this.cpt + 2;
      this.updateDuration();
    } else {
      this.init_currentduration();
      this.addDuration();
    }
  }

  init_currentduration(){
    this.currentduration.round = 0;
    this.currentduration.battery = 0;
    this.currentduration.patrol = 0;
    this.currentduration.walk = 0;
    this.currentduration.toolbox = 0;
    this.currentduration.logistic = 0;
    this.currentduration.date = new Date().toLocaleDateString("fr-CA");
  }
}
