// Allows Http communication with komnav
// cf Marc komnav_http_manual for more information
// https://drive.google.com/drive/u/3/folders/1-18kyKv6Ep-fzPU1F-dp0tQ1r8usyuZ7

import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable, OnInit, OnDestroy } from "@angular/core";
import { Subscription } from "rxjs";
import { App } from "ionic-angular";
import { first } from "rxjs/operators";
import { AlertService } from "./alert.service";
import { ParamService } from "./param.service";
import { ToastController } from "ionic-angular";
import { Observable } from "rxjs/Observable";
declare var ROSLIB: any;


export class Lift {
  floors: number;
  currentFloor: number;
  targetFloor: number;
  isBusy: boolean;
  isOpened: boolean;
  id: number;
}

export class Battery {
  autonomy: number; //the estimated remaining operation time, in minutes.
  current: number; //the actual current drawn from the battery, in Amps.
  remaining: number; //the percentage of energy remaining in the battery, range 0 - 100 %.
  status: number; // Charging 0 Chrged 1 Ok 2 Critical 3
  timestamp: number;
  voltage: number; // the actual battery voltage in Volts.
}

export class Statistics {
  timestamp: number;
  totalTime: number;
  totalDistance: number;
}

export class Anticollision {
  timestamp: number;
  enabled: boolean;
  locked: boolean;
  forward: number; // 0 ok , 1 speed low , 2 obstacle
  reverse: number;
  left: number;
  right: number;
}

export class Iostate {
  // receive robot's boutons input
  timestamp: number;
  dIn: any;
  aIn: any;
}

export class Docking {
  status: number;
  detected: boolean;
}
/*
{ Docking status
  "Unknown    ",
  "Undocked   ",
  "Docking    ",
  "Undocking  ",
  "Error      "
};*/

export class Trajectory {
  status: number;
}
//0 disabled
//1 waiting
//3 following

export class Navigation {
  avoided: number;
  status: number;
  trajectory:any;
}

// The status can have the following values:
// • 0 - Waiting: the robot is ready for a new destination speciﬁed with a PUT /navigation/destination request.
// • 1 - Following: the robot is following the trajectory computed to join the destination.
// • 2 - Aiming: the robot has completed the trajectory and is now rotating to aim the destination accurately
// • 3 - Translating: the robot has ﬁnished aiming and is now translating to reach the destination
// • 4 - Rotating: this is the last rotation, executed only if the destination was speciﬁed with “Rotate” : true
// • 5-Error: indicatesthattherobothasencounteredanerrorthatpreventedittojointherequireddestination.
// This can be cleared by sending a new destination.

export class Differential {
  status: number;

  CurrentLinearSpeed: number;

  CurrentAngularSpeed: number;
}

export class Location {
  id: number;
}

export class RoundPOI {
  location: Location;
  name: string;
  wait: number;
  speed: number;
  data: string;
  anticollisionProfile: number;
  disableAvoidance: boolean;
}

export class Round {
  round: any;
  acknowledge: boolean; // send to the next poi
  abort: boolean; // stop round
  pause: boolean; // pause the round
  status: number; //0none   1ready to be executed  2moving  3paused  4onPOI  5Completed
}

export class Localization {
  positionx: number;
  positiony: number;
  positiont: number;
}

export interface Offset {
  X: any;
  Y: any;
}

export class Pose {
  x: number;
  y: number;
  t: number;
}

export interface Localisation {
  timestamp: any;
  pose: Pose;
  currentStatus: any;
  sx: any;
  sy: any;
}

export class Poi {
  id: number;
  map: number;
  name: String;
  label: String;
  pose: Pose;
}

export interface MapProp {
  id: number;
  width: number;
  height: number;
  resolution: any;
  offset: Offset;
  name: String;
  data: any;
}

@Injectable()
export class ApiService implements OnInit, OnDestroy {
  batteryremaining: number;
  batterySubscription: Subscription;
  firstopen: boolean = true;
  battery_status: Battery;
  battery_socket: WebSocket;

  lift_status: Lift;
  lift_socket: WebSocket;

  differential_status: Differential;
  differential_socket: WebSocket;

  round_status: Round;
  round_socket: WebSocket;

  anticollision_status: Anticollision;
  anticollision_socket: WebSocket;

  statistics_socket: WebSocket;
  statistics_status: Statistics;

  io_socket: WebSocket;
  io_status: Iostate;

  docking_socket: WebSocket;
  docking_status: Docking;

  navigation_status: Navigation;
  navigation_socket: WebSocket;

  trajectory_status: Trajectory;
  trajectory_socket: WebSocket;

  localization_status: Localization;
  localization_socket: WebSocket;

  hourStart: Date;
  hour: any;
  start: boolean;
  b_pressed: boolean;
  btnPush: boolean;
  towardDocking: boolean;
  is_connected: boolean;
  is_blocked: boolean;
  is_background: boolean;
  is_localized: boolean;
  roundActive: boolean;
  cpt_jetsonOK: number;
  robotok: boolean;
  wifiok: boolean;
  batteryState: number;
  statusRobot: number; //0 ok,  1 jetson without internet,  2 no connexion with jetson
  //mapLoaded:boolean;
  close_app: boolean;
  fct_onGo: boolean;
  fct_startRound: boolean;
  appOpened: boolean;
  is_high: boolean;
  id_current_round: number;
  id_current_map: number;
  name_current_map: number;
  mapdata: any;
  rounddata: any;
  round_current_map: any;
  copy_round: any;
  all_maps: any;
  all_locations: any;
  copyRound: boolean;
  editRound: boolean;
  roundpoi_list: any = [];
  imgMap: any;
  towardPOI: boolean;
  mapok: boolean;
  textHeadBand: string;
  mapissue: boolean;
  smgthCreated: boolean;
  socketok: boolean = false;
  ros: any;
  listener_qr: any;
  qrdetected: any;
  listener_camera: any;
  listener_cameraf: any;
  QRselected: any;
  firstlocation: any;
  locationdata: any;
  adminMode: boolean = false;
  elevator: boolean = false;
  current_map:number;
  moovingchimio:boolean=false;
  moovingpharma:boolean=false;

  httpskomnav: string;
  wsskomnav: string;
  httpsros: string;
  wssros: string;

  slabel:boolean=true; // to display the selecteur label when add a new poi
  soptiondesinfection :boolean=false;


  constructor(
    public toastCtrl: ToastController,
    private httpClient: HttpClient,
    public app: App,
    public alert: AlertService,
    public param: ParamService
  ) {
    this.is_connected = false; // to know if we are connected to the robot
    this.is_background = false;
    //this.mapLoaded=false;
    this.is_localized = false;
    this.roundActive = false; // to know if the round is in process
    this.towardDocking = false; //to know if the robot is moving toward the docking
    this.start = false;
    this.fct_startRound = false;
    this.fct_onGo = false;
    this.statusRobot = 0;
    this.cpt_jetsonOK = 0;
    this.appOpened = false;
    this.close_app = false;
    this.is_blocked = false;
    this.is_high = false;
    this.copyRound = false;
    this.editRound = false;
    this.towardPOI = false;
    this.mapok = true;
    this.robotok = false;
    this.mapissue = false;
    this.smgthCreated = false;
  }

  instanciate() {
    this.socketok = true;
    var apiserv = this;
    ////////////////////// localization socket

    this.localization_status = new Localization();

    this.localization_socket = new WebSocket(
      this.wsskomnav + this.param.localhost + "/api/localization/socket",
      "json"
    );

    this.localization_socket.onerror = function (event) {
      console.log("error localization socket");
    };

    this.localization_socket.onopen = function (event) {
      console.log("localization socket opened");

      this.onmessage = function (event) {
        var test = JSON.parse(event.data);
        apiserv.localization_status.positionx = test["Pose"]["X"];
        apiserv.localization_status.positiony = test["Pose"]["Y"];
        apiserv.localization_status.positiont = test["Pose"]["T"];
      };

      this.onclose = function () {
        console.log("localization socket closed");
      };
    };

    ////////////////////// docking socket

    this.docking_status = new Docking();

    this.docking_socket = new WebSocket(
      this.wsskomnav + this.param.localhost + "/api/docking/socket",
      "json"
    );

    this.docking_socket.onerror = function (event) {
      console.log("error docking socket");
    };

    this.docking_socket.onopen = function (event) {
      console.log("docking socket opened");

      this.onmessage = function (event) {
        var test = JSON.parse(event.data);
        apiserv.docking_status.status = test["Status"];
        apiserv.docking_status.detected = test["Detected"];
      };

      this.onclose = function () {
        console.log("docking socket closed");
      };
    };
    ////////////////////// differential socket

    this.differential_status = new Differential();

    this.differential_socket = new WebSocket(
      this.wsskomnav + this.param.localhost + "/api/differential/socket",
      "json"
    );

    this.differential_socket.onerror = function (event) {
      console.log("error differential socket");
    };

    this.differential_socket.onopen = function (event) {
      console.log("differential socket opened");

      this.onmessage = function (event) {
        var test = JSON.parse(event.data);
        apiserv.differential_status.status = test["Status"];
        apiserv.differential_status.CurrentAngularSpeed = test["CurrentAngularSpeed"];
        apiserv.differential_status.CurrentLinearSpeed = test["CurrentLinearSpeed"];

      };

      this.onclose = function () {
        console.log("differential socket closed");
      };
    };
 ////////////////////// trajectory socket

 this.trajectory_status = new Trajectory();

 this.trajectory_socket = new WebSocket(
   this.wsskomnav + this.param.localhost + "/api/trajectory/socket",
   "json"
 );

 this.trajectory_socket.onerror = function (event) {
   console.log("error trajectory socket");
 };

 this.trajectory_socket.onopen = function (event) {
   console.log("trajectory socket opened");

   this.onmessage = function (event) {
     var test = JSON.parse(event.data);
     apiserv.trajectory_status.status = test["Status"];
   

   }
  

   this.onclose = function () {
     console.log("trajectory socket closed");
   };
 };
    ////////////////////// navigation socket

    this.navigation_status = new Navigation();

    this.navigation_socket = new WebSocket(
      this.wsskomnav + this.param.localhost + "/api/navigation/socket",
      "json"
    );

    this.navigation_socket.onerror = function (event) {
      console.log("error navigation socket");
    };

    this.navigation_socket.onopen = function (event) {
      console.log("navigation socket opened");

      this.onmessage = function (event) {
        var test = JSON.parse(event.data);
        apiserv.navigation_status.status = test["Status"];
        apiserv.navigation_status.avoided = test["Avoided"];
        apiserv.navigation_status.trajectory = test["Trajectory"];

        if (
          apiserv.navigation_status.status === 5 ||
          apiserv.navigation_status.status === 0
        ) {
          //smart button and button A of the game pad
          apiserv.towardPOI = false;
          apiserv.towardDocking = false;
        } else {
          apiserv.towardPOI = true;
        }
      };

      this.onclose = function () {
        console.log("navigation socket closed");
      };
    };

    ////////////////////// anticollision socket

    this.anticollision_status = new Anticollision();

    this.anticollision_socket = new WebSocket(
      this.wsskomnav + this.param.localhost + "/api/anticollision/socket",
      "json"
    );

    this.anticollision_socket.onerror = function (event) {
      console.log("error anticollision socket");
    };

    this.anticollision_socket.onopen = function (event) {
      console.log("anticollision socket opened");

      this.onmessage = function (event) {
        var test = JSON.parse(event.data);
        apiserv.anticollision_status.timestamp = test["Timestamp"];
        apiserv.anticollision_status.enabled = test["Enabled"];
        apiserv.anticollision_status.locked = test["Locked"];
        apiserv.anticollision_status.forward = test["Forward"];
        apiserv.anticollision_status.right = test["Right"];
        apiserv.anticollision_status.left = test["Left"];
      };

      this.onclose = function () {
        console.log("anticollision socket closed");
      };
    };

    ////////////////////// statistics socket

    this.statistics_status = new Statistics();

    this.statistics_socket = new WebSocket(
      this.wsskomnav + this.param.localhost + "/api/statistics/socket",
      "json"
    );

    this.statistics_socket.onerror = function (event) {
      console.log("error statistic socket");
    };

    this.statistics_socket.onopen = function (event) {
      console.log("statistic socket opened");

      this.onmessage = function (event) {
        var test = JSON.parse(event.data);
        apiserv.statistics_status.totalTime = test["TotalTime"];
        apiserv.statistics_status.totalDistance = test["TotalDistance"];
        apiserv.statistics_status.timestamp = test["Timestamp"];
      };

      this.onclose = function () {
        console.log("statistic socket closed");
      };
    };

    ///////////////////////// battery socket
    this.battery_status = new Battery();

    this.battery_socket = new WebSocket(
      this.wsskomnav + this.param.localhost + "/api/battery/socket",
      "json"
    );

    this.battery_socket.onerror = function (event) {
      console.log("error battery socket");
    };

    this.battery_socket.onopen = function (event) {
      console.log("battery socket opened");

      this.onmessage = function (event) {
        var test = JSON.parse(event.data);
        apiserv.battery_status.autonomy = test["Autonomy"];
        apiserv.battery_status.current = test["Current"];
        apiserv.battery_status.remaining = test["Remaining"];
        apiserv.battery_status.status = test["Status"];
        apiserv.battery_status.timestamp = test["Timestamp"];
        apiserv.battery_status.voltage = test["Voltage"];
      };

      this.onclose = function () {
        console.log("battery socket closed");
      };
    };


    ///////////////////////// lift socket
    if (this.elevator) {
      this.lift_status = new Lift();

      this.lift_socket = new WebSocket(
        this.wsskomnav + this.param.liftip + "/api/lift/socket",
        "json"
      );

      this.lift_socket.onerror = function (event) {
        console.log("error lift socket");
      };

      this.lift_socket.onopen = function (event) {
        console.log("lift socket opened");

        this.onmessage = function (event) {
          var test = JSON.parse(event.data);
          apiserv.lift_status.floors = test["Floors"];
          apiserv.lift_status.currentFloor = test["CurrentFloor"];
          apiserv.lift_status.targetFloor = test["TargetFloor"];
          apiserv.lift_status.isBusy = test["IsBusy"];
          apiserv.lift_status.isOpened = test["IsOpened"];
          apiserv.lift_status.id = test["Id"];
        };

        this.onclose = function () {
          console.log("lift socket closed");
        };
      };
    }



    //////////////////////// round socket
    // this.round_status = new Round();

    // this.round_socket = new WebSocket(this.wsskomnav+this.param.localhost+"/api/rounds/socket", "json");

    // this.round_socket.onerror = function(event)
    // {
    //     console.log("error round socket");
    // };

    // this.round_socket.onopen = function(event)
    // {
    //     console.log("round socket opened");

    //     this.onmessage = function(event)
    //     {
    //       var test = JSON.parse(event.data);
    //       apiserv.round_status.round=test['Round'];
    //       apiserv.round_status.acknowledge = test['Acknowledge'];
    //       apiserv.round_status.abort = test['Abort'];
    //       apiserv.round_status.pause = test['Pause'];
    //       apiserv.round_status.status = test['Status'];
    //     }

    //     this.onclose = function()
    //     {
    //         console.log("round socket closed");
    //     }
    // };

    //////////////////////////////// io socket
    this.b_pressed = false;
    this.btnPush = false;
    this.io_status = new Iostate();
    this.io_socket = new WebSocket(
      this.wsskomnav + this.param.localhost + "/api/io/socket",
      "json"
    );
    this.io_socket.onerror = function (event) {
      console.log("error iosocket");
    };

    this.io_socket.onopen = function (event) {
      console.log("io socket opened");

      this.onclose = function () {
        console.log("io socket closed");
      };
    };

    this.io_socket.onmessage = function (event) {
      var test = JSON.parse(event.data);
      apiserv.io_status.timestamp = test["Timestamp"];
      apiserv.io_status.dIn = test["DIn"];
      apiserv.io_status.aIn = test["AIn"];

      if (
        apiserv.io_status.dIn[2] ||
        apiserv.io_status.dIn[3] ||
        apiserv.io_status.dIn[8]
      ) {
        //smart button and button A of the game pad
        if (!apiserv.b_pressed) {
          apiserv.btnPush = true;
          apiserv.b_pressed = true;
          apiserv.abortNavHttp();
        }
      } else {
        if (apiserv.b_pressed) {
          apiserv.b_pressed = false;
        }
      }
    };

    //////////////////////////////// ROS TOPIC connection
    this.ros = new ROSLIB.Ros({
      url: this.wssros + this.param.robot.rosip
    });

    this.ros.on('connection', function () {
      console.log('Connected to ros websocket server.');
    });

    this.ros.on('error', function (error) {
      console.log('Error connecting to ros websocket server: ', error);
    });

    this.ros.on('close', function () {
      console.log('Connection to ros websocket server closed.');
    });

    //////////////////////////////// ROS TOPIC qrcode
    this.listener_qr = new ROSLIB.Topic({
      ros: this.ros,
      name: '/barcode',
      messageType: 'std_msgs/String'
    });
    //////////////////////////////// ROS TOPIC cam
    this.listener_camera = new ROSLIB.Topic({
      ros: this.ros,
      //name: '/top_webcam/image_raw/compressed',
      //name : '/fisheye_cam/image_raw',
      //name : '/D435_camera_FWD/color/image_raw',
      name : '/fisheye_cam/compressed',

      messageType: 'sensor_msgs/CompressedImage'
    });
    this.listener_cameraf = new ROSLIB.Topic({
      ros : this.ros,
      //name : '/top_webcam/image_raw',
      name : '/fisheye_cam/compressed',
      //name : '/D435_camera_FWD/color/image_raw',

      messageType : 'sensor_msgs/CompressedImage'
    });
  }

  ngOnInit() { }

  ngOnDestroy() {
    //this.batterySubscription.unsubscribe();
  }

  mailAddInformationBasic() {

    var now = new Date().toLocaleString("en-GB", {
      day: "numeric",
      month: "numeric",
      year: "numeric",
      hour: "numeric",
      minute: "numeric",
      second: "numeric",
    });
    return (
      "<br> Time : " +
      now +
      "<br> Serial number : " +
      this.param.serialnumber +
      "<br> Battery remaining: " +
      this.battery_status.remaining +
      "%" +
      "<br> Battery state : " +
      this.battery_status.status +
      "<br> Battery voltage : " +
      this.battery_status.voltage +
      "<br> Battery current : " +
      this.battery_status.current +
      "<br> Battery autonomy : " +
      this.battery_status.autonomy +
      "<br> Docking state : " +
      this.docking_status.status +
      "<br> Status Forward : " +
      this.anticollision_status.forward +
      "<br> Status Right : " +
      this.anticollision_status.right +
      "<br> Status Left : " +
      this.anticollision_status.left +
      "<br> Status Reverse : " +
      this.anticollision_status.reverse +
      "<br> Navigation state : " +
      this.navigation_status.status +
      "<br> Differential state : " +
      this.differential_status.status +
      "<br> Odometer : " +
      this.statistics_status.totalDistance +
      "<br> Position X : " +
      this.localization_status.positionx +
      "<br> Position Y : " +
      this.localization_status.positiony +
      "<br> Position T : " +
      this.localization_status.positiont +
      "<br> Application : 6" +
      "<br>"
    );
  }

  joystickHttp(lin, rad) {
    var cmd = { Enable: true, TargetLinearSpeed: lin, TargetAngularSpeed: rad };

    let body = JSON.stringify(cmd);
    let headers = new HttpHeaders({ "Content-Type": "application/json" });
    let options = { headers: headers };
    return new Promise((resolve) => {
      this.httpClient
        .put(
          this.httpskomnav + this.param.localhost + "/api/differential/command",
          body,
          options
        )
        .subscribe(
          (resp) => {
            console.log(resp);
            console.log("joystickOK");
          },
          (error) => {
            console.log(error);
            console.log("joystickPBM");
          }
        );
    });
  }

  jetsonOK() {
    this.checkInternet();
    this.alert.checkwifi(this.wifiok);
    this.httpClient
      .get(this.httpskomnav + this.param.localhost + "/api/battery/state", {
        observe: "response",
      })
      .pipe(first())
      .subscribe(
        (resp) => {
          if (this.statusRobot === 2) {
            this.alert.appError(this.mailAddInformationBasic());
            document.location.reload(); //if error then refresh the page and relaunch websocket
          }
          if (resp.status === 200) {
            this.cpt_jetsonOK = 0;
            this.statusRobot = 0; // everything is ok
            this.connect();
          } else {
            this.cpt_jetsonOK += 1;
            if (this.cpt_jetsonOK > 5) {
              this.statusRobot = 2; //no connection
              this.connectionLost();
            }
          }
        },
        (err) => {
          console.log(err); //no conection
          this.cpt_jetsonOK += 1;
          if (this.cpt_jetsonOK > 10) {
            this.statusRobot = 2;
            this.connectionLost();
          }
        }
      );
  }

  okToast(m: string) {
    const toast = this.toastCtrl.create({
      message: m,
      duration: 3000,
      position: "middle",
      cssClass: "toastok",
    });
    toast.present();
  }

  async isConnectedInternet(): Promise<boolean> {
    try {
      const response = await fetch('http://localhost/ionicDB/internetping.php');
      const text = await response.text(); // Récupère le contenu texte de la réponse
      //console.log(text);
      
      // Analyse du texte pour déterminer la connectivité Internet
      return text.trim() === 'true'; // Renvoie true si le texte est 'true', sinon false
      
    } catch (error) {
      console.error('Error checking internet connectivity:', error);
      return false;
    }
  }

  checkInternet() {
    this.isConnectedInternet().then(connecte => {
      if (connecte) {
        //console.log("L'ordinateur est connecté à Internet");
        this.wifiok = true;
      } else {
        //console.log("L'ordinateur n'est pas connecté à Internet");
        this.wifiok = false;
      }
    });

  }

  checkrobot() {
    this.httpClient
      .get(this.httpskomnav + this.param.localhost + "/api/battery/state")
      .subscribe(
        (data) => {
          this.robotok = true;
        },
        (err) => {
          console.log(err);
          this.robotok = false;
        }
      );
  }

  createRoundList(
    id: number,
    name: string,
    wait: number,
    speed: number,
    data: string,
    anticollisionProfile: number,
    disableAvoidance: boolean
  ) {
    var poiRound = new RoundPOI();
    var location = new Location();
    location.id = id;
    poiRound.location = location;
    poiRound.name = name;
    poiRound.wait = wait;
    poiRound.speed = speed;
    poiRound.data = data;
    poiRound.anticollisionProfile = anticollisionProfile;
    poiRound.disableAvoidance = disableAvoidance;

    this.roundpoi_list.push(poiRound);
  }

  connectHttp() {
    this.httpClient
      .put(this.httpskomnav + this.param.localhost + "/api/docking/connect", {
        observe: "response",
      })
      .pipe(first())
      .subscribe(
        (resp) => {
          console.log(resp);
          console.log("connectOK");
        },
        (err) => {
          console.log(err);
          console.log("connectPBM");
        }
      );
  }

  reachHttp(poiname: string) {
    this.httpClient
      .put(
        this.httpskomnav +
        this.param.localhost +
        "/api/navigation/destination/name/" +
        poiname,
        { observe: "response" }
      )
      .pipe(first())
      .subscribe(
        (resp) => {
          console.log(resp);
          console.log("reachOK");
        },
        (err) => {
          console.log(err);
          console.log("reachPBM");
        }
      );
  }

  abortNavHttp() {
    // stop the navigation
    this.httpClient
      .put(this.httpskomnav + this.param.localhost + "/api/navigation/abort", {
        observe: "response",
      })
      .pipe(first())
      .subscribe(
        (resp) => {
          console.log(resp);
          console.log("abortNavOK");
        },
        (err) => {
          console.log(err);
          console.log("abortNavPBM");
        }
      );
  }

  changeMapbyIdHttp(id: number) {
    this.httpClient
      .put(this.httpskomnav + this.param.localhost + "/api/maps/current/id/" + id, {
        observe: "response",
      })
      .pipe(first())
      .subscribe(
        (resp) => {
          console.log(resp);
          console.log("changeMapOK");
        },
        (err) => {
          console.log(err);
          console.log("changeMapPBM");
        }
      );
  }

  getAllMapsHttp() {
    this.httpClient
      .get(this.httpskomnav + this.param.localhost + "/api/maps/list")
      .subscribe(
        (data) => {
          console.log(data);
          console.log("getAllMapsOK");
          this.all_maps = data;
          //console.log(this.mapdata.Id);
          // if(this.all_maps!=undefined){

          // }
        },
        (err) => {
          console.log(err);
        }
      );
  }

  getCurrentMap() {
    this.httpClient
      .get(this.httpskomnav + this.param.localhost + "/api/maps/current/properties")
      .subscribe(
        (data) => {
          //console.log(data);
          this.mapdata = data;
          //console.log(this.mapdata.Id);
          if (this.mapdata) {
            this.id_current_map = this.mapdata.Id;
            this.name_current_map = this.mapdata.Name;
          }
          console.log("get current map ok");
        },
        (err) => {
          console.log(err);
        }
      );
  }

  // pour affichage map

  getImgMapHttp(): Observable<Blob> {
    return this.httpClient.get(
      this.httpskomnav + this.param.localhost + "/api/maps/current/image",
      { responseType: "blob" }
    );
  }

  // getMapPropertiesHttp(){
  //   return this.httpClient.get<MapProp>('http://'+this.param.localhost+'/api/maps/current/properties');
  // }

  // getLocalisationHttp(){
  //   return this.httpClient.get<Localisation>('http://'+this.param.localhost+'/api/localization/state');
  // }

  // getPOIListHttp(){

  //   //console.log(this.walker_states);
  //   return this.httpClient.get<Poi[]>('http://'+this.param.localhost+'/api/maps/current/locations');
  // }


  // fin pour affichage map

  getCurrentLocations() {
    //avoir les poi de la map actuelle
    this.httpClient
      .get(this.httpskomnav + this.param.localhost + "/api/maps/current/locations")
      .subscribe(
        (data) => {
          console.log("get locations ok");
          this.all_locations = data;
          console.log(data);
        },
        (err) => {
          console.log(err);
        }
      );
  }

  getRoundList() {
    //pour avoir la liste des rondes de la map actuelle
    this.httpClient
      .get(this.httpskomnav + this.param.localhost + "/api/rounds/list")
      .subscribe(
        (data) => {
          this.rounddata = data;
          console.log(this.rounddata);
          if (this.id_current_map && this.rounddata) {
            this.round_current_map = this.rounddata.filter(
              (round) => round.Map == this.id_current_map
            );

            //console.log(this.round_current_map);
          }
        },
        (err) => {
          console.log(err);
        }
      );
  }

  connect() {
    this.is_connected = true;
  }

  connectionLost() {
    this.is_connected = false;
  }

  background() {
    this.is_background = true;
  }

  foreground() {
    this.is_background = false;
  }

  postqr(namePOI: string, x: number, y: number, t: number) {
    const body = JSON.stringify({
      Name: namePOI,
      Pose: {
        X: x,
        Y: y,
        T: t,
      },
      Label: "qr",
    });
    this.httpClient
      .post(
        this.httpskomnav + this.param.localhost + "/api/maps/current/locations",
        body
      )
      .subscribe(
        (response) => {
          console.log(response);
          console.log("create poi ok");
        },
        (err) => {
          console.log(err);
        }
      );
  }

  postPoi(namePOI: string, label:string) {
    const body = JSON.stringify({
      Name: namePOI,
      Pose: {
        X: this.localization_status.positionx,
        Y: this.localization_status.positiony,
        T: this.localization_status.positiont,
      },
      Label: label,
    });
    this.httpClient
      .post(
        this.httpskomnav + this.param.localhost + "/api/maps/current/locations",
        body
      )
      .subscribe(
        (response) => {
          console.log(response);
          console.log("create poi ok");
        },
        (err) => {
          console.log(err);
        }
      );
  }

  configPOIDocking() {
    var angle;
    if (this.localization_status.positiont >= 0)
      angle = this.localization_status.positiont - Math.PI;
    else
      angle = this.localization_status.positiont + Math.PI;
    const dock = JSON.stringify({
      Name: "docking",
      Pose: {
        X: this.localization_status.positionx + 0.6 * Math.cos(angle),
        Y: this.localization_status.positiony + 0.6 * Math.sin(angle),
        T: this.localization_status.positiont,
      },
      Label: "docking",
    });
    console.log(dock);
    this.httpClient
      .post(
        this.httpskomnav + this.param.localhost + "/api/maps/current/locations",
        dock
      )
      .subscribe(
        (response) => {
          console.log(response);
          console.log("create poi ok");
        },
        (err) => {
          console.log(err);
        }
      );
  }

  postMap(f: FormData) {
    console.log(f);

    this.httpClient
      .post(this.httpskomnav + this.param.localhost + "/api/maps/list", f)
      .subscribe(
        (response) => {
          console.log(response);
          console.log("post map ok");
          this.mapok = true;
        },
        (err) => {
          console.log(err);
          this.mapok = false;
        }
      );
  }

  putMap(f: FormData) {
    this.httpClient
      .put(
        this.httpskomnav +
        this.param.localhost +
        "/api/maps/list/" +
        this.id_current_map,
        f
      )
      .subscribe(
        (response) => {
          console.log(response);
          console.log("put map ok");
          this.mapok = true;
        },
        (err) => {
          console.log(err);
          this.mapok = false;
        }
      );
  }

  postRound(nameRound: string) {
    const body = JSON.stringify({
      name: nameRound,
      map: this.id_current_map,
      Locations: this.roundpoi_list,
    });
    console.log(body);
    this.httpClient
      .post(this.httpskomnav + this.param.localhost + "/api/rounds/list", body)
      .subscribe(
        (response) => {
          console.log(response);
          console.log("create round ok");
        },
        (err) => {
          console.log(err);
        }
      );
  }

  putRound(nameRound: string, id: number) {
    const body = JSON.stringify({
      Name: nameRound,
      Map: this.id_current_map,
      Locations: this.roundpoi_list,
    });
    console.log(body);
    this.httpClient
      .put(this.httpskomnav + this.param.localhost + "/api/rounds/list/" + id, body)
      .subscribe(
        (response) => {
          console.log(response);
          console.log("edit round " + id + "ok");
        },
        (err) => {
          console.log(err);
        }
      );
  }

  putPOI(namePoi: string, label:string, id: number, x: number, y: number, t: number) {
    const body = JSON.stringify({
      Name: namePoi,
      Pose: { X: x, Y: y, T: t },
      Label: label,
    });
    console.log(body);
    this.httpClient
      .put(
        this.httpskomnav + this.param.localhost + "/api/maps/current/locations/" + id,
        body
      )
      .pipe(first())
      .subscribe(
        (response) => {
          console.log(response);
          console.log("edit round " + id + "ok");
        },
        (err) => {
          console.log(err);
        }
      );
  }

  deleteRound(id: number) {
    this.httpClient
      .delete(this.httpskomnav + this.param.localhost + "/api/rounds/list/" + id, {
        observe: "response",
      })
      .pipe(first())
      .subscribe(
        (response) => {
          console.log(response);
          console.log("delete round " + id + "ok");
        },
        (err) => {
          console.log(err);
        }
      );
  }

  // putLocalization(x: number, y: number, t: number) {
  //   const body = JSON.stringify({ X: x, Y: y, T: t });
  //   console.log(body);
  //   this.httpClient
  //     .put(this.httpskomnav + this.param.localhost + "/api/localization/pose", body)
  //     .subscribe(
  //       (response) => {
  //         console.log(response);
  //         console.log("reloc ok");
  //       },
  //       (err) => {
  //         console.log(err);
  //       }
  //     );
  // }

  deleteMap(id: number) {
    this.httpClient
      .delete(this.httpskomnav + this.param.localhost + "/api/maps/list/" + id, {
        observe: "response",
      })
      .pipe(first())
      .subscribe(
        (response) => {
          console.log(response);
          console.log("delete map" + id + " ok");
        },
        (err) => {
          console.log(err);
        }
      );
  }

  deletePOI(id: number) {
    this.httpClient
      .delete(
        this.httpskomnav + this.param.localhost + "/api/maps/current/locations/" + id,
        { observe: "response" }
      )
      .pipe(first())
      .subscribe(
        (response) => {
          console.log(response);
          console.log("delete poi" + id + " ok");
        },
        (err) => {
          console.log(err);
        }
      );
  }

  putDestination(p: any, s: number, avoidance: boolean) {
    const body = JSON.stringify({ X: p.X, Y: p.Y, T: p.T });
    this.httpClient
      .put(
        this.httpskomnav +
        this.param.localhost +
        "/api/navigation/destination/pose?speed=" +
        s +
        "&disable_avoidance=" +
        !avoidance, body,
        { observe: "response" }
      )
      .subscribe(
        (response) => {
          console.log(response);
          console.log("reach ok");
        },
        (err) => {
          console.log(err);
        }
      );
  }

  putLocalization(x: number, y: number, t: number) {
    var test = {
      X: x,
      Y: y,
      T: t,
    };

    //var jsonstring = JSON.stringify(test);
    //this.localization_socket.send(jsonstring);

    let body = JSON.stringify(test);
    let headers = new HttpHeaders({ "Content-Type": "application/json" });
    let options = { headers: headers };
    return new Promise((resolve) => {
      this.httpClient
        .put(
          this.httpskomnav + this.param.localhost + "/api/localization/pose",
          body,
          options
        )
        .subscribe(
          (response) => {
            console.log("reloc ok");
          },
          (error) => {
            //Failed to Login.
            //alert(error.text());
            console.log(error.text());
          }
        );
    });
  }

  reach_location(
    event: any,
    rotate: boolean,
    speed: number,
    disable_avoidance: boolean
  ) {
    var data = JSON.stringify({
      X: event.X,
      Y: event.Y,
      T: event.T,
    });

    console.log(event);
    console.log(rotate);
    console.log(speed);
    console.log(disable_avoidance);

    if (rotate) {
    
      let headers = new HttpHeaders({ "Content-Type": "application/json" });
      let options = { headers: headers };
      return new Promise((resolve) => {
        this.httpClient
          .put(
            this.httpskomnav +
            this.param.localhost +
            "/api/navigation/destination/pose?speed=" +
            speed +
            "&disable_avoidance=" +
            disable_avoidance,
            data,
            options
          )
          .subscribe(
            (response) => {
              console.log("test");
            },
            (error) => {
              //Failed to Login.
              //alert(error;
              console.log(error);
            }
          );
      });
    } else {
    
      let headers = new HttpHeaders({ "Content-Type": "application/json" });
      let options = { headers: headers };
      return new Promise((resolve) => {
        this.httpClient
          .put(
            this.httpskomnav +
            this.param.localhost +
            "/api/navigation/destination/point?speed=" +
            speed +
            "&disable_avoidance=" +
            disable_avoidance,
            data,
            options
          )
          .subscribe(
            (response) => {
              console.log("test");
            },
            (error) => {
              //Failed to Login.
              //alert(error);
              console.log(error);
            }
          );
      });
      
    }
  }

  eyesHttp(id: number) {
    this.httpClient
      .put(this.httpskomnav + this.param.localhost + "/api/eyes?id=" + id, {
        observe: "response",
      })
      .pipe(first())
      .subscribe(
        (resp) => {
          console.log(resp);
          console.log("eyesOK");
        },
        (err) => {
          console.log(err);
          console.log("eyesPBM");
        }
      );
  }


  deleteEyesHttp(id: number) {
    this.httpClient
      .delete(this.httpskomnav + this.param.localhost + "/api/eyes?id=" + id, {
        observe: "response",
      })
      .pipe(first())
      .subscribe(
        (resp) => {
          console.log(resp);
          console.log("eyesOK");
        },
        (err) => {
          console.log(err);
          console.log("eyesPBM");
        }
      );
  }

  cabinethttp(numcabinet: number, open: boolean) {
    var test = {
      Index: numcabinet,
      Value: open,

    };

    let body = JSON.stringify(test);
    this.httpClient
      .put(this.httpskomnav + this.param.localhost + "/api/io/dout", body, {
        observe: "response",
      })
      .pipe(first())
      .subscribe(
        (resp) => {
          console.log(resp);
          console.log("cabinet ok num:" + numcabinet + " open: " + open);
        },
        (err) => {
          console.log(err);
          console.log("cabinet pbm num:" + numcabinet + " open: " + open);
        }
      );

  }

  startQrDetection(start: boolean) {
    var serviceQrDetection = new ROSLIB.Service({
      ros: this.ros,
      name: '/toggle_barcode_detection_node',
      serviceType: 'std_srvs/SetBool'
    });

    var request = new ROSLIB.ServiceRequest({
      data: start,

    });

    serviceQrDetection.callService(request, function (result) {
      console.log("*********service qr****************")
      console.log(result);
    });

    if (!start) {
      this.listener_qr.removeAllListeners();
    }
  }


  resetLocationHttp() {
    var pose = {
      X: this.firstlocation[0].Pose.X,
      Y: this.firstlocation[0].Pose.Y,
      T: this.firstlocation[0].Pose.T,
    };

    let body = JSON.stringify(pose);
    let headers = new HttpHeaders({ "Content-Type": "application/json" });
    let options = { headers: headers };
    return new Promise((resolve) => {
      this.httpClient
        .put(
          this.httpskomnav + this.param.localhost + "/api/localization/pose",
          body,
          options
        )
        .subscribe(
          (resp) => {
            console.log(resp);
            console.log("resetlocationOK");
          },
          (error) => {
            console.log(error);
            console.log("resetlocationPBM");
          }
        );
    });
  }

  getFirstLocationHttp() {
    this.httpClient
      .get(this.httpskomnav + this.param.localhost + "/api/maps/current/locations")
      .subscribe(
        (data) => {
          console.log(data);
          this.locationdata = data;
          if (this.locationdata) {
            this.firstlocation = this.locationdata.filter(
              (x) => x.Id == this.QRselected.id_poi
            );
          }
        },
        (err) => {
          console.log(err);
        }
      );
  }

  call_lift(x:number, y:number){
    let headers = new HttpHeaders({ "Content-Type": "application/json" });
    let options = { headers: headers };
    this.httpClient
      .post(this.httpskomnav + this.param.liftip + "/api/lift/request?source="+x+"&destination="+y,options)
      .subscribe(
        (response) => {
          console.log(response);
          console.log("call lift ok");
        },
        (err) => {
          console.log(err);
        }
      );
  }

}
