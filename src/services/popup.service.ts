// to send mail and sms alert

import { Injectable, OnInit } from "@angular/core";
import { App } from "ionic-angular";
import { AlertService } from "./alert.service";
import { AlertController, Alert ,ToastController } from "ionic-angular";
import { ApiService } from "./api.service";
import { ParamService } from "./param.service";

@Injectable()
export class PopupService implements OnInit {
  alert_blocked: Alert;
  private deleteMap: () => void;
  private deletePOI: (id) => void;
  private deleteRound: (id) => void;
  private deleteqr: (qr,poi) => void;
  private accessparam: () => void;
  private removePOIdocking: () => void;
  private configDocking: () => void;
  onSomethingHappened1(fn : () => void) {
    this.deleteMap = fn;
  }
  onSomethingHappened2(fn : () => void) {
    this.deletePOI = fn;
  }
  onSomethingHappened3(fn : () => void) {
    this.deleteRound = fn;
  }
  onSomethingHappened4(fn : () => void) {
    this.deleteqr = fn;
  }
  onSomethingHappened5(fn : () => void) {
    this.accessparam = fn;
  }
  onSomethingHappened6(fn : () => void) {
    this.removePOIdocking = fn;
  }
  onSomethingHappened7(fn : () => void) {
    this.configDocking = fn;
  }
  constructor(
    public app: App,
    public alert: AlertService,
    public param: ParamService,
    public api: ApiService,
    private alertCtrl: AlertController,
    public toastCtrl:ToastController
  ) {}

  ngOnInit() {}

  blockedAlert() {
    this.alert_blocked = this.alertCtrl.create({
      title: this.param.datatext.blockedAlert_title,
      message: this.param.datatext.blockedAlert_message,
      cssClass: "alertstyle",
      enableBackdropDismiss: true,
      buttons: [
        {
          text: this.param.datatext.btn_ok,
          handler: () => {
            console.log("Oui clicked");
          },
        },
      ],
    });
    this.alert_blocked.present();
  }

  startFailedAlert() {
    this.alert_blocked = this.alertCtrl.create({
      title: this.param.datatext.error,
      message: this.param.datatext.cantstart,
      cssClass: "alertstyle",
      enableBackdropDismiss: false,
      buttons: [
        {
          text: this.param.datatext.btn_ok,
          handler: () => {
            console.log("Oui clicked");
            window.close();
          },
        },
      ],
    });
    this.alert_blocked.present();
  }

  lostAlert() {
    this.alert_blocked = this.alertCtrl.create({
      title: this.param.datatext.lostAlert_title,
      message: this.param.datatext.lostAlert_message,
      cssClass: "alertstyle",
      enableBackdropDismiss: true,
      buttons: [
        {
          text: this.param.datatext.btn_ok,
          handler: () => {
            console.log("Oui clicked");
          },
        },
      ],
    });
    this.alert_blocked.present();
  }

  errorlaunchAlert() {
    this.alert_blocked = this.alertCtrl.create({
      title: this.param.datatext.errorlaunchAlert_title,
      message: this.param.datatext.errorlaunchAlert_message,
      cssClass: "alertstyle",
      enableBackdropDismiss: true,
      buttons: [
        {
          text: this.param.datatext.btn_ok,
          handler: () => {
            console.log("Oui clicked");
          },
        },
      ],
    });
    this.alert_blocked.present();
  }

  errorNavAlert() {
    this.alert_blocked = this.alertCtrl.create({
      title: this.param.datatext.errorNavAlert_title,
      message: this.param.datatext.errorNavAlert_message,
      cssClass: "alertstyle",
      enableBackdropDismiss: true,
      buttons: [
        {
          text: this.param.datatext.btn_ok,
          handler: () => {
            console.log("Oui clicked");
          },
        },
      ],
    });
    this.alert_blocked.present();
  }

  robotmuststayondocking() {
    // pop up if the robot is in the docking when you start the round
    this.alert_blocked = this.alertCtrl.create({
      title: this.param.datatext.robotmuststayondocking_title,
      message: this.param.datatext.robotmuststayondocking_message,
      cssClass: "alertstyle",
      enableBackdropDismiss: true,
      buttons: [
        {
          text: this.param.datatext.btn_ok,
          handler: () => {
            console.log("Oui clicked");
          },
        },
      ],
    });
    this.alert_blocked.present();
  }

  quitConfirm() {
    let alert = this.alertCtrl.create({
      title: this.param.datatext.quitConfirm_title,
      cssClass: "alertstyle",
      enableBackdropDismiss: false,
      buttons: [
        {
          text: this.param.datatext.btn_no,
          role: "cancel",
          handler: () => {
            console.log("Non clicked");
          },
        },
        {
          text: this.param.datatext.btn_yes,
          handler: () => {
            console.log("Oui clicked");
            this.api.close_app = true;
            this.alert.appClosed(this.api.mailAddInformationBasic());
            //stop the round and quit the app
            this.api.abortNavHttp();
            setTimeout(() => {
              window.close();
            }, 1000);
          },
        },
      ],
    });
    alert.present();
  }

  errorBlocked() {
    let alert = this.alertCtrl.create({
      title: this.param.datatext.errorBlocked_title,
      message: this.param.datatext.errorBlocked_message,
      cssClass: "alertstyle",
      enableBackdropDismiss: true,
      buttons: [
        {
          text: this.param.datatext.btn_ok,
          handler: () => {
            console.log("OK clicked");
          },
        },
      ],
    });
    alert.present();
  }

  statusRedPresent() {
    let alert = this.alertCtrl.create({
      title: this.param.datatext.statusRedPresent_title,
      message: this.param.datatext.statusRedPresent_message,
      cssClass: "alertstyle",
      enableBackdropDismiss: true,
      buttons: [
        {
          text: this.param.datatext.btn_ok,
          handler: () => {
            console.log("OK clicked");
          },
        },
      ],
    });
    alert.present();
  }

  statusGreenPresent() {
    let alert = this.alertCtrl.create({
      title: this.param.datatext.statusGreenPresent_title,
      message: this.param.datatext.statusGreenPresent_message,
      cssClass: "alertstyle",
      enableBackdropDismiss: true,
      buttons: [
        {
          text: this.param.datatext.btn_ok,
          handler: () => {
            console.log("OK clicked");
          },
        },
      ],
    });
    alert.present();
  }

  DeleteMessage(type : string, id: number) {
    let alert = this.alertCtrl.create({
      title: this.param.datatext.deletion,
      message: this.param.datatext.deleteMessage,
      cssClass: "alertstyle",
      enableBackdropDismiss: true,
      buttons: [
        {
          text: this.param.datatext.btn_cancel,
          role: "cancel",
          handler: () => {
            console.log("No clicked");
          },
        },
        {
          text: this.param.datatext.btn_yes,
          handler: () => {
            console.log("Yes clicked");
            if(type==="Map") {
              this.deleteMap();
            } else if(type==="POI") {
              this.deletePOI(id);
            } else if(type==="Round") {
              this.deleteRound(id);
            } else {
              this.deleteqr(type,id);
            }
          },
        },
      ],
    });
    alert.present();
  }
  askpswd() {
    let alert = this.alertCtrl.create({
      title: this.param.datatext.password,
      message: this.param.datatext.enterPassword,
      cssClass: "alertstyle",
      enableBackdropDismiss: true,
      inputs: [
        {
          name: "password",
          placeholder: this.param.datatext.password,
          type: "password",
        },
      ],
      buttons: [
        {
          text: this.param.datatext.btn_cancel,
          role: "cancel",
          handler: (data) => {
            console.log("Cancel clicked");
          },
        },
        {
          text: this.param.datatext.btn_save,
          role: "backdrop",
          handler: (data) => {
            if (data.password === atob(this.param.robot.password)) {
              
              this.accessparam();
            } else {
              this.wrongPassword();
            }
          },
        },
      ],
    });
    alert.present();
  }

  wrongPassword() {
    let alert = this.alertCtrl.create({
      title: this.param.datatext.password,
      message: this.param.datatext.wrongPassword,
      cssClass: "alertstyle_wrongpass",
      enableBackdropDismiss: true,
      inputs: [
        {
          name: "password",
          placeholder: this.param.datatext.password,
          type: "password",
        },
      ],
      buttons: [
        {
          text: this.param.datatext.btn_cancel,
          role: "cancel",
          handler: (data) => {
            console.log("Cancel clicked");
          },
        },
        {
          text: this.param.datatext.btn_save,
          role: "backdrop",
          handler: (data) => {
            if (data.password === atob(this.param.robot.password )){
              this.accessparam();
            } else {
              this.wrongPassword();
            }
          },
        },
      ],
    });
    alert.present();
  }

  askRemoveDataDocking() {
    let alert = this.alertCtrl.create({
      title: this.param.datatext.dataRemove,
      message: this.param.datatext.dataRemoveInfo,
      cssClass: "alertstyle",
      enableBackdropDismiss: true,
      buttons: [
        {
          text: this.param.datatext.btn_cancel,
          role: "cancel",
          handler: (data) => {
            console.log("Cancel clicked");
          },
        },
        {
          text: this.param.datatext.continue,
          role: "backdrop",
          handler: (data) => {
            this.removePOIdocking();
          },
        },
      ],
    });
    alert.present();
  }

  askConfigDocking(ev) {
    ev.preventDefault();
    let alert = this.alertCtrl.create({
      title: this.param.datatext.configDocking,
      message: this.param.datatext.configDockingConfirm,
      cssClass: "alertstyle",
      enableBackdropDismiss: true,
      buttons: [
        {
          text: this.param.datatext.btn_cancel,
          role: "cancel",
          handler: (data) => {
            console.log("Cancel clicked");
          },
        },
        {
          text: this.param.datatext.btn_yes,
          role: "backdrop",
          handler: (data) => {
            this.configDocking();
          },
        },
      ],
    });
    alert.present();
  }
  

  showToastGreen(msg,duration,position) {
    const toast = this.toastCtrl.create({
      message: msg,
      duration: duration,
      position: position,//top, middle, bottom
      cssClass:"toastok",
    });
    toast.present();
  }

  showToastRed(msg,duration,position:string) {
    const toast = this.toastCtrl.create({
      message: msg,
      duration: duration,
      position: position,//top, middle, bottom
      cssClass:"toast",
    });
    toast.present();
  }

  showToastBlue(m: string, n: number) {
    const toast = this.toastCtrl.create({
      message: m,
      duration: n,
      position: "middle",
      cssClass: "toastam",
    });
    toast.present();
  }
}
