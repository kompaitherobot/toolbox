webpackJsonp([1],{

/***/ 14:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ParamService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__langage_fr_FR_json__ = __webpack_require__(446);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__langage_fr_FR_json___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__langage_fr_FR_json__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__langage_en_GB_json__ = __webpack_require__(447);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__langage_en_GB_json___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1__langage_en_GB_json__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__langage_es_ES_json__ = __webpack_require__(448);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__langage_es_ES_json___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2__langage_es_ES_json__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__langage_de_DE_json__ = __webpack_require__(449);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__langage_de_DE_json___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3__langage_de_DE_json__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__langage_it_IT_json__ = __webpack_require__(450);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__langage_it_IT_json___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4__langage_it_IT_json__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__langage_el_GR_json__ = __webpack_require__(451);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__langage_el_GR_json___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5__langage_el_GR_json__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__angular_common_http__ = __webpack_require__(75);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__angular_platform_browser__ = __webpack_require__(45);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
// the parameter script
//import paramdata from "../parameter/robot_parameter.json";









var ParamService = /** @class */ (function () {
    function ParamService(httpClient, sanitizer) {
        this.httpClient = httpClient;
        this.sanitizer = sanitizer;
        this.tableduration = {};
        this.tablerobot = {};
        this.qrcodes = {};
        this.tableqr = {};
        this.currentduration = {};
        this.tableparamuv = {};
        this.durationNS = {};
        this.tabledurationNS = {};
        this.liftip = "192.168.1.2:8000";
        this.cpt = 0;
        this.sendduration = false;
    }
    ParamService.prototype.getDataRobot = function () {
        var _this = this;
        this.httpClient.get("http://localhost/ionicDB/getrobot.php").subscribe(function (data) {
            _this.robot = data[0];
            _this.langage = _this.robot.langage;
            if (_this.langage === "fr-FR") {
                _this.datatext = __WEBPACK_IMPORTED_MODULE_0__langage_fr_FR_json___default.a;
            }
            else if (_this.langage === "en-GB") {
                _this.datatext = __WEBPACK_IMPORTED_MODULE_1__langage_en_GB_json___default.a;
            }
            else if (_this.langage === "es-ES") {
                _this.datatext = __WEBPACK_IMPORTED_MODULE_2__langage_es_ES_json___default.a;
            }
            else if (_this.langage === "de-DE") {
                _this.datatext = __WEBPACK_IMPORTED_MODULE_3__langage_de_DE_json___default.a;
            }
            else if (_this.langage === "el-GR") {
                _this.datatext = __WEBPACK_IMPORTED_MODULE_5__langage_el_GR_json___default.a;
            }
            else if (_this.langage === "it-IT") {
                _this.datatext = __WEBPACK_IMPORTED_MODULE_4__langage_it_IT_json___default.a;
            }
            _this.source = _this.sanitizer.bypassSecurityTrustResourceUrl(_this.datatext.URL_toolbox);
        }, function (err) {
            console.log(err);
        });
    };
    ParamService.prototype.getQR = function () {
        var _this = this;
        this.httpClient.get("http://localhost/ionicDB/getqrcodes.php").subscribe(function (data) {
            _this.qrcodes = data;
        }, function (err) {
            console.log(err);
        });
    };
    ParamService.prototype.addQR = function (qr, map, poi, round) {
        this.tableqr.action = "insert";
        this.tableqr.qr = qr;
        this.tableqr.id_map = map;
        this.tableqr.id_poi = poi;
        this.tableqr.id_round = round;
        this.httpClient
            .post("http://localhost/ionicDB/addqrcode.php", JSON.stringify(this.tableqr))
            .subscribe(function (data) {
            console.log(data);
        }, function (err) {
            console.log(err);
        });
    };
    ParamService.prototype.deleteQR = function (qr) {
        this.tableqr.action = "delete";
        this.tableqr.qr = qr;
        this.httpClient
            .post("http://localhost/ionicDB/deleteqrcode.php", JSON.stringify(this.tableqr))
            .subscribe(function (data) {
            console.log(data);
        }, function (err) {
            console.log(err);
        });
    };
    ParamService.prototype.addDuration = function () {
        this.tableduration.action = "insert";
        this.tableduration.serialnumber = this.serialnumber;
        this.tableduration.round = this.currentduration.round;
        this.tableduration.battery = this.currentduration.battery;
        this.tableduration.walk = this.currentduration.walk;
        this.tableduration.patrol = this.currentduration.patrol;
        this.tableduration.date = this.currentduration.date;
        this.httpClient
            .post("http://localhost/ionicDB/addduration.php", JSON.stringify(this.tableduration))
            .subscribe(function (data) {
            //console.log(data);
        }, function (err) {
            console.log(err);
        });
    };
    ParamService.prototype.getDurationNS = function () {
        var _this = this;
        this.httpClient.get("http://localhost/ionicDB/duration/getduration.php").subscribe(function (data) {
            _this.duration = data;
            _this.durationNS = data;
            //console.log(data);
            //console.log(this.durationNS.length);
        }, function (err) {
            console.log(err);
        });
    };
    ParamService.prototype.updateDurationNS = function (id) {
        this.tabledurationNS.action = "update";
        this.tabledurationNS.id_duration = id;
        this.httpClient
            .post("http://localhost/ionicDB/duration/updateduration.php", JSON.stringify(this.tabledurationNS))
            .subscribe(function (data) {
            console.log(data);
        }, function (err) {
            console.log(err);
        });
    };
    ParamService.prototype.getBattery = function () {
        var _this = this;
        this.httpClient.get("http://localhost/ionicDB/getbattery.php").subscribe(function (data) {
            _this.battery = data[0];
        }, function (err) {
            console.log(err);
        });
    };
    ParamService.prototype.updateRobot = function () {
        this.tablerobot.action = "update";
        this.tablerobot.langage = this.robot.langage;
        this.tablerobot.serialnumber = this.serialnumber;
        this.tablerobot.name = this.name;
        this.tablerobot.send_pic = this.robot.send_pic;
        this.tablerobot.password = this.robot.password;
        this.httpClient
            .post("http://localhost/ionicDB/updaterobotpassword.php", JSON.stringify(this.tablerobot))
            .subscribe(function (data) {
            //console.log(data);
        }, function (err) {
            console.log(err);
        });
    };
    ParamService.prototype.updateDuration = function () {
        this.tableduration.action = "update";
        this.tableduration.toolbox = this.currentduration.toolbox;
        this.tableduration.date = this.currentduration.date;
        this.httpClient
            .post("http://localhost/ionicDB/duration/updatedurationtoolbox.php", JSON.stringify(this.tableduration))
            .subscribe(function (data) {
            console.log(data);
        }, function (err) {
            console.log(err);
        });
    };
    ParamService.prototype.updateUVparam = function () {
        this.tableparamuv.action = "update";
        this.tableparamuv.preheating_time = this.paramuv.preheating_time;
        this.tableparamuv.evacuation_time = this.paramuv.evacuation_time;
        this.tableparamuv.battery_threshold = this.paramuv.battery_threshold;
        this.tableparamuv.uv_threshold = this.paramuv.uv_threshold;
        this.tableparamuv.lamp_threshold = this.paramuv.lamp_threshold;
        this.tableparamuv.person_detection = this.paramuv.person_detection;
        this.httpClient
            .post("http://localhost/ionicDB/updateparamuv.php", JSON.stringify(this.tableparamuv))
            .subscribe(function (data) {
            console.log(data);
        }, function (err) {
            console.log(err);
        });
    };
    ParamService.prototype.getUVparam = function () {
        var _this = this;
        this.httpClient.get("http://localhost/ionicDB/getparamuv.php").subscribe(function (data) {
            _this.paramuv = data[0];
            //console.log(this.paramuv);
        }, function (err) {
            console.log(err);
        });
    };
    ParamService.prototype.fillData = function () {
        this.allowspeech = this.robot.allowspeech;
        this.datamail = this.robot.maildata;
        this.maildatapassw = atob(this.robot.maildatapass);
        this.mailrobotpassw = atob(this.robot.mailrobotpass);
        this.localhost = this.robot.localhost;
        this.serialnumber = this.robot.serialnumber;
        this.robotmail = this.robot.mailrobot;
        this.datamail = this.robot.maildata;
        this.datamaillist = ["data@kompai.com"];
        this.allowskip = this.robot.allowskip;
        if (this.duration.length > 0) {
            if (this.duration[this.duration.length - 1].date ===
                new Date().toLocaleDateString("fr-CA")) {
                this.currentduration.date = this.duration[this.duration.length - 1].date;
                this.currentduration.round = this.duration[this.duration.length - 1].round;
                this.currentduration.battery = this.duration[this.duration.length - 1].battery;
                this.currentduration.patrol = this.duration[this.duration.length - 1].patrol;
                this.currentduration.walk = this.duration[this.duration.length - 1].walk;
                this.currentduration.toolbox = this.duration[this.duration.length - 1].toolbox;
                this.currentduration.logistic = this.duration[this.duration.length - 1].logistic;
            }
            else {
                if (!this.sendduration) {
                    this.sendduration = true;
                    this.init_currentduration();
                    this.addDuration();
                }
            }
        }
        else {
            if (!this.sendduration) {
                this.sendduration = true;
                this.init_currentduration();
                this.addDuration();
            }
        }
    };
    ParamService.prototype.cptDuration = function () {
        if (this.currentduration.date === new Date().toLocaleDateString("fr-CA")) {
            this.cpt = parseInt(this.currentduration.toolbox);
            this.currentduration.toolbox = this.cpt + 2;
            this.updateDuration();
        }
        else {
            this.init_currentduration();
            this.addDuration();
        }
    };
    ParamService.prototype.init_currentduration = function () {
        this.currentduration.round = 0;
        this.currentduration.battery = 0;
        this.currentduration.patrol = 0;
        this.currentduration.walk = 0;
        this.currentduration.toolbox = 0;
        this.currentduration.logistic = 0;
        this.currentduration.date = new Date().toLocaleDateString("fr-CA");
    };
    ParamService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_6__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_7__angular_common_http__["a" /* HttpClient */], __WEBPACK_IMPORTED_MODULE_8__angular_platform_browser__["c" /* DomSanitizer */]])
    ], ParamService);
    return ParamService;
}());

//# sourceMappingURL=param.service.js.map

/***/ }),

/***/ 141:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PoiPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_api_service__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_alert_service__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_popup_service__ = __webpack_require__(35);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_param_service__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__services_speech_service__ = __webpack_require__(54);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_jsqr__ = __webpack_require__(263);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_jsqr___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7_jsqr__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__rounddisplay_rounddisplay__ = __webpack_require__(264);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__qrcode_qrcode__ = __webpack_require__(266);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__docking_docking__ = __webpack_require__(267);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__lift_lift__ = __webpack_require__(268);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






//import { File } from "@ionic-native/file";







var PoiPage = /** @class */ (function () {
    function PoiPage(renderer, navCtrl, loadingCtrl, toastCtrl, popup, api, speech, param, alert //private alertCtrl: AlertController
    ) {
        this.renderer = renderer;
        this.navCtrl = navCtrl;
        this.loadingCtrl = loadingCtrl;
        this.toastCtrl = toastCtrl;
        this.popup = popup;
        this.api = api;
        this.speech = speech;
        this.param = param;
        this.alert = alert; //private alertCtrl: AlertController
        this.rdPage = __WEBPACK_IMPORTED_MODULE_8__rounddisplay_rounddisplay__["a" /* RoundDisplayPage */];
        this.qrcPage = __WEBPACK_IMPORTED_MODULE_9__qrcode_qrcode__["a" /* QRCodePage */];
        this.dockPage = __WEBPACK_IMPORTED_MODULE_10__docking_docking__["a" /* DockingPage */];
        this.liftPage = __WEBPACK_IMPORTED_MODULE_11__lift_lift__["a" /* LiftPage */];
        this.start = {
            x: 0,
            y: 0,
        };
        this.clickpose = {
            x: 0,
            y: 0,
        };
        this.map_url = "http://" + this.param.localhost + "/api/maps/current/image?t=" + new Date().getTime();
        this.SelectLabel = "none";
        this.videoWidth = 0;
        this.videoHeight = 0;
        this.constraints = {
            //audio: false,
            //video: {deviceId: "f9860b260e18d7fd1687d8567e21b49c4bb26f87606020741717e11f6179f409"},
            video: true,
            facingMode: "environment",
            width: { ideal: 1920 },
            height: { ideal: 1080 },
        };
        this.qrdetection = false;
        this.newpoi = false;
        this.namepoi = "";
        this.namemap = "";
        this.editnamepoi = "";
        this.editpoi = false;
        this.resetposition = false;
        this.gopoi = false;
        this.waygo = 1;
        this.avoidance = true;
        this.speed = 70;
        this.newmap = false;
        this.editmap = false;
        this.loadimg = false;
        this.reloc = false;
        this.selectLabel = {
            title: "Label",
        };
        if (this.param.robot.cam_USB == 1) {
            this.setupconstraint();
        }
    }
    PoiPage.prototype.setupconstraint = function () {
        var _this = this;
        // demande de permission camera
        navigator.mediaDevices.getUserMedia(this.constraints);
        // verification des permissions video et de la disponibilité de devices
        if (!!(navigator.mediaDevices && navigator.mediaDevices.getUserMedia)) {
            // enumeration des appareils connectes pour filtrer
            navigator.mediaDevices.enumerateDevices().then(function (result) {
                var constraints;
                console.log(result);
                result.forEach(function (device) {
                    // filtrer pour garder les flux video
                    if (device.kind.includes("videoinput")) {
                        // filtrer le label de la camera
                        if (device.label.includes("USB")) {
                            constraints = {
                                audio: false,
                                //video: {deviceId: "f9860b260e18d7fd1687d8567e21b49c4bb26f87606020741717e11f6179f409"},
                                // affecter le deviceid filtré
                                video: { deviceId: device.deviceId },
                                facingMode: "environment",
                                width: { ideal: 1920 },
                                height: { ideal: 1080 },
                            };
                        }
                    }
                });
                // on lance la connexion de la vidéo
                if (constraints) {
                    _this.constraints = constraints;
                    _this.startCamera(constraints);
                    console.log("startcam");
                }
                //this.startCamera(constraints);
            });
        }
        else {
            console.log("Sorry, camera not available.");
        }
    };
    PoiPage.prototype.onLabelChange = function () {
        console.log("change label");
    };
    PoiPage.prototype.convertDataURIToBinary2 = function (dataURI) {
        var raw = window.atob(dataURI);
        var rawLength = raw.length;
        var array = new Uint8Array(new ArrayBuffer(rawLength));
        for (var i = 0; i < rawLength; i++) {
            array[i] = raw.charCodeAt(i);
        }
        return array;
    };
    PoiPage.prototype.ngOnInit = function () {
        //this.current_map=this.api.id_current_map;
        this._CANVAS = this.canvasEl.nativeElement;
        this._CONTEXT = this._CANVAS.getContext("2d");
        this._CANVAS.style.width = "100%";
        this._CANVAS.style.height = "100%";
        this.heightcol =
            document.getElementById("body4").clientHeight -
                document.getElementById("firstrow4").clientHeight * 2;
        this.widthcol = document.getElementById("testcol4").clientWidth;
        //this.speech.getVoice();
        this.popup.onSomethingHappened1(this.deleteMap.bind(this));
        this.popup.onSomethingHappened2(this.deletePOI.bind(this));
        this.getImageFromService();
    };
    PoiPage.prototype.abort = function (ev) {
        ev.preventDefault();
        if (this.intervpoi) {
            clearInterval(this.intervgopoi);
        }
        this.api.abortNavHttp();
    };
    PoiPage.prototype.scan = function (ev) {
        var _this = this;
        ev.preventDefault();
        ///si cam usb
        this.alert.scanqr(this.api.mailAddInformationBasic());
        if (this.param.robot.cam_USB == 1) {
            this.qrdetection = true;
            console.log("enter scan usb");
            setTimeout(function () {
                if (_this.qrdetection) {
                    _this.qrdetection = false;
                }
            }, 3000);
            try {
                this.videoCanvas.nativeElement.height = this.videoElement.nativeElement.videoHeight;
                this.videoCanvas.nativeElement.width = this.videoElement.nativeElement.videoWidth;
                var g = this.videoCanvas.nativeElement.getContext("2d");
                g.drawImage(this.videoElement.nativeElement, 0, 0, this.videoCanvas.nativeElement.width, this.videoCanvas.nativeElement.height);
                var imageData = g.getImageData(0, 0, this.videoCanvas.nativeElement.width, this.videoCanvas.nativeElement.height);
                var code_1 = __WEBPACK_IMPORTED_MODULE_7_jsqr___default()(imageData.data, imageData.width, imageData.height, {
                    inversionAttempts: "dontInvert",
                });
                if (code_1) {
                    console.log(code_1.data.trim());
                    if (code_1.data !== null &&
                        this.param.qrcodes.filter(function (x) { return x.qr == code_1.data.trim(); })
                            .length > 0) {
                        this.api.QRselected = this.param.qrcodes.filter(function (x) { return x.qr == code_1.data.trim(); })[0];
                        this.onReloc();
                    }
                    else if (code_1.data !== null &&
                        this.param.qrcodes.filter(function (x) { return x.qr == code_1.data.trim(); })
                            .length === 0) {
                        this.popup.showToastRed(this.param.datatext.unknownqrcode, 4000, "middle");
                        this.speech.speak(this.param.datatext.unknownqrcode);
                    }
                }
                else {
                    setTimeout(function () {
                        _this.popup.showToastRed(_this.param.datatext.noqrdetected, 4000, "middle");
                        _this.speech.speak(_this.param.datatext.noqrdetected);
                    }, 1000);
                }
            }
            catch (err) {
                console.log("Error", err);
            }
            /////si cam ros
        }
        else {
            console.log("scan qr ros");
            this.qrdetection = true;
            this.api.startQrDetection(true);
            setTimeout(function () {
                if (_this.qrdetection) {
                    _this.qrdetection = false;
                    _this.api.startQrDetection(false);
                    _this.popup.showToastRed(_this.param.datatext.noqrdetected, 5000, "middle");
                    _this.speech.speak(_this.param.datatext.noqrdetected);
                }
            }, 5000);
            this.api.listener_qr.subscribe(function (message) {
                console.log('Received message on ' + _this.api.listener_qr.name + ': ' + message.data);
                if (_this.qrdetection) {
                    _this.qrdetection = false;
                    _this.api.startQrDetection(false);
                    if (message.data !== null &&
                        _this.param.qrcodes.filter(function (x) { return x.qr == message.data.trim(); })
                            .length > 0) {
                        _this.api.QRselected = _this.param.qrcodes.filter(function (x) { return x.qr == message.data.trim(); })[0];
                        _this.onReloc();
                    }
                    else if (message.data !== null &&
                        _this.param.qrcodes.filter(function (x) { return x.qr == message.data.trim(); })
                            .length === 0) {
                        _this.popup.showToastRed(_this.param.datatext.unknownqrcode, 4000, "middle");
                        _this.speech.speak(_this.param.datatext.unknownqrcode);
                    }
                }
            });
        }
    };
    PoiPage.prototype.onReloc = function () {
        var _this = this;
        console.log("onreloc");
        this.showLoading();
        this.speech.speak(this.param.datatext.alertReloc);
        //this.api.getCurrentMap();
        console.log(this.api.id_current_map);
        console.log(this.api.QRselected.id_map);
        if (!(this.api.id_current_map == this.api.QRselected.id_map)) {
            this.api.current_map = this.api.QRselected.id_map;
            console.log("onchangemap");
            this.onMapChange(this.api.QRselected.id_map);
        }
        setTimeout(function () {
            _this.api.getFirstLocationHttp();
        }, 4000);
        setTimeout(function () {
            _this.api.resetLocationHttp();
        }, 6000);
        setTimeout(function () {
            _this.imge.src = "http://" + _this.param.localhost + "/api/maps/current/image?t=" + new Date().getTime();
        }, 8000);
        setTimeout(function () {
            _this.createImageFromBlob();
            _this.dismissLoading();
            _this.popup.showToastBlue(_this.param.datatext.localizationDone + _this.api.mapdata.Name, 4000);
            _this.speech.speak(_this.param.datatext.localizationDone + _this.api.mapdata.Name);
        }, 9000);
    };
    PoiPage.prototype.ionViewDidEnter = function () {
        var _this = this;
        this.filen = null;
        this.api.textHeadBand = this.param.datatext.toolbox;
        if (this.api.id_current_map && this.api.firstopen) {
            this.api.current_map = this.api.mapdata.Id;
            this.api.firstopen = false;
            this.getImageFromService();
        }
        this.api.getCurrentLocations();
        this.api.getRoundList();
        this.intervmap = setInterval(function () { return _this.updatemap(); }, 400);
        // CAMERA ROS
        if (this.param.robot.cam_USB == 0) {
            this.api.listener_camera.subscribe(function (message) {
                //console.log('Received message on ' + listener_compressed.name + ': ');
                _this.imgRos = document.getElementById('imgcompressed');
                //console.log(message);
                var bufferdata = _this.convertDataURIToBinary2(message.data);
                var blob = new Blob([bufferdata], { type: 'image/jpeg' });
                var blobUrl = URL.createObjectURL(blob);
                _this.imgRos.src = blobUrl;
            });
        }
    };
    PoiPage.prototype.updatemap = function () {
        if (this.reloc || this.api.differential_status.CurrentAngularSpeed > 0.005 || this.api.differential_status.CurrentLinearSpeed > 0.01 || this._CANVAS.width == 0) {
            this.createImageFromBlob();
        }
    };
    PoiPage.prototype.ionViewWillLeave = function () {
        clearInterval(this.intervmap);
        if (this.param.robot.cam_USB == 0) {
            this.api.listener_camera.removeAllListeners();
        }
    };
    PoiPage.prototype.onClickNewPOI = function (ev) {
        ev.preventDefault();
        this.newpoi = true;
        this.SelectLabel = "none";
        this.namepoi = "";
    };
    PoiPage.prototype.onClickNewMap = function (ev) {
        ev.preventDefault();
        this.filen = null;
        this.newmap = true;
        this.namemap = "";
    };
    PoiPage.prototype.onClickEditMap = function (ev) {
        ev.preventDefault();
        this.filen = null;
        this.editmap = true;
        this.nameeditmap = this.api.mapdata.Name;
    };
    PoiPage.prototype.onClickGoPOI = function (ev, id, n, p) {
        var _this = this;
        ev.preventDefault();
        this.pose = p;
        this.idpoi_to_edit = id;
        this.namepoi = n;
        if (this.namepoi === "docked" || this.namepoi === "docking") {
            this.api.towardDocking = true;
            this.api.reachHttp("docking");
            this.intervgodocking = setInterval(function () { return _this.updateTrajectory(); }, 500);
        }
        else {
            this.intervpoi = true;
            this.gopoi = true;
            this.intervgopoi = setInterval(function () { return _this.updateGoPOI(); }, 500);
        }
    };
    PoiPage.prototype.updateGoPOI = function () {
        if (this.api.navigation_status.status === 2) {
            this.intervpoi = false;
            this.gopoi = false;
            clearInterval(this.intervgopoi);
        }
    };
    PoiPage.prototype.onClickEditPOI = function (ev, n, l, poi) {
        ev.preventDefault();
        this.editpoi = true;
        this.SelectLabel = l;
        this.editnamepoi = n;
        this.idpoi_to_edit = poi;
    };
    PoiPage.prototype.ReachPoi = function (ev) {
        ev.preventDefault();
        this.alert.gopoi(this.api.mailAddInformationBasic());
        this.speed = Math.abs(this.speed);
        if (Math.abs(this.speed) > 100) {
            this.speed = 100;
        }
        this.api.putDestination(this.pose, (this.waygo * this.speed) / 100, this.avoidance);
    };
    PoiPage.prototype.updateTrajectory = function () {
        // listen the round status to allow the robot to go to the next
        //// go docking or go poi in progress
        if (this.api.towardDocking) {
            if (this.api.docking_status.status === 3) {
                this.api.towardDocking = false;
            }
            else if (this.api.towardDocking &&
                this.api.navigation_status.status === 0 &&
                this.api.docking_status.detected) {
                //connect to the docking when he detect it
                this.api.connectHttp();
            }
        }
        else {
            clearInterval(this.intervgodocking);
        }
    };
    PoiPage.prototype.cancelpoi = function (ev) {
        ev.preventDefault();
        this.newpoi = false;
    };
    PoiPage.prototype.cancelmap = function (ev) {
        ev.preventDefault();
        this.api.mapok = true;
        this.newmap = false;
        this.editmap = false;
        this.filen = null;
    };
    PoiPage.prototype.cancelgopoi = function (ev) {
        ev.preventDefault();
        this.gopoi = false;
        this.api.towardDocking = false;
    };
    PoiPage.prototype.canceleditpoi = function (ev) {
        ev.preventDefault();
        this.editpoi = false;
        this.resetposition = false;
    };
    PoiPage.prototype.editPoi = function (ev) {
        var _this = this;
        ev.preventDefault();
        this.alert.editpoi(this.api.mailAddInformationBasic());
        this.editnamepoi = this.editnamepoi.replace(new RegExp("[<>&$§µ£#*_|`¤~(){}°²@%/]", "g"), "");
        this.editnamepoi = this.editnamepoi.replace(new RegExp('["]', "g"), "");
        this.editnamepoi = this.editnamepoi.replace(new RegExp(/\\/, "g"), "");
        this.editnamepoi = this.editnamepoi.replace(new RegExp(/[\[\]]/, "g"), "");
        if (this.editnamepoi != "") {
            var x;
            var y;
            var t;
            this.showLoading();
            if (this.resetposition) {
                x = this.api.localization_status.positionx;
                y = this.api.localization_status.positiony;
                t = this.api.localization_status.positiont;
            }
            else {
                x = this.api.all_locations.filter(function (poi) { return poi.Id == _this.idpoi_to_edit; })[0].Pose.X;
                y = this.api.all_locations.filter(function (poi) { return poi.Id == _this.idpoi_to_edit; })[0].Pose.Y;
                t = this.api.all_locations.filter(function (poi) { return poi.Id == _this.idpoi_to_edit; })[0].Pose.T;
            }
            this.api.putPOI(this.editnamepoi, this.SelectLabel, this.idpoi_to_edit, x, y, t);
            setTimeout(function () {
                _this.editpoi = false;
                _this.api.getCurrentLocations();
            }, 2000);
            setTimeout(function () {
                _this.createImageFromBlob();
                _this.dismissLoading();
            }, 4000);
        }
    };
    PoiPage.prototype.createpoi = function (ev) {
        var _this = this;
        ev.preventDefault();
        this.alert.createpoi(this.api.mailAddInformationBasic());
        this.namepoi = this.namepoi.replace(new RegExp("[<>&$§µ£#*_|`¤~(){}°²@%/]", "g"), "");
        this.namepoi = this.namepoi.replace(new RegExp('["]', "g"), "");
        this.namepoi = this.namepoi.replace(new RegExp(/\\/, "g"), "");
        this.namepoi = this.namepoi.replace(new RegExp(/[\[\]]/, "g"), "");
        if (this.namepoi != "") {
            this.showLoading();
            this.api.postPoi(this.namepoi, this.SelectLabel);
            setTimeout(function () {
                _this.newpoi = false;
                _this.api.changeMapbyIdHttp(_this.api.id_current_map);
                _this.api.getCurrentLocations();
            }, 2000);
            setTimeout(function () {
                _this.createImageFromBlob();
                _this.dismissLoading();
                _this.content1.scrollToBottom();
            }, 4000);
        }
    };
    PoiPage.prototype.addMap = function (ev) {
        var _this = this;
        ev.preventDefault();
        this.alert.addmap(this.api.mailAddInformationBasic());
        this.namemap = this.namemap.replace(new RegExp("[<>&$§µ£#*_|`¤~(){}°²@%/]", "g"), "");
        this.namemap = this.namemap.replace(new RegExp('["]', "g"), "");
        this.namemap = this.namemap.replace(new RegExp(/\\/, "g"), "");
        this.namemap = this.namemap.replace(new RegExp(/[\[\]]/, "g"), "");
        if (this.namemap != "" && this.filen) {
            this.showLoading();
            var formul = document.getElementById("update_form");
            var formData_1 = new FormData(formul);
            formData_1.append("resolution", "0.04");
            formData_1.append("offset_x", "0.0");
            formData_1.append("offset_y", "0.0");
            formData_1.append("filename", this.b);
            console.log(formData_1.get("filename"));
            setTimeout(function () {
                _this.api.postMap(formData_1);
            }, 500);
            setTimeout(function () {
                if (_this.api.mapok) {
                    _this.api.getAllMapsHttp();
                }
            }, 3000);
            setTimeout(function () {
                if (_this.api.mapok) {
                    _this.newmap = false;
                    _this.filen = null;
                    _this.dismissLoading();
                    _this.onMapChange(_this.api.all_maps[_this.api.all_maps.length - 1].Id);
                }
                else {
                    _this.dismissLoading();
                }
            }, 4000);
        }
    };
    PoiPage.prototype.editMap = function (ev) {
        var _this = this;
        ev.preventDefault();
        this.alert.editmap(this.api.mailAddInformationBasic());
        this.nameeditmap = this.nameeditmap.replace(new RegExp("[<>&$§µ£#*_|`¤~(){}°²@%/]", "g"), "");
        this.nameeditmap = this.nameeditmap.replace(new RegExp('["]', "g"), "");
        this.nameeditmap = this.nameeditmap.replace(new RegExp(/\\/, "g"), "");
        this.nameeditmap = this.nameeditmap.replace(new RegExp(/[\[\]]/, "g"), "");
        if (this.nameeditmap != "") {
            this.showLoading();
            var formul = document.getElementById("update_formul");
            var formData_2 = new FormData(formul);
            formData_2.append("resolution", "0.04");
            formData_2.append("offset_x", "0.0");
            formData_2.append("offset_y", "0.0");
            if (this.filen) {
                formData_2.append("filename", this.b);
            }
            else {
                formData_2.append("filename", null);
            }
            console.log(formData_2.get("filename"));
            setTimeout(function () {
                _this.api.putMap(formData_2);
            }, 500);
            setTimeout(function () {
                if (_this.api.mapok) {
                    _this.api.getAllMapsHttp();
                }
            }, 4000);
            setTimeout(function () {
                if (_this.api.mapok) {
                    _this.editmap = false;
                    _this.dismissLoading();
                    if (_this.filen) {
                        _this.onMapChange(_this.api.id_current_map);
                    }
                    _this.filen = null;
                }
                else {
                    _this.dismissLoading();
                }
            }, 5000);
        }
    };
    PoiPage.prototype.showLoading = function () {
        if (!this.loading) {
            this.loading = this.loadingCtrl.create({
                content: this.param.datatext.wait,
            });
            this.loading.present();
        }
    };
    PoiPage.prototype.popupDelete = function (ev, type, id) {
        ev.preventDefault();
        this.popup.DeleteMessage(type, id);
    };
    PoiPage.prototype.deleteMap = function () {
        var _this = this;
        this.alert.deletemap(this.api.mailAddInformationBasic());
        this.showLoading();
        this.api.deleteMap(this.api.id_current_map);
        this.api.all_locations = [];
        this.param.qrcodes.forEach(function (qr) {
            if (qr.id_map == _this.api.id_current_map) {
                _this.api.deletePOI(qr.id_poi);
                _this.param.deleteQR(qr.qr);
            }
        });
        // this.current_map = undefined;
        // this.api.id_current_map = undefined;
        // this.api.imgMap = undefined;
        setTimeout(function () {
            _this.api.getAllMapsHttp();
            _this.api.current_map = undefined;
            _this.api.id_current_map = undefined;
            _this.api.imgMap = undefined;
            _this.createImageFromBlob();
            _this.dismissLoading();
        }, 3000);
    };
    PoiPage.prototype.roundPage = function (ev) {
        ev.preventDefault();
        if (this.api.all_locations.length > 1) {
            this.api.getRoundList();
            this.navCtrl.push(this.rdPage);
        }
    };
    PoiPage.prototype.dockingPage = function (ev) {
        var _this = this;
        ev.preventDefault();
        if (this.api.current_map) {
            this.showLoading();
            setTimeout(function () {
                _this.api.changeMapbyIdHttp(_this.api.id_current_map);
                _this.api.getCurrentLocations();
            }, 3000);
            setTimeout(function () {
                _this.dismissLoading();
            }, 4000);
            this.navCtrl.push(this.dockPage);
        }
    };
    PoiPage.prototype.goLiftPage = function (ev) {
        ev.preventDefault();
        this.navCtrl.push(this.liftPage);
    };
    PoiPage.prototype.qrPage = function (ev) {
        ev.preventDefault();
        this.api.getRoundList();
        this.param.getQR();
        this.navCtrl.push(this.qrcPage);
    };
    PoiPage.prototype.deletePOI = function (id) {
        var _this = this;
        this.alert.deletepoi(this.api.mailAddInformationBasic());
        this.showLoading();
        this.api.deletePOI(id);
        setTimeout(function () {
            _this.api.getCurrentLocations();
        }, 2000);
        setTimeout(function () {
            _this.createImageFromBlob();
            _this.dismissLoading();
        }, 3000);
    };
    PoiPage.prototype.dismissLoading = function () {
        if (this.loading) {
            this.loading.dismiss();
            this.loading = null;
        }
    };
    PoiPage.prototype.onMapChange = function (event) {
        var _this = this;
        this.alert.loadmap(this.api.mailAddInformationBasic());
        this.showLoading();
        this.api.changeMapbyIdHttp(event);
        setTimeout(function () {
            _this.imge.src = "http://" + _this.param.localhost + "/api/maps/current/image?t=" + new Date().getTime();
        }, 4500);
        setTimeout(function () {
            _this.api.getCurrentLocations();
            _this.api.getCurrentMap();
        }, 5500);
        setTimeout(function () {
            if (_this.api.current_map != _this.api.id_current_map) {
                _this.api.current_map = _this.api.id_current_map;
            }
        }, 6500);
        setTimeout(function () {
            _this.getImageFromService();
            _this.dismissLoading();
        }, 7000);
    };
    PoiPage.prototype.on_mouse_down = function (evt) {
        this.createImageFromBlob();
        if (evt.button == 0) {
            evt.preventDefault();
            this.moving = true;
            this.start.x = evt.x;
            this.start.y = evt.y;
            var x = (evt.offsetX * this._CANVAS.width) / this._CANVAS.clientWidth;
            var y = (evt.offsetY * this._CANVAS.height) / this._CANVAS.clientHeight;
            this.clickpose.x =
                (x - this.offsetX - this._CANVAS.width / 2) * this.resomap;
            this.clickpose.y =
                -(y - this.offsetY - this._CANVAS.height / 2) * this.resomap;
        }
        if (this.reloc) {
            this.api.putLocalization(this.xtest, this.ytest, 0);
        }
    };
    PoiPage.prototype.on_mouse_up = function (evt) {
        this.createImageFromBlob();
        if (this.moving) {
            var offset = {
                x: this.offsetX,
                y: this.offsetY,
                t: 0,
            };
            offset.x += evt.x - this.start.x;
            offset.y += evt.y - this.start.y;
            this.start.x = evt.x;
            this.start.y = evt.y;
            this.offsetX = offset.x;
            this.offsetY = offset.y;
        }
        this.moving = false;
        if (this.reloc) {
            this.api.putLocalization(this.xtest, this.ytest, 0);
        }
    };
    PoiPage.prototype.on_mouse_move = function (evt) {
        if (this.moving) {
            this.createImageFromBlob();
            evt.preventDefault();
            var offset = {
                x: this.offsetX,
                y: this.offsetY,
            };
            offset.x += evt.x - this.start.x;
            offset.y += evt.y - this.start.y;
            this.start.x = evt.x;
            this.start.y = evt.y;
            this.offsetX = offset.x;
            this.offsetY = offset.y;
        }
    };
    PoiPage.prototype.on_wheel = function (evt) {
        this.createImageFromBlob();
        evt.preventDefault();
        if (evt.deltaY > 0) {
            if (!(this.resomap >= this.initialzoom)) {
                this.resomap = this.resomap + 150 / 100000;
            }
            else {
                this.offsetX = 0;
                this.offsetY = 0;
            }
        }
        else {
            if (this.resomap > this.initialzoom / 4) {
                this.resomap = this.resomap - 150 / 100000;
            }
        }
    };
    PoiPage.prototype.open = function (ev, itemSlide) {
        ev.preventDefault();
        if (itemSlide.getSlidingPercent() == 0) {
            // Two calls intentional after examination of vendor.js function
            itemSlide.moveSliding(-390);
            itemSlide.moveSliding(-390);
        }
        else {
            itemSlide.close();
        }
    };
    PoiPage.prototype.on_zoom_out = function (ev) {
        ev.preventDefault();
        //zoom -
        if (!(this.resomap >= this.initialzoom)) {
            this.resomap = this.resomap + 150 / 100000;
        }
        else {
            this.offsetX = 0;
            this.offsetY = 0;
        }
        this.createImageFromBlob();
    };
    PoiPage.prototype.on_zoom_in = function (ev) {
        ev.preventDefault();
        //zoom +
        if (this.resomap > this.initialzoom / 4) {
            this.resomap = this.resomap - 150 / 100000;
        }
        this.createImageFromBlob();
    };
    PoiPage.prototype.createImageFromBlob = function () {
        var _this = this;
        console.log("createImageFromBlob4");
        if (!this.api.current_map) {
            console.log("no current map");
        }
        if (this._CONTEXT !== undefined && !this.api.current_map) {
            console.log("no map");
            this._CONTEXT.clearRect(0, 0, this._CANVAS.width, this._CANVAS.height);
            this._CONTEXT.beginPath();
            this._CONTEXT.fillStyle = "rgba(0, 0, 0, 0.0)";
            this._CONTEXT.fillRect(0, 0, this._CANVAS.width, this._CANVAS.height);
        }
        if (this.api.current_map && this._CANVAS.getContext) {
            //console.log(this._CANVAS);
            //console.log(i);
            this._CANVAS.width = this.imge.width;
            this._CANVAS.height = this.imge.height;
            if (this._CANVAS.width == 0 && this.api.current_map) {
                console.log("width 0");
            }
            var w = this._CANVAS.width;
            var h = this._CANVAS.height;
            var r = this.resomap;
            var x = this.offsetX;
            var y = this.offsetY;
            var xm = this.api.mapdata.Offset.X;
            var ym = this.api.mapdata.Offset.Y;
            var rm = this.api.mapdata.Resolution;
            var wm = this.api.mapdata.Width;
            var hm = this.api.mapdata.Height;
            var dx = xm / r + x;
            var dy = h - (ym + hm * rm) * (1 / r) + y;
            var dw = (wm * rm) / r;
            var dh = (hm * rm) / r;
            if (this.api.current_map) {
                this._CONTEXT.drawImage(this.imge, dx, dy, dw, dh);
                //console.log('dessin image');
            }
            if (this.api.all_locations) {
                this.api.all_locations.forEach(function (evenement) {
                    if (evenement.Label !== "qr") {
                        _this._CONTEXT.beginPath();
                        _this._CONTEXT.font =
                            "" + Math.round(_this._CANVAS.width * 0.025) + "px Arial";
                        _this._CONTEXT.fillStyle = "rgba(26, 156, 195,0.7)";
                        _this._CONTEXT.fillText(evenement.Id, //+ "- " + evenement.Name,
                        evenement.Pose.X / _this.resomap + _this.offsetX - 20, _this._CANVAS.height -
                            (evenement.Pose.Y / _this.resomap - _this.offsetY) -
                            10);
                        if (evenement.Name === "docking" || evenement.Name === "docked") {
                            _this._CONTEXT.fillText(evenement.Id + "- " + evenement.Name, evenement.Pose.X / _this.resomap + _this.offsetX - 20, _this._CANVAS.height -
                                (evenement.Pose.Y / _this.resomap - _this.offsetY) -
                                10);
                        }
                        _this._CONTEXT.beginPath();
                        _this._CONTEXT.arc(evenement.Pose.X / _this.resomap + _this.offsetX, _this._CANVAS.height -
                            (evenement.Pose.Y / _this.resomap - _this.offsetY), _this._CANVAS.width * 0.005, 0, 2 * Math.PI);
                        _this._CONTEXT.lineWidth = 1;
                        _this._CONTEXT.fillStyle = "rgb(26, 156, 195)";
                        _this._CONTEXT.fill();
                        _this._CONTEXT.beginPath();
                        _this._CONTEXT.save();
                        _this._CONTEXT.translate(evenement.Pose.X / _this.resomap + _this.offsetX, _this._CANVAS.height -
                            (evenement.Pose.Y / _this.resomap - _this.offsetY));
                        _this._CONTEXT.rotate(-evenement.Pose.T);
                        _this._CONTEXT.moveTo(0, 0);
                        _this._CONTEXT.lineWidth = 2;
                        _this._CONTEXT.lineTo(2 * _this._CANVAS.width * 0.009, 0);
                        _this._CONTEXT.strokeStyle = "rgb(26, 156, 195)";
                        _this._CONTEXT.stroke();
                        _this._CONTEXT.restore();
                    }
                });
            }
            var xc = w / 2 + this.clickpose.x / r + x;
            var yc = h / 2 - this.clickpose.y / r + y;
            this.xtest = (xc - x) * r;
            this.ytest = -((yc - y - h) * r);
            this._CONTEXT.beginPath();
            this._CONTEXT.arc(xc, yc, this._CANVAS.width * 0.009, 0, 2 * Math.PI);
            this._CONTEXT.lineWidth = 1;
            this._CONTEXT.fillStyle = "#32CD32";
            this._CONTEXT.fill();
            this._CONTEXT.beginPath();
            this._CONTEXT.save();
            this._CONTEXT.translate(this.api.localization_status.positionx / this.resomap + this.offsetX, this._CANVAS.height -
                this.api.localization_status.positiony / this.resomap +
                this.offsetY);
            this._CONTEXT.rotate(-this.api.localization_status.positiont);
            this._CONTEXT.moveTo(0, -15);
            this._CONTEXT.lineTo(15, 0);
            this._CONTEXT.lineTo(0, 15);
            this._CONTEXT.lineTo(-10, 15);
            this._CONTEXT.lineTo(-10, -15);
            this._CONTEXT.lineTo(0, -15);
            this._CONTEXT.closePath();
            this._CONTEXT.lineWidth = 2;
            this._CONTEXT.fill("nonzero");
            this._CONTEXT.stroke();
            this._CONTEXT.restore();
            if (this.api.navigation_status) {
                if (this.api.navigation_status.status == 1) {
                    console.log("trajectoire reach");
                    var traj = this.api.navigation_status.trajectory;
                    traj.forEach(function (evenement) {
                        _this._CONTEXT.beginPath();
                        _this._CONTEXT.moveTo(evenement.Start.X / _this.resomap + _this.offsetX, _this._CANVAS.height -
                            (evenement.Start.Y / _this.resomap - _this.offsetY));
                        _this._CONTEXT.lineTo(evenement.End.X / _this.resomap + _this.offsetX, _this._CANVAS.height -
                            (evenement.End.Y / _this.resomap - _this.offsetY));
                        _this._CONTEXT.strokeStyle = "#32CD32";
                        _this._CONTEXT.stroke();
                    });
                }
            }
        }
    };
    PoiPage.prototype.on_click_relocate = function (ev) {
        ev.preventDefault();
        if (!this.reloc) {
            this.alert.localisemanually(this.api.mailAddInformationBasic());
            this.reloc = true;
            this.api.putLocalization(this.xtest, this.ytest, 0);
        }
        else {
            this.reloc = false;
        }
    };
    PoiPage.prototype.on_click_relocate_left = function (ev) {
        ev.preventDefault();
        this.api.putLocalization(this.api.localization_status.positionx, this.api.localization_status.positiony, (this.api.localization_status.positiont + 0.46) % (2 * Math.PI));
    };
    PoiPage.prototype.on_click_relocate_right = function (ev) {
        ev.preventDefault();
        this.api.putLocalization(this.api.localization_status.positionx, this.api.localization_status.positiony, (this.api.localization_status.positiont - 0.46) % (2 * Math.PI));
    };
    PoiPage.prototype.getImageFromService = function () {
        var _this = this;
        this.imge = document.getElementById("imgmap4");
        if (this.api.current_map) {
            this.offsetX = this.api.mapdata.Offset.X;
            this.offsetY = this.api.mapdata.Offset.Y;
            this.resomap = this.api.mapdata.Resolution;
            this.initialzoom = this.resomap;
        }
        this.api.getImgMapHttp().subscribe(function (data) {
            //console.log(data);
            _this.api.imgMap = data;
            console.log("GetImageFromService");
            _this.createImageFromBlob();
        }, function (error) {
            _this.getImageFromService();
            console.log(error);
        });
    };
    PoiPage.prototype.on_click_goToPose = function (ev) {
        ev.preventDefault();
        this.alert.reachgreenpoint(this.api.mailAddInformationBasic());
        var p = {
            X: this.xtest,
            Y: this.ytest,
            T: 0,
        };
        this.api.reach_location(p, false, 0.8, false);
    };
    PoiPage.prototype.openfileBrowser = function (ev) {
        document.getElementById('mapBrowserFileInput').click();
    };
    PoiPage.prototype.openfile = function (ev) {
        var _this = this;
        ev.preventDefault();
        this.api.mapok = true;
        var file = ev.target.files[0];
        var reader = new FileReader();
        reader.onloadend = function (evt) {
            try {
                var result = evt.target;
                var byteString = atob(result["result"].split(",")[1]);
                var ab = new ArrayBuffer(byteString.length);
                var ia = new Uint8Array(ab);
                for (var i = 0; i < byteString.length; i++) {
                    ia[i] = byteString.charCodeAt(i);
                }
                _this.b = new Blob([ab], { type: "image/png" });
                _this.loadimg = true;
            }
            catch (error) {
                console.error("Error reading file:", error);
            }
        };
        if (file) {
            this.filen = file.name;
            reader.readAsDataURL(file);
        }
    };
    PoiPage.prototype.onSliderRelease = function (ev, id) {
        ev.preventDefault();
        id.open();
    };
    PoiPage.prototype.startCamera = function (cs) {
        if (!!(navigator.mediaDevices && navigator.mediaDevices.getUserMedia)) {
            // recupération du flux avec les constraints configurés puis on attache le flux à la balise video
            navigator.mediaDevices
                .getUserMedia(cs)
                .then(this.attachVideo.bind(this))
                .catch(this.handleError);
        }
        else {
            alert("Sorry, camera not available.");
        }
    };
    PoiPage.prototype.handleError = function (error) {
        console.log("Error: ", error);
    };
    // function qui attache le flux video à la balise video
    PoiPage.prototype.attachVideo = function (stream) {
        var _this = this;
        this.renderer.setProperty(this.videoElement.nativeElement, "srcObject", stream);
        this.renderer.listen(this.videoElement.nativeElement, "play", function (event) {
            _this.videoHeight = _this.videoElement.nativeElement.videoHeight;
            _this.videoWidth = _this.videoElement.nativeElement.videoWidth;
        });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])("canvas4"),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* ElementRef */])
    ], PoiPage.prototype, "canvasEl", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])("imgmap4"),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* ElementRef */])
    ], PoiPage.prototype, "imgmapElement", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])("content1"),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_6_ionic_angular__["c" /* Content */])
    ], PoiPage.prototype, "content1", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])("video"),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* ElementRef */])
    ], PoiPage.prototype, "videoElement", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])("videoCanvas"),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* ElementRef */])
    ], PoiPage.prototype, "videoCanvas", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])("select1"),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_6_ionic_angular__["j" /* Select */])
    ], PoiPage.prototype, "select1", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])("select2"),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_6_ionic_angular__["j" /* Select */])
    ], PoiPage.prototype, "select2", void 0);
    PoiPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: "page-poi",template:/*ion-inline-start:"C:\Users\laele\Documents\KomApp\toolbox\src\pages\poi\poi.html"*/'<!-- home page html template -->\n\n\n\n<ion-header no-border>\n\n  <headpage pageName="poi"></headpage>\n\n</ion-header>\n\n\n\n<ion-content id="body4">\n\n  \n\n    <ion-grid class="heightstyle">\n\n      <ion-row class="heightstyle10" id="firstrow4">\n\n        <ion-col col-4>\n\n          <ion-list>\n\n            <ion-item>\n\n              <ion-label>{{this.param.datatext.maps}} : </ion-label>\n\n              <ion-select #select1\n\n                interface="popover"\n\n                [disabled]="newpoi || editpoi || gopoi || newmap || editmap"\n\n                [(ngModel)]="api.current_map"\n\n                okText="{{this.param.datatext.btn_ok}}"\n\n                cancelText="{{this.param.datatext.btn_cancel}}"\n\n                (ionChange)="onMapChange($event)"\n\n                (mouseup)="onSliderRelease($event,select1)"\n\n              >\n\n                <ion-option *ngFor="let m of this.api.all_maps" value="{{m.Id}}"\n\n                  >{{m.Id}}. {{m.Name}}</ion-option\n\n                >\n\n              </ion-select>\n\n            </ion-item>\n\n          </ion-list>\n\n        </ion-col>\n\n\n\n        <ion-col col-1>\n\n          <ion-buttons class="div_btn_map">\n\n            <button\n\n              [disabled]="newpoi || editpoi || gopoi || newmap || !api.adminMode"\n\n              *ngIf="api.id_current_map!=undefined"\n\n              (mouseup)="popupDelete($event,\'Map\',1)"\n\n              ion-button\n\n              color="danger"\n\n              class="btn_map"\n\n            >\n\n              <ion-icon style="font-size: 3vw" name="trash"></ion-icon></button\n\n          ></ion-buttons>\n\n        </ion-col>\n\n        <ion-col col-1>\n\n          <ion-buttons class="div_btn_map">\n\n            <button\n\n              [disabled]="newpoi || editpoi || gopoi || newmap || editmap || !api.adminMode"\n\n              *ngIf="api.id_current_map!=undefined"\n\n              ion-button\n\n              color="secondary"\n\n              (mouseup)=" onClickEditMap($event)"\n\n              class="btn_map"\n\n            >\n\n              <ion-icon style="font-size: 3vw" name="create"></ion-icon></button\n\n          ></ion-buttons>\n\n        </ion-col>\n\n        <ion-col col-1>\n\n          <ion-buttons class="div_btn_map">\n\n            <button\n\n              [disabled]="newpoi || editpoi || gopoi || newmap || qrdetection"\n\n              (mouseup)="scan($event)"\n\n              ion-button\n\n              color="warning"\n\n              class="btn_map"\n\n            >\n\n              <ion-icon style="font-size: 3vw" name="qr-scanner"></ion-icon></button\n\n          ></ion-buttons>\n\n        </ion-col>\n\n        <ion-col col-1>\n\n          <ion-buttons class="div_btn_map">\n\n            <button\n\n              [disabled]="newpoi || editpoi || gopoi || newmap || editmap"\n\n              ion-button\n\n              (mouseup)="onClickNewMap($event)"\n\n              class="btn_map"\n\n            >\n\n              <ion-icon style="font-size: 3vw" name="add"></ion-icon></button\n\n          ></ion-buttons>\n\n        </ion-col>\n\n\n\n        <ion-col col-3 style="background-color: rgb(182, 217, 230)">\n\n          <ion-item style="background-color: transparent">\n\n            <ion-label>\n\n              <h1\n\n                style="font-size: x-large"\n\n                [hidden]="newpoi || editpoi || gopoi || newmap || api.towardPOI || editmap"\n\n              >\n\n                {{param.datatext.POI}}\n\n              </h1>\n\n              <h1 style="font-size: x-large" [hidden]="!newpoi">\n\n                {{param.datatext.addPOI}}\n\n              </h1>\n\n              <h1 style="font-size: x-large" [hidden]="!editpoi">\n\n                {{param.datatext.editPOI}}\n\n              </h1>\n\n              <h1 style="font-size: x-large" [hidden]="!newmap">\n\n                {{param.datatext.addMap}}\n\n              </h1>\n\n              <h1 style="font-size: x-large" [hidden]="!editmap">\n\n                {{param.datatext.editMap}}\n\n              </h1>\n\n              <h1 style="font-size: x-large" [hidden]="!gopoi">\n\n                {{param.datatext.goToPOI}}\n\n              </h1>\n\n            </ion-label></ion-item\n\n          >\n\n        </ion-col>\n\n        <ion-col col-1 style="background-color: rgb(182, 217, 230)">\n\n          <ion-buttons class="div_btn_map">\n\n            <button\n\n              [hidden]="newpoi || editpoi || gopoi || api.towardPOI || newmap || editmap"\n\n              (mouseup)="onClickNewPOI($event)"\n\n              ion-button\n\n              class="btn_map"\n\n              *ngIf="api.id_current_map!=undefined"\n\n            >\n\n              <ion-icon style="font-size: 3vw" name="add"></ion-icon></button\n\n          ></ion-buttons>\n\n        </ion-col>\n\n      </ion-row>\n\n      <ion-row class="heightstyle80">\n\n        <ion-col\n\n          style="background-color: rgba(0, 0, 0, 0.904)"\n\n          col-8\n\n          class="colcanvas"\n\n        >\n\n          <ion-row class="rowcanvas">\n\n            <ion-col col-12 id="testcol4" class="heightstyle">\n\n              <canvas\n\n                #canvas4\n\n                [hidden]="qrdetection"\n\n                (wheel)="on_wheel($event);"\n\n                (mouseup)="on_mouse_up($event);"\n\n                (mousedown)="on_mouse_down($event);"\n\n                (mousemove)="on_mouse_move($event);"\n\n              ></canvas\n\n            >\n\n            \n\n            <canvas #videoCanvas style="display: none;"></canvas> \n\n            <img  [hidden]="!qrdetection || param.robot.cam_USB == 1" class="video" id="imgcompressed" />\n\n           \n\n            <img id="imgmap4" #imgmap4 src={{map_url}} crossorigin="anonymous"  style="display:none;" />\n\n            <video  [hidden]="!qrdetection || param.robot.cam_USB == 0" #video autoplay class="video"></video>\n\n          </ion-col>\n\n          </ion-row>\n\n          <ion-row class="btnsmap" [hidden]="qrdetection">\n\n            <ion-col col-2>\n\n              <button class="btnmap" (mouseup)="on_zoom_out($event)" ion-button>\n\n                <ion-icon name="search"></ion-icon> -\n\n              </button> </ion-col\n\n            ><ion-col col-2>\n\n              <button ion-button (mouseup)="on_zoom_in($event)" class="btnmap">\n\n                <ion-icon name="search"></ion-icon> +\n\n              </button>\n\n            </ion-col>\n\n            <ion-col col-2>\n\n              <button (mouseup)="on_click_goToPose($event)" ion-button class="btnmap">\n\n                {{param.datatext.btn_go}}\n\n              </button>\n\n            </ion-col>\n\n            <ion-col col-2>\n\n              <button\n\n                [hidden]="reloc"\n\n                ion-button\n\n                class="btnmap"\n\n                (mouseup)="on_click_relocate($event)"\n\n              >\n\n                {{param.datatext.reloc}}\n\n              </button>\n\n              <button\n\n                [hidden]="!reloc"\n\n                ion-button\n\n                color="secondary"\n\n                class="btnmap"\n\n                (mouseup)="on_click_relocate($event)"\n\n              >\n\n                {{param.datatext.confirm}}\n\n              </button>\n\n            </ion-col>\n\n            <ion-col col-2>\n\n              <button\n\n                [disabled]="!reloc"\n\n                class="btnmap"\n\n                (mouseup)="on_click_relocate_left($event)"\n\n                ion-button\n\n              >\n\n                <ion-icon name="undo"></ion-icon>\n\n              </button>\n\n            </ion-col>\n\n            <ion-col col-2>\n\n              <button\n\n                [disabled]="!reloc"\n\n                ion-button\n\n                (mouseup)="on_click_relocate_right($event)"\n\n                class="btnmap"\n\n              >\n\n                <ion-icon name="redo"></ion-icon>\n\n              </button>\n\n            </ion-col>\n\n          </ion-row>\n\n        </ion-col>\n\n        <ion-col\n\n          col-4\n\n          class="heightstyle"\n\n          style="background-color: rgb(182, 217, 230)"\n\n        >\n\n          <ion-content\n\n            [hidden]="newpoi || editpoi || gopoi || api.towardPOI || newmap || editmap"\n\n            style="height: 100%; width: 100%; background-color: rgb(182, 217, 230);"\n\n            class="scroll"\n\n            #content1\n\n          >\n\n            <ion-list style="background-color: transparent">\n\n              <ion-item-sliding\n\n                style="background-color: transparent"\n\n                (mouseup)="open($event,slidingItem)"\n\n                #slidingItem\n\n                *ngFor="let m of this.api.all_locations; let index = index;"\n\n              >\n\n                <ion-item\n\n                  #item\n\n                  *ngIf="m.Label!==\'qr\'"\n\n                  style="background-color: rgba(255, 255, 255, 0.76)"\n\n                >\n\n                  {{m.Id}}. {{m.Name}}\n\n                </ion-item>\n\n                <ion-item-options side="right">\n\n                  <button\n\n                    [disabled]="!api.adminMode"\n\n                    ion-button\n\n                    (mouseup)="popupDelete($event,\'POI\',m.Id)"\n\n                    style="min-width: 60px"\n\n                    color="danger"\n\n                  >\n\n                    <ion-icon name="trash"></ion-icon>\n\n                    {{param.datatext.delete}}\n\n                  </button>\n\n\n\n                  <button\n\n                    [disabled]="!api.adminMode"\n\n                    style="min-width: 60px"\n\n                    (mouseup)="onClickEditPOI($event,m.Name,m.Label,m.Id)"\n\n                    ion-button\n\n                    color="secondary"\n\n                  >\n\n                    <ion-icon name="create"></ion-icon>\n\n                    {{param.datatext.edit}}\n\n                  </button>\n\n                   <button\n\n                    style="min-width: 60px"\n\n                    (mouseup)="onClickGoPOI($event,m.Id,m.Name,m.Pose)"\n\n                    ion-button\n\n                    color="primary"\n\n                  >\n\n                    <ion-icon name="navigate"></ion-icon>\n\n                    {{param.datatext.btn_go}}\n\n                  </button>\n\n                </ion-item-options>\n\n              </ion-item-sliding>\n\n            </ion-list></ion-content\n\n          >\n\n\n\n          <div [hidden]="!newpoi || api.towardPOI" class="newpoistyle">\n\n            <ion-item>\n\n              <ion-label text-wrap>{{param.datatext.POIname}}</ion-label>\n\n              <ion-input type="text" [(ngModel)]="namepoi"></ion-input>\n\n            </ion-item>\n\n              <ion-item [hidden]="!api.slabel" >\n\n              <ion-label>Label : </ion-label>\n\n              <ion-select slot="end" okText="{{this.param.datatext.btn_ok}}"\n\n              cancelText="{{this.param.datatext.btn_cancel}}" [selectOptions]="selectLabel" class="myCustomSelect"\n\n              placeholder="Label" [(ngModel)]="SelectLabel"\n\n              (ionChange)="onLabelChange()" >\n\n              <ion-option value="none">none</ion-option>\n\n              <ion-option value="waypoint">waypoint</ion-option>\n\n              <ion-option value="lift_departure:1">lift_departure:1</ion-option>\n\n              <ion-option value="lift_arrival:1">lift_arrival:1</ion-option>\n\n              <ion-option value="lift_entry:1">lift_entry:1</ion-option>\n\n              <ion-option value="docking">docking</ion-option>\n\n            \n\n            </ion-select>\n\n            </ion-item>\n\n\n\n            <h2\n\n              text-wrap\n\n              style="\n\n                color: rgb(26, 156, 195);\n\n                margin-bottom: 30px;\n\n                margin-top: 30px;\n\n                text-align: left;\n\n              "\n\n            >\n\n              {{param.datatext.indicationAddPOI}}\n\n            </h2>\n\n\n\n            <ion-row>\n\n              <ion-col> </ion-col>\n\n              <ion-col>\n\n                <button\n\n                  (mouseup)="cancelpoi($event)"\n\n                  style="\n\n                    width: 100%;\n\n                    color: rgb(26, 156, 195);\n\n                    text-decoration: underline;\n\n                    font-size: x-large;\n\n                  "\n\n                  ion-button\n\n                  clear\n\n                >\n\n                  <div>{{param.datatext.btn_cancel}}</div>\n\n                </button>\n\n              </ion-col>\n\n              <ion-col align-self-center text-center>\n\n                <button\n\n                  [disabled]="namepoi===\'\'"\n\n                  (mouseup)="createpoi($event)"\n\n                  ion-button\n\n                  style="width: 100%; font-size: x-large"\n\n                >\n\n                  {{param.datatext.btn_save}}\n\n                </button>\n\n              </ion-col>\n\n            </ion-row>\n\n          </div>\n\n\n\n          <div [hidden]="!newmap || api.towardPOI" class="newpoistyle">\n\n            <form id="update_form" enctype="multipart/form-data">\n\n              <ion-item>\n\n                <ion-label text-wrap>{{param.datatext.mapName}}</ion-label>\n\n                <ion-input\n\n                  type="text"\n\n                  [(ngModel)]="namemap"\n\n                  name="name"\n\n                  required\n\n                ></ion-input>\n\n              </ion-item>\n\n              <ion-row>\n\n                <ion-col col-7>\n\n                  <input type="file" accept="image/png" id="mapBrowserFileInput" (change)="openfile($event)" style="display: none" />\n\n                  <button\n\n                    (mouseup)="openfileBrowser($event)"\n\n                    style="\n\n                      width: 100%;\n\n                      height: 100%;\n\n                      font-size: large;\n\n                      background-color: gray;\n\n                    "\n\n                    ion-button\n\n                  >\n\n                    <div>{{param.datatext.map_selection}}</div>\n\n                  </button>\n\n                  \n\n                </ion-col>\n\n                <ion-col col-5>\n\n                  <ion-item style="height: 100%"\n\n                    ><ion-label [hidden]="filen" style="font-size: small">\n\n                      {{param.datatext.nofile}}\n\n                    </ion-label>\n\n                    <ion-label [hidden]="!filen" style="font-size: small">\n\n                      {{param.datatext.map_loaded}}\n\n                    </ion-label>\n\n                  </ion-item>\n\n                </ion-col>\n\n              </ion-row>\n\n\n\n            \n\n\n\n              <h2\n\n                *ngIf="!api.mapok"\n\n                text-wrap\n\n                style="\n\n                  color: rgb(195, 26, 26);\n\n                  margin-bottom: 40px;\n\n                  margin-top: 40px;\n\n                  text-align: left;\n\n                "\n\n              >\n\n                {{param.datatext.colorIndication}}\n\n              </h2>\n\n\n\n              <ion-row>\n\n                <ion-col> </ion-col>\n\n                <ion-col> </ion-col>\n\n                <ion-col> </ion-col>\n\n              </ion-row>\n\n\n\n              <ion-row>\n\n                <ion-col> </ion-col>\n\n                <ion-col>\n\n                  <button\n\n                    (mouseup)="cancelmap($event)"\n\n                    style="\n\n                      width: 100%;\n\n                      color: rgb(26, 156, 195);\n\n                      text-decoration: underline;\n\n                      font-size: x-large;\n\n                    "\n\n                    ion-button\n\n                    clear\n\n                  >\n\n                    <div>{{param.datatext.btn_cancel}}</div>\n\n                  </button>\n\n                </ion-col>\n\n                <ion-col align-self-center text-center>\n\n                  <button\n\n                    [disabled]="!loadimg || namemap===\'\'"\n\n                    (mouseup)="addMap($event)"\n\n                    ion-button\n\n                    style="width: 100%; font-size: x-large"\n\n                  >\n\n                    {{param.datatext.btn_save}}\n\n                  </button>\n\n                </ion-col>\n\n              </ion-row>\n\n            </form>\n\n          </div>\n\n\n\n          <div [hidden]="!editmap || api.towardPOI" class="newpoistyle">\n\n            <form id="update_formul" enctype="multipart/form-data">\n\n              <ion-item>\n\n                <ion-label text-wrap>{{param.datatext.mapName}}</ion-label>\n\n                <ion-input\n\n                  type="text"\n\n                  [(ngModel)]="nameeditmap"\n\n                  name="name"\n\n                ></ion-input>\n\n              </ion-item>\n\n\n\n              <ion-row>\n\n                <ion-col col-7>\n\n                  <button\n\n                    (mouseup)="openfileBrowser($event)"\n\n                    style="\n\n                      width: 100%;\n\n                      height: 100%;\n\n                      font-size: large;\n\n                      background-color: gray;\n\n                    "\n\n                    ion-button\n\n                  >\n\n                    <div>{{param.datatext.map_selection}}</div>\n\n                  </button>\n\n                  \n\n                </ion-col>\n\n                <ion-col col-5>\n\n                  <ion-item style="height: 100%"\n\n                    ><ion-label [hidden]="filen" style="font-size: small">\n\n                      {{param.datatext.nofile}}\n\n                    </ion-label>\n\n                    <ion-label [hidden]="!filen" style="font-size: small">\n\n                      {{param.datatext.map_loaded}}\n\n                    </ion-label>\n\n                  </ion-item>\n\n                </ion-col>\n\n              </ion-row>\n\n              <h2\n\n                *ngIf="!api.mapok"\n\n                text-wrap\n\n                style="\n\n                  color: rgb(195, 26, 26);\n\n                  margin-bottom: 40px;\n\n                  margin-top: 40px;\n\n                  text-align: left;\n\n                "\n\n              >\n\n                {{param.datatext.colorIndication}}\n\n              </h2>\n\n              <ion-row>\n\n                <ion-col> </ion-col>\n\n                <ion-col> </ion-col>\n\n                <ion-col> </ion-col>\n\n              </ion-row>\n\n\n\n              <ion-row>\n\n                <ion-col> </ion-col>\n\n                <ion-col>\n\n                  <button\n\n                    (mouseup)="cancelmap($event)"\n\n                    style="\n\n                      width: 100%;\n\n                      color: rgb(26, 156, 195);\n\n                      text-decoration: underline;\n\n                      font-size: x-large;\n\n                    "\n\n                    ion-button\n\n                    clear\n\n                  >\n\n                    <div>{{param.datatext.btn_cancel}}</div>\n\n                  </button>\n\n                </ion-col>\n\n                <ion-col align-self-center text-center>\n\n                  <button\n\n                    [disabled]="nameeditmap===\'\'"\n\n                    (mouseup)="editMap($event)"\n\n                    ion-button\n\n                    style="width: 100%; font-size: x-large"\n\n                  >\n\n                    {{param.datatext.btn_save}}\n\n                  </button>\n\n                </ion-col>\n\n              </ion-row>\n\n            </form>\n\n          </div>\n\n\n\n          <div [hidden]="!editpoi || api.towardPOI" class="newpoistyle">\n\n            <ion-item>\n\n              <ion-label text-wrap>{{param.datatext.POIname}}</ion-label>\n\n              <ion-input type="text" [(ngModel)]="editnamepoi"></ion-input>\n\n            </ion-item>\n\n\n\n\n\n            <ion-item [hidden]="!api.slabel" >\n\n              <ion-label>Label : </ion-label>\n\n              <ion-select slot="end" okText="{{this.param.datatext.btn_ok}}"\n\n              cancelText="{{this.param.datatext.btn_cancel}}" [selectOptions]="selectLabel" class="myCustomSelect"\n\n              placeholder="Label" [(ngModel)]="SelectLabel"\n\n              (ionChange)="onLabelChange()" >\n\n              <ion-option value="none">none</ion-option>\n\n              <ion-option value="waypoint">waypoint</ion-option>\n\n              <ion-option value="lift_departure:1">lift_departure:1</ion-option>\n\n              <ion-option value="lift_arrival:1">lift_arrival:1</ion-option>\n\n              <ion-option value="lift_entry:1">lift_entry:1</ion-option>\n\n              <ion-option value="docking">docking</ion-option>\n\n            \n\n            </ion-select>\n\n            </ion-item>\n\n            \n\n            <ion-item>\n\n              <ion-label>{{param.datatext.relocate}}</ion-label>\n\n              <ion-checkbox\n\n                [(ngModel)]="resetposition"\n\n                color="primary"\n\n                (mouseup)="isChecked = !isChecked"\n\n                [checked]="isChecked"\n\n              ></ion-checkbox>\n\n            </ion-item>\n\n            <h2\n\n              [hidden]="!resetposition"\n\n              text-wrap\n\n              style="\n\n                color: rgb(26, 156, 195);\n\n                margin-bottom: 50px;\n\n                margin-top: 50px;\n\n                text-align: left;\n\n              "\n\n            >\n\n              {{param.datatext.indicationAddPOI}}\n\n            </h2>\n\n\n\n            <ion-row>\n\n              <ion-col> </ion-col>\n\n              <ion-col>\n\n                <button\n\n                  (mouseup)="canceleditpoi($event)"\n\n                  style="\n\n                    width: 100%;\n\n                    color: rgb(26, 156, 195);\n\n                    text-decoration: underline;\n\n                    font-size: x-large;\n\n                  "\n\n                  ion-button\n\n                  clear\n\n                >\n\n                  <div>{{param.datatext.btn_cancel}}</div>\n\n                </button>\n\n              </ion-col>\n\n              <ion-col align-self-center text-center>\n\n                <button\n\n                  [disabled]="editnamepoi===\'\'"\n\n                  (mouseup)="editPoi($event)"\n\n                  ion-button\n\n                  style="width: 100%; font-size: x-large"\n\n                >\n\n                  {{param.datatext.btn_save}}\n\n                </button>\n\n              </ion-col>\n\n            </ion-row>\n\n          </div>\n\n\n\n          <div [hidden]="!gopoi || api.towardPOI" class="newpoistyle">\n\n            <h1 text-wrap>{{namepoi}}</h1>\n\n            <ion-row>\n\n              <ion-col col-7>\n\n                <ion-item style="height: 100%">\n\n                  <ion-label>{{param.datatext.speedOption}}</ion-label>\n\n                  <ion-input\n\n                    type="number"\n\n                    min="1"\n\n                    max="100"\n\n                    step="5"\n\n                    [(ngModel)]="speed"\n\n                  ></ion-input>\n\n                </ion-item>\n\n              </ion-col>\n\n              <ion-col col-5>\n\n                <ion-item style="height: 100%"\n\n                  ><ion-label style="font-size: small">\n\n                    Vmax = 50 cm/s\n\n                  </ion-label></ion-item\n\n                >\n\n              </ion-col>\n\n            </ion-row>\n\n            <ion-item>\n\n              <ion-label>{{param.datatext.directionOption}}</ion-label>\n\n              <ion-select #select2\n\n                interface="popover"\n\n                [(ngModel)]="waygo"\n\n                okText="{{this.param.datatext.btn_ok}}"\n\n                cancelText="{{this.param.datatext.btn_cancel}}"\n\n                (mouseup)="onSliderRelease($event,select2)"\n\n              >\n\n                <ion-option value="1"\n\n                  >{{param.datatext.frontOption}}</ion-option\n\n                >\n\n                <ion-option value="-1"\n\n                  >{{param.datatext.backOption}}</ion-option\n\n                >\n\n              </ion-select>\n\n            </ion-item>\n\n            <ion-item [hidden]="waygo==-1">\n\n              <ion-label>{{param.datatext.avoidanceOption}}</ion-label>\n\n              <ion-toggle [(ngModel)]="avoidance"></ion-toggle>\n\n            </ion-item>\n\n\n\n            <ion-row>\n\n              <ion-col> </ion-col>\n\n              <ion-col>\n\n                <button\n\n                  (mouseup)="cancelgopoi($event)"\n\n                  style="\n\n                    width: 100%;\n\n                    color: rgb(26, 156, 195);\n\n                    text-decoration: underline;\n\n                    font-size: x-large;\n\n                  "\n\n                  ion-button\n\n                  clear\n\n                >\n\n                  <div>{{param.datatext.btn_cancel}}</div>\n\n                </button>\n\n              </ion-col>\n\n              <ion-col align-self-center text-center>\n\n                <button\n\n                  (mouseup)="ReachPoi($event)"\n\n                  ion-button\n\n                  style="width: 100%; font-size: x-large"\n\n                >\n\n                  {{param.datatext.btn_go}}\n\n                </button>\n\n              </ion-col>\n\n            </ion-row>\n\n          </div>\n\n\n\n          <ion-buttons class="div_btn_go" [hidden]="!api.towardPOI">\n\n            <button ion-button color="danger" (mouseup)="abort($event)" class="btn_go">\n\n              <ion-icon color="light" name="hand" class="icon_style">\n\n                <p class="legende">{{this.param.datatext.btn_stop}}</p>\n\n              </ion-icon>\n\n            </button>\n\n          </ion-buttons>\n\n        </ion-col>\n\n      </ion-row>\n\n      <ion-row class="lastrow">\n\n       \n\n        <ion-col col-4 *ngIf="api.elevator">\n\n          <button\n\n            [disabled]="reloc || !api.current_map"\n\n            ion-button\n\n            class="btnchangepage"\n\n            (mouseup)="goLiftPage($event)"\n\n            style="font-size: x-large"\n\n          >\n\n            LIFT\n\n          </button>\n\n        </ion-col>\n\n        <ion-col col-4 *ngIf="!api.elevator">\n\n          <button\n\n            [disabled]="reloc || !api.current_map || !api.adminMode"\n\n            ion-button\n\n            class="btnchangepage"\n\n            (mouseup)="dockingPage($event)"\n\n            style="font-size: x-large"\n\n          >\n\n            DOCKING\n\n          </button>\n\n        </ion-col>\n\n\n\n        <ion-col col-4>\n\n          <button\n\n            [disabled]="reloc || !api.current_map"\n\n            ion-button\n\n            class="btnchangepage"\n\n            (mouseup)="roundPage($event)"\n\n            style="font-size: x-large"\n\n          >\n\n            {{param.datatext.rounds}}\n\n          </button>\n\n        </ion-col>\n\n        <ion-col col-4>\n\n          <button\n\n            [disabled]="reloc || !api.current_map"\n\n            class="btnchangepage"\n\n            (mouseup)="qrPage($event)"\n\n            ion-button\n\n            style="font-size: x-large"\n\n          >\n\n            {{param.datatext.qrcodes}}\n\n          </button>\n\n        </ion-col>\n\n      </ion-row>\n\n    </ion-grid>\n\n \n\n</ion-content>\n\n'/*ion-inline-end:"C:\Users\laele\Documents\KomApp\toolbox\src\pages\poi\poi.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_core__["W" /* Renderer2 */],
            __WEBPACK_IMPORTED_MODULE_6_ionic_angular__["h" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_6_ionic_angular__["g" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_6_ionic_angular__["k" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_3__services_popup_service__["a" /* PopupService */],
            __WEBPACK_IMPORTED_MODULE_1__services_api_service__["a" /* ApiService */],
            __WEBPACK_IMPORTED_MODULE_5__services_speech_service__["a" /* SpeechService */],
            __WEBPACK_IMPORTED_MODULE_4__services_param_service__["a" /* ParamService */],
            __WEBPACK_IMPORTED_MODULE_2__services_alert_service__["a" /* AlertService */] //private alertCtrl: AlertController
        ])
    ], PoiPage);
    return PoiPage;
}());

//# sourceMappingURL=poi.js.map

/***/ }),

/***/ 142:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RoundCreationPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_param_service__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_api_service__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_alert_service__ = __webpack_require__(21);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var RoundCreationPage = /** @class */ (function () {
    function RoundCreationPage(param, navCtrl, toastCtrl, loadingCtrl, api, alert) {
        this.param = param;
        this.navCtrl = navCtrl;
        this.toastCtrl = toastCtrl;
        this.loadingCtrl = loadingCtrl;
        this.api = api;
        this.alert = alert;
        this.roundCreationPage = RoundCreationPage_1;
        this.map_url = "http://" + this.param.localhost + "/api/maps/current/image?t=" + new Date().getTime();
        this.start = {
            x: 0,
            y: 0,
        };
        this.poiWait = 0;
        this.poiWay = 1;
        this.poiSpeed = 80;
        this.poiData = "none";
        this.nameRound = "";
        this.poiAnticollisionProfile = 0;
        this.poiDisableAvoidance = false; //avoidance activated
        if (this.api.copyRound) {
            this.nameRound = this.api.copy_round.Name + "-copy";
            this.displayCopy();
        }
        if (this.api.editRound) {
            this.api.textHeadBand = this.param.datatext.rounds_edit;
            this.nameRound = this.api.copy_round.Name;
        }
        this.api.all_locations = this.api.all_locations.filter(function (x) { return x.Label !== "qr" && x.Name !== "docking" && x.Name !== "docked"; });
    }
    RoundCreationPage_1 = RoundCreationPage;
    RoundCreationPage.prototype.ngOnInit = function () {
        this.creation = "carte";
        this._CANVAS = this.canvasEl.nativeElement;
        this._CONTEXT = this._CANVAS.getContext("2d");
        this._CANVAS.style.width = "100%";
        this._CANVAS.style.height = "100%";
        this.heightcol = document.getElementById("body3").clientHeight - 100;
        this.widthcol = document.getElementById("testcol3").clientWidth;
    };
    RoundCreationPage.prototype.ionViewDidEnter = function () {
        if (this.api.editRound) {
            this.api.textHeadBand = this.param.datatext.rounds_edit;
        }
        this.getImageFromService();
    };
    RoundCreationPage.prototype.open = function (ev, itemSlide) {
        ev.preventDefault();
        if (itemSlide.getSlidingPercent() == 0) {
            // Two calls intentional after examination of vendor.js function
            itemSlide.moveSliding(-170);
            itemSlide.moveSliding(-170);
        }
        else {
            itemSlide.close();
        }
    };
    RoundCreationPage.prototype.on_zoom_out = function (ev) {
        ev.preventDefault();
        if (!(this.resomap >= this.initialzoom)) {
            this.resomap = this.resomap + 150 / 100000;
        }
        else {
            this.offsetX = 0;
            this.offsetY = 0;
        }
        this.createImageFromBlob();
    };
    RoundCreationPage.prototype.on_zoom_in = function (ev) {
        ev.preventDefault();
        if (this.resomap > this.initialzoom / 4) {
            this.resomap = this.resomap - 150 / 100000;
        }
        this.createImageFromBlob();
    };
    RoundCreationPage.prototype.ionViewWillLeave = function () {
        //clearInterval(this.interv);
    };
    RoundCreationPage.prototype.displayCopy = function () {
        var _this = this;
        this.api.copy_round.Locations.forEach(function (element) {
            _this.api.createRoundList(element.Location.Id, element.Location.Name, element.Wait, element.Speed, element.Data, element.AnticollisionProfile, element.DisableAvoidance);
        });
    };
    RoundCreationPage.prototype.onWayChange = function (event) {
        console.log(event);
        this.poiWay = parseInt(event);
        console.log(this.poiWay);
    };
    RoundCreationPage.prototype.onGabaritChange = function () {
        console.log(this.poiAnticollisionProfile);
    };
    RoundCreationPage.prototype.onOptionChange = function (event) {
        console.log(event);
        this.poiData = event;
        console.log(this.poiData);
    };
    RoundCreationPage.prototype.onToggleChange = function () {
        this.poiDisableAvoidance = !this.poiDisableAvoidance;
        console.log(this.poiDisableAvoidance);
    };
    RoundCreationPage.prototype.onPOIChange = function (event) {
        this.poiId = event;
        this.poiName = this.api.all_locations.filter(function (poi) { return poi.Id == event; })[0].Name;
        console.log(this.poiName);
        console.log(this.poiId);
    };
    RoundCreationPage.prototype.addPOIRound = function (ev) {
        var _this = this;
        ev.preventDefault();
        this.poiWait = Math.abs(this.poiWait);
        if (Math.abs(this.poiSpeed) > 100) {
            this.poiSpeed = 100;
        }
        console.log(this.poiWait);
        console.log(this.poiSpeed);
        console.log(this.poiWay);
        this.api.createRoundList(this.poiId, this.poiName, this.poiWait, (Math.abs(this.poiSpeed) * this.poiWay) / 100, this.poiData, this.poiAnticollisionProfile, this.poiDisableAvoidance);
        setTimeout(function () {
            _this.content1.scrollToBottom();
        }, 300);
    };
    RoundCreationPage.prototype.cancelCreation = function (ev) {
        ev.preventDefault();
        this.navCtrl.pop();
        this.api.roundpoi_list = [];
        if (this.api.editRound) {
            this.displayCopy();
        }
    };
    RoundCreationPage.prototype.reorderItems = function (indexes) {
        var element = this.api.roundpoi_list[indexes.from];
        this.api.roundpoi_list.splice(indexes.from, 1);
        this.api.roundpoi_list.splice(indexes.to, 0, element);
    };
    RoundCreationPage.prototype.createRound = function (ev) {
        var _this = this;
        this.alert.addround(this.api.mailAddInformationBasic());
        ev.preventDefault();
        this.nameRound = this.nameRound.replace(new RegExp("[<>&$§µ£#*_|`¤~(){}°²@%/]", "g"), "");
        this.nameRound = this.nameRound.replace(new RegExp('["]', "g"), "");
        this.nameRound = this.nameRound.replace(new RegExp(/\\/, "g"), "");
        this.nameRound = this.nameRound.replace(new RegExp(/[\[\]]/, "g"), "");
        if (this.nameRound != "") {
            this.showLoading();
            if (!this.api.editRound && this.api.roundpoi_list.length > 1) {
                this.api.postRound(this.nameRound);
                console.log("post");
            }
            else {
                this.api.copy_round.Name = this.nameRound;
                this.api.putRound(this.nameRound, this.api.copy_round.Id);
            }
            console.log(this.nameRound);
            setTimeout(function () {
                _this.api.getRoundList();
            }, 2000);
            setTimeout(function () {
                if (_this.api.editRound) {
                    _this.api.copy_round = _this.api.round_current_map.filter(function (x) { return x.Id == _this.api.copy_round.Id; })[0];
                }
                _this.dismissLoading();
                _this.api.smgthCreated = true;
                _this.navCtrl.pop();
            }, 3500);
        }
    };
    RoundCreationPage.prototype.deletePOI = function (ev, index) {
        ev.preventDefault();
        this.api.roundpoi_list.splice(index, 1);
    };
    RoundCreationPage.prototype.on_mouse_down = function (evt) {
        this.createImageFromBlob();
        //console.log(evt);
        if (evt.button == 0) {
            evt.preventDefault();
            this.moving = true;
            this.start.x = evt.x;
            this.start.y = evt.y;
            //console.log(evt.x + " " + this.start.x);
        }
    };
    RoundCreationPage.prototype.on_mouse_up = function (evt) {
        this.createImageFromBlob();
        //console.log(evt);
        if (this.moving) {
            var offset = {
                x: this.offsetX,
                y: this.offsetY,
                t: 0,
            };
            offset.x += evt.x - this.start.x;
            offset.y += evt.y - this.start.y;
            this.start.x = evt.x;
            this.start.y = evt.y;
            this.offsetX = offset.x;
            this.offsetY = offset.y;
        }
        this.moving = false;
    };
    RoundCreationPage.prototype.on_mouse_move = function (evt) {
        if (this.moving) {
            this.createImageFromBlob();
            evt.preventDefault();
            var offset = {
                x: this.offsetX,
                y: this.offsetY,
            };
            //console.log(evt.x + " " + this.start.x);
            offset.x += evt.x - this.start.x;
            offset.y += evt.y - this.start.y;
            this.start.x = evt.x;
            this.start.y = evt.y;
            this.offsetX = offset.x;
            this.offsetY = offset.y;
        }
    };
    RoundCreationPage.prototype.on_wheel = function (evt) {
        this.createImageFromBlob();
        evt.preventDefault();
        //console.log(evt.deltaY);
        if (evt.deltaY > 0) {
            if (!(this.resomap >= this.initialzoom)) {
                this.resomap = this.resomap + 150 / 100000;
            }
            else {
                this.offsetX = 0;
                this.offsetY = 0;
            }
        }
        else {
            if (this.resomap > this.initialzoom / 4) {
                this.resomap = this.resomap - 150 / 100000;
            }
        }
        console.log(this.resomap);
    };
    RoundCreationPage.prototype.showLoading = function () {
        if (!this.loading) {
            this.loading = this.loadingCtrl.create({
                content: this.param.datatext.wait,
            });
            this.loading.present();
        }
    };
    RoundCreationPage.prototype.dismissLoading = function () {
        if (this.loading) {
            this.loading.dismiss();
            this.loading = null;
        }
    };
    RoundCreationPage.prototype.createImageFromBlob = function () {
        var _this = this;
        if (this._CANVAS.getContext) {
            this._CANVAS.width = this.imge.width;
            this._CANVAS.height = this.imge.height;
            var w = this._CANVAS.width;
            var h = this._CANVAS.height;
            var r = this.resomap;
            var x = this.offsetX;
            var y = this.offsetY;
            var xm = this.api.mapdata.Offset.X;
            var ym = this.api.mapdata.Offset.Y;
            var rm = this.api.mapdata.Resolution;
            var wm = this.api.mapdata.Width;
            var hm = this.api.mapdata.Height;
            var dx = xm / r + x;
            var dy = h - (ym + hm * rm) * (1 / r) + y;
            var dw = (wm * rm) / r;
            var dh = (hm * rm) / r;
            this._CONTEXT.drawImage(this.imge, dx, dy, dw, dh);
            //console.log('dessin image');
            if (this.api.all_locations != undefined) {
                this.api.all_locations.forEach(function (evenement) {
                    if (evenement.Label !== "qr") {
                        _this._CONTEXT.beginPath();
                        _this._CONTEXT.font =
                            "" + Math.round(_this._CANVAS.width * 0.025) + "px Arial";
                        //console.log(evenement);
                        _this._CONTEXT.fillStyle = "rgba(26, 156, 195,0.7)";
                        _this._CONTEXT.fillText(evenement.Id, //+ "- " + evenement.Name,
                        evenement.Pose.X / _this.resomap + _this.offsetX - 20, _this._CANVAS.height -
                            (evenement.Pose.Y / _this.resomap - _this.offsetY) -
                            10);
                        if (evenement.Name === "docking" || evenement.Name === "docked") {
                            _this._CONTEXT.fillText(evenement.Id + "- " + evenement.Name, evenement.Pose.X / _this.resomap + _this.offsetX - 20, _this._CANVAS.height -
                                (evenement.Pose.Y / _this.resomap - _this.offsetY) -
                                10);
                        }
                        _this._CONTEXT.beginPath();
                        //console.log(evenement.Pose)
                        _this._CONTEXT.arc(evenement.Pose.X / _this.resomap + _this.offsetX, _this._CANVAS.height -
                            (evenement.Pose.Y / _this.resomap - _this.offsetY), _this._CANVAS.width * 0.005, 0, 2 * Math.PI);
                        _this._CONTEXT.lineWidth = 1;
                        _this._CONTEXT.fillStyle = "rgb(26, 156, 195)";
                        _this._CONTEXT.fill();
                        _this._CONTEXT.beginPath();
                        _this._CONTEXT.save();
                        _this._CONTEXT.translate(evenement.Pose.X / _this.resomap + _this.offsetX, _this._CANVAS.height -
                            (evenement.Pose.Y / _this.resomap - _this.offsetY));
                        _this._CONTEXT.rotate(-evenement.Pose.T);
                        _this._CONTEXT.moveTo(0, 0);
                        _this._CONTEXT.lineWidth = 2;
                        _this._CONTEXT.lineTo(2 * _this._CANVAS.width * 0.009, 0);
                        _this._CONTEXT.strokeStyle = "rgb(26, 156, 195)";
                        _this._CONTEXT.stroke();
                        _this._CONTEXT.restore();
                    }
                });
            }
        }
    };
    RoundCreationPage.prototype.onSliderRelease = function (ev, id) {
        ev.preventDefault();
        id.open();
    };
    RoundCreationPage.prototype.getImageFromService = function () {
        var _this = this;
        this.imge = document.getElementById("imgmap3");
        if (this.api.id_current_map != undefined) {
            this.offsetX = this.api.mapdata.Offset.X;
            this.offsetY = this.api.mapdata.Offset.Y;
            this.resomap = this.api.mapdata.Resolution;
            this.initialzoom = this.resomap;
        }
        console.log("GetImageFromService");
        this.api.getImgMapHttp().subscribe(function (data) {
            _this.api.imgMap = data;
            _this.createImageFromBlob();
            console.log("GetImageFromService");
        }, function (error) {
            console.log(error);
        });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])("canvas3"),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* ElementRef */])
    ], RoundCreationPage.prototype, "canvasEl", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])("imgmap3"),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* ElementRef */])
    ], RoundCreationPage.prototype, "imgmapElement", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])("content1"),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["c" /* Content */])
    ], RoundCreationPage.prototype, "content1", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])("select1"),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["j" /* Select */])
    ], RoundCreationPage.prototype, "select1", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])("select2"),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["j" /* Select */])
    ], RoundCreationPage.prototype, "select2", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])("select3"),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["j" /* Select */])
    ], RoundCreationPage.prototype, "select3", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])("select4"),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["j" /* Select */])
    ], RoundCreationPage.prototype, "select4", void 0);
    RoundCreationPage = RoundCreationPage_1 = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: "page-roundcreation",template:/*ion-inline-start:"C:\Users\laele\Documents\KomApp\toolbox\src\pages\roundcreation\roundcreation.html"*/'<!-- mail page html template -->\n\n<ion-header no-border>\n\n  <headpage pageName="roundcreation"></headpage>\n\n</ion-header>\n\n\n\n<ion-content id="body3">\n\n\n\n  <ion-grid class="heightstyle">\n\n    <ion-row class="heightstyle10" id="firstrow3">\n\n      <ion-col col-5>\n\n        <ion-item>\n\n          <ion-label fixed>{{param.datatext.name}}</ion-label>\n\n          <ion-input type="text" [(ngModel)]="nameRound"></ion-input>\n\n        </ion-item>\n\n      </ion-col>\n\n      <ion-col col-3></ion-col>\n\n      <ion-col col-2 style="background-color: rgb(182, 217, 230)">\n\n        <button style="\n\n              width: 100%;\n\n              color: rgb(26, 156, 195);\n\n              text-decoration: underline;\n\n              font-size: x-large;\n\n            " ion-button clear class="heightstyle" (mouseup)="cancelCreation($event)">\n\n          {{param.datatext.btn_cancel}}\n\n        </button>\n\n      </ion-col>\n\n      <ion-col col-2 style="background-color: rgb(182, 217, 230)">\n\n        <button [disabled]="this.nameRound.length ==0 || !(this.api.roundpoi_list.length>1)" class="heightstyle"\n\n          ion-button (mouseup)="createRound($event)" style="width: 100%; font-size: x-large">\n\n          {{param.datatext.btn_save}}\n\n        </button>\n\n      </ion-col>\n\n    </ion-row>\n\n    <ion-row class="heightstyle90">\n\n      <ion-col col-8 id="testcol3" class="heightstyle">\n\n        <ion-segment class="rowsegment" id="test" [(ngModel)]="creation">\n\n          <ion-segment-button value="carte" style="font-size: x-large">\n\n            {{param.datatext.map}}\n\n          </ion-segment-button>\n\n          <ion-segment-button value="poi" style="font-size: x-large">\n\n            {{param.datatext.addPOIfunction}}\n\n          </ion-segment-button>\n\n        </ion-segment>\n\n\n\n        <ion-row *ngIf="creation===\'poi\'" class="rowoptions">\n\n          <ion-row class="optrow">\n\n            <ion-col col-6>\n\n              <ion-item style="height: 100%">\n\n                <ion-label>{{param.datatext.POIselected}}</ion-label>\n\n                <ion-select #select1 interface="popover" [(ngModel)]="poi_added" okText="{{this.param.datatext.btn_ok}}"\n\n                  cancelText="{{this.param.datatext.btn_cancel}}" (ionChange)="onPOIChange($event)"\n\n                  (mouseup)="onSliderRelease($event,select1)">\n\n                  <ion-option *ngFor="let m of this.api.all_locations" value="{{m.Id}}">{{m.Id}}.\n\n                    {{m.Name}}</ion-option>\n\n                </ion-select>\n\n              </ion-item>\n\n\n\n            </ion-col>\n\n            <ion-col col-6>\n\n              <ion-item style="height: 100%">\n\n                <ion-label>{{param.datatext.breakOption}}</ion-label>\n\n                <ion-input type="number" min="0" max="1000000" step="10" placeholder="0"\n\n                  [(ngModel)]="poiWait"></ion-input>\n\n              </ion-item>\n\n\n\n            </ion-col>\n\n          </ion-row>\n\n\n\n\n\n          <ion-row class="optrow">\n\n            <ion-col col-7>\n\n              <ion-item style="height: 100%">\n\n                <ion-label>{{param.datatext.speedOption}}</ion-label>\n\n                <ion-input type="number" min="1" max="100" step="5" placeholder="80" [(ngModel)]="poiSpeed"></ion-input>\n\n              </ion-item></ion-col>\n\n            <ion-col col-5>\n\n              <ion-item style="height: 100%"><ion-label> Vmax = 50 cm/s </ion-label></ion-item>\n\n            </ion-col>\n\n          </ion-row>\n\n          <ion-row class="optrow">\n\n            <ion-item [hidden]="!api.soptiondesinfection">\n\n              <ion-label>{{param.datatext.directionOption}}</ion-label>\n\n              <ion-select #select2 interface="popover" okText="{{this.param.datatext.btn_ok}}"\n\n                cancelText="{{this.param.datatext.btn_cancel}}" (ionChange)="onWayChange($event)" [(ngModel)]="poiWay"\n\n                (mouseup)="onSliderRelease($event,select2)">\n\n                <ion-option selected value="1">{{param.datatext.frontOption}}</ion-option>\n\n                <ion-option value="-1">{{param.datatext.backOption}}</ion-option>\n\n              </ion-select></ion-item>\n\n            <ion-col col-6>\n\n              <ion-item style="height: 100%">\n\n                <ion-label>{{param.datatext.avoidanceOption}}</ion-label>\n\n                <ion-toggle [checked]="!this.poiDisableAvoidance" (ionChange)="onToggleChange()"></ion-toggle>\n\n              </ion-item>\n\n\n\n            </ion-col>\n\n            <ion-col col-6>\n\n              <ion-item style="height: 100%">\n\n                <ion-label>{{param.datatext.gabarit}}</ion-label>\n\n                <ion-select #select3 [(ngModel)]="poiAnticollisionProfile" interface="popover"\n\n                  okText="{{this.param.datatext.btn_ok}}" cancelText="{{this.param.datatext.btn_cancel}}"\n\n                  (ionChange)="onGabaritChange()" (mouseup)="onSliderRelease($event,select3)">\n\n                  <ion-option *ngFor="let m of [0,1,2,3,4]" value="{{m}}">{{m}}</ion-option>\n\n                </ion-select>\n\n              </ion-item>\n\n            </ion-col>\n\n          </ion-row>\n\n\n\n\n\n\n\n          <ion-item [hidden]="!api.soptiondesinfection">\n\n            <ion-label>{{param.datatext.options}}</ion-label>\n\n            <ion-select #select4 interface="popover" okText="{{this.param.datatext.btn_ok}}"\n\n              cancelText="{{this.param.datatext.btn_cancel}}" (ionChange)="onOptionChange($event)" [(ngModel)]="poiData"\n\n              (mouseup)="onSliderRelease($event,select4)">\n\n              <ion-option selected value="none">{{param.datatext.none}}</ion-option>\n\n              <ion-option value="lampon">{{param.datatext.lampon}}</ion-option>\n\n              <ion-option value="lampoff">{{param.datatext.lampoff}}</ion-option>\n\n              <ion-option value="lampoff+">{{param.datatext.lampoff1}}</ion-option>\n\n            </ion-select>\n\n          </ion-item>\n\n\n\n        </ion-row>\n\n        <ion-row *ngIf="creation===\'poi\'" class="btnsmap">\n\n          <ion-buttons class="div_btn_map">\n\n            <button [disabled]="!poi_added" [hidden]="newpoi || editpoi || gopoi || api.towardPOI || newmap || editmap"\n\n              (mouseup)="addPOIRound($event)" ion-button class="btnadd" *ngIf="api.id_current_map!=undefined">\n\n              <ion-icon style="font-size: 3vw" name="add"></ion-icon></button></ion-buttons>\n\n        </ion-row>\n\n        <ion-row [hidden]="creation===\'poi\'" class="rowcanvas">\n\n          <canvas #canvas3 style="\n\n                background-color: rgba(0, 0, 0, 0.904);\n\n               \n\n              " (wheel)="on_wheel($event);" (mouseup)="on_mouse_up($event);" (mousedown)="on_mouse_down($event);"\n\n            (mousemove)="on_mouse_move($event);"></canvas>\n\n          <img id="imgmap3" #imgmap3 src={{map_url}} crossorigin="anonymous" class="video" style="display:none;" />\n\n        </ion-row><ion-row style="background-color: rgba(0, 0, 0, 0.904)" [hidden]="creation===\'poi\'" class="btnsmap">\n\n          <ion-col col-6>\n\n            <button class="btnmap" (mouseup)="on_zoom_out($event)" ion-button>\n\n              <ion-icon name="search"></ion-icon> -\n\n            </button> </ion-col><ion-col col-6>\n\n            <button ion-button (mouseup)="on_zoom_in($event)" class="btnmap">\n\n              <ion-icon name="search"></ion-icon> +\n\n            </button>\n\n          </ion-col>\n\n        </ion-row>\n\n      </ion-col>\n\n      <ion-col col-4 style="background-color: rgb(182, 217, 230)">\n\n        <ion-label style="font-size: x-large">{{param.datatext.POI_list}}</ion-label>\n\n\n\n        <ion-content #content1 class="scroll" style="height: 90%; width: 100%; background-color: rgb(182, 217, 230)">\n\n          <ion-list style="background-color: transparent" reorder="true" (ionItemReorder)="reorderItems($event)">\n\n            <ion-item-sliding (mouseup)="open($event,slidingItem)" #slidingItem style="background-color: transparent"\n\n              *ngFor="let m of this.api.roundpoi_list; let index = index;">\n\n              <ion-item style="background-color: rgba(255, 255, 255, 0.76)">\n\n                <h2>{{m.location.id}}. {{m.name}}</h2>\n\n                <p>\n\n                  {{param.datatext.break}}{{m.wait}},\n\n                  {{param.datatext.speed}}{{m.speed*100}}\n\n                </p>\n\n                <p>\n\n                  {{param.datatext.avoidanceOption}}{{!m.disableAvoidance}},\n\n                  {{param.datatext.option}}{{m.data}}\n\n                </p>\n\n                <p>\n\n                  {{param.datatext.gabarit}}:{{m.anticollisionProfile}}\n\n                </p>\n\n              </ion-item>\n\n              <ion-item-options side="right">\n\n                <button ion-button color="danger" (mouseup)="deletePOI($event,index)">\n\n                  <ion-icon name="trash"></ion-icon>\n\n                  {{param.datatext.delete}}\n\n                </button>\n\n              </ion-item-options>\n\n            </ion-item-sliding>\n\n          </ion-list>\n\n        </ion-content>\n\n      </ion-col>\n\n    </ion-row>\n\n  </ion-grid>\n\n\n\n</ion-content>'/*ion-inline-end:"C:\Users\laele\Documents\KomApp\toolbox\src\pages\roundcreation\roundcreation.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__services_param_service__["a" /* ParamService */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["h" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["k" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["g" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_3__services_api_service__["a" /* ApiService */],
            __WEBPACK_IMPORTED_MODULE_4__services_alert_service__["a" /* AlertService */]])
    ], RoundCreationPage);
    return RoundCreationPage;
    var RoundCreationPage_1;
}());

//# sourceMappingURL=roundcreation.js.map

/***/ }),

/***/ 157:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 157;

/***/ }),

/***/ 19:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export Lift */
/* unused harmony export Battery */
/* unused harmony export Statistics */
/* unused harmony export Anticollision */
/* unused harmony export Iostate */
/* unused harmony export Docking */
/* unused harmony export Trajectory */
/* unused harmony export Navigation */
/* unused harmony export Differential */
/* unused harmony export Location */
/* unused harmony export RoundPOI */
/* unused harmony export Round */
/* unused harmony export Localization */
/* unused harmony export Pose */
/* unused harmony export Poi */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ApiService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(75);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_operators__ = __webpack_require__(348);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_operators___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_operators__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__alert_service__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__param_service__ = __webpack_require__(14);
// Allows Http communication with komnav
// cf Marc komnav_http_manual for more information
// https://drive.google.com/drive/u/3/folders/1-18kyKv6Ep-fzPU1F-dp0tQ1r8usyuZ7
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};







var Lift = /** @class */ (function () {
    function Lift() {
    }
    return Lift;
}());

var Battery = /** @class */ (function () {
    function Battery() {
    }
    return Battery;
}());

var Statistics = /** @class */ (function () {
    function Statistics() {
    }
    return Statistics;
}());

var Anticollision = /** @class */ (function () {
    function Anticollision() {
    }
    return Anticollision;
}());

var Iostate = /** @class */ (function () {
    function Iostate() {
    }
    return Iostate;
}());

var Docking = /** @class */ (function () {
    function Docking() {
    }
    return Docking;
}());

/*
{ Docking status
  "Unknown    ",
  "Undocked   ",
  "Docking    ",
  "Undocking  ",
  "Error      "
};*/
var Trajectory = /** @class */ (function () {
    function Trajectory() {
    }
    return Trajectory;
}());

//0 disabled
//1 waiting
//3 following
var Navigation = /** @class */ (function () {
    function Navigation() {
    }
    return Navigation;
}());

// The status can have the following values:
// • 0 - Waiting: the robot is ready for a new destination speciﬁed with a PUT /navigation/destination request.
// • 1 - Following: the robot is following the trajectory computed to join the destination.
// • 2 - Aiming: the robot has completed the trajectory and is now rotating to aim the destination accurately
// • 3 - Translating: the robot has ﬁnished aiming and is now translating to reach the destination
// • 4 - Rotating: this is the last rotation, executed only if the destination was speciﬁed with “Rotate” : true
// • 5-Error: indicatesthattherobothasencounteredanerrorthatpreventedittojointherequireddestination.
// This can be cleared by sending a new destination.
var Differential = /** @class */ (function () {
    function Differential() {
    }
    return Differential;
}());

var Location = /** @class */ (function () {
    function Location() {
    }
    return Location;
}());

var RoundPOI = /** @class */ (function () {
    function RoundPOI() {
    }
    return RoundPOI;
}());

var Round = /** @class */ (function () {
    function Round() {
    }
    return Round;
}());

var Localization = /** @class */ (function () {
    function Localization() {
    }
    return Localization;
}());

var Pose = /** @class */ (function () {
    function Pose() {
    }
    return Pose;
}());

var Poi = /** @class */ (function () {
    function Poi() {
    }
    return Poi;
}());

var ApiService = /** @class */ (function () {
    function ApiService(toastCtrl, httpClient, app, alert, param) {
        this.toastCtrl = toastCtrl;
        this.httpClient = httpClient;
        this.app = app;
        this.alert = alert;
        this.param = param;
        this.firstopen = true;
        this.roundpoi_list = [];
        this.socketok = false;
        this.adminMode = false;
        this.elevator = false;
        this.moovingchimio = false;
        this.moovingpharma = false;
        this.slabel = true; // to display the selecteur label when add a new poi
        this.soptiondesinfection = false;
        this.is_connected = false; // to know if we are connected to the robot
        this.is_background = false;
        //this.mapLoaded=false;
        this.is_localized = false;
        this.roundActive = false; // to know if the round is in process
        this.towardDocking = false; //to know if the robot is moving toward the docking
        this.start = false;
        this.fct_startRound = false;
        this.fct_onGo = false;
        this.statusRobot = 0;
        this.cpt_jetsonOK = 0;
        this.appOpened = false;
        this.close_app = false;
        this.is_blocked = false;
        this.is_high = false;
        this.copyRound = false;
        this.editRound = false;
        this.towardPOI = false;
        this.mapok = true;
        this.robotok = false;
        this.mapissue = false;
        this.smgthCreated = false;
    }
    ApiService.prototype.instanciate = function () {
        this.socketok = true;
        var apiserv = this;
        ////////////////////// localization socket
        this.localization_status = new Localization();
        this.localization_socket = new WebSocket(this.wsskomnav + this.param.localhost + "/api/localization/socket", "json");
        this.localization_socket.onerror = function (event) {
            console.log("error localization socket");
        };
        this.localization_socket.onopen = function (event) {
            console.log("localization socket opened");
            this.onmessage = function (event) {
                var test = JSON.parse(event.data);
                apiserv.localization_status.positionx = test["Pose"]["X"];
                apiserv.localization_status.positiony = test["Pose"]["Y"];
                apiserv.localization_status.positiont = test["Pose"]["T"];
            };
            this.onclose = function () {
                console.log("localization socket closed");
            };
        };
        ////////////////////// docking socket
        this.docking_status = new Docking();
        this.docking_socket = new WebSocket(this.wsskomnav + this.param.localhost + "/api/docking/socket", "json");
        this.docking_socket.onerror = function (event) {
            console.log("error docking socket");
        };
        this.docking_socket.onopen = function (event) {
            console.log("docking socket opened");
            this.onmessage = function (event) {
                var test = JSON.parse(event.data);
                apiserv.docking_status.status = test["Status"];
                apiserv.docking_status.detected = test["Detected"];
            };
            this.onclose = function () {
                console.log("docking socket closed");
            };
        };
        ////////////////////// differential socket
        this.differential_status = new Differential();
        this.differential_socket = new WebSocket(this.wsskomnav + this.param.localhost + "/api/differential/socket", "json");
        this.differential_socket.onerror = function (event) {
            console.log("error differential socket");
        };
        this.differential_socket.onopen = function (event) {
            console.log("differential socket opened");
            this.onmessage = function (event) {
                var test = JSON.parse(event.data);
                apiserv.differential_status.status = test["Status"];
                apiserv.differential_status.CurrentAngularSpeed = test["CurrentAngularSpeed"];
                apiserv.differential_status.CurrentLinearSpeed = test["CurrentLinearSpeed"];
            };
            this.onclose = function () {
                console.log("differential socket closed");
            };
        };
        ////////////////////// trajectory socket
        this.trajectory_status = new Trajectory();
        this.trajectory_socket = new WebSocket(this.wsskomnav + this.param.localhost + "/api/trajectory/socket", "json");
        this.trajectory_socket.onerror = function (event) {
            console.log("error trajectory socket");
        };
        this.trajectory_socket.onopen = function (event) {
            console.log("trajectory socket opened");
            this.onmessage = function (event) {
                var test = JSON.parse(event.data);
                apiserv.trajectory_status.status = test["Status"];
            };
            this.onclose = function () {
                console.log("trajectory socket closed");
            };
        };
        ////////////////////// navigation socket
        this.navigation_status = new Navigation();
        this.navigation_socket = new WebSocket(this.wsskomnav + this.param.localhost + "/api/navigation/socket", "json");
        this.navigation_socket.onerror = function (event) {
            console.log("error navigation socket");
        };
        this.navigation_socket.onopen = function (event) {
            console.log("navigation socket opened");
            this.onmessage = function (event) {
                var test = JSON.parse(event.data);
                apiserv.navigation_status.status = test["Status"];
                apiserv.navigation_status.avoided = test["Avoided"];
                apiserv.navigation_status.trajectory = test["Trajectory"];
                if (apiserv.navigation_status.status === 5 ||
                    apiserv.navigation_status.status === 0) {
                    //smart button and button A of the game pad
                    apiserv.towardPOI = false;
                    apiserv.towardDocking = false;
                }
                else {
                    apiserv.towardPOI = true;
                }
            };
            this.onclose = function () {
                console.log("navigation socket closed");
            };
        };
        ////////////////////// anticollision socket
        this.anticollision_status = new Anticollision();
        this.anticollision_socket = new WebSocket(this.wsskomnav + this.param.localhost + "/api/anticollision/socket", "json");
        this.anticollision_socket.onerror = function (event) {
            console.log("error anticollision socket");
        };
        this.anticollision_socket.onopen = function (event) {
            console.log("anticollision socket opened");
            this.onmessage = function (event) {
                var test = JSON.parse(event.data);
                apiserv.anticollision_status.timestamp = test["Timestamp"];
                apiserv.anticollision_status.enabled = test["Enabled"];
                apiserv.anticollision_status.locked = test["Locked"];
                apiserv.anticollision_status.forward = test["Forward"];
                apiserv.anticollision_status.right = test["Right"];
                apiserv.anticollision_status.left = test["Left"];
            };
            this.onclose = function () {
                console.log("anticollision socket closed");
            };
        };
        ////////////////////// statistics socket
        this.statistics_status = new Statistics();
        this.statistics_socket = new WebSocket(this.wsskomnav + this.param.localhost + "/api/statistics/socket", "json");
        this.statistics_socket.onerror = function (event) {
            console.log("error statistic socket");
        };
        this.statistics_socket.onopen = function (event) {
            console.log("statistic socket opened");
            this.onmessage = function (event) {
                var test = JSON.parse(event.data);
                apiserv.statistics_status.totalTime = test["TotalTime"];
                apiserv.statistics_status.totalDistance = test["TotalDistance"];
                apiserv.statistics_status.timestamp = test["Timestamp"];
            };
            this.onclose = function () {
                console.log("statistic socket closed");
            };
        };
        ///////////////////////// battery socket
        this.battery_status = new Battery();
        this.battery_socket = new WebSocket(this.wsskomnav + this.param.localhost + "/api/battery/socket", "json");
        this.battery_socket.onerror = function (event) {
            console.log("error battery socket");
        };
        this.battery_socket.onopen = function (event) {
            console.log("battery socket opened");
            this.onmessage = function (event) {
                var test = JSON.parse(event.data);
                apiserv.battery_status.autonomy = test["Autonomy"];
                apiserv.battery_status.current = test["Current"];
                apiserv.battery_status.remaining = test["Remaining"];
                apiserv.battery_status.status = test["Status"];
                apiserv.battery_status.timestamp = test["Timestamp"];
                apiserv.battery_status.voltage = test["Voltage"];
            };
            this.onclose = function () {
                console.log("battery socket closed");
            };
        };
        ///////////////////////// lift socket
        if (this.elevator) {
            this.lift_status = new Lift();
            this.lift_socket = new WebSocket(this.wsskomnav + this.param.liftip + "/api/lift/socket", "json");
            this.lift_socket.onerror = function (event) {
                console.log("error lift socket");
            };
            this.lift_socket.onopen = function (event) {
                console.log("lift socket opened");
                this.onmessage = function (event) {
                    var test = JSON.parse(event.data);
                    apiserv.lift_status.floors = test["Floors"];
                    apiserv.lift_status.currentFloor = test["CurrentFloor"];
                    apiserv.lift_status.targetFloor = test["TargetFloor"];
                    apiserv.lift_status.isBusy = test["IsBusy"];
                    apiserv.lift_status.isOpened = test["IsOpened"];
                    apiserv.lift_status.id = test["Id"];
                };
                this.onclose = function () {
                    console.log("lift socket closed");
                };
            };
        }
        //////////////////////// round socket
        // this.round_status = new Round();
        // this.round_socket = new WebSocket(this.wsskomnav+this.param.localhost+"/api/rounds/socket", "json");
        // this.round_socket.onerror = function(event)
        // {
        //     console.log("error round socket");
        // };
        // this.round_socket.onopen = function(event)
        // {
        //     console.log("round socket opened");
        //     this.onmessage = function(event)
        //     {
        //       var test = JSON.parse(event.data);
        //       apiserv.round_status.round=test['Round'];
        //       apiserv.round_status.acknowledge = test['Acknowledge'];
        //       apiserv.round_status.abort = test['Abort'];
        //       apiserv.round_status.pause = test['Pause'];
        //       apiserv.round_status.status = test['Status'];
        //     }
        //     this.onclose = function()
        //     {
        //         console.log("round socket closed");
        //     }
        // };
        //////////////////////////////// io socket
        this.b_pressed = false;
        this.btnPush = false;
        this.io_status = new Iostate();
        this.io_socket = new WebSocket(this.wsskomnav + this.param.localhost + "/api/io/socket", "json");
        this.io_socket.onerror = function (event) {
            console.log("error iosocket");
        };
        this.io_socket.onopen = function (event) {
            console.log("io socket opened");
            this.onclose = function () {
                console.log("io socket closed");
            };
        };
        this.io_socket.onmessage = function (event) {
            var test = JSON.parse(event.data);
            apiserv.io_status.timestamp = test["Timestamp"];
            apiserv.io_status.dIn = test["DIn"];
            apiserv.io_status.aIn = test["AIn"];
            if (apiserv.io_status.dIn[2] ||
                apiserv.io_status.dIn[3] ||
                apiserv.io_status.dIn[8]) {
                //smart button and button A of the game pad
                if (!apiserv.b_pressed) {
                    apiserv.btnPush = true;
                    apiserv.b_pressed = true;
                    apiserv.abortNavHttp();
                }
            }
            else {
                if (apiserv.b_pressed) {
                    apiserv.b_pressed = false;
                }
            }
        };
        //////////////////////////////// ROS TOPIC connection
        this.ros = new ROSLIB.Ros({
            url: this.wssros + this.param.robot.rosip
        });
        this.ros.on('connection', function () {
            console.log('Connected to ros websocket server.');
        });
        this.ros.on('error', function (error) {
            console.log('Error connecting to ros websocket server: ', error);
        });
        this.ros.on('close', function () {
            console.log('Connection to ros websocket server closed.');
        });
        //////////////////////////////// ROS TOPIC qrcode
        this.listener_qr = new ROSLIB.Topic({
            ros: this.ros,
            name: '/barcode',
            messageType: 'std_msgs/String'
        });
        //////////////////////////////// ROS TOPIC cam
        this.listener_camera = new ROSLIB.Topic({
            ros: this.ros,
            //name: '/top_webcam/image_raw/compressed',
            //name : '/fisheye_cam/image_raw',
            //name : '/D435_camera_FWD/color/image_raw',
            name: '/fisheye_cam/compressed',
            messageType: 'sensor_msgs/CompressedImage'
        });
        this.listener_cameraf = new ROSLIB.Topic({
            ros: this.ros,
            //name : '/top_webcam/image_raw',
            name: '/fisheye_cam/compressed',
            //name : '/D435_camera_FWD/color/image_raw',
            messageType: 'sensor_msgs/CompressedImage'
        });
    };
    ApiService.prototype.ngOnInit = function () { };
    ApiService.prototype.ngOnDestroy = function () {
        //this.batterySubscription.unsubscribe();
    };
    ApiService.prototype.mailAddInformationBasic = function () {
        var now = new Date().toLocaleString("en-GB", {
            day: "numeric",
            month: "numeric",
            year: "numeric",
            hour: "numeric",
            minute: "numeric",
            second: "numeric",
        });
        return ("<br> Time : " +
            now +
            "<br> Serial number : " +
            this.param.serialnumber +
            "<br> Battery remaining: " +
            this.battery_status.remaining +
            "%" +
            "<br> Battery state : " +
            this.battery_status.status +
            "<br> Battery voltage : " +
            this.battery_status.voltage +
            "<br> Battery current : " +
            this.battery_status.current +
            "<br> Battery autonomy : " +
            this.battery_status.autonomy +
            "<br> Docking state : " +
            this.docking_status.status +
            "<br> Status Forward : " +
            this.anticollision_status.forward +
            "<br> Status Right : " +
            this.anticollision_status.right +
            "<br> Status Left : " +
            this.anticollision_status.left +
            "<br> Status Reverse : " +
            this.anticollision_status.reverse +
            "<br> Navigation state : " +
            this.navigation_status.status +
            "<br> Differential state : " +
            this.differential_status.status +
            "<br> Odometer : " +
            this.statistics_status.totalDistance +
            "<br> Position X : " +
            this.localization_status.positionx +
            "<br> Position Y : " +
            this.localization_status.positiony +
            "<br> Position T : " +
            this.localization_status.positiont +
            "<br> Application : 6" +
            "<br>");
    };
    ApiService.prototype.joystickHttp = function (lin, rad) {
        var _this = this;
        var cmd = { Enable: true, TargetLinearSpeed: lin, TargetAngularSpeed: rad };
        var body = JSON.stringify(cmd);
        var headers = new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["c" /* HttpHeaders */]({ "Content-Type": "application/json" });
        var options = { headers: headers };
        return new Promise(function (resolve) {
            _this.httpClient
                .put(_this.httpskomnav + _this.param.localhost + "/api/differential/command", body, options)
                .subscribe(function (resp) {
                console.log(resp);
                console.log("joystickOK");
            }, function (error) {
                console.log(error);
                console.log("joystickPBM");
            });
        });
    };
    ApiService.prototype.jetsonOK = function () {
        var _this = this;
        this.checkInternet();
        this.alert.checkwifi(this.wifiok);
        this.httpClient
            .get(this.httpskomnav + this.param.localhost + "/api/battery/state", {
            observe: "response",
        })
            .pipe(Object(__WEBPACK_IMPORTED_MODULE_3_rxjs_operators__["first"])())
            .subscribe(function (resp) {
            if (_this.statusRobot === 2) {
                _this.alert.appError(_this.mailAddInformationBasic());
                document.location.reload(); //if error then refresh the page and relaunch websocket
            }
            if (resp.status === 200) {
                _this.cpt_jetsonOK = 0;
                _this.statusRobot = 0; // everything is ok
                _this.connect();
            }
            else {
                _this.cpt_jetsonOK += 1;
                if (_this.cpt_jetsonOK > 5) {
                    _this.statusRobot = 2; //no connection
                    _this.connectionLost();
                }
            }
        }, function (err) {
            console.log(err); //no conection
            _this.cpt_jetsonOK += 1;
            if (_this.cpt_jetsonOK > 10) {
                _this.statusRobot = 2;
                _this.connectionLost();
            }
        });
    };
    ApiService.prototype.okToast = function (m) {
        var toast = this.toastCtrl.create({
            message: m,
            duration: 3000,
            position: "middle",
            cssClass: "toastok",
        });
        toast.present();
    };
    ApiService.prototype.isConnectedInternet = function () {
        return __awaiter(this, void 0, void 0, function () {
            var response, text, error_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 3, , 4]);
                        return [4 /*yield*/, fetch('http://localhost/ionicDB/internetping.php')];
                    case 1:
                        response = _a.sent();
                        return [4 /*yield*/, response.text()];
                    case 2:
                        text = _a.sent();
                        //console.log(text);
                        // Analyse du texte pour déterminer la connectivité Internet
                        return [2 /*return*/, text.trim() === 'true']; // Renvoie true si le texte est 'true', sinon false
                    case 3:
                        error_1 = _a.sent();
                        console.error('Error checking internet connectivity:', error_1);
                        return [2 /*return*/, false];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    ApiService.prototype.checkInternet = function () {
        var _this = this;
        this.isConnectedInternet().then(function (connecte) {
            if (connecte) {
                //console.log("L'ordinateur est connecté à Internet");
                _this.wifiok = true;
            }
            else {
                //console.log("L'ordinateur n'est pas connecté à Internet");
                _this.wifiok = false;
            }
        });
    };
    ApiService.prototype.checkrobot = function () {
        var _this = this;
        this.httpClient
            .get(this.httpskomnav + this.param.localhost + "/api/battery/state")
            .subscribe(function (data) {
            _this.robotok = true;
        }, function (err) {
            console.log(err);
            _this.robotok = false;
        });
    };
    ApiService.prototype.createRoundList = function (id, name, wait, speed, data, anticollisionProfile, disableAvoidance) {
        var poiRound = new RoundPOI();
        var location = new Location();
        location.id = id;
        poiRound.location = location;
        poiRound.name = name;
        poiRound.wait = wait;
        poiRound.speed = speed;
        poiRound.data = data;
        poiRound.anticollisionProfile = anticollisionProfile;
        poiRound.disableAvoidance = disableAvoidance;
        this.roundpoi_list.push(poiRound);
    };
    ApiService.prototype.connectHttp = function () {
        this.httpClient
            .put(this.httpskomnav + this.param.localhost + "/api/docking/connect", {
            observe: "response",
        })
            .pipe(Object(__WEBPACK_IMPORTED_MODULE_3_rxjs_operators__["first"])())
            .subscribe(function (resp) {
            console.log(resp);
            console.log("connectOK");
        }, function (err) {
            console.log(err);
            console.log("connectPBM");
        });
    };
    ApiService.prototype.reachHttp = function (poiname) {
        this.httpClient
            .put(this.httpskomnav +
            this.param.localhost +
            "/api/navigation/destination/name/" +
            poiname, { observe: "response" })
            .pipe(Object(__WEBPACK_IMPORTED_MODULE_3_rxjs_operators__["first"])())
            .subscribe(function (resp) {
            console.log(resp);
            console.log("reachOK");
        }, function (err) {
            console.log(err);
            console.log("reachPBM");
        });
    };
    ApiService.prototype.abortNavHttp = function () {
        // stop the navigation
        this.httpClient
            .put(this.httpskomnav + this.param.localhost + "/api/navigation/abort", {
            observe: "response",
        })
            .pipe(Object(__WEBPACK_IMPORTED_MODULE_3_rxjs_operators__["first"])())
            .subscribe(function (resp) {
            console.log(resp);
            console.log("abortNavOK");
        }, function (err) {
            console.log(err);
            console.log("abortNavPBM");
        });
    };
    ApiService.prototype.changeMapbyIdHttp = function (id) {
        this.httpClient
            .put(this.httpskomnav + this.param.localhost + "/api/maps/current/id/" + id, {
            observe: "response",
        })
            .pipe(Object(__WEBPACK_IMPORTED_MODULE_3_rxjs_operators__["first"])())
            .subscribe(function (resp) {
            console.log(resp);
            console.log("changeMapOK");
        }, function (err) {
            console.log(err);
            console.log("changeMapPBM");
        });
    };
    ApiService.prototype.getAllMapsHttp = function () {
        var _this = this;
        this.httpClient
            .get(this.httpskomnav + this.param.localhost + "/api/maps/list")
            .subscribe(function (data) {
            console.log(data);
            console.log("getAllMapsOK");
            _this.all_maps = data;
            //console.log(this.mapdata.Id);
            // if(this.all_maps!=undefined){
            // }
        }, function (err) {
            console.log(err);
        });
    };
    ApiService.prototype.getCurrentMap = function () {
        var _this = this;
        this.httpClient
            .get(this.httpskomnav + this.param.localhost + "/api/maps/current/properties")
            .subscribe(function (data) {
            //console.log(data);
            _this.mapdata = data;
            //console.log(this.mapdata.Id);
            if (_this.mapdata) {
                _this.id_current_map = _this.mapdata.Id;
                _this.name_current_map = _this.mapdata.Name;
            }
            console.log("get current map ok");
        }, function (err) {
            console.log(err);
        });
    };
    // pour affichage map
    ApiService.prototype.getImgMapHttp = function () {
        return this.httpClient.get(this.httpskomnav + this.param.localhost + "/api/maps/current/image", { responseType: "blob" });
    };
    // getMapPropertiesHttp(){
    //   return this.httpClient.get<MapProp>('http://'+this.param.localhost+'/api/maps/current/properties');
    // }
    // getLocalisationHttp(){
    //   return this.httpClient.get<Localisation>('http://'+this.param.localhost+'/api/localization/state');
    // }
    // getPOIListHttp(){
    //   //console.log(this.walker_states);
    //   return this.httpClient.get<Poi[]>('http://'+this.param.localhost+'/api/maps/current/locations');
    // }
    // fin pour affichage map
    ApiService.prototype.getCurrentLocations = function () {
        var _this = this;
        //avoir les poi de la map actuelle
        this.httpClient
            .get(this.httpskomnav + this.param.localhost + "/api/maps/current/locations")
            .subscribe(function (data) {
            console.log("get locations ok");
            _this.all_locations = data;
            console.log(data);
        }, function (err) {
            console.log(err);
        });
    };
    ApiService.prototype.getRoundList = function () {
        var _this = this;
        //pour avoir la liste des rondes de la map actuelle
        this.httpClient
            .get(this.httpskomnav + this.param.localhost + "/api/rounds/list")
            .subscribe(function (data) {
            _this.rounddata = data;
            console.log(_this.rounddata);
            if (_this.id_current_map && _this.rounddata) {
                _this.round_current_map = _this.rounddata.filter(function (round) { return round.Map == _this.id_current_map; });
                //console.log(this.round_current_map);
            }
        }, function (err) {
            console.log(err);
        });
    };
    ApiService.prototype.connect = function () {
        this.is_connected = true;
    };
    ApiService.prototype.connectionLost = function () {
        this.is_connected = false;
    };
    ApiService.prototype.background = function () {
        this.is_background = true;
    };
    ApiService.prototype.foreground = function () {
        this.is_background = false;
    };
    ApiService.prototype.postqr = function (namePOI, x, y, t) {
        var body = JSON.stringify({
            Name: namePOI,
            Pose: {
                X: x,
                Y: y,
                T: t,
            },
            Label: "qr",
        });
        this.httpClient
            .post(this.httpskomnav + this.param.localhost + "/api/maps/current/locations", body)
            .subscribe(function (response) {
            console.log(response);
            console.log("create poi ok");
        }, function (err) {
            console.log(err);
        });
    };
    ApiService.prototype.postPoi = function (namePOI, label) {
        var body = JSON.stringify({
            Name: namePOI,
            Pose: {
                X: this.localization_status.positionx,
                Y: this.localization_status.positiony,
                T: this.localization_status.positiont,
            },
            Label: label,
        });
        this.httpClient
            .post(this.httpskomnav + this.param.localhost + "/api/maps/current/locations", body)
            .subscribe(function (response) {
            console.log(response);
            console.log("create poi ok");
        }, function (err) {
            console.log(err);
        });
    };
    ApiService.prototype.configPOIDocking = function () {
        var angle;
        if (this.localization_status.positiont >= 0)
            angle = this.localization_status.positiont - Math.PI;
        else
            angle = this.localization_status.positiont + Math.PI;
        var dock = JSON.stringify({
            Name: "docking",
            Pose: {
                X: this.localization_status.positionx + 0.6 * Math.cos(angle),
                Y: this.localization_status.positiony + 0.6 * Math.sin(angle),
                T: this.localization_status.positiont,
            },
            Label: "docking",
        });
        console.log(dock);
        this.httpClient
            .post(this.httpskomnav + this.param.localhost + "/api/maps/current/locations", dock)
            .subscribe(function (response) {
            console.log(response);
            console.log("create poi ok");
        }, function (err) {
            console.log(err);
        });
    };
    ApiService.prototype.postMap = function (f) {
        var _this = this;
        console.log(f);
        this.httpClient
            .post(this.httpskomnav + this.param.localhost + "/api/maps/list", f)
            .subscribe(function (response) {
            console.log(response);
            console.log("post map ok");
            _this.mapok = true;
        }, function (err) {
            console.log(err);
            _this.mapok = false;
        });
    };
    ApiService.prototype.putMap = function (f) {
        var _this = this;
        this.httpClient
            .put(this.httpskomnav +
            this.param.localhost +
            "/api/maps/list/" +
            this.id_current_map, f)
            .subscribe(function (response) {
            console.log(response);
            console.log("put map ok");
            _this.mapok = true;
        }, function (err) {
            console.log(err);
            _this.mapok = false;
        });
    };
    ApiService.prototype.postRound = function (nameRound) {
        var body = JSON.stringify({
            name: nameRound,
            map: this.id_current_map,
            Locations: this.roundpoi_list,
        });
        console.log(body);
        this.httpClient
            .post(this.httpskomnav + this.param.localhost + "/api/rounds/list", body)
            .subscribe(function (response) {
            console.log(response);
            console.log("create round ok");
        }, function (err) {
            console.log(err);
        });
    };
    ApiService.prototype.putRound = function (nameRound, id) {
        var body = JSON.stringify({
            Name: nameRound,
            Map: this.id_current_map,
            Locations: this.roundpoi_list,
        });
        console.log(body);
        this.httpClient
            .put(this.httpskomnav + this.param.localhost + "/api/rounds/list/" + id, body)
            .subscribe(function (response) {
            console.log(response);
            console.log("edit round " + id + "ok");
        }, function (err) {
            console.log(err);
        });
    };
    ApiService.prototype.putPOI = function (namePoi, label, id, x, y, t) {
        var body = JSON.stringify({
            Name: namePoi,
            Pose: { X: x, Y: y, T: t },
            Label: label,
        });
        console.log(body);
        this.httpClient
            .put(this.httpskomnav + this.param.localhost + "/api/maps/current/locations/" + id, body)
            .pipe(Object(__WEBPACK_IMPORTED_MODULE_3_rxjs_operators__["first"])())
            .subscribe(function (response) {
            console.log(response);
            console.log("edit round " + id + "ok");
        }, function (err) {
            console.log(err);
        });
    };
    ApiService.prototype.deleteRound = function (id) {
        this.httpClient
            .delete(this.httpskomnav + this.param.localhost + "/api/rounds/list/" + id, {
            observe: "response",
        })
            .pipe(Object(__WEBPACK_IMPORTED_MODULE_3_rxjs_operators__["first"])())
            .subscribe(function (response) {
            console.log(response);
            console.log("delete round " + id + "ok");
        }, function (err) {
            console.log(err);
        });
    };
    // putLocalization(x: number, y: number, t: number) {
    //   const body = JSON.stringify({ X: x, Y: y, T: t });
    //   console.log(body);
    //   this.httpClient
    //     .put(this.httpskomnav + this.param.localhost + "/api/localization/pose", body)
    //     .subscribe(
    //       (response) => {
    //         console.log(response);
    //         console.log("reloc ok");
    //       },
    //       (err) => {
    //         console.log(err);
    //       }
    //     );
    // }
    ApiService.prototype.deleteMap = function (id) {
        this.httpClient
            .delete(this.httpskomnav + this.param.localhost + "/api/maps/list/" + id, {
            observe: "response",
        })
            .pipe(Object(__WEBPACK_IMPORTED_MODULE_3_rxjs_operators__["first"])())
            .subscribe(function (response) {
            console.log(response);
            console.log("delete map" + id + " ok");
        }, function (err) {
            console.log(err);
        });
    };
    ApiService.prototype.deletePOI = function (id) {
        this.httpClient
            .delete(this.httpskomnav + this.param.localhost + "/api/maps/current/locations/" + id, { observe: "response" })
            .pipe(Object(__WEBPACK_IMPORTED_MODULE_3_rxjs_operators__["first"])())
            .subscribe(function (response) {
            console.log(response);
            console.log("delete poi" + id + " ok");
        }, function (err) {
            console.log(err);
        });
    };
    ApiService.prototype.putDestination = function (p, s, avoidance) {
        var body = JSON.stringify({ X: p.X, Y: p.Y, T: p.T });
        this.httpClient
            .put(this.httpskomnav +
            this.param.localhost +
            "/api/navigation/destination/pose?speed=" +
            s +
            "&disable_avoidance=" +
            !avoidance, body, { observe: "response" })
            .subscribe(function (response) {
            console.log(response);
            console.log("reach ok");
        }, function (err) {
            console.log(err);
        });
    };
    ApiService.prototype.putLocalization = function (x, y, t) {
        var _this = this;
        var test = {
            X: x,
            Y: y,
            T: t,
        };
        //var jsonstring = JSON.stringify(test);
        //this.localization_socket.send(jsonstring);
        var body = JSON.stringify(test);
        var headers = new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["c" /* HttpHeaders */]({ "Content-Type": "application/json" });
        var options = { headers: headers };
        return new Promise(function (resolve) {
            _this.httpClient
                .put(_this.httpskomnav + _this.param.localhost + "/api/localization/pose", body, options)
                .subscribe(function (response) {
                console.log("reloc ok");
            }, function (error) {
                //Failed to Login.
                //alert(error.text());
                console.log(error.text());
            });
        });
    };
    ApiService.prototype.reach_location = function (event, rotate, speed, disable_avoidance) {
        var _this = this;
        var data = JSON.stringify({
            X: event.X,
            Y: event.Y,
            T: event.T,
        });
        console.log(event);
        console.log(rotate);
        console.log(speed);
        console.log(disable_avoidance);
        if (rotate) {
            var headers = new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["c" /* HttpHeaders */]({ "Content-Type": "application/json" });
            var options_1 = { headers: headers };
            return new Promise(function (resolve) {
                _this.httpClient
                    .put(_this.httpskomnav +
                    _this.param.localhost +
                    "/api/navigation/destination/pose?speed=" +
                    speed +
                    "&disable_avoidance=" +
                    disable_avoidance, data, options_1)
                    .subscribe(function (response) {
                    console.log("test");
                }, function (error) {
                    //Failed to Login.
                    //alert(error;
                    console.log(error);
                });
            });
        }
        else {
            var headers = new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["c" /* HttpHeaders */]({ "Content-Type": "application/json" });
            var options_2 = { headers: headers };
            return new Promise(function (resolve) {
                _this.httpClient
                    .put(_this.httpskomnav +
                    _this.param.localhost +
                    "/api/navigation/destination/point?speed=" +
                    speed +
                    "&disable_avoidance=" +
                    disable_avoidance, data, options_2)
                    .subscribe(function (response) {
                    console.log("test");
                }, function (error) {
                    //Failed to Login.
                    //alert(error);
                    console.log(error);
                });
            });
        }
    };
    ApiService.prototype.eyesHttp = function (id) {
        this.httpClient
            .put(this.httpskomnav + this.param.localhost + "/api/eyes?id=" + id, {
            observe: "response",
        })
            .pipe(Object(__WEBPACK_IMPORTED_MODULE_3_rxjs_operators__["first"])())
            .subscribe(function (resp) {
            console.log(resp);
            console.log("eyesOK");
        }, function (err) {
            console.log(err);
            console.log("eyesPBM");
        });
    };
    ApiService.prototype.deleteEyesHttp = function (id) {
        this.httpClient
            .delete(this.httpskomnav + this.param.localhost + "/api/eyes?id=" + id, {
            observe: "response",
        })
            .pipe(Object(__WEBPACK_IMPORTED_MODULE_3_rxjs_operators__["first"])())
            .subscribe(function (resp) {
            console.log(resp);
            console.log("eyesOK");
        }, function (err) {
            console.log(err);
            console.log("eyesPBM");
        });
    };
    ApiService.prototype.cabinethttp = function (numcabinet, open) {
        var test = {
            Index: numcabinet,
            Value: open,
        };
        var body = JSON.stringify(test);
        this.httpClient
            .put(this.httpskomnav + this.param.localhost + "/api/io/dout", body, {
            observe: "response",
        })
            .pipe(Object(__WEBPACK_IMPORTED_MODULE_3_rxjs_operators__["first"])())
            .subscribe(function (resp) {
            console.log(resp);
            console.log("cabinet ok num:" + numcabinet + " open: " + open);
        }, function (err) {
            console.log(err);
            console.log("cabinet pbm num:" + numcabinet + " open: " + open);
        });
    };
    ApiService.prototype.startQrDetection = function (start) {
        var serviceQrDetection = new ROSLIB.Service({
            ros: this.ros,
            name: '/toggle_barcode_detection_node',
            serviceType: 'std_srvs/SetBool'
        });
        var request = new ROSLIB.ServiceRequest({
            data: start,
        });
        serviceQrDetection.callService(request, function (result) {
            console.log("*********service qr****************");
            console.log(result);
        });
        if (!start) {
            this.listener_qr.removeAllListeners();
        }
    };
    ApiService.prototype.resetLocationHttp = function () {
        var _this = this;
        var pose = {
            X: this.firstlocation[0].Pose.X,
            Y: this.firstlocation[0].Pose.Y,
            T: this.firstlocation[0].Pose.T,
        };
        var body = JSON.stringify(pose);
        var headers = new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["c" /* HttpHeaders */]({ "Content-Type": "application/json" });
        var options = { headers: headers };
        return new Promise(function (resolve) {
            _this.httpClient
                .put(_this.httpskomnav + _this.param.localhost + "/api/localization/pose", body, options)
                .subscribe(function (resp) {
                console.log(resp);
                console.log("resetlocationOK");
            }, function (error) {
                console.log(error);
                console.log("resetlocationPBM");
            });
        });
    };
    ApiService.prototype.getFirstLocationHttp = function () {
        var _this = this;
        this.httpClient
            .get(this.httpskomnav + this.param.localhost + "/api/maps/current/locations")
            .subscribe(function (data) {
            console.log(data);
            _this.locationdata = data;
            if (_this.locationdata) {
                _this.firstlocation = _this.locationdata.filter(function (x) { return x.Id == _this.QRselected.id_poi; });
            }
        }, function (err) {
            console.log(err);
        });
    };
    ApiService.prototype.call_lift = function (x, y) {
        var headers = new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["c" /* HttpHeaders */]({ "Content-Type": "application/json" });
        var options = { headers: headers };
        this.httpClient
            .post(this.httpskomnav + this.param.liftip + "/api/lift/request?source=" + x + "&destination=" + y, options)
            .subscribe(function (response) {
            console.log(response);
            console.log("call lift ok");
        }, function (err) {
            console.log(err);
        });
    };
    ApiService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["k" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["a" /* HttpClient */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["b" /* App */],
            __WEBPACK_IMPORTED_MODULE_4__alert_service__["a" /* AlertService */],
            __WEBPACK_IMPORTED_MODULE_5__param_service__["a" /* ParamService */]])
    ], ApiService);
    return ApiService;
}());

//# sourceMappingURL=api.service.js.map

/***/ }),

/***/ 201:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 201;

/***/ }),

/***/ 21:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AlertService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__param_service__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_common_http__ = __webpack_require__(75);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
// to send mail and sms alert




// function send mail in index.html
//declare var envoyerSMS;
// var in index.html
//declare var messageAenvoyer;
//declare var numTelToElement;
var AlertService = /** @class */ (function () {
    function AlertService(app, param, http) {
        this.app = app;
        this.param = param;
        this.http = http;
        this.videoWidth = 0;
        this.videoHeight = 0;
        this.constraints = {
            //audio: false,
            //video: {deviceId: "f9860b260e18d7fd1687d8567e21b49c4bb26f87606020741717e11f6179f409"},
            video: true,
            facingMode: "environment",
            width: { ideal: 1920 },
            height: { ideal: 1080 },
        };
    }
    AlertService.prototype.ngOnInit = function () { };
    AlertService.prototype.checkwifi = function (bool) {
        this.allowmail = bool;
    };
    AlertService.prototype.handleError = function (error) {
        console.log("Error: ", error);
    };
    // function send mail without image
    // bod : corps du mail
    // suj : sujet du mail
    // dest : adresse mail du destinataire
    AlertService.prototype.sendmailPerso = function (bod, suj, dest, data, usern, passw, robotn) {
        var _this = this;
        //console.log("sendmail");
        var usermailData = {
            username: usern,
            email: data,
            bcc: dest,
            date: new Date(Date.now()).toDateString(),
            subject: suj,
            message: bod,
            password: passw,
            robotname: robotn
        };
        var link = "http://localhost/ionicDB/SendMail/sendmail.php";
        var postParams = JSON.stringify(usermailData);
        //console.log(postParams);
        if (this.allowmail) {
            this.http.post(link, postParams).subscribe(function (response) {
                //console.log(response);
                //console.log(this.param.durationNS);
                if (response == "OK") {
                    var s = _this.param.serialnumber + ' : Duration App';
                    if (suj == s) {
                        _this.param.updateDurationNS(_this.param.durationNS[0].id_duration);
                        _this.param.durationNS.shift();
                        //console.log(this.param.durationNS);
                    }
                }
            }, function (error) {
                console.log(error);
            });
        }
    };
    AlertService.prototype.sendAlertData = function (bod, suj) {
        this.sendmailPerso(bod, suj, this.param.datamaillist, this.param.datamail, this.param.datamail, this.param.maildatapassw, this.param.datamail);
    };
    AlertService.prototype.sendAlertClient = function (bod, suj) {
        if (this.param.maillist.length) {
            this.sendmailPerso(bod, suj, this.param.maillist, this.param.datamail, this.param.robotmail, this.param.mailrobotpassw, this.param.name);
        }
    };
    // function send mail with image
    // bod : corps du mail
    // suj : sujet du mail
    // dest : adresse mail du destinataire
    // url : image attachment
    AlertService.prototype.sendmailImgperso = function (url, bod, suj, dest, data, usern, passw, robotn) {
        //console.log("sendmail");
        var usermailData = {
            username: usern,
            email: data,
            bcc: dest,
            date: new Date(Date.now()).toDateString(),
            subject: suj,
            message: bod,
            password: passw,
            robotname: robotn,
            url: url
        };
        var link = "http://localhost/ionicDB/SendMail/sendmailImg.php";
        var postParams = JSON.stringify(usermailData);
        //console.log(postParams);
        if (this.allowmail) {
            this.http.post(link, postParams).subscribe(function (response) {
                console.log(response);
            }, function (error) {
                console.log(error);
            });
        }
    };
    AlertService.prototype.sendAlertImgData = function (url, bod, suj) {
        this.sendmailImgperso(url, bod, suj, this.param.datamail, this.param.datamail, this.param.datamail, this.param.maildatapassw, this.param.datamail);
    };
    AlertService.prototype.sendAlertImgClient = function (url, bod, suj) {
        if (this.param.maillist.length) {
            this.sendmailImgperso(url, bod, suj, this.param.maillist, this.param.datamail, this.param.robotmail, this.param.mailrobotpassw, this.param.name);
        }
    };
    // function send sms
    // sendsmsperso(mess, num) {
    //   messageAenvoyer = mess;
    //   numTelToElement = num;
    //   envoyerSMS();
    // }
    AlertService.prototype.appError = function (info) {
        this.sendAlertData("<br> An error occurred, the round application has been reload automatically" +
            "<br> Code alert : 1" +
            info, this.param.serialnumber + " : Error (App Tool box)");
    };
    AlertService.prototype.appOpen = function (info) {
        this.sendAlertData("<br> Someone oppened the Tool box app." + "<br> Code alert : 24" + info, this.param.serialnumber + " : App opened (Tool box)");
    };
    AlertService.prototype.gopoi = function (info) {
        this.sendAlertData("<br> A person sent the robot to a PoI" +
            "<br> Code alert : 33" + info, this.param.serialnumber + " : Go PoI");
    };
    AlertService.prototype.reachgreenpoint = function (info) {
        this.sendAlertData("<br> A person sent the robot to a position" +
            "<br> Code alert : 33" +
            info, this.param.serialnumber + " : Reach Green point");
    };
    AlertService.prototype.createpoi = function (info) {
        this.sendAlertData("<br> A person create a poi" +
            "<br> Code alert : 34" +
            info, this.param.serialnumber + " : Add PoI");
    };
    AlertService.prototype.deletepoi = function (info) {
        this.sendAlertData("<br> A person delete a poi" +
            "<br> Code alert : 35" +
            info, this.param.serialnumber + " : Delete PoI");
    };
    AlertService.prototype.editpoi = function (info) {
        this.sendAlertData("<br> A person edit a poi" +
            "<br> Code alert : 36" +
            info, this.param.serialnumber + " : Edit PoI");
    };
    AlertService.prototype.addmap = function (info) {
        this.sendAlertData("<br> A person has added a new map" +
            "<br> Code alert : 37" +
            info, this.param.serialnumber + " : Add a map");
    };
    AlertService.prototype.deletemap = function (info) {
        this.sendAlertData("<br> A person has deleted a map" +
            "<br> Code alert : 38" +
            info, this.param.serialnumber + " : Delete a map");
    };
    AlertService.prototype.editmap = function (info) {
        this.sendAlertData("<br> A person has edited a map" +
            "<br> Code alert : 39" +
            info, this.param.serialnumber + " : Edit a map");
    };
    AlertService.prototype.addround = function (info) {
        this.sendAlertData("<br> A person has added a new round" +
            "<br> Code alert : 40" +
            info, this.param.serialnumber + " : Add a round");
    };
    AlertService.prototype.deleteround = function (info) {
        this.sendAlertData("<br> A person has deleted a round" +
            "<br> Code alert : 41" +
            info, this.param.serialnumber + " : Delete a round");
    };
    AlertService.prototype.editround = function (info) {
        this.sendAlertData("<br> A person has edited a round" +
            "<br> Code alert : 42" +
            info, this.param.serialnumber + " : Edit a round");
    };
    AlertService.prototype.localisemanually = function (info) {
        this.sendAlertData("<br> A person localise the robot manually" +
            "<br> Code alert : 43" +
            info, this.param.serialnumber + " : Localise manually");
    };
    AlertService.prototype.scanqr = function (info) {
        this.sendAlertData("<br> A person has scanned a qr" +
            "<br> Code alert : 44" +
            info, this.param.serialnumber + " : Scan QR");
    };
    AlertService.prototype.loadmap = function (info) {
        this.sendAlertData("<br> A person has loaded a map" +
            "<br> Code alert : 45" +
            info, this.param.serialnumber + " : Load a map");
    };
    AlertService.prototype.lowBattery = function (info) {
        this.sendAlertData("<br> The robot must be sent to the docking station" +
            "<br> Code alert : 5" +
            info, this.param.serialnumber + " : Battery Critical");
    };
    AlertService.prototype.errorblocked = function (info) {
        this.sendAlertData("<br> The robot has been stopped because of a navigation error" +
            "<br> Code alert : 6" +
            info, this.param.serialnumber + " : Navigation error");
    };
    AlertService.prototype.displayTuto = function (info) {
        this.sendAlertData("<br> Someone is reading the tuto" + "<br> Code alert : 10" + info, this.param.serialnumber + " : Tuto opened");
    };
    AlertService.prototype.noLongerBlocked = function (info) {
        this.sendAlertData("<br> The robot is no longer blocked" + "<br> Code alert : 3" + info, this.param.serialnumber + " : Automatic release");
    };
    AlertService.prototype.manualintervention = function (info) {
        this.sendAlertData("<br> Someone unlocked the robot" + "<br> Code alert : 25" + info, this.param.serialnumber + " : Manual release");
    };
    AlertService.prototype.naverror = function (info) {
        this.sendAlertData("<br> A navigation error has occurred. The robot did not manage to plan its route" +
            "<br> Code alert : 4" +
            info, this.param.serialnumber + " : Navigation error");
    };
    AlertService.prototype.robotLost = function (info) {
        this.sendAlertData("<br> The robot is lost. It must be relocated" +
            "<br> Code alert : 7" +
            info, this.param.serialnumber + " : Robot Lost");
    };
    AlertService.prototype.duration = function (olddata, info) {
        //console.log("duration alert");
        this.sendAlertData("<br> Here is a count of the duration of use of the apps in minutes" +
            "<br> Date duration : " +
            olddata.date +
            "<br> Round duration : " +
            olddata.round +
            "<br> Patrol duration : " +
            olddata.patrol +
            "<br> Walk duration : " +
            olddata.walk +
            "<br> Battery duration : " +
            olddata.battery +
            '<br> Toolbox duration : ' + olddata.toolbox +
            '<br> Logistic duration : ' + olddata.logistic +
            "<br> Code alert : 26" +
            info, this.param.serialnumber + " : Duration App");
    };
    AlertService.prototype.appClosed = function (info) {
        this.sendAlertData("<br> Somebody has closed the Tool box application" +
            "<br> Code alert : 13" +
            info, this.param.serialnumber + " : App Tool box Closed");
    };
    AlertService.prototype.blocking = function (info) {
        this.sendAlertData("<br> The robot is blocked by an obstacle" +
            "<br> Code alert : 19" +
            info, this.param.serialnumber + " : Blocking");
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])("video"),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* ElementRef */])
    ], AlertService.prototype, "videoElement", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])("canvas"),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* ElementRef */])
    ], AlertService.prototype, "canvas", void 0);
    AlertService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* App */], __WEBPACK_IMPORTED_MODULE_2__param_service__["a" /* ParamService */], __WEBPACK_IMPORTED_MODULE_3__angular_common_http__["a" /* HttpClient */]])
    ], AlertService);
    return AlertService;
}());

//# sourceMappingURL=alert.service.js.map

/***/ }),

/***/ 244:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(243);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(241);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_api_service__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_first_first__ = __webpack_require__(262);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__services_alert_service__ = __webpack_require__(21);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var MyApp = /** @class */ (function () {
    //rootPage:any =  TestYoutubePage;
    function MyApp(platform, statusBar, splashScreen, api, alert) {
        // This code loads the IFrame Player API code asynchronously.
        var _this = this;
        this.api = api;
        this.alert = alert;
        this.rootPage = __WEBPACK_IMPORTED_MODULE_5__pages_first_first__["a" /* FirstPage */];
        platform.ready().then(function () {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            statusBar.styleDefault();
            splashScreen.hide();
            platform.pause.subscribe(function () {
                _this.api.background();
                window.close();
            });
            platform.resume.subscribe(function () { });
        });
    }
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"C:\Users\laele\Documents\KomApp\toolbox\src\app\app.html"*/'<ion-nav [root]="rootPage"></ion-nav>\n\n'/*ion-inline-end:"C:\Users\laele\Documents\KomApp\toolbox\src\app\app.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */],
            __WEBPACK_IMPORTED_MODULE_4__services_api_service__["a" /* ApiService */],
            __WEBPACK_IMPORTED_MODULE_6__services_alert_service__["a" /* AlertService */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 262:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FirstPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_param_service__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_api_service__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_alert_service__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__poi_poi__ = __webpack_require__(141);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__services_popup_service__ = __webpack_require__(35);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__services_speech_service__ = __webpack_require__(54);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var FirstPage = /** @class */ (function () {
    function FirstPage(loadingCtrl, popup, param, alert, api, speech, navCtrl) {
        this.loadingCtrl = loadingCtrl;
        this.popup = popup;
        this.param = param;
        this.alert = alert;
        this.api = api;
        this.speech = speech;
        this.navCtrl = navCtrl;
        this.poiPage = __WEBPACK_IMPORTED_MODULE_5__poi_poi__["a" /* PoiPage */];
        this.cpt_open = 0;
        this.loading = this.loadingCtrl.create({});
        this.loading.present();
    }
    FirstPage.prototype.ngOnInit = function () {
        var _this = this;
        this.appopen = setInterval(function () { return _this.appOpen(); }, 900);
        this.speech.getVoice();
    };
    FirstPage.prototype.appOpen = function () {
        if (this.param.localhost != undefined) {
            this.api.checkrobot();
        }
        this.api.checkInternet();
        this.alert.checkwifi(this.api.wifiok);
        this.cpt_open += 1;
        if (this.cpt_open === 15) {
            this.popup.startFailedAlert();
            this.speech.speak(this.param.datatext.cantstart);
        }
        if (!this.api.appOpened) {
            this.param.getDataRobot();
            this.param.getUVparam();
            this.param.getQR();
            this.param.getBattery();
            this.param.getDurationNS();
            if (this.param.robot) {
                this.param.fillData();
                if (this.param.robot.httpskomnav == 0) {
                    this.api.httpskomnav = "http://";
                    this.api.wsskomnav = "ws://";
                }
                else {
                    this.api.httpskomnav = "https://";
                    this.api.wsskomnav = "wss://";
                }
                if (this.param.robot.httpsros == 0) {
                    this.api.wssros = "ws://";
                }
                else {
                    this.api.wssros = "wss://";
                }
            }
            if (this.api.robotok && this.param.langage) {
                this.api.getCurrentMap();
                this.api.getAllMapsHttp();
                if (!this.api.socketok) {
                    this.api.instanciate();
                }
                this.api.eyesHttp(4);
                this.api.appOpened = true;
            }
        }
        else if (this.api.appOpened && this.cpt_open >= 6) {
            this.api.abortNavHttp();
            this.alert.appOpen(this.api.mailAddInformationBasic());
            this.navCtrl.setRoot(this.poiPage);
            this.loading.dismiss();
            if (this.popup.alert_blocked) {
                this.popup.alert_blocked.dismiss();
            }
            clearInterval(this.appopen);
            console.log(this.cpt_open);
        }
    };
    FirstPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: "page-first",template:/*ion-inline-start:"C:\Users\laele\Documents\KomApp\toolbox\src\pages\first\first.html"*/'<ion-header no-border>\n\n \n\n</ion-header>\n\n<ion-content padding >\n\n  <ion-card text-center class="big_card" >\n\n      <ion-grid class="heightstyle">\n\n       \n\n      </ion-grid> \n\n\n\n  </ion-card>\n\n \n\n\n\n</ion-content>'/*ion-inline-end:"C:\Users\laele\Documents\KomApp\toolbox\src\pages\first\first.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_6__services_popup_service__["a" /* PopupService */],
            __WEBPACK_IMPORTED_MODULE_2__services_param_service__["a" /* ParamService */],
            __WEBPACK_IMPORTED_MODULE_4__services_alert_service__["a" /* AlertService */],
            __WEBPACK_IMPORTED_MODULE_3__services_api_service__["a" /* ApiService */],
            __WEBPACK_IMPORTED_MODULE_7__services_speech_service__["a" /* SpeechService */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */]])
    ], FirstPage);
    return FirstPage;
}());

//# sourceMappingURL=first.js.map

/***/ }),

/***/ 264:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RoundDisplayPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_param_service__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_api_service__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__roundcreation_roundcreation__ = __webpack_require__(142);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__roundedit_roundedit__ = __webpack_require__(265);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__services_popup_service__ = __webpack_require__(35);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__services_alert_service__ = __webpack_require__(21);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var RoundDisplayPage = /** @class */ (function () {
    function RoundDisplayPage(loadingCtrl, navCtrl, param, toastCtrl, api, popup, alert) {
        this.loadingCtrl = loadingCtrl;
        this.navCtrl = navCtrl;
        this.param = param;
        this.toastCtrl = toastCtrl;
        this.api = api;
        this.popup = popup;
        this.alert = alert;
        this.roundCreationPage = __WEBPACK_IMPORTED_MODULE_4__roundcreation_roundcreation__["a" /* RoundCreationPage */];
        this.roundEditPage = __WEBPACK_IMPORTED_MODULE_5__roundedit_roundedit__["a" /* RoundEditPage */];
        this.map_url = "http://" + this.param.localhost + "/api/maps/current/image?t=" + new Date().getTime();
        this.start = {
            x: 0,
            y: 0,
        };
        this.current_map = this.api.id_current_map;
    }
    RoundDisplayPage.prototype.ngOnInit = function () {
        this.popup.onSomethingHappened3(this.deleteRound.bind(this));
        this._CANVAS = this.canvasEl.nativeElement;
        this._CANVAS.style.width = "100%";
        this._CANVAS.style.height = "100%";
        this._CONTEXT = this._CANVAS.getContext("2d");
        this.heightcol =
            document.getElementById("body").clientHeight -
                document.getElementById("firstrow").clientHeight;
        this.widthcol = document.getElementById("testcol").clientWidth;
        this.api.getRoundList();
    };
    RoundDisplayPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        setTimeout(function () {
            if (_this.api.smgthCreated)
                _this.content1.scrollToBottom();
            _this.api.smgthCreated = false;
        }, 300);
    };
    RoundDisplayPage.prototype.open = function (ev, itemSlide) {
        ev.preventDefault();
        if (itemSlide.getSlidingPercent() == 0) {
            // Two calls intentional after examination of vendor.js function
            itemSlide.moveSliding(-390);
            itemSlide.moveSliding(-390);
        }
        else {
            itemSlide.close();
        }
    };
    RoundDisplayPage.prototype.on_zoom_out = function (ev) {
        ev.preventDefault();
        if (!(this.resomap >= this.initialzoom)) {
            this.resomap = this.resomap + 150 / 100000;
        }
        else {
            this.offsetX = 0;
            this.offsetY = 0;
        }
        this.createImageFromBlob();
    };
    RoundDisplayPage.prototype.on_zoom_in = function (ev) {
        ev.preventDefault();
        if (this.resomap > this.initialzoom / 4) {
            this.resomap = this.resomap - 150 / 100000;
        }
        this.createImageFromBlob();
    };
    RoundDisplayPage.prototype.showLoading = function () {
        if (!this.loading) {
            this.loading = this.loadingCtrl.create({
                content: this.param.datatext.wait,
            });
            this.loading.present();
        }
    };
    RoundDisplayPage.prototype.dismissLoading = function () {
        if (this.loading) {
            this.loading.dismiss();
            this.loading = null;
        }
    };
    RoundDisplayPage.prototype.ionViewDidEnter = function () {
        this.getImageFromService();
        this.api.textHeadBand = this.param.datatext.headBand_rounds;
    };
    RoundDisplayPage.prototype.ionViewDidLeave = function () {
        //clearInterval(this.interv);
    };
    RoundDisplayPage.prototype.onMapChange = function (event) {
        var _this = this;
        this.showLoading();
        this.api.changeMapbyIdHttp(event);
        setTimeout(function () {
            _this.imge.src = "http://" + _this.param.localhost + "/api/maps/current/image?t=" + new Date().getTime();
        }, 4500);
        setTimeout(function () {
            _this.api.getCurrentLocations();
            _this.api.getCurrentMap();
        }, 5500);
        setTimeout(function () {
            _this.api.getRoundList();
            _this.getImageFromService();
        }, 6500);
        setTimeout(function () {
            if (_this.current_map != _this.api.id_current_map) {
                _this.current_map = _this.api.id_current_map;
            }
            _this.dismissLoading();
        }, 8000);
    };
    RoundDisplayPage.prototype.goCopyRound = function (ev, index) {
        ev.preventDefault();
        this.api.roundpoi_list = [];
        this.api.editRound = false;
        this.api.copyRound = true;
        this.api.copy_round = this.api.round_current_map[index];
        console.log(this.api.copy_round);
        //this.api.getCurrentLocations();
        this.navCtrl.push(this.roundCreationPage);
    };
    RoundDisplayPage.prototype.popupDelete = function (ev, type, id) {
        ev.preventDefault();
        this.popup.DeleteMessage(type, id);
    };
    RoundDisplayPage.prototype.deleteRound = function (id) {
        var _this = this;
        this.alert.deleteround(this.api.mailAddInformationBasic());
        this.showLoading();
        //console.log(id);
        this.api.deleteRound(id);
        this.param.qrcodes.forEach(function (qr) {
            if (qr.id_round == id) {
                _this.api.deletePOI(qr.id_poi);
                _this.param.deleteQR(qr.qr);
            }
        });
        setTimeout(function () {
            _this.api.getRoundList();
            _this.dismissLoading();
        }, 3000);
    };
    RoundDisplayPage.prototype.goEditRound = function (ev, index) {
        ev.preventDefault();
        this.api.roundpoi_list = [];
        this.api.editRound = true;
        this.api.copyRound = false;
        this.api.copy_round = this.api.round_current_map[index];
        console.log(this.api.copy_round);
        //this.api.getCurrentLocations();
        this.navCtrl.push(this.roundEditPage);
    };
    RoundDisplayPage.prototype.goCreateRound = function (ev) {
        ev.preventDefault();
        if (this.api.id_current_map != undefined) {
            this.api.editRound = false;
            this.api.copyRound = false;
            this.api.roundpoi_list = [];
            //this.api.getCurrentLocations();
            this.navCtrl.push(this.roundCreationPage);
        }
    };
    RoundDisplayPage.prototype.on_mouse_down = function (evt) {
        this.createImageFromBlob();
        //console.log(evt);
        if (evt.button == 0) {
            evt.preventDefault();
            this.moving = true;
            this.start.x = evt.x;
            this.start.y = evt.y;
            //console.log(evt.x + " " + this.start.x);
        }
    };
    RoundDisplayPage.prototype.on_mouse_up = function (evt) {
        this.createImageFromBlob();
        //console.log(evt);
        if (this.moving) {
            var offset = {
                x: this.offsetX,
                y: this.offsetY,
                t: 0,
            };
            offset.x += evt.x - this.start.x;
            offset.y += evt.y - this.start.y;
            this.start.x = evt.x;
            this.start.y = evt.y;
            this.offsetX = offset.x;
            this.offsetY = offset.y;
        }
        this.moving = false;
    };
    RoundDisplayPage.prototype.on_mouse_move = function (evt) {
        //console.log(evt);
        if (this.moving) {
            this.createImageFromBlob();
            evt.preventDefault();
            var offset = {
                x: this.offsetX,
                y: this.offsetY,
            };
            //console.log(evt.x + " " + this.start.x);
            offset.x += evt.x - this.start.x;
            offset.y += evt.y - this.start.y;
            this.start.x = evt.x;
            this.start.y = evt.y;
            this.offsetX = offset.x;
            this.offsetY = offset.y;
        }
    };
    RoundDisplayPage.prototype.on_wheel = function (evt) {
        this.createImageFromBlob();
        evt.preventDefault();
        //console.log(evt.deltaY);
        if (evt.deltaY > 0) {
            if (!(this.resomap >= this.initialzoom)) {
                this.resomap = this.resomap + 150 / 100000;
            }
            else {
                this.offsetX = 0;
                this.offsetY = 0;
            }
        }
        else {
            if (this.resomap > this.initialzoom / 4) {
                this.resomap = this.resomap - 150 / 100000;
            }
        }
        console.log(this.resomap);
    };
    RoundDisplayPage.prototype.createImageFromBlob = function () {
        var _this = this;
        if (this._CANVAS.getContext) {
            this._CANVAS.width = this.imge.width;
            this._CANVAS.height = this.imge.height;
            if (this._CANVAS.width == 0 && this.current_map) {
                console.log("width 0");
            }
            var w = this._CANVAS.width;
            var h = this._CANVAS.height;
            var r = this.resomap;
            var x = this.offsetX;
            var y = this.offsetY;
            var xm = this.api.mapdata.Offset.X;
            var ym = this.api.mapdata.Offset.Y;
            var rm = this.api.mapdata.Resolution;
            var wm = this.api.mapdata.Width;
            var hm = this.api.mapdata.Height;
            var dx = xm / r + x;
            var dy = h - (ym + hm * rm) * (1 / r) + y;
            var dw = (wm * rm) / r;
            var dh = (hm * rm) / r;
            if (this.current_map) {
                this._CONTEXT.drawImage(this.imge, dx, dy, dw, dh);
                //console.log('dessin image');
            }
            if (this.api.all_locations != undefined) {
                this.api.all_locations.forEach(function (evenement) {
                    if (evenement.Label !== "qr") {
                        _this._CONTEXT.beginPath();
                        _this._CONTEXT.font =
                            "" + Math.round(_this._CANVAS.width * 0.025) + "px Arial";
                        //console.log(evenement);
                        _this._CONTEXT.fillStyle = "rgba(26, 156, 195,0.7)";
                        _this._CONTEXT.fillText(evenement.Id, // + "- " + evenement.Name,
                        evenement.Pose.X / _this.resomap + _this.offsetX - 20, _this._CANVAS.height -
                            (evenement.Pose.Y / _this.resomap - _this.offsetY) -
                            10);
                        if (evenement.Name === "docking" || evenement.Name === "docked") {
                            _this._CONTEXT.fillText(evenement.Id + "- " + evenement.Name, evenement.Pose.X / _this.resomap + _this.offsetX - 20, _this._CANVAS.height -
                                (evenement.Pose.Y / _this.resomap - _this.offsetY) -
                                10);
                        }
                        _this._CONTEXT.beginPath();
                        //console.log(evenement.Pose)
                        _this._CONTEXT.arc(evenement.Pose.X / _this.resomap + _this.offsetX, _this._CANVAS.height -
                            (evenement.Pose.Y / _this.resomap - _this.offsetY), _this._CANVAS.width * 0.005, 0, 2 * Math.PI);
                        _this._CONTEXT.lineWidth = 1;
                        _this._CONTEXT.fillStyle = "rgb(26, 156, 195)";
                        _this._CONTEXT.fill();
                        _this._CONTEXT.beginPath();
                        _this._CONTEXT.save();
                        _this._CONTEXT.translate(evenement.Pose.X / _this.resomap + _this.offsetX, _this._CANVAS.height -
                            (evenement.Pose.Y / _this.resomap - _this.offsetY));
                        _this._CONTEXT.rotate(-evenement.Pose.T);
                        _this._CONTEXT.moveTo(0, 0);
                        _this._CONTEXT.lineWidth = 2;
                        _this._CONTEXT.lineTo(2 * _this._CANVAS.width * 0.009, 0);
                        _this._CONTEXT.strokeStyle = "rgb(26, 156, 195)";
                        _this._CONTEXT.stroke();
                        _this._CONTEXT.restore();
                    }
                });
            }
        }
    };
    RoundDisplayPage.prototype.getImageFromService = function () {
        var _this = this;
        this.imge = document.getElementById("imgmap");
        if (this.api.id_current_map != undefined) {
            this.offsetX = this.api.mapdata.Offset.X;
            this.offsetY = this.api.mapdata.Offset.Y;
            this.resomap = this.api.mapdata.Resolution;
            this.initialzoom = this.resomap;
        }
        console.log("GetImageFromService");
        this.api.getImgMapHttp().subscribe(function (data) {
            //console.log(data);
            _this.api.imgMap = data;
            console.log("EndcreateImageFromBlob");
            _this.createImageFromBlob();
        }, function (error) {
            console.log(error);
        });
    };
    RoundDisplayPage.prototype.onSliderRelease = function (ev, id) {
        ev.preventDefault();
        id.open();
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])("canvas"),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* ElementRef */])
    ], RoundDisplayPage.prototype, "canvasEl", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])("imgmap"),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* ElementRef */])
    ], RoundDisplayPage.prototype, "imgmapElement", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])("select1"),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["j" /* Select */])
    ], RoundDisplayPage.prototype, "select1", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])("content1"),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["c" /* Content */])
    ], RoundDisplayPage.prototype, "content1", void 0);
    RoundDisplayPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: "page-rounddisplay",template:/*ion-inline-start:"C:\Users\laele\Documents\KomApp\toolbox\src\pages\rounddisplay\rounddisplay.html"*/'<!-- mail page html template -->\n\n<ion-header no-border>\n\n  <headpage pageName="rounddisplay"></headpage>\n\n</ion-header>\n\n\n\n<ion-content id="body">\n\n \n\n    <ion-grid class="heightstyle">\n\n    \n\n      <ion-row class="heightstyle" id="firstrow">\n\n        <ion-col\n\n          col-8\n\n          id="testcol"\n\n          class="heightstyle"\n\n          style="background-color: rgba(0, 0, 0, 0.904)"\n\n        >\n\n          <ion-row class="rowcanvas">\n\n            <canvas\n\n              #canvas\n\n              (wheel)="on_wheel($event);"\n\n              (mouseup)="on_mouse_up($event);"\n\n              (mousedown)="on_mouse_down($event);"\n\n              (mousemove)="on_mouse_move($event);"\n\n            ></canvas>\n\n            <img id="imgmap" #imgmap src={{map_url}} crossorigin="anonymous" class="video" style="display:none;" />\n\n            </ion-row\n\n          ><ion-row class="btnsmap">\n\n            <ion-col col-6>\n\n              <button class="btnmap" (mouseup)="on_zoom_out($event)" ion-button>\n\n                <ion-icon name="search"></ion-icon> -\n\n              </button> </ion-col\n\n            ><ion-col col-6>\n\n              <button ion-button (mouseup)="on_zoom_in($event)" class="btnmap">\n\n                <ion-icon name="search"></ion-icon> +\n\n              </button>\n\n            </ion-col>\n\n          </ion-row>\n\n        </ion-col>\n\n        <ion-col col-4 style="background-color: rgb(182, 217, 230)">\n\n          <ion-row class="heightstyle10">\n\n            <ion-col col-9 style="background-color: rgb(182, 217, 230)">\n\n              <ion-item style="background-color: transparent">\n\n                <ion-label>\n\n                  <h1 style="font-size: x-large">\n\n                    {{param.datatext.rounds_list}}\n\n                  </h1>\n\n                </ion-label></ion-item\n\n              >\n\n            </ion-col>\n\n          <ion-col col-3 style="background-color: rgb(182, 217, 230)">\n\n            <ion-buttons class="div_btn_map">\n\n              <button\n\n                [hidden]="newpoi || editpoi || gopoi || api.towardPOI || newmap || editmap"\n\n                (mouseup)="goCreateRound($event)"\n\n                ion-button\n\n                class="btn_map"\n\n                *ngIf="api.id_current_map!=undefined"\n\n              >\n\n                <ion-icon style="font-size: 3vw" name="add"></ion-icon></button\n\n            ></ion-buttons>\n\n          </ion-col>\n\n        </ion-row>\n\n        <ion-row class="heightstyle90">\n\n          \n\n          <ion-content #content1 class="scroll" style="height: 100%; width: 100%; background-color: rgb(182, 217, 230)">\n\n            <ion-list>\n\n              <ion-item-sliding\n\n                *ngFor="let m of this.api.round_current_map; let index = index;"\n\n                style="background-color: transparent"\n\n                (mouseup)="open($event,slidingItem)"\n\n                #slidingItem\n\n              >\n\n                <ion-item style="background-color: rgba(255, 255, 255, 0.76)">\n\n                {{m.Id}}. {{m.Name}}\n\n                </ion-item>\n\n\n\n                <ion-item-options side="right">\n\n                  <button\n\n                    [disabled]="!api.adminMode"\n\n                    style="min-width: 60px"\n\n                    ion-button\n\n                    color="danger"\n\n                    (mouseup)="popupDelete($event,\'Round\',m.Id)"\n\n                  >\n\n                    <ion-icon name="trash"></ion-icon>\n\n                    {{param.datatext.delete}}\n\n                  </button>\n\n                  <button\n\n                    ion-button\n\n                    style="min-width: 60px"\n\n                    color="primary"\n\n                    (mouseup)="goCopyRound($event,index)"\n\n                  >\n\n                    <ion-icon name="copy"></ion-icon>\n\n                    {{param.datatext.copy}}\n\n                  </button>\n\n                  <button\n\n                    ion-button\n\n                    style="min-width: 60px"\n\n                    color="secondary"\n\n                    (mouseup)="goEditRound($event,index)"\n\n                  >\n\n                    <ion-icon name="create"></ion-icon>\n\n                    {{param.datatext.edit}}\n\n                  </button>\n\n                </ion-item-options>\n\n              </ion-item-sliding>\n\n            </ion-list></ion-content\n\n          >\n\n        </ion-row>\n\n        </ion-col>\n\n      </ion-row>\n\n    </ion-grid>\n\n  \n\n</ion-content>\n\n'/*ion-inline-end:"C:\Users\laele\Documents\KomApp\toolbox\src\pages\rounddisplay\rounddisplay.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["g" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["h" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1__services_param_service__["a" /* ParamService */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["k" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_3__services_api_service__["a" /* ApiService */],
            __WEBPACK_IMPORTED_MODULE_6__services_popup_service__["a" /* PopupService */],
            __WEBPACK_IMPORTED_MODULE_7__services_alert_service__["a" /* AlertService */]])
    ], RoundDisplayPage);
    return RoundDisplayPage;
}());

//# sourceMappingURL=rounddisplay.js.map

/***/ }),

/***/ 265:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RoundEditPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_param_service__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_api_service__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__roundcreation_roundcreation__ = __webpack_require__(142);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__services_alert_service__ = __webpack_require__(21);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var RoundEditPage = /** @class */ (function () {
    function RoundEditPage(api, navCtrl, loadingCtrl, param, toastCtrl, alert) {
        this.api = api;
        this.navCtrl = navCtrl;
        this.loadingCtrl = loadingCtrl;
        this.param = param;
        this.toastCtrl = toastCtrl;
        this.alert = alert;
        this.roundEditPage = RoundEditPage_1;
        this.roundCreationPage = __WEBPACK_IMPORTED_MODULE_4__roundcreation_roundcreation__["a" /* RoundCreationPage */];
        this.map_url = "http://" + this.param.localhost + "/api/maps/current/image?t=" + new Date().getTime();
        this.start = {
            x: 0,
            y: 0,
        };
    }
    RoundEditPage_1 = RoundEditPage;
    RoundEditPage.prototype.ngOnInit = function () {
        this._CANVAS = this.canvasEl.nativeElement;
        this._CONTEXT = this._CANVAS.getContext("2d");
        this._CANVAS.style.width = "100%";
        this._CANVAS.style.height = "100%";
        this.heightcol =
            document.getElementById("body2").clientHeight -
                document.getElementById("firstrow2").clientHeight;
        this.widthcol = document.getElementById("testcol2").clientWidth;
    };
    RoundEditPage.prototype.ionViewDidEnter = function () {
        this.api.roundpoi_list = [];
        this.displayCopy();
        this.getImageFromService();
    };
    RoundEditPage.prototype.ionViewWillLeave = function () {
    };
    RoundEditPage.prototype.on_zoom_out = function (ev) {
        ev.preventDefault();
        if (!(this.resomap >= this.initialzoom)) {
            this.resomap = this.resomap + 150 / 100000;
        }
        else {
            this.offsetX = 0;
            this.offsetY = 0;
        }
        this.createImageFromBlob();
    };
    RoundEditPage.prototype.on_zoom_in = function (ev) {
        ev.preventDefault();
        if (this.resomap > this.initialzoom / 4) {
            this.resomap = this.resomap - 150 / 100000;
        }
        this.createImageFromBlob();
    };
    RoundEditPage.prototype.displayCopy = function () {
        var _this = this;
        console.log(this.api.copy_round.Locations);
        this.api.copy_round.Locations.forEach(function (element) {
            _this.api.createRoundList(element.Location.Id, element.Location.Name, element.Wait, element.Speed, element.Data, element.AnticollisionProfile, element.DisableAvoidance);
        });
    };
    RoundEditPage.prototype.goEditRound = function (ev) {
        this.alert.editround(this.api.mailAddInformationBasic());
        ev.preventDefault();
        this.navCtrl.push(this.roundCreationPage);
    };
    RoundEditPage.prototype.on_mouse_down = function (evt) {
        this.createImageFromBlob();
        //console.log(evt);
        if (evt.button == 0) {
            evt.preventDefault();
            this.moving = true;
            this.start.x = evt.x;
            this.start.y = evt.y;
            //console.log(evt.x + " " + this.start.x);
        }
    };
    RoundEditPage.prototype.on_mouse_up = function (evt) {
        this.createImageFromBlob();
        //console.log(evt);
        if (this.moving) {
            var offset = {
                x: this.offsetX,
                y: this.offsetY,
                t: 0,
            };
            offset.x += evt.x - this.start.x;
            offset.y += evt.y - this.start.y;
            this.start.x = evt.x;
            this.start.y = evt.y;
            this.offsetX = offset.x;
            this.offsetY = offset.y;
        }
        this.moving = false;
    };
    RoundEditPage.prototype.on_mouse_move = function (evt) {
        //console.log(evt);
        if (this.moving) {
            this.createImageFromBlob();
            evt.preventDefault();
            var offset = {
                x: this.offsetX,
                y: this.offsetY,
            };
            //console.log(evt.x + " " + this.start.x);
            offset.x += evt.x - this.start.x;
            offset.y += evt.y - this.start.y;
            this.start.x = evt.x;
            this.start.y = evt.y;
            this.offsetX = offset.x;
            this.offsetY = offset.y;
        }
    };
    RoundEditPage.prototype.on_wheel = function (evt) {
        this.createImageFromBlob();
        evt.preventDefault();
        //console.log(evt.deltaY);
        if (evt.deltaY > 0) {
            if (!(this.resomap >= this.initialzoom)) {
                this.resomap = this.resomap + 150 / 100000;
            }
            else {
                this.offsetX = 0;
                this.offsetY = 0;
            }
        }
        else {
            if (this.resomap > this.initialzoom / 4) {
                this.resomap = this.resomap - 150 / 100000;
            }
        }
        console.log(this.resomap);
    };
    RoundEditPage.prototype.createImageFromBlob = function () {
        //console.log("createImageFromBlob");
        var _this = this;
        if (this._CANVAS.getContext) {
            this._CANVAS.width = this.imge.width;
            this._CANVAS.height = this.imge.height;
            var w = this._CANVAS.width;
            var h = this._CANVAS.height;
            var r = this.resomap;
            var x = this.offsetX;
            var y = this.offsetY;
            var xm = this.api.mapdata.Offset.X;
            var ym = this.api.mapdata.Offset.Y;
            var rm = this.api.mapdata.Resolution;
            var wm = this.api.mapdata.Width;
            var hm = this.api.mapdata.Height;
            var dx = xm / r + x;
            var dy = h - (ym + hm * rm) * (1 / r) + y;
            var dw = (wm * rm) / r;
            var dh = (hm * rm) / r;
            this._CONTEXT.drawImage(this.imge, dx, dy, dw, dh);
            //console.log('dessin image');
            this.api.copy_round.Locations.forEach(function (evenement) {
                if (evenement.Label !== "qr") {
                    _this._CONTEXT.beginPath();
                    _this._CONTEXT.font =
                        "" + Math.round(_this._CANVAS.width * 0.025) + "px Arial";
                    //console.log(evenement);
                    _this._CONTEXT.fillStyle = "rgba(26, 156, 195,0.7)";
                    _this._CONTEXT.fillText(evenement.Location.Id, // + "- " + evenement.Location.Name,
                    evenement.Location.Pose.X / _this.resomap + _this.offsetX - 20, _this._CANVAS.height -
                        (evenement.Location.Pose.Y / _this.resomap - _this.offsetY) -
                        10);
                    _this._CONTEXT.beginPath();
                    //console.log(evenement.Pose)
                    _this._CONTEXT.arc(evenement.Location.Pose.X / _this.resomap + _this.offsetX, _this._CANVAS.height -
                        (evenement.Location.Pose.Y / _this.resomap - _this.offsetY), _this._CANVAS.width * 0.005, 0, 2 * Math.PI);
                    _this._CONTEXT.lineWidth = 1;
                    if (_this.api.copy_round.Locations[0].Location.Id == evenement.Location.Id) {
                        _this._CONTEXT.fillStyle = "rgb(145,221,145)";
                    }
                    else {
                        if (_this.api.copy_round.Locations[_this.api.copy_round.Locations.length - 1].Location.Id == evenement.Location.Id) {
                            _this._CONTEXT.fillStyle = "rgb(221,145,145)";
                        }
                        else {
                            _this._CONTEXT.fillStyle = "rgb(26, 156, 195)";
                        }
                    }
                    _this._CONTEXT.fill();
                    _this._CONTEXT.beginPath();
                    _this._CONTEXT.save();
                    _this._CONTEXT.translate(evenement.Location.Pose.X / _this.resomap + _this.offsetX, _this._CANVAS.height -
                        (evenement.Location.Pose.Y / _this.resomap - _this.offsetY));
                    _this._CONTEXT.rotate(-evenement.Location.Pose.T);
                    _this._CONTEXT.moveTo(0, 0);
                    _this._CONTEXT.lineWidth = 2;
                    _this._CONTEXT.lineTo(2 * _this._CANVAS.width * 0.009, 0);
                    _this._CONTEXT.strokeStyle = "rgb(26, 156, 195)";
                    _this._CONTEXT.stroke();
                    _this._CONTEXT.restore();
                }
            });
            for (var i = 0; i < this.api.copy_round.Locations.length - 1; i++) {
                this._CONTEXT.beginPath();
                if (this.api.copy_round.Locations[i + 1].Speed > 0) {
                    this._CONTEXT.setLineDash([]);
                }
                else {
                    this._CONTEXT.setLineDash([5]);
                }
                this._CONTEXT.moveTo(this.api.copy_round.Locations[i].Location.Pose.X / this.resomap + this.offsetX, this._CANVAS.height -
                    (this.api.copy_round.Locations[i].Location.Pose.Y / this.resomap - this.offsetY));
                this._CONTEXT.lineTo(this.api.copy_round.Locations[i + 1].Location.Pose.X / this.resomap + this.offsetX, this._CANVAS.height -
                    (this.api.copy_round.Locations[i + 1].Location.Pose.Y / this.resomap - this.offsetY));
                this._CONTEXT.stroke();
            }
            //this._CONTEXT.stroke();
            this._CONTEXT.restore();
        }
    };
    ;
    RoundEditPage.prototype.getImageFromService = function () {
        var _this = this;
        this.imge = document.getElementById("imgmap2");
        if (this.api.id_current_map != undefined) {
            this.offsetX = this.api.mapdata.Offset.X;
            this.offsetY = this.api.mapdata.Offset.Y;
            this.resomap = this.api.mapdata.Resolution;
            this.initialzoom = this.resomap;
        }
        console.log("GetImageFromService");
        this.api.getImgMapHttp().subscribe(function (data) {
            _this.api.imgMap = data;
            _this.createImageFromBlob();
            console.log("GetImageFromService");
        }, function (error) {
            console.log(error);
        });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])("canvas2"),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* ElementRef */])
    ], RoundEditPage.prototype, "canvasEl", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])("imgmap2"),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* ElementRef */])
    ], RoundEditPage.prototype, "imgmapElement", void 0);
    RoundEditPage = RoundEditPage_1 = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: "page-roundedit",template:/*ion-inline-start:"C:\Users\laele\Documents\KomApp\toolbox\src\pages\roundedit\roundedit.html"*/'<!-- mail page html template -->\n\n<ion-header no-border>\n\n  <headpage pageName="roundedit"></headpage>\n\n</ion-header>\n\n\n\n<ion-content id="body2">\n\n\n\n    <ion-grid class="heightstyle">\n\n      <ion-row id="firstrow2" class="heightstyle10">\n\n        <ion-col col-5>\n\n          <ion-item>\n\n            <ion-label\n\n              >{{param.datatext.map}} : {{api.name_current_map}}</ion-label\n\n            >\n\n          </ion-item>\n\n        </ion-col>\n\n        <ion-col col-3></ion-col>\n\n        <ion-col col-4 style="background-color: rgb(182, 217, 230)">\n\n          <ion-item style="background-color: transparent">\n\n            <ion-label>\n\n              <h1 style="font-size: x-large">\n\n                {{param.datatext.POI_edit}} {{api.copy_round.Name}}\n\n              </h1>\n\n            </ion-label></ion-item\n\n          ></ion-col\n\n        >\n\n      </ion-row>\n\n      <ion-row class="heightstyle90">\n\n        <ion-col\n\n          col-8\n\n          id="testcol2"\n\n          style="background-color: rgba(0, 0, 0, 0.904)"\n\n          class="heightstyle"\n\n        >\n\n          <ion-row class="rowcanvas">\n\n            <canvas\n\n              #canvas2\n\n              \n\n              (wheel)="on_wheel($event);"\n\n             \n\n              (mouseup)="on_mouse_up($event);"\n\n              (mousedown)="on_mouse_down($event);"\n\n              (mousemove)="on_mouse_move($event);"\n\n            ></canvas>\n\n            <img id="imgmap2" #imgmap2 src={{map_url}} crossorigin="anonymous" class="video" style="display:none;" />\n\n            </ion-row\n\n          ><ion-row class="btnsmap">\n\n            <ion-col col-6>\n\n              <button class="btnmap" (mouseup)="on_zoom_out($event)" ion-button>\n\n                <ion-icon name="search"></ion-icon> -\n\n              </button> </ion-col\n\n            ><ion-col col-6>\n\n              <button ion-button (mouseup)="on_zoom_in($event)" class="btnmap">\n\n                <ion-icon name="search"></ion-icon> +\n\n              </button>\n\n            </ion-col>\n\n          </ion-row>\n\n        </ion-col>\n\n        <ion-col col-4 style="background-color: rgb(182, 217, 230)">\n\n          <ion-scroll scrollY="true" class="poilist">\n\n            <ion-list\n\n              *ngFor="let m of this.api.roundpoi_list; let index = index;"\n\n            >\n\n              <ion-item style="background-color: rgba(255, 255, 255, 0.76)">\n\n                <h2>{{m.location.id}}. {{m.name}}</h2>\n\n                <p>\n\n                  {{param.datatext.break}}{{m.wait}},\n\n                  {{param.datatext.speed}}{{m.speed*100}}\n\n                </p>\n\n                <p>\n\n                  {{param.datatext.avoidanceOption}}{{!m.disableAvoidance}},\n\n                  {{param.datatext.option}}{{m.data}},{{param.datatext.gabarit}}:{{m.anticollisionProfile}}\n\n                </p>\n\n                <p>\n\n                  {{param.datatext.gabarit}}:{{m.anticollisionProfile}}\n\n                </p>\n\n              </ion-item>\n\n            </ion-list>\n\n          </ion-scroll>\n\n          <ion-buttons class="div_btn_map">\n\n            <button\n\n              [disabled]="!api.adminMode"\n\n              ion-button\n\n              color="secondary"\n\n              class="btnedit"\n\n              (mouseup)="goEditRound($event)"\n\n            >\n\n              <ion-icon class="icon_style" name="create"></ion-icon></button\n\n          ></ion-buttons>\n\n        </ion-col>\n\n      </ion-row>\n\n    </ion-grid>\n\n\n\n</ion-content>\n\n'/*ion-inline-end:"C:\Users\laele\Documents\KomApp\toolbox\src\pages\roundedit\roundedit.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__services_api_service__["a" /* ApiService */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["h" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["g" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_1__services_param_service__["a" /* ParamService */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["k" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_5__services_alert_service__["a" /* AlertService */]])
    ], RoundEditPage);
    return RoundEditPage;
    var RoundEditPage_1;
}());

//# sourceMappingURL=roundedit.js.map

/***/ }),

/***/ 266:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return QRCodePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_alert_service__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_api_service__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_param_service__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__services_popup_service__ = __webpack_require__(35);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_jsqr__ = __webpack_require__(263);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_jsqr___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_jsqr__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__services_speech_service__ = __webpack_require__(54);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var QRCodePage = /** @class */ (function () {
    function QRCodePage(navCtrl, loadingCtrl, renderer, toastCtrl, popup, api, param, speech, alert) {
        this.navCtrl = navCtrl;
        this.loadingCtrl = loadingCtrl;
        this.renderer = renderer;
        this.toastCtrl = toastCtrl;
        this.popup = popup;
        this.api = api;
        this.param = param;
        this.speech = speech;
        this.alert = alert;
        this.start = {
            x: 0,
            y: 0,
        };
        this.clickpose = {
            x: 0,
            y: 0,
        };
        this.map_url = "http://" + this.param.localhost + "/api/maps/current/image?t=" + new Date().getTime();
        this.videoWidth = 0;
        this.videoHeight = 0;
        this.constraints = {
            //audio: false,
            //video: {deviceId: "f9860b260e18d7fd1687d8567e21b49c4bb26f87606020741717e11f6179f409"},
            video: true,
            facingMode: "environment",
            width: { ideal: 1920 },
            height: { ideal: 1080 },
        };
        this.addqr = false;
        this.qrdetected = false;
        this.link_to_round = false;
        this.reloc = false;
        if (this.param.robot.cam_USB == 1) {
            this.setupconstraint();
        }
    }
    QRCodePage.prototype.ngOnInit = function () {
        this.popup.onSomethingHappened4(this.deleteqr.bind(this));
        this._CANVAS = this.canvasEl.nativeElement;
        this._CANVAS.style.width = "100%";
        this._CANVAS.style.height = "100%";
        this._CONTEXT = this._CANVAS.getContext("2d");
        this.heightcol =
            document.getElementById("body5").clientHeight -
                document.getElementById("firstrow5").clientHeight * 2;
        this.widthcol = document.getElementById("testcol5").clientWidth;
        this.qrdetection = false;
    };
    QRCodePage.prototype.ionViewDidEnter = function () {
        var _this = this;
        this.qrdetection = false;
        this.current_map = this.api.id_current_map;
        if (this.api.id_current_map != undefined) {
            this.api.getCurrentLocations();
        }
        this.api.getRoundList();
        this.getImageFromService();
        this.intervmap = setInterval(function () { return _this.updatemap(); }, 400);
        // CAMERA ROS
        if (this.param.robot.cam_USB == 0) {
            this.api.listener_camera.subscribe(function (message) {
                //console.log('Received message on ' + listener_compressed.name + ': ');
                _this.imgRos = document.getElementById('imgcompressed2');
                //console.log(message);
                var bufferdata = _this.convertDataURIToBinary2(message.data);
                var blob = new Blob([bufferdata], { type: 'image/jpeg' });
                var blobUrl = URL.createObjectURL(blob);
                _this.imgRos.src = blobUrl;
            });
        }
        // CAMERA ROS FIN
    };
    QRCodePage.prototype.setupconstraint = function () {
        var _this = this;
        // demande de permission camera
        navigator.mediaDevices.getUserMedia(this.constraints);
        // verification des permissions video et de la disponibilité de devices
        if (!!(navigator.mediaDevices && navigator.mediaDevices.getUserMedia)) {
            // enumeration des appareils connectes pour filtrer
            navigator.mediaDevices.enumerateDevices().then(function (result) {
                var constraints;
                console.log(result);
                result.forEach(function (device) {
                    // filtrer pour garder les flux video
                    if (device.kind.includes("videoinput")) {
                        // filtrer le label de la camera
                        if (device.label.includes("USB")) {
                            constraints = {
                                audio: false,
                                //video: {deviceId: "f9860b260e18d7fd1687d8567e21b49c4bb26f87606020741717e11f6179f409"},
                                // affecter le deviceid filtré
                                video: { deviceId: device.deviceId },
                                facingMode: "environment",
                                width: { ideal: 1920 },
                                height: { ideal: 1080 },
                            };
                        }
                    }
                });
                // on lance la connexion de la vidéo
                if (constraints) {
                    _this.constraints = constraints;
                    _this.startCamera(constraints);
                    console.log("startcam");
                }
                //this.startCamera(constraints);
            });
        }
        else {
            console.log("Sorry, camera not available.");
        }
    };
    QRCodePage.prototype.startCamera = function (cs) {
        if (!!(navigator.mediaDevices && navigator.mediaDevices.getUserMedia)) {
            // recupération du flux avec les constraints configurés puis on attache le flux à la balise video
            navigator.mediaDevices
                .getUserMedia(cs)
                .then(this.attachVideo.bind(this))
                .catch(this.handleError);
        }
        else {
            alert("Sorry, camera not available.");
        }
    };
    QRCodePage.prototype.handleError = function (error) {
        console.log("Error: ", error);
    };
    QRCodePage.prototype.attachVideo = function (stream) {
        var _this = this;
        this.renderer.setProperty(this.videoElement2.nativeElement, "srcObject", stream);
        this.renderer.listen(this.videoElement2.nativeElement, "play", function (event) {
            _this.videoHeight = _this.videoElement2.nativeElement.videoHeight;
            _this.videoWidth = _this.videoElement2.nativeElement.videoWidth;
        });
    };
    QRCodePage.prototype.convertDataURIToBinary2 = function (dataURI) {
        var raw = window.atob(dataURI);
        var rawLength = raw.length;
        var array = new Uint8Array(new ArrayBuffer(rawLength));
        for (var i = 0; i < rawLength; i++) {
            array[i] = raw.charCodeAt(i);
        }
        return array;
    };
    QRCodePage.prototype.open = function (ev, itemSlide) {
        ev.preventDefault();
        if (itemSlide.getSlidingPercent() == 0) {
            // Two calls intentional after examination of vendor.js function
            itemSlide.moveSliding(-370);
            itemSlide.moveSliding(-370);
        }
        else {
            itemSlide.close();
        }
    };
    QRCodePage.prototype.updatemap = function () {
        if (this.reloc || this.api.differential_status.CurrentAngularSpeed > 0.005 || this.api.differential_status.CurrentLinearSpeed > 0.01 || this._CANVAS.width == 0) {
            this.createImageFromBlob();
        }
    };
    QRCodePage.prototype.createqr = function (ev) {
        var _this = this;
        ev.preventDefault();
        var poi;
        this.showLoading();
        this.api.postqr(this.qr_read, this.px, this.py, this.pt);
        setTimeout(function () {
            _this.api.changeMapbyIdHttp(_this.api.id_current_map);
            _this.api.getCurrentLocations();
        }, 3000);
        setTimeout(function () {
            poi = _this.api.all_locations[_this.api.all_locations.length - 1].Id;
        }, 5000);
        setTimeout(function () {
            if (_this.link_to_round) {
                _this.param.addQR(_this.qr_read, _this.api.id_current_map, poi, _this.round);
            }
            else {
                _this.param.addQR(_this.qr_read, _this.api.id_current_map, poi, -1);
            }
            _this.createImageFromBlob();
        }, 6000);
        setTimeout(function () {
            _this.param.getQR();
            _this.dismissLoading();
            _this.addqr = false;
            _this.qrdetected = false;
            _this.content1.scrollToBottom(1000);
        }, 8000);
    };
    QRCodePage.prototype.on_click_relocate = function (ev) {
        ev.preventDefault();
        if (!this.reloc) {
            this.reloc = true;
            console.log(this.clickpose);
            this.api.putLocalization(this.xtest, this.ytest, 0);
        }
        else {
            this.reloc = false;
        }
    };
    QRCodePage.prototype.on_click_relocate_left = function (ev) {
        ev.preventDefault();
        this.api.putLocalization(this.api.localization_status.positionx, this.api.localization_status.positiony, (this.api.localization_status.positiont + 0.46) % (2 * Math.PI));
    };
    QRCodePage.prototype.on_click_relocate_right = function (ev) {
        ev.preventDefault();
        this.api.putLocalization(this.api.localization_status.positionx, this.api.localization_status.positiony, (this.api.localization_status.positiont - 0.46) % (2 * Math.PI));
    };
    QRCodePage.prototype.scan = function (ev) {
        var _this = this;
        ev.preventDefault();
        this.alert.scanqr(this.api.mailAddInformationBasic());
        //console.log("enter scan");
        if (this.param.robot.cam_USB == 1) {
            console.log("enter scan usb");
            this.qrdetection = true;
            setTimeout(function () {
                if (_this.qrdetection) {
                    _this.qrdetection = false;
                }
            }, 3000);
            try {
                this.videoCanvas2.nativeElement.height = this.videoElement2.nativeElement.videoHeight;
                this.videoCanvas2.nativeElement.width = this.videoElement2.nativeElement.videoWidth;
                var g = this.videoCanvas2.nativeElement.getContext("2d");
                g.drawImage(this.videoElement2.nativeElement, 0, 0, this.videoCanvas2.nativeElement.width, this.videoCanvas2.nativeElement.height);
                var imageData = g.getImageData(0, 0, this.videoCanvas2.nativeElement.width, this.videoCanvas2.nativeElement.height);
                var code_1 = __WEBPACK_IMPORTED_MODULE_6_jsqr___default()(imageData.data, imageData.width, imageData.height, {
                    inversionAttempts: "dontInvert",
                });
                if (code_1) {
                    console.log(code_1.data.trim());
                    if (code_1.data !== null &&
                        this.param.qrcodes.filter(function (x) { return x.qr == code_1.data.trim(); })
                            .length > 0) {
                        this.popup.showToastRed(this.param.datatext.qrcodeUsed1 +
                            code_1.data.trim() +
                            this.param.datatext.qrcodeUsed2, 4000, "middle");
                        this.speech.speak(this.param.datatext.error + this.param.datatext.qrcodeUsed1 +
                            this.param.datatext.qrcodeUsed2);
                    }
                    else if (code_1.data !== null &&
                        this.param.qrcodes.filter(function (x) { return x.qr == code_1.data.trim(); })
                            .length === 0) {
                        this.qr_read = code_1.data.trim();
                        this.addqr = false;
                        this.qrdetected = true;
                        this.px = this.api.localization_status.positionx;
                        this.py = this.api.localization_status.positiony;
                        this.pt = this.api.localization_status.positiont;
                    }
                }
                else {
                    this.popup.showToastRed(this.param.datatext.noqrdetected, 5000, "middle");
                    this.speech.speak(this.param.datatext.noqrdetected);
                }
            }
            catch (err) {
                console.log("Error", err);
            }
        }
        else if (this.param.robot.cam_USB == 0) {
            console.log("scan qr ros");
            this.qrdetection = true;
            this.api.startQrDetection(true);
            setTimeout(function () {
                if (_this.qrdetection) {
                    _this.qrdetection = false;
                    _this.api.startQrDetection(false);
                    _this.popup.showToastRed(_this.param.datatext.noqrdetected, 3000, "middle");
                    _this.speech.speak(_this.param.datatext.noqrdetected);
                }
            }, 10000);
            this.api.listener_qr.subscribe(function (message) {
                console.log('Received message on ' + _this.api.listener_qr.name + ': ' + message.data);
                if (_this.qrdetection &&
                    _this.param.qrcodes.filter(function (x) { return x.qr == message.data.trim(); })
                        .length > 0) {
                    _this.popup.showToastRed(_this.param.datatext.qrcodeUsed1 +
                        message.data.trim() +
                        _this.param.datatext.qrcodeUsed2, 3000, "middle");
                    _this.speech.speak(_this.param.datatext.error + _this.param.datatext.qrcodeUsed1 +
                        _this.param.datatext.qrcodeUsed2);
                }
                else {
                    _this.qr_read = message.data.trim();
                    _this.addqr = false;
                    _this.qrdetected = true;
                    _this.px = _this.api.localization_status.positionx;
                    _this.py = _this.api.localization_status.positiony;
                    _this.pt = _this.api.localization_status.positiont;
                }
                _this.qrdetection = false;
                _this.api.startQrDetection(false);
            });
        }
    };
    QRCodePage.prototype.popupDelete = function (ev, qr, poi) {
        ev.preventDefault();
        this.popup.DeleteMessage(qr, poi);
    };
    QRCodePage.prototype.deleteqr = function (qr, poi) {
        var _this = this;
        this.showLoading();
        this.api.deletePOI(poi);
        this.param.deleteQR(qr);
        setTimeout(function () {
            _this.param.getQR();
            _this.api.getCurrentLocations();
        }, 2000);
        setTimeout(function () {
            _this.createImageFromBlob();
            _this.dismissLoading();
        }, 3000);
    };
    QRCodePage.prototype.ionViewWillLeave = function () {
        if (this.param.robot.cam_USB == 0) {
            this.api.listener_camera.removeAllListeners();
            this.api.startQrDetection(false);
        }
        clearInterval(this.intervmap);
    };
    QRCodePage.prototype.canceladdqr = function (ev) {
        ev.preventDefault();
        this.addqr = false;
        this.qrdetected = false;
        this.qrdetection = false;
        if (this.param.robot.cam_USB == 0) {
            this.api.startQrDetection(false);
        }
    };
    QRCodePage.prototype.onClickAddQR = function (ev) {
        ev.preventDefault();
        this.addqr = true;
    };
    QRCodePage.prototype.showLoading = function () {
        if (!this.loading) {
            this.loading = this.loadingCtrl.create({
                content: this.param.datatext.wait,
            });
            this.loading.present();
        }
    };
    QRCodePage.prototype.dismissLoading = function () {
        if (this.loading) {
            this.loading.dismiss();
            this.loading = null;
        }
    };
    QRCodePage.prototype.onMapChange = function (event) {
        var _this = this;
        this.showLoading();
        this.api.changeMapbyIdHttp(event);
        setTimeout(function () {
            _this.imge.src = "http://" + _this.param.localhost + "/api/maps/current/image?t=" + new Date().getTime();
        }, 4500);
        setTimeout(function () {
            _this.api.getCurrentLocations();
            _this.api.getCurrentMap();
        }, 5500);
        setTimeout(function () {
            _this.api.getRoundList();
            _this.getImageFromService();
        }, 6500);
        setTimeout(function () {
            if (_this.current_map != _this.api.id_current_map) {
                _this.current_map = _this.api.id_current_map;
            }
            _this.dismissLoading();
        }, 8000);
    };
    QRCodePage.prototype.on_mouse_down = function (evt) {
        this.createImageFromBlob();
        //console.log(evt);
        if (evt.button == 0) {
            evt.preventDefault();
            this.moving = true;
            this.start.x = evt.x;
            this.start.y = evt.y;
            //console.log(evt.x + " " + this.start.x);
            //var t = evt.changedTouches[0];
            //var x = evt.x;
            //var y = evt.y;
            var x = (evt.offsetX * this._CANVAS.width) / this._CANVAS.clientWidth;
            //var y = evt.offsetY;
            var y = (evt.offsetY * this._CANVAS.height) / this._CANVAS.clientHeight;
            this.clickpose.x =
                (x - this.offsetX - this._CANVAS.width / 2) * this.resomap;
            this.clickpose.y =
                -(y - this.offsetY - this._CANVAS.height / 2) * this.resomap;
            console.log(this.clickpose);
            console.log(evt.x + " " + evt.y);
        }
        if (this.reloc) {
            console.log(this.clickpose);
            this.api.putLocalization(this.xtest, this.ytest, 0);
        }
    };
    QRCodePage.prototype.on_mouse_up = function (evt) {
        this.createImageFromBlob();
        //console.log(evt);
        if (this.moving) {
            var offset = {
                x: this.offsetX,
                y: this.offsetY,
                t: 0,
            };
            offset.x += evt.x - this.start.x;
            offset.y += evt.y - this.start.y;
            this.start.x = evt.x;
            this.start.y = evt.y;
            this.offsetX = offset.x;
            this.offsetY = offset.y;
        }
        this.moving = false;
        if (this.reloc) {
            console.log(this.clickpose);
            this.api.putLocalization(this.xtest, this.ytest, 0);
        }
    };
    QRCodePage.prototype.on_mouse_move = function (evt) {
        if (this.moving) {
            this.createImageFromBlob();
            evt.preventDefault();
            var offset = {
                x: this.offsetX,
                y: this.offsetY,
            };
            offset.x += evt.x - this.start.x;
            offset.y += evt.y - this.start.y;
            this.start.x = evt.x;
            this.start.y = evt.y;
            this.offsetX = offset.x;
            this.offsetY = offset.y;
        }
    };
    QRCodePage.prototype.on_wheel = function (evt) {
        this.createImageFromBlob();
        evt.preventDefault();
        //console.log(evt.deltaY);
        if (evt.deltaY > 0) {
            if (!(this.resomap >= this.initialzoom)) {
                this.resomap = this.resomap + 150 / 100000;
            }
            else {
                this.offsetX = 0;
                this.offsetY = 0;
            }
        }
        else {
            if (this.resomap > this.initialzoom / 4) {
                this.resomap = this.resomap - 150 / 100000;
            }
        }
        console.log(this.resomap);
    };
    QRCodePage.prototype.getname = function (id_round) {
        return this.api.round_current_map.filter(function (x) { return x.Id == id_round; })[0].Name;
    };
    QRCodePage.prototype.createImageFromBlob = function () {
        var _this = this;
        console.log("createImageFromBlob");
        if (this._CANVAS.getContext) {
            this._CANVAS.width = this.imge.width;
            this._CANVAS.height = this.imge.height;
            var w = this._CANVAS.width;
            var h = this._CANVAS.height;
            var r = this.resomap;
            var x = this.offsetX;
            var y = this.offsetY;
            var xm = this.api.mapdata.Offset.X;
            var ym = this.api.mapdata.Offset.Y;
            var rm = this.api.mapdata.Resolution;
            var wm = this.api.mapdata.Width;
            var hm = this.api.mapdata.Height;
            var dx = xm / r + x;
            var dy = h - (ym + hm * rm) * (1 / r) + y;
            var dw = (wm * rm) / r;
            var dh = (hm * rm) / r;
            this._CONTEXT.drawImage(this.imge, dx, dy, dw, dh);
            //console.log('dessin image');
            if (this.api.all_locations != undefined) {
                this.api.all_locations.forEach(function (evenement) {
                    if (evenement.Label === "qr") {
                        _this._CONTEXT.beginPath();
                        _this._CONTEXT.font =
                            "" + Math.round(_this._CANVAS.width * 0.025) + "px Arial";
                        //console.log(evenement);
                        _this._CONTEXT.fillStyle = "rgba(26, 156, 195,0.7)";
                        _this._CONTEXT.fillText(evenement.Id + "- " + evenement.Name, evenement.Pose.X / _this.resomap + _this.offsetX - 20, _this._CANVAS.height -
                            (evenement.Pose.Y / _this.resomap - _this.offsetY) -
                            10);
                        _this._CONTEXT.beginPath();
                        //console.log(evenement.Pose)
                        _this._CONTEXT.arc(evenement.Pose.X / _this.resomap + _this.offsetX, _this._CANVAS.height -
                            (evenement.Pose.Y / _this.resomap - _this.offsetY), _this._CANVAS.width * 0.005, 0, 2 * Math.PI);
                        _this._CONTEXT.lineWidth = 1;
                        _this._CONTEXT.fillStyle = "rgb(26, 156, 195)";
                        _this._CONTEXT.fill();
                        _this._CONTEXT.beginPath();
                        _this._CONTEXT.save();
                        _this._CONTEXT.translate(evenement.Pose.X / _this.resomap + _this.offsetX, _this._CANVAS.height -
                            (evenement.Pose.Y / _this.resomap - _this.offsetY));
                        _this._CONTEXT.rotate(-evenement.Pose.T);
                        _this._CONTEXT.moveTo(0, 0);
                        _this._CONTEXT.lineWidth = 2;
                        _this._CONTEXT.lineTo(2 * _this._CANVAS.width * 0.009, 0);
                        _this._CONTEXT.strokeStyle = "rgb(26, 156, 195)";
                        _this._CONTEXT.stroke();
                        _this._CONTEXT.restore();
                    }
                });
            }
            var xc = w / 2 + this.clickpose.x / r + x;
            var yc = h / 2 - this.clickpose.y / r + y;
            this.xtest = (xc - x) * r;
            this.ytest = -((yc - y - h) * r);
            this._CONTEXT.beginPath();
            this._CONTEXT.arc(xc, yc, this._CANVAS.width * 0.009, 0, 2 * Math.PI);
            this._CONTEXT.lineWidth = 1;
            this._CONTEXT.fillStyle = "#32CD32";
            this._CONTEXT.fill();
            this._CONTEXT.beginPath();
            this._CONTEXT.save();
            this._CONTEXT.translate(this.api.localization_status.positionx / this.resomap + this.offsetX, this._CANVAS.height -
                this.api.localization_status.positiony / this.resomap +
                this.offsetY);
            this._CONTEXT.rotate(-this.api.localization_status.positiont);
            this._CONTEXT.moveTo(0, -15);
            this._CONTEXT.lineTo(15, 0);
            this._CONTEXT.lineTo(0, 15);
            this._CONTEXT.lineTo(-10, 15);
            this._CONTEXT.lineTo(-10, -15);
            this._CONTEXT.lineTo(0, -15);
            this._CONTEXT.lineWidth = 2;
            this._CONTEXT.fill("nonzero");
            this._CONTEXT.stroke();
            this._CONTEXT.restore();
        }
    };
    ;
    QRCodePage.prototype.on_zoom_out = function (ev) {
        ev.preventDefault();
        if (!(this.resomap >= this.initialzoom)) {
            this.resomap = this.resomap + 150 / 100000;
        }
        else {
            this.offsetX = 0;
            this.offsetY = 0;
        }
        this.createImageFromBlob();
    };
    QRCodePage.prototype.on_zoom_in = function (ev) {
        ev.preventDefault();
        if (this.resomap > this.initialzoom / 4) {
            this.resomap = this.resomap - 150 / 100000;
        }
        this.createImageFromBlob();
    };
    QRCodePage.prototype.getImageFromService = function () {
        var _this = this;
        this.imge = document.getElementById("imgmap5");
        if (this.api.id_current_map != undefined) {
            this.offsetX = this.api.mapdata.Offset.X;
            this.offsetY = this.api.mapdata.Offset.Y;
            this.resomap = this.api.mapdata.Resolution;
            this.initialzoom = this.resomap;
        }
        console.log("GetImageFromService");
        this.api.getImgMapHttp().subscribe(function (data) {
            _this.api.imgMap = data;
            _this.createImageFromBlob();
            console.log("GetImageFromService");
        }, function (error) {
            console.log(error);
        });
    };
    QRCodePage.prototype.onSliderRelease = function (ev, id) {
        ev.preventDefault();
        id.open();
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])("canvas5"),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* ElementRef */])
    ], QRCodePage.prototype, "canvasEl", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])("imgmap5"),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* ElementRef */])
    ], QRCodePage.prototype, "imgmapElement", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])("content1"),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* Content */])
    ], QRCodePage.prototype, "content1", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])("videoCanvas2"),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* ElementRef */])
    ], QRCodePage.prototype, "videoCanvas2", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])("video2"),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* ElementRef */])
    ], QRCodePage.prototype, "videoElement2", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])("select1"),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* Select */])
    ], QRCodePage.prototype, "select1", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])("select2"),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* Select */])
    ], QRCodePage.prototype, "select2", void 0);
    QRCodePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: "page-qrcode",template:/*ion-inline-start:"C:\Users\laele\Documents\KomApp\toolbox\src\pages\qrcode\qrcode.html"*/'<!-- param page html template -->\n\n<ion-header no-border>\n\n  <headpage pageName="qrcode"></headpage>\n\n</ion-header>\n\n\n\n<ion-content id="body5">\n\n \n\n    <ion-grid class="heightstyle">\n\n     \n\n      <ion-row class="heightstyle" id="firstrow5">\n\n        <ion-col\n\n          col-8\n\n          id="testcol5"\n\n          class="heightstyle"\n\n          style="background-color: rgba(0, 0, 0, 0.904)"\n\n        >\n\n          <ion-row class="rowcanvas">\n\n            <canvas\n\n            [hidden]="addqr"\n\n              #canvas5\n\n              (wheel)="on_wheel($event);"\n\n              (mouseup)="on_mouse_up($event);"\n\n              (mousedown)="on_mouse_down($event);"\n\n              (mousemove)="on_mouse_move($event);"\n\n            ></canvas>\n\n            <video  [hidden]="!addqr || param.robot.cam_USB == 0" #video2 autoplay class="video"></video>\n\n            <canvas #videoCanvas2 style="display: none;"></canvas> \n\n            <img  [hidden]="!addqr || param.robot.cam_USB == 1" class="video" id="imgcompressed2" />\n\n            \n\n            <img id="imgmap5" #imgmap5 src={{map_url}} crossorigin="anonymous" class="video" style="display:none;" />\n\n          </ion-row>\n\n          <ion-row class="btnsmap" [hidden]="addqr">\n\n            <ion-col col-3 >\n\n              <button class="btnmap" (mouseup)="on_zoom_out($event)" ion-button>\n\n                <ion-icon name="search"></ion-icon> -\n\n              </button>\n\n            </ion-col>\n\n            <ion-col col-3>\n\n              <button ion-button (mouseup)="on_zoom_in($event)" class="btnmap">\n\n                <ion-icon name="search"></ion-icon> +\n\n              </button>\n\n            </ion-col>\n\n            <ion-col col-2>\n\n              <button\n\n                [hidden]="reloc"\n\n                ion-button\n\n                class="btnmap"\n\n                (mouseup)="on_click_relocate($event)"\n\n              >\n\n                {{param.datatext.reloc}}\n\n              </button>\n\n              <button\n\n                [hidden]="!reloc"\n\n                ion-button\n\n                color="secondary"\n\n                class="btnmap"\n\n                (mouseup)="on_click_relocate($event)"\n\n              >\n\n                {{param.datatext.confirm}}\n\n              </button>\n\n            </ion-col>\n\n            <ion-col col-2>\n\n              <button\n\n                [disabled]="!reloc"\n\n                class="btnmap"\n\n                (mouseup)="on_click_relocate_left($event)"\n\n                ion-button\n\n              >\n\n                <ion-icon name="undo"></ion-icon>\n\n              </button>\n\n            </ion-col>\n\n            <ion-col col-2>\n\n              <button\n\n                [disabled]="!reloc"\n\n                ion-button\n\n                (mouseup)="on_click_relocate_right($event)"\n\n                class="btnmap"\n\n              >\n\n                <ion-icon name="redo"></ion-icon>\n\n              </button>\n\n            </ion-col>\n\n          </ion-row>\n\n        </ion-col>\n\n\n\n\n\n        <ion-col col-4 style="background-color: rgb(182, 217, 230)">\n\n          <ion-row class="heightstyle10">\n\n          <ion-col col-9 style="background-color: rgb(182, 217, 230)">\n\n            <ion-item style="background-color: transparent">\n\n              <ion-label>\n\n                <h1 style="font-size: x-large" [hidden]="addqr || qrdetected">\n\n                  {{param.datatext.qrcodes_list}}\n\n                </h1>\n\n              </ion-label>\n\n            </ion-item>\n\n          </ion-col>\n\n          <ion-col col-3 style="background-color: rgb(182, 217, 230)">\n\n            <ion-buttons class="div_btn_map">\n\n              <button\n\n                [hidden]="addqr || qrdetected"\n\n                (mouseup)="onClickAddQR($event)"\n\n                ion-button\n\n                class="btn_map"\n\n                *ngIf="api.id_current_map!=undefined"\n\n              >\n\n                <ion-icon style="font-size: 3vw" name="add"></ion-icon></button\n\n            ></ion-buttons>\n\n          </ion-col>\n\n          </ion-row>\n\n\n\n          <ion-row class="heightstyle90" >\n\n          <ion-content\n\n            #content1\n\n            class="scroll"\n\n            [hidden]="addqr || qrdetected"\n\n            style="height: 100%; width: 100%; background-color: transparent;"\n\n          >\n\n            <ion-list *ngFor="let m of this.param.qrcodes; let index = index;">\n\n              <ion-item-sliding\n\n                *ngIf="m.id_map ==api.id_current_map"\n\n                style="background-color: transparent"\n\n                (mouseup)="open($event,slidingItem)"\n\n                #slidingItem\n\n              >\n\n                <ion-item style="background-color: rgba(255, 255, 255, 0.76)">\n\n                  <h2>qr-{{m.qr}}</h2>\n\n                  <p *ngIf="m.id_round < 0">{{param.datatext.POI_edit}} --</p>\n\n                  <p *ngIf="m.id_round >= 0">\n\n                  {{param.datatext.POI_edit}} {{getname(m.id_round)}}\n\n                  </p>\n\n                </ion-item>\n\n                <ion-item-options side="right">\n\n                  <button\n\n                  [disabled]="!api.adminMode"\n\n                    (mouseup)="popupDelete($event,m.qr,m.id_poi)"\n\n                    ion-button\n\n                    style="min-width: 60px"\n\n                    color="danger"\n\n                  >\n\n                    <ion-icon name="trash"></ion-icon>\n\n                    {{param.datatext.delete}}\n\n                  </button>\n\n                </ion-item-options>\n\n              </ion-item-sliding>\n\n            </ion-list></ion-content\n\n          >\n\n\n\n          <div [hidden]="!addqr" class="newpoistyle">\n\n            <h1\n\n              [hidden]="!qrdetection"\n\n              text-wrap\n\n              style="\n\n                color: rgb(26, 156, 195);\n\n                margin-bottom: 20px;\n\n                text-align: left;\n\n              "\n\n            >\n\n            Recherche de QR code en cours ...\n\n            </h1>\n\n            <h1\n\n            [hidden]="qrdetection"\n\n              text-wrap\n\n              style="\n\n                color: rgb(26, 156, 195);\n\n                margin-bottom: 20px;\n\n                text-align: left;\n\n              "\n\n            >\n\n              {{param.datatext.qrcodeIndication}}\n\n            </h1>\n\n            <h1\n\n            [hidden]="qrdetection"\n\n              style="\n\n                color: rgb(26, 156, 195);\n\n                margin-bottom: 30px;\n\n                text-align: left;\n\n              "\n\n              text-wrap\n\n            >\n\n              {{param.datatext.qrcodeIndication1}}\n\n            </h1>\n\n\n\n            <ion-row>\n\n              <ion-col> </ion-col>\n\n              <ion-col>\n\n                <button\n\n\n\n                  (mouseup)="canceladdqr($event)"\n\n                  style="\n\n                    width: 100%;\n\n                    color: rgb(26, 156, 195);\n\n                    text-decoration: underline;\n\n                    font-size: x-large;\n\n                  "\n\n                  ion-button\n\n                  clear\n\n                >\n\n                  <div>{{param.datatext.btn_cancel}}</div>\n\n                </button>\n\n              </ion-col>\n\n              <ion-col align-self-center text-center>\n\n                <button\n\n                [hidden]="qrdetection"\n\n                  (mouseup)="scan($event)"\n\n                  ion-button\n\n                  style="width: 100%; font-size: x-large"\n\n                >\n\n                  {{param.datatext.qrcodeScan}}\n\n                </button>\n\n              </ion-col>\n\n            </ion-row>\n\n          </div>\n\n\n\n          <div [hidden]="!qrdetected" class="newpoistyle">\n\n            <h1 text-wrap>\n\n              {{param.datatext.qrcodeDetected1}} \'{{qr_read}}\'\n\n              {{param.datatext.qrcodeDetected2}}.\n\n            </h1>\n\n\n\n            <ion-item>\n\n              <ion-label>{{param.datatext.tieRound}}</ion-label>\n\n              <ion-checkbox\n\n                [(ngModel)]="link_to_round"\n\n                color="primary"\n\n              ></ion-checkbox>\n\n            </ion-item>\n\n            <ion-item [hidden]="!link_to_round">\n\n              <ion-label>{{param.datatext.round_qrcodesPage}}</ion-label>\n\n              <ion-select #select2\n\n                interface="popover"\n\n                [(ngModel)]="round"\n\n                okText="{{this.param.datatext.btn_ok}}"\n\n                cancelText="{{this.param.datatext.btn_cancel}}"\n\n                (mouseup)="onSliderRelease($event,select2)"\n\n              >\n\n                <ion-option\n\n                  *ngFor="let m of this.api.round_current_map"\n\n                  value="{{m.Id}}"\n\n                  >{{m.Name}}</ion-option\n\n                >\n\n              </ion-select>\n\n            </ion-item>\n\n            <ion-row>\n\n              <ion-col> </ion-col>\n\n              <ion-col>\n\n                <button\n\n                  (mouseup)="canceladdqr($event)"\n\n                  style="\n\n                    width: 100%;\n\n                    color: rgb(26, 156, 195);\n\n                    text-decoration: underline;\n\n                    font-size: x-large;\n\n                  "\n\n                  ion-button\n\n                  clear\n\n                >\n\n                  <div>{{param.datatext.btn_cancel}}</div>\n\n                </button>\n\n              </ion-col>\n\n              <ion-col align-self-center text-center>\n\n                <button\n\n                  (mouseup)="createqr($event)"\n\n                  ion-button\n\n                  style="width: 100%; font-size: x-large"\n\n                >\n\n                  {{param.datatext.btn_save}}\n\n                </button>\n\n              </ion-col>\n\n            </ion-row>\n\n          </div>\n\n\n\n\n\n          </ion-row>\n\n        </ion-col>\n\n      </ion-row>\n\n    </ion-grid>\n\n  \n\n</ion-content>\n\n'/*ion-inline-end:"C:\Users\laele\Documents\KomApp\toolbox\src\pages\qrcode\qrcode.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_0__angular_core__["W" /* Renderer2 */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_5__services_popup_service__["a" /* PopupService */],
            __WEBPACK_IMPORTED_MODULE_3__services_api_service__["a" /* ApiService */],
            __WEBPACK_IMPORTED_MODULE_4__services_param_service__["a" /* ParamService */],
            __WEBPACK_IMPORTED_MODULE_7__services_speech_service__["a" /* SpeechService */],
            __WEBPACK_IMPORTED_MODULE_2__services_alert_service__["a" /* AlertService */]])
    ], QRCodePage);
    return QRCodePage;
}());

//# sourceMappingURL=qrcode.js.map

/***/ }),

/***/ 267:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DockingPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_alert_service__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_api_service__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_param_service__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__services_popup_service__ = __webpack_require__(35);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var DockingPage = /** @class */ (function () {
    function DockingPage(navCtrl, loadingCtrl, toastCtrl, popup, api, param, alert) {
        this.navCtrl = navCtrl;
        this.loadingCtrl = loadingCtrl;
        this.toastCtrl = toastCtrl;
        this.popup = popup;
        this.api = api;
        this.param = param;
        this.alert = alert;
        this.start = {
            x: 0,
            y: 0,
        };
        this.clickpose = {
            x: 0,
            y: 0,
        };
        this.map_url = "http://" + this.param.localhost + "/api/maps/current/image?t=" + new Date().getTime();
        this.reloc = false;
        this.config = false;
    }
    DockingPage.prototype.ngOnInit = function () {
        this._CANVAS = this.canvasEl.nativeElement;
        this._CONTEXT = this._CANVAS.getContext("2d");
        this._CANVAS.style.width = "100%";
        this._CANVAS.style.height = "100%";
        this.heightcol =
            document.getElementById("body8").clientHeight -
                document.getElementById("firstrow8").clientHeight * 2;
        this.widthcol = document.getElementById("testcol8").clientWidth;
        this.popup.onSomethingHappened6(this.removePOIdocking.bind(this));
        this.popup.onSomethingHappened7(this.configDocking.bind(this));
    };
    DockingPage.prototype.ionViewWillEnter = function () {
        this.config = false;
    };
    DockingPage.prototype.ionViewDidEnter = function () {
        var _this = this;
        this.current_map = this.api.id_current_map;
        if (this.api.id_current_map != undefined) {
            this.api.getCurrentLocations();
        }
        this.api.getRoundList();
        this.getImageFromService();
        this.intervmap = setInterval(function () { return _this.updatemap(); }, 400);
    };
    DockingPage.prototype.updatemap = function () {
        if (this.reloc || this.api.differential_status.CurrentAngularSpeed > 0.005 || this.api.differential_status.CurrentLinearSpeed > 0.01 || this._CANVAS.width == 0) {
            this.createImageFromBlob();
        }
    };
    DockingPage.prototype.okToast = function (m, n) {
        var toast = this.toastCtrl.create({
            message: m,
            duration: n,
            position: "middle",
            cssClass: "toastam",
        });
        toast.present();
    };
    DockingPage.prototype.on_click_relocate = function (ev) {
        ev.preventDefault();
        if (!this.reloc) {
            this.reloc = true;
            console.log(this.clickpose);
            this.api.putLocalization(this.xtest, this.ytest, 0);
        }
        else {
            this.reloc = false;
        }
    };
    DockingPage.prototype.on_click_relocate_left = function (ev) {
        ev.preventDefault();
        this.api.putLocalization(this.api.localization_status.positionx, this.api.localization_status.positiony, (this.api.localization_status.positiont + 0.46) % (2 * Math.PI));
    };
    DockingPage.prototype.on_click_relocate_right = function (ev) {
        ev.preventDefault();
        this.api.putLocalization(this.api.localization_status.positionx, this.api.localization_status.positiony, (this.api.localization_status.positiont - 0.46) % (2 * Math.PI));
    };
    DockingPage.prototype.ionViewWillLeave = function () {
        clearInterval(this.intervmap);
    };
    DockingPage.prototype.showLoading = function () {
        if (!this.loading) {
            this.loading = this.loadingCtrl.create({
                content: this.param.datatext.wait,
            });
            this.loading.present();
        }
    };
    DockingPage.prototype.dismissLoading = function () {
        if (this.loading) {
            this.loading.dismiss();
            this.loading = null;
        }
    };
    DockingPage.prototype.onMapChange = function (event) {
        var _this = this;
        this.showLoading();
        this.api.changeMapbyIdHttp(event);
        setTimeout(function () {
            _this.imge.src = "http://" + _this.param.localhost + "/api/maps/current/image?t=" + new Date().getTime();
        }, 4500);
        setTimeout(function () {
            _this.api.getCurrentLocations();
            _this.api.getCurrentMap();
        }, 5500);
        setTimeout(function () {
            _this.api.getRoundList();
            _this.getImageFromService();
        }, 6500);
        setTimeout(function () {
            if (_this.current_map != _this.api.id_current_map) {
                _this.current_map = _this.api.id_current_map;
            }
            _this.dismissLoading();
        }, 8000);
    };
    DockingPage.prototype.on_mouse_down = function (evt) {
        this.createImageFromBlob();
        //console.log(evt);
        if (evt.button == 0) {
            evt.preventDefault();
            this.moving = true;
            this.start.x = evt.x;
            this.start.y = evt.y;
            //console.log(evt.x + " " + this.start.x);
            //var t = evt.changedTouches[0];
            //var x = evt.x;
            //var y = evt.y;
            var x = (evt.offsetX * this._CANVAS.width) / this._CANVAS.clientWidth;
            //var y = evt.offsetY;
            var y = (evt.offsetY * this._CANVAS.height) / this._CANVAS.clientHeight;
            this.clickpose.x =
                (x - this.offsetX - this._CANVAS.width / 2) * this.resomap;
            this.clickpose.y =
                -(y - this.offsetY - this._CANVAS.height / 2) * this.resomap;
            console.log(this.clickpose);
            console.log(evt.x + " " + evt.y);
        }
        if (this.reloc) {
            console.log(this.clickpose);
            this.api.putLocalization(this.xtest, this.ytest, 0);
        }
    };
    DockingPage.prototype.on_mouse_up = function (evt) {
        this.createImageFromBlob();
        //console.log(evt);
        if (this.moving) {
            var offset = {
                x: this.offsetX,
                y: this.offsetY,
                t: 0,
            };
            offset.x += evt.x - this.start.x;
            offset.y += evt.y - this.start.y;
            this.start.x = evt.x;
            this.start.y = evt.y;
            this.offsetX = offset.x;
            this.offsetY = offset.y;
        }
        this.moving = false;
        if (this.reloc) {
            console.log(this.clickpose);
            this.api.putLocalization(this.xtest, this.ytest, 0);
        }
    };
    DockingPage.prototype.on_mouse_move = function (evt) {
        if (this.moving) {
            this.createImageFromBlob();
            evt.preventDefault();
            var offset = {
                x: this.offsetX,
                y: this.offsetY,
            };
            offset.x += evt.x - this.start.x;
            offset.y += evt.y - this.start.y;
            this.start.x = evt.x;
            this.start.y = evt.y;
            this.offsetX = offset.x;
            this.offsetY = offset.y;
        }
    };
    DockingPage.prototype.on_wheel = function (evt) {
        this.createImageFromBlob();
        evt.preventDefault();
        //console.log(evt.deltaY);
        if (evt.deltaY > 0) {
            if (!(this.resomap >= this.initialzoom)) {
                this.resomap = this.resomap + 150 / 100000;
            }
            else {
                this.offsetX = 0;
                this.offsetY = 0;
            }
        }
        else {
            if (this.resomap > this.initialzoom / 4) {
                this.resomap = this.resomap - 150 / 100000;
            }
        }
        console.log(this.resomap);
    };
    DockingPage.prototype.getname = function (id_round) {
        return this.api.round_current_map.filter(function (x) { return x.Id == id_round; })[0].Name;
    };
    DockingPage.prototype.createImageFromBlob = function () {
        var _this = this;
        console.log("createImageFromBlob");
        if (this.current_map && this._CANVAS.getContext) {
            this._CANVAS.width = this.imge.width;
            this._CANVAS.height = this.imge.height;
            var w = this._CANVAS.width;
            var h = this._CANVAS.height;
            var r = this.resomap;
            var x = this.offsetX;
            var y = this.offsetY;
            var xm = this.api.mapdata.Offset.X;
            var ym = this.api.mapdata.Offset.Y;
            var rm = this.api.mapdata.Resolution;
            var wm = this.api.mapdata.Width;
            var hm = this.api.mapdata.Height;
            var dx = xm / r + x;
            var dy = h - (ym + hm * rm) * (1 / r) + y;
            var dw = (wm * rm) / r;
            var dh = (hm * rm) / r;
            this._CONTEXT.drawImage(this.imge, dx, dy, dw, dh);
            //console.log('dessin image');
            if (this.api.all_locations != undefined) {
                this.api.all_locations.forEach(function (evenement) {
                    if (evenement.Name === "docking" || evenement.Name === "docked") {
                        _this._CONTEXT.beginPath();
                        _this._CONTEXT.font =
                            "" + Math.round(_this._CANVAS.width * 0.025) + "px Arial";
                        //console.log(evenement);
                        _this._CONTEXT.fillStyle = "rgba(26, 156, 195,0.7)";
                        _this._CONTEXT.fillText(evenement.Id + "- " + evenement.Name, evenement.Pose.X / _this.resomap + _this.offsetX - 20, _this._CANVAS.height -
                            (evenement.Pose.Y / _this.resomap - _this.offsetY) -
                            10);
                        _this._CONTEXT.beginPath();
                        //console.log(evenement.Pose)
                        _this._CONTEXT.arc(evenement.Pose.X / _this.resomap + _this.offsetX, _this._CANVAS.height -
                            (evenement.Pose.Y / _this.resomap - _this.offsetY), _this._CANVAS.width * 0.005, 0, 2 * Math.PI);
                        _this._CONTEXT.lineWidth = 1;
                        _this._CONTEXT.fillStyle = "rgb(26, 156, 195)";
                        _this._CONTEXT.fill();
                        _this._CONTEXT.beginPath();
                        _this._CONTEXT.save();
                        _this._CONTEXT.translate(evenement.Pose.X / _this.resomap + _this.offsetX, _this._CANVAS.height -
                            (evenement.Pose.Y / _this.resomap - _this.offsetY));
                        _this._CONTEXT.rotate(-evenement.Pose.T);
                        _this._CONTEXT.moveTo(0, 0);
                        _this._CONTEXT.lineWidth = 2;
                        _this._CONTEXT.lineTo(2 * _this._CANVAS.width * 0.009, 0);
                        _this._CONTEXT.strokeStyle = "rgb(26, 156, 195)";
                        _this._CONTEXT.stroke();
                        _this._CONTEXT.restore();
                    }
                });
            }
            var xc = w / 2 + this.clickpose.x / r + x;
            var yc = h / 2 - this.clickpose.y / r + y;
            this.xtest = (xc - x) * r;
            this.ytest = -((yc - y - h) * r);
            this._CONTEXT.beginPath();
            this._CONTEXT.arc(xc, yc, this._CANVAS.width * 0.009, 0, 2 * Math.PI);
            this._CONTEXT.lineWidth = 1;
            this._CONTEXT.fillStyle = "#32CD32";
            this._CONTEXT.fill();
            this._CONTEXT.beginPath();
            this._CONTEXT.save();
            this._CONTEXT.translate(this.api.localization_status.positionx / this.resomap + this.offsetX, this._CANVAS.height -
                this.api.localization_status.positiony / this.resomap +
                this.offsetY);
            this._CONTEXT.rotate(-this.api.localization_status.positiont);
            this._CONTEXT.moveTo(0, -15);
            this._CONTEXT.lineTo(15, 0);
            this._CONTEXT.lineTo(0, 15);
            this._CONTEXT.lineTo(-10, 15);
            this._CONTEXT.lineTo(-10, -15);
            this._CONTEXT.lineTo(0, -15);
            this._CONTEXT.lineWidth = 2;
            this._CONTEXT.fill("nonzero");
            this._CONTEXT.stroke();
            this._CONTEXT.restore();
        }
    };
    DockingPage.prototype.on_zoom_out = function (ev) {
        ev.preventDefault();
        if (!(this.resomap >= this.initialzoom)) {
            this.resomap = this.resomap + 150 / 100000;
        }
        else {
            this.offsetX = 0;
            this.offsetY = 0;
        }
        this.createImageFromBlob();
    };
    DockingPage.prototype.on_zoom_in = function (ev) {
        ev.preventDefault();
        if (this.resomap > this.initialzoom / 4) {
            this.resomap = this.resomap - 150 / 100000;
        }
        this.createImageFromBlob();
    };
    DockingPage.prototype.getImageFromService = function () {
        var _this = this;
        this.imge = document.getElementById("imgmap8");
        if (this.api.id_current_map != undefined) {
            this.offsetX = this.api.mapdata.Offset.X;
            this.offsetY = this.api.mapdata.Offset.Y;
            this.resomap = this.api.mapdata.Resolution;
            this.initialzoom = this.resomap;
        }
        console.log("GetImageFromService");
        this.api.getImgMapHttp().subscribe(function (data) {
            _this.api.imgMap = data;
            _this.createImageFromBlob();
            console.log("GetImageFromService");
        }, function (error) {
            console.log(error);
        });
    };
    DockingPage.prototype.onSliderRelease = function (ev, id) {
        ev.preventDefault();
        id.open();
    };
    DockingPage.prototype.configDocking = function () {
        var _this = this;
        this.showLoading();
        this.api.postPoi("docked", "none");
        this.api.configPOIDocking();
        setTimeout(function () {
            _this.api.changeMapbyIdHttp(_this.api.id_current_map);
            _this.api.getCurrentLocations();
        }, 3000);
        setTimeout(function () {
            _this.createImageFromBlob();
            _this.dismissLoading();
            _this.config = !_this.config;
        }, 4000);
        setTimeout(function () {
            _this.createImageFromBlob();
            _this.createImageFromBlob();
        }, 5000);
    };
    DockingPage.prototype.removePOIdocking = function () {
        var _this = this;
        this.showLoading();
        var docking = this.api.all_locations.filter(function (poi) { return poi.Name == "docking" || poi.Name == "docked"; });
        docking.forEach(function (poi) {
            _this.api.deletePOI(poi.Id);
        });
        setTimeout(function () {
            _this.api.getCurrentLocations();
        }, 2000);
        setTimeout(function () {
            _this.createImageFromBlob();
            _this.dismissLoading();
            _this.config = !_this.config;
        }, 3000);
    };
    DockingPage.prototype.onChangeConfig = function (ev) {
        ev.preventDefault();
        var docking = this.api.all_locations.filter(function (poi) { return poi.Name == "docking" || poi.Name == "docked"; });
        if (docking.length > 0)
            this.popup.askRemoveDataDocking();
        else
            this.config = !this.config;
    };
    DockingPage.prototype.canceldocking = function (ev) {
        ev.preventDefault();
        this.navCtrl.pop();
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])("canvas8"),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* ElementRef */])
    ], DockingPage.prototype, "canvasEl", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])("content1"),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* Content */])
    ], DockingPage.prototype, "content1", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])("imgmap8"),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* ElementRef */])
    ], DockingPage.prototype, "imgmapElement", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])("select1"),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* Select */])
    ], DockingPage.prototype, "select1", void 0);
    DockingPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: "page-docking",template:/*ion-inline-start:"C:\Users\laele\Documents\KomApp\toolbox\src\pages\docking\docking.html"*/'<!-- param page html template -->\n\n<ion-header no-border>\n\n    <headpage pageName="docking"></headpage>\n\n  </ion-header>\n\n  \n\n  <ion-content padding id="body8">\n\n   \n\n      <ion-grid class="heightstyle">\n\n        <!-- <ion-row class="heightstyle10" id="firstrow8">\n\n          <ion-col col-5>\n\n            <ion-list>\n\n              <ion-item>\n\n                <ion-label>{{this.param.datatext.maps}} : </ion-label>\n\n                <ion-select #select1\n\n                  interface="popover"\n\n                  [(ngModel)]="current_map"\n\n                  okText="{{this.param.datatext.btn_ok}}"\n\n                  cancelText="{{this.param.datatext.btn_cancel}}"\n\n                  (ionChange)="onMapChange($event)"\n\n                  (mouseup)="onSliderRelease($event,select1)"\n\n                >\n\n                  <ion-option *ngFor="let m of this.api.all_maps" value="{{m.Id}}"\n\n                    >{{m.Name}}</ion-option\n\n                  >\n\n                </ion-select>\n\n              </ion-item>\n\n            </ion-list>\n\n          </ion-col>\n\n          <ion-col col-3></ion-col>\n\n          <ion-col col-4 style="background-color: rgb(182, 217, 230)"></ion-col>\n\n        </ion-row> -->\n\n        <ion-row class="heightstyle" id="firstrow8">\n\n          <ion-col\n\n            col-8\n\n            id="testcol8"\n\n            class="heightstyle"\n\n            style="background-color: rgba(0, 0, 0, 0.904)"\n\n          >\n\n            <ion-row class="rowcanvas">\n\n              <canvas\n\n                #canvas8\n\n                \n\n                (wheel)="on_wheel($event);"\n\n               \n\n                (mouseup)="on_mouse_up($event);"\n\n                (mousedown)="on_mouse_down($event);"\n\n                (mousemove)="on_mouse_move($event);"\n\n              ></canvas>\n\n              <img id="imgmap8" #imgma8 src={{map_url}} crossorigin="anonymous" class="video" style="display:none;" />\n\n            </ion-row>\n\n            <ion-row class="btnsmap">\n\n              <ion-col col-3>\n\n                <button class="btnmap" (mouseup)="on_zoom_out($event)" ion-button>\n\n                  <ion-icon name="search"></ion-icon> -\n\n                </button>\n\n              </ion-col>\n\n              <ion-col col-3>\n\n                <button ion-button (mouseup)="on_zoom_in($event)" class="btnmap">\n\n                  <ion-icon name="search"></ion-icon> +\n\n                </button>\n\n              </ion-col>\n\n              <ion-col col-2>\n\n                <button\n\n                  [hidden]="reloc"\n\n                  ion-button\n\n                  class="btnmap"\n\n                  (mouseup)="on_click_relocate($event)"\n\n                >\n\n                  {{param.datatext.reloc}}\n\n                </button>\n\n                <button\n\n                  [hidden]="!reloc"\n\n                  ion-button\n\n                  color="secondary"\n\n                  class="btnmap"\n\n                  (mouseup)="on_click_relocate($event)"\n\n                >\n\n                  {{param.datatext.confirm}}\n\n                </button>\n\n              </ion-col>\n\n              <ion-col col-2>\n\n                <button\n\n                  [disabled]="!reloc"\n\n                  class="btnmap"\n\n                  (mouseup)="on_click_relocate_left($event)"\n\n                  ion-button\n\n                >\n\n                  <ion-icon name="undo"></ion-icon>\n\n                </button>\n\n              </ion-col>\n\n              <ion-col col-2>\n\n                <button\n\n                  [disabled]="!reloc"\n\n                  ion-button\n\n                  (mouseup)="on_click_relocate_right($event)"\n\n                  class="btnmap"\n\n                >\n\n                  <ion-icon name="redo"></ion-icon>\n\n                </button>\n\n              </ion-col>\n\n            </ion-row>\n\n          </ion-col>\n\n          <ion-col col-4 style="background-color: rgb(182, 217, 230)">\n\n            <ion-label>\n\n              <h1 text-wrap style="font-size: 3vw; text-align: left;">\n\n                {{param.datatext.configDocking}} :\n\n              </h1>\n\n            </ion-label>\n\n            <ion-content *ngIf="!config" style="height: 100%; width: 100%; background-color: transparent;">\n\n              <h2\n\n                  text-wrap\n\n                  style="\n\n                  color: rgb(26, 156, 195);\n\n                  margin-bottom: 30px;\n\n                  margin-top: 30px;\n\n                  text-align: left;\n\n                  font-size: 2vw;"\n\n              >\n\n              {{param.datatext.configDockingInfo1}}\n\n              </h2>\n\n\n\n              <ion-row>\n\n                  <ion-col col-8> </ion-col>\n\n                  <ion-col col-4 align-self-center text-center>\n\n                  <button\n\n                      (mouseup)="onChangeConfig($event)"\n\n                      ion-button\n\n                      style="width: 100%; font-size: x-large;"\n\n                  >\n\n                      {{param.datatext.btn_ok}}\n\n                  </button>\n\n                  </ion-col>\n\n              </ion-row>\n\n            </ion-content>\n\n            <ion-content *ngIf="config" style="height: 100%; width: 100%; background-color: transparent;">\n\n                <h2\n\n                    text-wrap\n\n                    style="\n\n                    color: rgb(26, 156, 195);\n\n                    margin-bottom: 30px;\n\n                    margin-top: 30px;\n\n                    text-align: left;\n\n                    font-size: 2vw;"\n\n                >\n\n                {{param.datatext.configDockingInfo2}}\n\n                </h2>\n\n\n\n                <ion-row>\n\n                    <ion-col> </ion-col>\n\n                    <ion-col>\n\n                    <button\n\n                        (mouseup)="canceldocking($event)"\n\n                        style="\n\n                        width: 100%;\n\n                        color: rgb(26, 156, 195);\n\n                        text-decoration: underline;\n\n                        font-size: x-large;\n\n                        "\n\n                        ion-button\n\n                        clear\n\n                    >\n\n                        <div>{{param.datatext.btn_cancel}}</div>\n\n                    </button>\n\n                    </ion-col>\n\n                    <ion-col align-self-center text-center>\n\n                    <button\n\n                        (mouseup)="popup.askConfigDocking($event)"\n\n                        ion-button\n\n                        style="width: 100%; font-size: x-large;"\n\n                    >\n\n                        {{param.datatext.btn_save}}\n\n                    </button>\n\n                    </ion-col>\n\n                </ion-row>\n\n            </ion-content>\n\n          </ion-col>\n\n        </ion-row>\n\n      </ion-grid>\n\n    \n\n  </ion-content>'/*ion-inline-end:"C:\Users\laele\Documents\KomApp\toolbox\src\pages\docking\docking.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_5__services_popup_service__["a" /* PopupService */],
            __WEBPACK_IMPORTED_MODULE_3__services_api_service__["a" /* ApiService */],
            __WEBPACK_IMPORTED_MODULE_4__services_param_service__["a" /* ParamService */],
            __WEBPACK_IMPORTED_MODULE_2__services_alert_service__["a" /* AlertService */]])
    ], DockingPage);
    return DockingPage;
}());

//# sourceMappingURL=docking.js.map

/***/ }),

/***/ 268:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LiftPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_alert_service__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_api_service__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_param_service__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__services_popup_service__ = __webpack_require__(35);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_nipplejs__ = __webpack_require__(452);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_nipplejs___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_nipplejs__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__services_speech_service__ = __webpack_require__(54);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var LiftPage = /** @class */ (function () {
    function LiftPage(navCtrl, loadingCtrl, toastCtrl, popup, api, param, speech, alert) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.loadingCtrl = loadingCtrl;
        this.toastCtrl = toastCtrl;
        this.popup = popup;
        this.api = api;
        this.param = param;
        this.speech = speech;
        this.alert = alert;
        this.start = {
            x: 0,
            y: 0,
        };
        this.clickpose = {
            x: 0,
            y: 0,
        };
        this.map_url = "http://" + this.param.localhost + "/api/maps/current/image?t=" + new Date().getTime();
        this.can_leave_lift = false;
        this.chimiostate = 0;
        this.pharmastate = 0;
        this.chargemap = false;
        this.cpt_locked = 0;
        this.size = 40;
        this.maxrad = 0.08;
        this.maxlin = 0.12;
        this.maxback = 0.08;
        this.cam = false;
        this.currentx = 0;
        this.currenty = 0;
        this.lastx = 0;
        this.lasty = 0;
        this.checkjoystick = false;
        this.stopjoystick = false;
        this.reloc = false;
        // CAMERA ROS
        // if( this.param.robot.cam_USB==0 ){
        this.api.listener_cameraf.subscribe(function (message) {
            //console.log('Received message on ' + listener_camera.name + ': ');
            _this.imgRos = document.getElementById('imgcpsd');
            //console.log(message);
            var bufferdata = _this.convertDataURIToBinary2(message.data);
            var blob = new Blob([bufferdata], { type: 'image/jpeg' });
            var blobUrl = URL.createObjectURL(blob);
            _this.imgRos.src = blobUrl;
        });
        //}
        // CAMERA ROS FIN
    }
    LiftPage.prototype.ngAfterViewInit = function () {
        console.log(document.getElementById("zone_joystick"));
        this.createjoystick();
    };
    LiftPage.prototype.change_cam = function () {
        this.cam = !this.cam;
    };
    LiftPage.prototype.joystickmove = function () {
        this.api.joystickHttp(this.vlin, this.vrad);
    };
    LiftPage.prototype.joystickstop = function () {
        this.api.joystickHttp(0, 0);
    };
    LiftPage.prototype.createjoystick = function () {
        var _this = this;
        this.manager = __WEBPACK_IMPORTED_MODULE_6_nipplejs___default.a.create({
            zone: document.getElementById("zone_joystick"),
            size: 3 * this.size
        });
        this.manager.on("move", function (evt, nipple) {
            _this.currentx = nipple.raw.position.x;
            _this.currenty = nipple.raw.position.y;
            _this.checkjoystick = true;
            if (!_this.stopjoystick) {
                if (nipple.direction && nipple.direction.angle && nipple.angle) {
                    //console.log(nipple.direction.angle);
                    if (nipple.direction.angle === "left") {
                        _this.vlin = 0;
                        _this.vrad = (0.3 * nipple.distance) / 50;
                    }
                    else if (nipple.direction.angle === "right") {
                        _this.vlin = 0;
                        _this.vrad = -(0.3 * nipple.distance) / 50;
                    }
                    else if (nipple.direction.angle === "up") {
                        _this.vlin = (_this.maxlin * nipple.distance) / 50;
                        _this.vrad = 0;
                        // if (Math.abs((nipple.angle.radian - Math.PI / 2) / 2) > 0.18) {
                        //   this.vrad = (nipple.angle.radian - Math.PI / 2) / 2 - 0.09;
                        // } else {
                        // this.vrad = 0;
                        // }
                    }
                    else if (nipple.direction.angle === "down") {
                        _this.vlin = -(_this.maxback * nipple.distance) / 50;
                        _this.vrad = 0;
                    }
                }
            }
            else {
                _this.vlin = 0;
                _this.vrad = 0;
            }
        });
        this.manager.on("added", function (evt, nipple) {
            console.log("added");
            _this.gamepadinterv = setInterval(function () { if (!_this.stopjoystick) {
                _this.joystickmove();
            } }, 500);
        });
        this.manager.on("end", function (evt, nipple) {
            console.log("end");
            _this.vlin = 0;
            _this.vrad = 0;
            clearInterval(_this.gamepadinterv);
            _this.checkjoystick = false;
            _this.stopjoystick = false;
            _this.joystickstop();
        });
    };
    LiftPage.prototype.ngOnInit = function () {
        this.api.is_blocked = false;
        this._CANVAS = this.canvasEl.nativeElement;
        this._CANVAS.style.width = "100%";
        this._CANVAS.style.height = "100%";
        this._CONTEXT = this._CANVAS.getContext("2d");
        this.heightcol =
            document.getElementById("body7").clientHeight -
                document.getElementById("firstrow7").clientHeight * 2;
        this.widthcol = document.getElementById("testcol7").clientWidth;
    };
    LiftPage.prototype.calcDiffMin = function (one, two) {
        var DateBegMin = one.getMinutes();
        var DateBegHours = one.getHours() * 60;
        var DateBeg = DateBegMin + DateBegHours;
        var DateEndMin = two.getMinutes();
        var DateEndHours = two.getHours() * 60;
        return DateEndMin + DateEndHours - DateBeg;
    };
    LiftPage.prototype.watchIfLocked = function () {
        if (this.api.moovingchimio || this.api.moovingpharma) {
            if (!this.api.anticollision_status.locked) {
                if (this.cpt_locked > 0) {
                    this.cpt_locked = 0;
                }
                if (this.api.is_blocked) {
                    this.time_blocked = this.calcDiffMin(this.date_blocked, new Date());
                    this.alert.noLongerBlocked("<br> Blocked for : " +
                        this.time_blocked +
                        "<br>" +
                        this.api.mailAddInformationBasic());
                    this.api.is_blocked = false;
                    if (this.popup.alert_blocked) {
                        this.popup.alert_blocked.dismiss();
                    }
                    this.time_blocked = 0;
                }
            }
            else if (this.api.anticollision_status.locked) {
                this.cpt_locked += 1;
            }
            if (this.cpt_locked > 10 && !this.api.is_blocked) {
                this.speech.speak("Un obstacle m'empêche de passer. Veuillez m'aider s'il vous plait. J'ai une mission importante à terminer");
                this.date_blocked = new Date();
                this.popup.blockedAlert();
                this.api.is_blocked = true;
                this.alert.blocking(this.api.mailAddInformationBasic());
            }
        }
    };
    LiftPage.prototype.ionViewDidEnter = function () {
        var _this = this;
        this.current_map = this.api.id_current_map;
        if (this.api.id_current_map != undefined) {
            this.api.getCurrentLocations();
        }
        this.api.getRoundList();
        this.getImageFromService();
        this.intervmap = setInterval(function () { return _this.updatemap(); }, 400);
        this.stoprobot();
    };
    LiftPage.prototype.convertDataURIToBinary2 = function (dataURI) {
        var raw = window.atob(dataURI);
        var rawLength = raw.length;
        var array = new Uint8Array(new ArrayBuffer(rawLength));
        for (var i = 0; i < rawLength; i++) {
            array[i] = raw.charCodeAt(i);
        }
        return array;
    };
    LiftPage.prototype.open = function (ev, itemSlide) {
        ev.preventDefault();
        if (itemSlide.getSlidingPercent() == 0) {
            // Two calls intentional after examination of vendor.js function
            itemSlide.moveSliding(-370);
            itemSlide.moveSliding(-370);
        }
        else {
            itemSlide.close();
        }
    };
    LiftPage.prototype.updatemap = function () {
        var _this = this;
        if (this.reloc || this.api.differential_status.CurrentAngularSpeed > 0.005 || this.api.differential_status.CurrentLinearSpeed > 0.01 || this._CANVAS.width == 0) {
            this.createImageFromBlob();
        }
        if (this.checkjoystick) {
            if (this.currentx == this.lastx) {
                if (this.currenty == this.lasty) {
                    this.countjoystick = this.countjoystick + 1;
                    if (this.countjoystick >= 5) {
                        console.log("stopjoystick");
                        this.joystickstop();
                        this.stopjoystick = true;
                    }
                    if (this.countjoystick >= 15) {
                        clearInterval(this.gamepadinterv);
                        this.checkjoystick = false;
                        this.manager.destroy();
                        this.createjoystick();
                        this.checkjoystick = false;
                    }
                }
                else {
                    this.countjoystick = 0;
                    this.stopjoystick = false;
                    this.checkjoystick = true;
                    this.lastx = this.currentx;
                    this.lasty = this.currenty;
                }
            }
            else {
                this.countjoystick = 0;
                this.stopjoystick = false;
                this.checkjoystick = true;
                this.lastx = this.currentx;
                this.lasty = this.currenty;
            }
        }
        if (this.api.navigation_status.status == 5) {
            this.stoprobot();
            this.alert.naverror(this.api.mailAddInformationBasic());
            this.popup.errorNavAlert();
            this.speech.speak("Erreur. Impossible de calculer mon trajet.");
        }
        if (this.api.moovingchimio) {
            this.watchIfLocked();
            if (this.api.b_pressed) {
                this.stoprobot();
            }
            if (this.chimiostate == 3) {
                // code pour attendre la fin de la mission et dire "mission terminé" quand robot sur poi chimio
                if (this.api.trajectory_status.status == 1 && this.api.navigation_status.status == 0) {
                    this.speech.speak("Mission terminée ! vous pouvez récuperer les bacs de chimio.");
                    this.popup.showToastGreen("Mission terminée !", 3000, "middle");
                    this.api.moovingchimio = false;
                    this.chimiostate = 0;
                    this.stoprobot();
                }
            }
            else if (this.chimiostate == 2) {
                if (!this.chargemap && this.api.trajectory_status.status == 1 && this.api.navigation_status.status == 0 && this.getDistance() < 0.8) {
                    //this.speech.speak("Chargement de la carte chimio")
                    this.popup.showToastGreen("Changement de carte", 15000, "middle");
                    this.reloc_chimio();
                    this.chargemap = true;
                    setTimeout(function () {
                        if (_this.api.moovingchimio) {
                            _this.can_leave_lift = true;
                        }
                    }, 15000);
                }
                if (this.api.trajectory_status.status == 0 && this.chargemap && this.can_leave_lift) {
                    this.chimiostate = 3;
                    this.speech.speak("En route vers le service chimio");
                    this.api.reachHttp("chimio");
                }
            }
            else if (this.chimiostate == 1) {
                if ((this.api.trajectory_status.status == 1 && this.api.navigation_status.status == 0) || (this.api.trajectory_status.status == 0 && this.api.navigation_status.status == 0)) {
                    this.chimiostate = 2;
                    this.enter_lift();
                }
            }
        }
        else if (this.api.moovingpharma) {
            if (this.api.b_pressed) {
                this.stoprobot();
            }
            this.watchIfLocked();
            if (this.pharmastate == 3) {
                // code pour attendre la fin de la mission et dire "mission terminé" quand robot sur poi pharma
                if (this.api.trajectory_status.status == 1 && this.api.navigation_status.status == 0) {
                    this.speech.speak("Mission terminée ! Robot sur position PHARMA");
                    this.popup.showToastGreen("Mission terminée !", 3000, "middle");
                    this.api.moovingpharma = false;
                    this.pharmastate = 0;
                    this.stoprobot();
                }
            }
            else if (this.pharmastate == 2) {
                if (!this.chargemap && this.api.trajectory_status.status == 1 && this.api.navigation_status.status == 0 && this.getDistance() < 0.8) {
                    this.popup.showToastGreen("Changement de carte", 15000, "middle");
                    this.reloc_pharma();
                    this.chargemap = true;
                    setTimeout(function () {
                        if (_this.api.moovingpharma) {
                            _this.can_leave_lift = true;
                        }
                    }, 15000);
                }
                if (this.api.trajectory_status.status == 0 && this.chargemap && this.can_leave_lift) {
                    this.pharmastate = 3;
                    this.can_leave_lift = false;
                    this.speech.speak("En route vers le service pharma");
                    this.api.reachHttp("pharma");
                }
            }
            else if (this.pharmastate == 1) {
                if (this.api.trajectory_status.status < 2 && this.api.navigation_status.status == 0) {
                    this.pharmastate = 2;
                    this.enter_lift();
                }
            }
        }
    };
    LiftPage.prototype.on_click_relocate = function (ev) {
        ev.preventDefault();
        if (!this.reloc) {
            this.reloc = true;
            console.log(this.clickpose);
            this.api.putLocalization(this.xtest, this.ytest, 0);
        }
        else {
            this.reloc = false;
        }
    };
    LiftPage.prototype.stoprobot = function () {
        this.api.moovingchimio = false;
        this.api.moovingpharma = false;
        this.chargemap = false;
        this.can_leave_lift = false;
        this.chimiostate = 0;
        this.pharmastate = 0;
        console.log("stoprobot");
        this.api.abortNavHttp();
    };
    LiftPage.prototype.on_click_relocate_left = function (ev) {
        ev.preventDefault();
        this.api.putLocalization(this.api.localization_status.positionx, this.api.localization_status.positiony, (this.api.localization_status.positiont + 0.46) % (2 * Math.PI));
    };
    LiftPage.prototype.on_click_relocate_right = function (ev) {
        ev.preventDefault();
        this.api.putLocalization(this.api.localization_status.positionx, this.api.localization_status.positiony, (this.api.localization_status.positiont - 0.46) % (2 * Math.PI));
    };
    LiftPage.prototype.ionViewWillLeave = function () {
        if (this.param.robot.cam_USB == 0) {
            this.api.listener_camera.removeAllListeners();
            this.api.startQrDetection(false);
        }
        clearInterval(this.intervmap);
        this.api.listener_cameraf.unsubscribe();
    };
    LiftPage.prototype.enter_lift = function () {
        this.speech.speak("Attention, j'entre dans l'ascenseur");
        this.api.reachHttp("enterlift");
    };
    LiftPage.prototype.getDistance = function () {
        var x2 = this.api.localization_status.positionx;
        var y2 = this.api.localization_status.positiony;
        var x1 = this.enterlift[0].Pose.X;
        var y1 = this.enterlift[0].Pose.Y;
        var y = x2 - x1;
        var x = y2 - y1;
        var res = Math.sqrt(x * x + y * y);
        console.log(res);
        return res;
    };
    LiftPage.prototype.leave_lift = function (ev) {
        ev.preventDefault();
        this.speech.speak("Attention, je sors de l'ascenseur");
        this.api.reachHttp("leavelift");
    };
    LiftPage.prototype.go_pharma = function (ev) {
        var _this = this;
        ev.preventDefault();
        this.enterlift = this.api.all_locations.filter(function (poi) { return poi.Name == "enterlift"; });
        this.stoprobot();
        this.chargemap = false;
        this.api.moovingchimio = false;
        var index = this.api.mapdata.Name.toLowerCase().indexOf("pharma"); //savoir si on est sur map chimio
        this.alert.gopoi(this.api.mailAddInformationBasic());
        if (index !== -1) {
            console.log("on est sur map parma");
            this.speech.speak("Direction le service pharma");
            this.api.moovingpharma = true;
            setTimeout(function () {
                if (_this.api.moovingpharma)
                    _this.api.reachHttp("pharma");
            }, 1000);
            setTimeout(function () {
                if (_this.api.moovingpharma)
                    _this.pharmastate = 3;
            }, 2000);
        }
        else {
            console.log("on est pas sur map pharma");
            this.speech.speak("Direction l'ascenseur logistique propre");
            this.api.moovingpharma = true;
            setTimeout(function () {
                if (_this.api.moovingpharma) {
                    _this.api.reachHttp("waitlift");
                }
            }, 1000);
            setTimeout(function () {
                if (_this.api.moovingpharma)
                    _this.pharmastate = 1;
            }, 2000);
        }
    };
    LiftPage.prototype.go_chimio = function (ev) {
        var _this = this;
        ev.preventDefault();
        this.enterlift = this.api.all_locations.filter(function (poi) { return poi.Name == "enterlift"; });
        this.stoprobot();
        this.chargemap = false;
        this.api.moovingpharma = false;
        var index = this.api.mapdata.Name.toLowerCase().indexOf("chimio"); //savoir si on est sur map chimio
        this.alert.gopoi(this.api.mailAddInformationBasic());
        if (index !== -1) {
            console.log("on est sur map chimio");
            this.api.moovingchimio = true;
            this.speech.speak("Direction le service chimio");
            setTimeout(function () {
                if (_this.api.moovingchimio)
                    _this.api.reachHttp("chimio");
            }, 1000);
            setTimeout(function () {
                if (_this.api.moovingchimio)
                    _this.chimiostate = 3;
            }, 2000);
        }
        else {
            console.log("on est pas sur map chimio");
            this.speech.speak("Direction l'ascenseur logistique propre");
            this.api.moovingchimio = true;
            setTimeout(function () {
                _this.api.reachHttp("waitlift");
            }, 1000);
            setTimeout(function () {
                _this.chimiostate = 1;
            }, 2000);
        }
    };
    LiftPage.prototype.showLoading = function () {
        if (!this.loading) {
            this.loading = this.loadingCtrl.create({
                content: this.param.datatext.wait,
            });
            this.loading.present();
        }
    };
    LiftPage.prototype.dismissLoading = function () {
        if (this.loading) {
            this.loading.dismiss();
            this.loading = null;
        }
    };
    LiftPage.prototype.onMapChange = function (event) {
        var _this = this;
        this.showLoading();
        this.api.changeMapbyIdHttp(event);
        setTimeout(function () {
            _this.imge.src = "http://" + _this.param.localhost + "/api/maps/current/image?t=" + new Date().getTime();
        }, 4500);
        setTimeout(function () {
            _this.api.getCurrentLocations();
            _this.api.getCurrentMap();
        }, 5500);
        setTimeout(function () {
            _this.api.getRoundList();
            _this.getImageFromService();
        }, 6500);
        setTimeout(function () {
            if (_this.current_map != _this.api.id_current_map) {
                _this.current_map = _this.api.id_current_map;
            }
            _this.dismissLoading();
            _this.api.current_map = _this.api.id_current_map;
        }, 8000);
    };
    LiftPage.prototype.on_mouse_down = function (evt) {
        this.createImageFromBlob();
        //console.log(evt);
        if (evt.button == 0) {
            evt.preventDefault();
            this.moving = true;
            this.start.x = evt.x;
            this.start.y = evt.y;
            //console.log(evt.x + " " + this.start.x);
            //var t = evt.changedTouches[0];
            //var x = evt.x;
            //var y = evt.y;
            var x = (evt.offsetX * this._CANVAS.width) / this._CANVAS.clientWidth;
            //var y = evt.offsetY;
            var y = (evt.offsetY * this._CANVAS.height) / this._CANVAS.clientHeight;
            this.clickpose.x =
                (x - this.offsetX - this._CANVAS.width / 2) * this.resomap;
            this.clickpose.y =
                -(y - this.offsetY - this._CANVAS.height / 2) * this.resomap;
            console.log(this.clickpose);
            console.log(evt.x + " " + evt.y);
        }
        if (this.reloc) {
            console.log(this.clickpose);
            this.api.putLocalization(this.xtest, this.ytest, 0);
        }
    };
    LiftPage.prototype.on_mouse_up = function (evt) {
        this.createImageFromBlob();
        //console.log(evt);
        if (this.moving) {
            var offset = {
                x: this.offsetX,
                y: this.offsetY,
                t: 0,
            };
            offset.x += evt.x - this.start.x;
            offset.y += evt.y - this.start.y;
            this.start.x = evt.x;
            this.start.y = evt.y;
            this.offsetX = offset.x;
            this.offsetY = offset.y;
        }
        this.moving = false;
        if (this.reloc) {
            console.log(this.clickpose);
            this.api.putLocalization(this.xtest, this.ytest, 0);
        }
    };
    LiftPage.prototype.on_mouse_move = function (evt) {
        if (this.moving) {
            this.createImageFromBlob();
            evt.preventDefault();
            var offset = {
                x: this.offsetX,
                y: this.offsetY,
            };
            offset.x += evt.x - this.start.x;
            offset.y += evt.y - this.start.y;
            this.start.x = evt.x;
            this.start.y = evt.y;
            this.offsetX = offset.x;
            this.offsetY = offset.y;
        }
    };
    LiftPage.prototype.on_wheel = function (evt) {
        this.createImageFromBlob();
        evt.preventDefault();
        //console.log(evt.deltaY);
        if (evt.deltaY > 0) {
            if (!(this.resomap >= this.initialzoom)) {
                this.resomap = this.resomap + 150 / 100000;
            }
            else {
                this.offsetX = 0;
                this.offsetY = 0;
            }
        }
        else {
            if (this.resomap > this.initialzoom / 4) {
                this.resomap = this.resomap - 150 / 100000;
            }
        }
        console.log(this.resomap);
    };
    LiftPage.prototype.getname = function (id_round) {
        return this.api.round_current_map.filter(function (x) { return x.Id == id_round; })[0].Name;
    };
    LiftPage.prototype.createImageFromBlob = function () {
        var _this = this;
        console.log("createImageFromBlob");
        if (this._CANVAS.getContext) {
            this._CANVAS.width = this.imge.width;
            this._CANVAS.height = this.imge.height;
            var w = this._CANVAS.width;
            var h = this._CANVAS.height;
            var r = this.resomap;
            var x = this.offsetX;
            var y = this.offsetY;
            var xm = this.api.mapdata.Offset.X;
            var ym = this.api.mapdata.Offset.Y;
            var rm = this.api.mapdata.Resolution;
            var wm = this.api.mapdata.Width;
            var hm = this.api.mapdata.Height;
            var dx = xm / r + x;
            var dy = h - (ym + hm * rm) * (1 / r) + y;
            var dw = (wm * rm) / r;
            var dh = (hm * rm) / r;
            this._CONTEXT.drawImage(this.imge, dx, dy, dw, dh);
            //console.log('dessin image');
            if (this.api.all_locations != undefined) {
                this.api.all_locations.forEach(function (evenement) {
                    _this._CONTEXT.beginPath();
                    _this._CONTEXT.font =
                        "" + Math.round(_this._CANVAS.width * 0.025) + "px Arial";
                    //console.log(evenement);
                    _this._CONTEXT.fillStyle = "rgba(26, 156, 195,0.7)";
                    _this._CONTEXT.fillText(evenement.Id + "- " + evenement.Name, evenement.Pose.X / _this.resomap + _this.offsetX - 20, _this._CANVAS.height -
                        (evenement.Pose.Y / _this.resomap - _this.offsetY) -
                        10);
                    _this._CONTEXT.beginPath();
                    //console.log(evenement.Pose)
                    _this._CONTEXT.arc(evenement.Pose.X / _this.resomap + _this.offsetX, _this._CANVAS.height -
                        (evenement.Pose.Y / _this.resomap - _this.offsetY), _this._CANVAS.width * 0.005, 0, 2 * Math.PI);
                    _this._CONTEXT.lineWidth = 1;
                    _this._CONTEXT.fillStyle = "rgb(26, 156, 195)";
                    _this._CONTEXT.fill();
                    _this._CONTEXT.beginPath();
                    _this._CONTEXT.save();
                    _this._CONTEXT.translate(evenement.Pose.X / _this.resomap + _this.offsetX, _this._CANVAS.height -
                        (evenement.Pose.Y / _this.resomap - _this.offsetY));
                    _this._CONTEXT.rotate(-evenement.Pose.T);
                    _this._CONTEXT.moveTo(0, 0);
                    _this._CONTEXT.lineWidth = 2;
                    _this._CONTEXT.lineTo(2 * _this._CANVAS.width * 0.009, 0);
                    _this._CONTEXT.strokeStyle = "rgb(26, 156, 195)";
                    _this._CONTEXT.stroke();
                    _this._CONTEXT.restore();
                });
            }
            var xc = w / 2 + this.clickpose.x / r + x;
            var yc = h / 2 - this.clickpose.y / r + y;
            this.xtest = (xc - x) * r;
            this.ytest = -((yc - y - h) * r);
            this._CONTEXT.beginPath();
            this._CONTEXT.arc(xc, yc, this._CANVAS.width * 0.009, 0, 2 * Math.PI);
            this._CONTEXT.lineWidth = 1;
            this._CONTEXT.fillStyle = "#32CD32";
            this._CONTEXT.fill();
            this._CONTEXT.beginPath();
            this._CONTEXT.save();
            this._CONTEXT.translate(this.api.localization_status.positionx / this.resomap + this.offsetX, this._CANVAS.height -
                this.api.localization_status.positiony / this.resomap +
                this.offsetY);
            this._CONTEXT.rotate(-this.api.localization_status.positiont);
            this._CONTEXT.moveTo(0, -15);
            this._CONTEXT.lineTo(15, 0);
            this._CONTEXT.lineTo(0, 15);
            this._CONTEXT.lineTo(-10, 15);
            this._CONTEXT.lineTo(-10, -15);
            this._CONTEXT.lineTo(0, -15);
            this._CONTEXT.lineWidth = 2;
            this._CONTEXT.fill("nonzero");
            this._CONTEXT.stroke();
            this._CONTEXT.restore();
            if (this.api.navigation_status) {
                if (this.api.navigation_status.status == 1) {
                    var traj = this.api.navigation_status.trajectory;
                    traj.forEach(function (evenement) {
                        _this._CONTEXT.beginPath();
                        _this._CONTEXT.moveTo(evenement.Start.X / _this.resomap + _this.offsetX, _this._CANVAS.height -
                            (evenement.Start.Y / _this.resomap - _this.offsetY));
                        _this._CONTEXT.lineTo(evenement.End.X / _this.resomap + _this.offsetX, _this._CANVAS.height -
                            (evenement.End.Y / _this.resomap - _this.offsetY));
                        _this._CONTEXT.strokeStyle = "#32CD32";
                        _this._CONTEXT.stroke();
                    });
                }
            }
        }
    };
    ;
    LiftPage.prototype.on_zoom_out = function (ev) {
        ev.preventDefault();
        if (!(this.resomap >= this.initialzoom)) {
            this.resomap = this.resomap + 150 / 100000;
        }
        else {
            this.offsetX = 0;
            this.offsetY = 0;
        }
        this.createImageFromBlob();
    };
    LiftPage.prototype.on_zoom_in = function (ev) {
        ev.preventDefault();
        if (this.resomap > this.initialzoom / 4) {
            this.resomap = this.resomap - 150 / 100000;
        }
        this.createImageFromBlob();
    };
    LiftPage.prototype.getImageFromService = function () {
        var _this = this;
        this.imge = document.getElementById("imgmap7");
        if (this.api.id_current_map != undefined) {
            this.offsetX = this.api.mapdata.Offset.X;
            this.offsetY = this.api.mapdata.Offset.Y;
            this.resomap = this.api.mapdata.Resolution;
            this.initialzoom = this.resomap;
        }
        console.log("GetImageFromService");
        this.api.getImgMapHttp().subscribe(function (data) {
            _this.api.imgMap = data;
            _this.createImageFromBlob();
            console.log("GetImageFromService");
        }, function (error) {
            console.log(error);
        });
    };
    LiftPage.prototype.onSliderRelease = function (ev, id) {
        ev.preventDefault();
        id.open();
    };
    LiftPage.prototype.chimiotopharma = function (ev) {
        ev.preventDefault();
        this.api.call_lift(2, 0);
    };
    LiftPage.prototype.pharmatochimio = function (ev) {
        ev.preventDefault();
        this.api.call_lift(0, 2);
    };
    LiftPage.prototype.reloc_pharma = function () {
        console.log(this.param.qrcodes);
        this.api.QRselected = this.param.qrcodes.filter(function (x) { return x.qr == "relocpharma"; })[0];
        this.onReloc();
    };
    LiftPage.prototype.onReloc = function () {
        var _this = this;
        console.log("onreloc");
        this.showLoading();
        this.speech.speak(this.param.datatext.alertReloc);
        //this.api.getCurrentMap();
        console.log(this.api.id_current_map);
        console.log(this.api.QRselected.id_map);
        if (!(this.api.id_current_map == this.api.QRselected.id_map)) {
            this.api.current_map = this.api.QRselected.id_map;
            console.log("onchangemap");
            this.onMapChange(this.api.QRselected.id_map);
        }
        setTimeout(function () {
            _this.api.getFirstLocationHttp();
        }, 4000);
        setTimeout(function () {
            _this.api.resetLocationHttp();
        }, 6000);
        setTimeout(function () {
            _this.imge.src = "http://" + _this.param.localhost + "/api/maps/current/image?t=" + new Date().getTime();
        }, 8000);
        setTimeout(function () {
            _this.createImageFromBlob();
            _this.dismissLoading();
            _this.popup.showToastBlue(_this.param.datatext.localizationDone + _this.api.mapdata.Name, 4000);
            _this.speech.speak(_this.param.datatext.localizationDone + _this.api.mapdata.Name);
            _this.api.abortNavHttp();
        }, 9000);
    };
    LiftPage.prototype.reloc_chimio = function () {
        console.log(this.param.qrcodes);
        this.api.QRselected = this.param.qrcodes.filter(function (x) { return x.qr == "relocchimio"; })[0];
        this.onReloc();
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])("canvas7"),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* ElementRef */])
    ], LiftPage.prototype, "canvasEl", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])("imgmap7"),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* ElementRef */])
    ], LiftPage.prototype, "imgmapElement", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])("content1"),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* Content */])
    ], LiftPage.prototype, "content1", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])("select1"),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* Select */])
    ], LiftPage.prototype, "select1", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])("select2"),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* Select */])
    ], LiftPage.prototype, "select2", void 0);
    LiftPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: "page-lift",template:/*ion-inline-start:"C:\Users\laele\Documents\KomApp\toolbox\src\pages\lift\lift.html"*/'<!-- lift page html template -->\n\n<ion-header no-border>\n\n  <headpage pageName="lift"></headpage>\n\n</ion-header>\n\n\n\n<ion-content padding id="body7">\n\n  <ion-card text-center class="big_card">\n\n    <ion-grid class="heightstyle">\n\n      <ion-row class="heightstyle10" id="firstrow7">\n\n        <ion-col col-4>\n\n          <ion-list>\n\n            <ion-item>\n\n              <ion-label>{{this.param.datatext.maps}} : </ion-label>\n\n              <ion-select #select1\n\n                interface="popover"\n\n                [(ngModel)]="current_map"\n\n                okText="{{this.param.datatext.btn_ok}}"\n\n                cancelText="{{this.param.datatext.btn_cancel}}"\n\n                (ionChange)="onMapChange($event)"\n\n                (mouseup)="onSliderRelease($event,select1)"\n\n              >\n\n                <ion-option *ngFor="let m of this.api.all_maps" value="{{m.Id}}"\n\n                  >{{m.Name}}</ion-option\n\n                >\n\n              </ion-select>\n\n            </ion-item>\n\n          </ion-list>\n\n        </ion-col>\n\n        <ion-col col-2>\n\n          <!-- <ion-buttons class="div_btn_map">\n\n            <button\n\n              (mouseup)="reloc_pharma()"\n\n              ion-button\n\n              color="warning"\n\n              class="btn_map"\n\n            >\n\n            RELOC PHARMA\n\n             </button\n\n          ></ion-buttons> -->\n\n        </ion-col>\n\n        <ion-col col-2>\n\n          <!-- <ion-buttons class="div_btn_map">\n\n            <button\n\n             (mouseup)="reloc_chimio()"\n\n              ion-button\n\n              color="warning"\n\n              class="btn_map"\n\n            >\n\n              RELOC CHIMIO</button\n\n          ></ion-buttons> -->\n\n        </ion-col>\n\n        <ion-col col-2 >\n\n          <ion-buttons class="div_btn_map">\n\n            <button\n\n            [disabled]="api.moovingpharma"\n\n             (mouseup)="go_pharma($event)"\n\n              ion-button\n\n              color="secondary"\n\n              class="btn_map"\n\n            >\n\n              GO PHARMA</button\n\n          ></ion-buttons>\n\n        </ion-col>\n\n        <ion-col col-2>\n\n          <ion-buttons class="div_btn_map">\n\n            <button\n\n            [disabled]="api.moovingchimio"\n\n             (mouseup)="go_chimio($event)"\n\n              ion-button\n\n              color="secondary"\n\n              class="btn_map"\n\n            >\n\n              GO CHIMIO</button\n\n          ></ion-buttons>\n\n        </ion-col>\n\n      </ion-row>\n\n      <ion-row class="heightstyle90" id="firstrow7">\n\n        <ion-col\n\n         \n\n          col-8\n\n          id="testcol7"\n\n          class="heightstyle"\n\n          style="background-color: rgba(0, 0, 0, 0.904)"\n\n        >\n\n          <ion-row [hidden]="cam" class="heightstyle90">\n\n            <canvas\n\n              #canvas7\n\n              (wheel)="on_wheel($event);"\n\n              (mouseup)="on_mouse_up($event);"\n\n              (mousedown)="on_mouse_down($event);"\n\n              (mousemove)="on_mouse_move($event);"\n\n            ></canvas>\n\n            \n\n            <img id="imgmap7" #imgmap7 src={{map_url}} crossorigin="anonymous" class="video" style="display:none;" />\n\n          </ion-row>\n\n          <!-- <ion-row [hidden]="!cam" class="heightstyle90">\n\n            <img class="video2" id="imgcpsd" /> \n\n            <div class="joy" id="zone_joystick"></div>\n\n          </ion-row> -->\n\n          <ion-row class="heightstyle10" >\n\n            <ion-col col-3 >\n\n              <button class="btnmap" (mouseup)="on_zoom_out($event)" ion-button>\n\n                <ion-icon name="search"></ion-icon> -\n\n              </button>\n\n            </ion-col>\n\n            <ion-col col-3>\n\n              <button ion-button (mouseup)="on_zoom_in($event)" class="btnmap">\n\n                <ion-icon name="search"></ion-icon> +\n\n              </button>\n\n            </ion-col>\n\n            <!-- <ion-col col-2 >\n\n              <button class="btnmap" (mouseup)="change_cam()" ion-button>\n\n                <ion-icon\n\n                *ngIf="!cam"\n\n               \n\n                name="camera"\n\n                \n\n              ></ion-icon>\n\n              <ion-icon\n\n                *ngIf="cam"\n\n                \n\n                name="map"\n\n                \n\n              ></ion-icon>\n\n              </button>\n\n            </ion-col> -->\n\n            <ion-col col-2>\n\n              <button\n\n                [hidden]="reloc"\n\n                [disabled]="cam"\n\n                ion-button\n\n                class="btnmap"\n\n                (mouseup)="on_click_relocate($event)"\n\n              >\n\n                {{param.datatext.reloc}}\n\n              </button>\n\n              <button\n\n                [hidden]="!reloc"\n\n                \n\n                ion-button\n\n                color="secondary"\n\n                class="btnmap"\n\n                (mouseup)="on_click_relocate($event)"\n\n              >\n\n                {{param.datatext.confirm}}\n\n              </button>\n\n            </ion-col>\n\n            <ion-col col-2>\n\n              <button\n\n                [disabled]="!reloc"\n\n                class="btnmap"\n\n                (mouseup)="on_click_relocate_left($event)"\n\n                ion-button\n\n              >\n\n                <ion-icon name="undo"></ion-icon>\n\n              </button>\n\n            </ion-col>\n\n            <ion-col col-2>\n\n              <button\n\n                [disabled]="!reloc"\n\n                ion-button\n\n                (mouseup)="on_click_relocate_right($event)"\n\n                class="btnmap"\n\n              >\n\n                <ion-icon name="redo"></ion-icon>\n\n              </button>\n\n            </ion-col>\n\n          </ion-row>\n\n        </ion-col>\n\n\n\n\n\n        <ion-col col-4 >\n\n         \n\n          <!-- <ion-label class="labelcss"> "Floors": {{this.api.lift_status.floors}} </ion-label>\n\n          <ion-label class="labelcss">"CurrentFloor": {{this.api.lift_status.currentFloor}}  </ion-label>\n\n          <ion-label class="labelcss">"TargetFloor": {{this.api.lift_status.targetFloor}}  </ion-label>\n\n          <ion-label class="labelcss">"IsBusy" : {{this.api.lift_status.isBusy}}  </ion-label>\n\n          <ion-label class="labelcss">"Id": {{this.api.lift_status.id}}  </ion-label> -->\n\n          <!-- <button class="btnmap" (mouseup)="enter_lift()" ion-button>\n\n            Enter Lift\n\n          </button>\n\n          <button class="btnmap" (mouseup)="leave_lift($event)" ion-button>\n\n            Leave Lift\n\n          </button> -->\n\n          <!-- <button [disabled]="api.lift_status.isBusy" class="btnmap" (mouseup)="pharmatochimio($event)" ion-button>\n\n            CALL LIFT CHIMIO\n\n          </button>\n\n          <button [disabled]="api.lift_status.isBusy" class="btnmap" (mouseup)="chimiotopharma($event)" ion-button>\n\n            CALL LIFT  PHARMA\n\n          </button> -->\n\n          <div class="divideo">\n\n          \n\n          <div class="joy" id="zone_joystick"><img class="video3" id="imgcpsd" /></div>\n\n          </div>\n\n          <button class="btn_go" color="danger" (mouseup)="stoprobot()" ion-button>\n\n            STOP\n\n          </button>\n\n        </ion-col>\n\n      </ion-row>\n\n    </ion-grid>\n\n  </ion-card>\n\n</ion-content>\n\n'/*ion-inline-end:"C:\Users\laele\Documents\KomApp\toolbox\src\pages\lift\lift.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_5__services_popup_service__["a" /* PopupService */],
            __WEBPACK_IMPORTED_MODULE_3__services_api_service__["a" /* ApiService */],
            __WEBPACK_IMPORTED_MODULE_4__services_param_service__["a" /* ParamService */],
            __WEBPACK_IMPORTED_MODULE_7__services_speech_service__["a" /* SpeechService */],
            __WEBPACK_IMPORTED_MODULE_2__services_alert_service__["a" /* AlertService */]])
    ], LiftPage);
    return LiftPage;
}());

//# sourceMappingURL=lift.js.map

/***/ }),

/***/ 269:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HeadpageComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__pages_poi_poi__ = __webpack_require__(141);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__pages_tuto_tuto__ = __webpack_require__(270);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_api_service__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__services_alert_service__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__services_popup_service__ = __webpack_require__(35);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__services_speech_service__ = __webpack_require__(54);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__services_param_service__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_param_param__ = __webpack_require__(287);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};











var HeadpageComponent = /** @class */ (function () {
    function HeadpageComponent(speech, toastCtrl, navCtrl, param, api, alert, popup) {
        this.speech = speech;
        this.toastCtrl = toastCtrl;
        this.navCtrl = navCtrl;
        this.param = param;
        this.api = api;
        this.alert = alert;
        this.popup = popup;
        this.poiPage = __WEBPACK_IMPORTED_MODULE_1__pages_poi_poi__["a" /* PoiPage */];
        this.tutoPage = __WEBPACK_IMPORTED_MODULE_2__pages_tuto_tuto__["a" /* TutoPage */];
        this.paramPage = __WEBPACK_IMPORTED_MODULE_9__pages_param_param__["a" /* ParamPage */];
        this.cabinet_close = true;
        this.optiondateofday = { weekday: "long", day: "numeric", month: "long" };
        this.microon = false;
        this.cpt_battery = 0;
        this.cpt_open = 0;
        if (this.param.allowspeech == 1) {
            this.volumeState = 1;
        }
        else {
            this.volumeState = 0;
        }
    }
    HeadpageComponent.prototype.ionViewWillLeave = function () {
        clearInterval(this.update);
        clearInterval(this.lowbattery);
        clearInterval(this.source);
        clearInterval(this.cptduration);
    };
    HeadpageComponent.prototype.ngOnDestroy = function () {
        clearInterval(this.update);
        clearInterval(this.lowbattery);
        clearInterval(this.source);
        clearInterval(this.cptduration);
    };
    HeadpageComponent.prototype.updateBattery = function () {
        if (this.api.statusRobot === 2) {
            // battery logo will not apppear
            this.api.batteryState = 10;
        }
        else if (this.api.battery_status.status < 2) {
            //charging
            this.api.batteryState = 4;
        }
        else if (this.api.battery_status.status === 3 ||
            this.api.battery_status.remaining <= this.param.battery.critical) {
            //critical
            this.api.batteryState = 0;
        }
        else if (this.api.battery_status.remaining > this.param.battery.high) {
            //hight
            this.api.batteryState = 3;
        }
        else if (this.api.battery_status.remaining > this.param.battery.low) {
            //mean
            this.api.batteryState = 2;
        }
        else if (this.api.battery_status.remaining <= this.param.battery.low) {
            //low
            this.api.batteryState = 1;
        }
        else {
            // battery logo will not apppear
            this.api.batteryState = 10;
        }
    };
    HeadpageComponent.prototype.onTouchWifi = function (ev) {
        ev.preventDefault();
        if (!this.api.wifiok) {
            this.okToast(this.param.datatext.nointernet);
        }
        else {
            this.okToast(this.param.datatext.robotConnectedToI);
        }
    };
    HeadpageComponent.prototype.mail = function () {
        this.param.cptDuration(); //update duration
        if (this.param.durationNS.length > 1) {
            if (this.api.wifiok) {
                //this.param.updateDurationNS(this.param.durationNS[0].id_duration);
                this.alert.duration(this.param.durationNS[0], this.api.mailAddInformationBasic());
                //this.param.getDurationNS();
            }
        }
    };
    HeadpageComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.popup.onSomethingHappened5(this.accessparam.bind(this));
        // bouton 's text (Return / Exit), to the left of the header, depends on the page you are on
        if (this.pageName === "poi") {
            this.update = setInterval(function () { return _this.getUpdate(); }, 1000); //update hour, battery every 1/2 secondes
            this.lowbattery = setInterval(function () { return _this.monitoringBattery(); }, 60000);
            this.source = setInterval(function () { return _this.api.jetsonOK(); }, 2000);
            this.cptduration = setInterval(function () { return _this.mail(); }, 120000);
            this.textBoutonHeader = this.param.datatext.quit;
            this.api.textHeadBand = this.param.datatext.toolbox;
        }
        else if (this.pageName === "rounddisplay") {
            this.textBoutonHeader = this.param.datatext.return;
            this.api.textHeadBand = this.param.datatext.headBand_rounds;
        }
        else if (this.pageName === "roundcreation") {
            this.textBoutonHeader = this.param.datatext.return;
            this.api.textHeadBand = this.param.datatext.roundcreation;
        }
        else if (this.pageName === "param") {
            this.textBoutonHeader = this.param.datatext.return;
            this.api.textHeadBand = this.param.datatext.param;
        }
        else if (this.pageName === "roundedit") {
            this.textBoutonHeader = this.param.datatext.return;
            this.api.textHeadBand = this.param.datatext.rounds_edit;
        }
        else if (this.pageName === "paramuv") {
            this.textBoutonHeader = this.param.datatext.return;
            this.api.textHeadBand = this.param.datatext.uvsettings.toUpperCase();
        }
        else if (this.pageName === "qrcode") {
            this.textBoutonHeader = this.param.datatext.return;
            this.api.textHeadBand = this.param.datatext.qrcodes;
        }
        else if (this.pageName === "lift") {
            this.textBoutonHeader = this.param.datatext.return;
            this.api.textHeadBand = "BELHARRA";
        }
        else if (this.pageName === "langue") {
            this.textBoutonHeader = this.param.datatext.return;
            this.api.textHeadBand = this.param.datatext.changelangage;
        }
        else if (this.pageName === "password") {
            this.textBoutonHeader = this.param.datatext.return;
            this.api.textHeadBand = this.param.datatext.editpswd;
        }
        else if (this.pageName === "docking") {
            this.textBoutonHeader = this.param.datatext.return;
            this.api.textHeadBand = this.param.datatext.configDocking.toUpperCase();
        }
        else {
            this.textBoutonHeader = this.param.datatext.return;
            this.api.textHeadBand = this.param.datatext.tutorial;
        }
    };
    HeadpageComponent.prototype.accessparam = function () {
        this.navCtrl.push(this.paramPage);
    };
    HeadpageComponent.prototype.onTouchParam = function (ev) {
        ev.preventDefault();
        if (!this.api.roundActive && !this.api.towardDocking) {
            //robot is not moving
            if (this.pageName === "param" ||
                this.pageName === "sms" ||
                this.pageName === "mail" ||
                this.pageName === "paramuv" ||
                this.pageName === "langue" ||
                this.pageName === "password") {
                console.log("already");
            }
            else {
                this.popup.askpswd();
            }
        }
    };
    HeadpageComponent.prototype.onTouchStatus = function (ev) {
        ev.preventDefault();
        if (!this.api.roundActive && !this.api.towardDocking) {
            if (this.api.statusRobot === 2) {
                // if status red
                this.popup.statusRedPresent();
            }
            else {
                this.okToast(this.param.datatext.statusGreenPresent_message);
            }
        }
    };
    HeadpageComponent.prototype.okToast = function (m) {
        var toast = this.toastCtrl.create({
            message: m,
            duration: 3000,
            position: "middle",
            cssClass: "toastok",
        });
        toast.present();
    };
    HeadpageComponent.prototype.monitoringBattery = function () {
        var _this = this;
        if (this.api.batteryState === 0) {
            this.alert.lowBattery(this.api.mailAddInformationBasic());
            if (!this.api.towardDocking) {
                this.api.towardDocking = true;
                setTimeout(function () {
                    _this.api.reachHttp("docking");
                }, 2000);
            }
            clearInterval(this.lowbattery);
        }
    };
    HeadpageComponent.prototype.onTouchVolume = function () {
        if (this.volumeState === 0) {
            this.volumeState = 1;
            this.param.allowspeech = 1;
        }
        else {
            this.volumeState = 0;
            this.param.allowspeech = 0;
        }
    };
    HeadpageComponent.prototype.onTouchBattery = function (ev) {
        ev.preventDefault();
        if (this.api.batteryState == 0) {
            this.popup.showToastRed(this.param.datatext.lowBattery_title +
                " : " +
                this.api.battery_status.remaining +
                "%", 3000, "top");
        }
        else {
            this.okToast(this.param.datatext.battery +
                " : " +
                this.api.battery_status.remaining +
                "%");
        }
    };
    HeadpageComponent.prototype.getUpdate = function () {
        //update hour, battery
        this.updateBattery();
        this.updateBand();
        var now = new Date().toLocaleString("fr-FR", { hour: "numeric", minute: "numeric" });
        this.api.hour = now;
        //console.log(this.batteryState);
        if (this.api.statusRobot === 0 && this.api.batteryState === 10) {
            // importante to know if the application has run correctly
            this.cpt_battery += 1;
        }
        else {
            this.cpt_battery = 0;
        }
        if (this.cpt_battery > 10) {
            this.api.statusRobot = 2;
        }
    };
    HeadpageComponent.prototype.updateBand = function () {
        if (this.api.is_blocked || this.api.is_high) {
            this.textHeadBand = this.param.datatext.errorBlocked_title.toUpperCase();
        }
        else if (this.pageName === "poi") {
            this.textHeadBand = this.param.datatext.toolbox;
        }
    };
    HeadpageComponent.prototype.onTouchHelp = function (ev) {
        ev.preventDefault();
        if (!this.api.roundActive && !this.api.towardDocking) {
            //robot is not moving
            if (!(this.pageName === "tuto")) {
                this.alert.displayTuto(this.api.mailAddInformationBasic());
                this.navCtrl.push(this.tutoPage);
            }
        }
    };
    HeadpageComponent.prototype.onTouchLocked = function (ev) {
        var _this = this;
        ev.preventDefault();
        if (this.param.robot.num_of_cabinet != 0) {
            this.cabinet_close = false;
            this.popup.showToastGreen(this.param.datatext.cabinet_open, 5000, "middle");
            for (var i = 0; i < this.param.robot.num_of_cabinet; i++) {
                console.log;
                this.api.cabinethttp(i, true);
            }
            setTimeout(function () {
                _this.cabinet_close = true;
            }, 5000);
        }
    };
    HeadpageComponent.prototype.clicOnMenu = function (ev) {
        ev.preventDefault();
        if (this.pageName === "tuto" ||
            this.pageName === "param" ||
            this.pageName === "rounddisplay" ||
            this.pageName === "qrcode" ||
            this.pageName === "lift" ||
            this.pageName === "docking") {
            this.navCtrl.popToRoot();
        }
        else if (this.pageName === "paramuv" ||
            this.pageName === "langue" ||
            this.pageName === "password" ||
            this.pageName === "roundcreation" ||
            this.pageName === "roundedit") {
            this.navCtrl.pop();
        }
        else {
            this.api.close_app = true;
            this.alert.appClosed(this.api.mailAddInformationBasic());
            //stop the round and quit the app
            setTimeout(function () {
                window.close();
            }, 1000);
        }
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", String)
    ], HeadpageComponent.prototype, "pageName", void 0);
    HeadpageComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: "headpage",template:/*ion-inline-start:"C:\Users\laele\Documents\KomApp\toolbox\src\components\headpage\headpage.html"*/'<!-- Html component of the HMI header with the Back / Exit button, the time, the sound buttons, microphone, parameters etc ...-->\n\n\n\n<ion-navbar hideBackButton>\n\n  <ion-buttons left class="btn_menu_size">\n\n    <button ion-button solid large class="btn_menu" (mouseup)="clicOnMenu($event)">\n\n      <font class="font_menu_size">{{this.textBoutonHeader}}</font>\n\n    </button>\n\n  </ion-buttons>\n\n\n\n  <ion-title text-center><font class="hour_size">{{api.hour}}</font></ion-title>\n\n\n\n  <ion-buttons right>\n\n    <!-- <button [disabled]="!cabinet_close" *ngIf="param.robot.num_of_cabinet>0" ion-button class="btn_header" (mouseup)="onTouchLocked($event)" >\n\n      <ion-icon name="unlock" class="iconlock"></ion-icon>\n\n    </button> -->\n\n    <button ion-button class="btn_header" (mouseup)="onTouchHelp($event)">\n\n      <img class="imgicon" src="./assets/imgs/help.png" />\n\n    </button>\n\n    <button ion-button class="btn_header" (mouseup)="onTouchParam($event)">\n\n      <img class="imgicon" src="./assets/imgs/parameter.png" />\n\n    </button>\n\n    <!-- <button ion-button disabled class="btn_header"  >\n\n      <img *ngIf="!microon" class="imgicon" src="./assets/imgs/microoff.png"/>\n\n      <img *ngIf="microon" class="imgicon" src="./assets/imgs/microon.png"/>\n\n    </button> -->\n\n\n\n    <button ion-button class="btn_header" (mouseup)="onTouchWifi($event)">\n\n      <img\n\n        *ngIf="!api.wifiok"\n\n        class="imginternet"\n\n        src="./assets/imgs/wifioff.png"\n\n      />\n\n      <img\n\n        *ngIf="api.wifiok"\n\n        class="imginternet"\n\n        src="./assets/imgs/wifion.png"\n\n      />\n\n    </button>\n\n\n\n    <!-- <button ion-button class="btn_header" (click)="onTouchVolume()">\n\n      <img *ngIf="volumeState === 0" class="imgicon" src="./assets/imgs/volumemute.png"/>\n\n      <img *ngIf="volumeState === 1" class="imgicon" src="./assets/imgs/volumelow.png"/>\n\n      <img *ngIf="volumeState === 2" class="imgicon" src="./assets/imgs/volumehight.png"/>\n\n    </button> -->\n\n    <button ion-button class="btn_header" (mouseup)="onTouchStatus($event)">\n\n      <img\n\n        *ngIf="api.statusRobot===0"\n\n        class="imgwifi"\n\n        src="./assets/imgs/statusgreen.png"\n\n      />\n\n      <img\n\n        *ngIf="api.statusRobot ===1"\n\n        class="imgwifi"\n\n        src="./assets/imgs/statusorange.png"\n\n      />\n\n      <img\n\n        *ngIf="api.statusRobot===2"\n\n        class="imgwifi"\n\n        src="./assets/imgs/statusred.png"\n\n      />\n\n    </button>\n\n    <button ion-button class="btn_header" (mouseup)="onTouchBattery($event)">\n\n      <img\n\n        *ngIf="api.batteryState === 0"\n\n        class="imgbattery"\n\n        src="./assets/imgs/batteryoff.png"\n\n      />\n\n      <img\n\n        *ngIf="api.batteryState === 1"\n\n        class="imgbattery"\n\n        src="./assets/imgs/batterylow.png"\n\n      />\n\n      <img\n\n        *ngIf="api.batteryState === 2"\n\n        class="imgbattery"\n\n        src="./assets/imgs/batterymean.png"\n\n      />\n\n      <img\n\n        *ngIf="api.batteryState === 3"\n\n        class="imgbattery"\n\n        src="./assets/imgs/batteryhight.png"\n\n      />\n\n      <img\n\n        *ngIf="api.batteryState === 4"\n\n        class="imgbattery"\n\n        src="./assets/imgs/batterycharge.png"\n\n      />\n\n    </button>\n\n  </ion-buttons>\n\n</ion-navbar>\n\n<ion-toolbar no-padding>\n\n  <div class="scroll_parent">\n\n    <ion-title class="scroll">\n\n      <font class="scroll_text">{{this.api.textHeadBand}}</font>\n\n    </ion-title>\n\n  </div>\n\n</ion-toolbar>\n\n'/*ion-inline-end:"C:\Users\laele\Documents\KomApp\toolbox\src\components\headpage\headpage.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_7__services_speech_service__["a" /* SpeechService */],
            __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["k" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["h" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_8__services_param_service__["a" /* ParamService */],
            __WEBPACK_IMPORTED_MODULE_4__services_api_service__["a" /* ApiService */],
            __WEBPACK_IMPORTED_MODULE_5__services_alert_service__["a" /* AlertService */],
            __WEBPACK_IMPORTED_MODULE_6__services_popup_service__["a" /* PopupService */]])
    ], HeadpageComponent);
    return HeadpageComponent;
}());

//# sourceMappingURL=headpage.js.map

/***/ }),

/***/ 270:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TutoPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_param_service__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_pdfjs_dist_webpack__ = __webpack_require__(453);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_pdfjs_dist_webpack___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_pdfjs_dist_webpack__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var TutoPage = /** @class */ (function () {
    function TutoPage(param) {
        this.param = param;
        this.pageNum = 1;
        this.PDFJSViewer = __WEBPACK_IMPORTED_MODULE_2_pdfjs_dist_webpack__;
        this.currPage = 1; //Pages are 1-based not 0-based
        this.numPages = 0;
        this.thePDF = null;
        this.pageRendering = false;
        this.pageNumPending = null;
    }
    TutoPage.prototype.ngOnInit = function () {
    };
    TutoPage.prototype.ionViewDidEnter = function () {
        // deuxième méthode
        this.launchdoc();
    };
    TutoPage.prototype.launchdoc = function () {
        var _this = this;
        //This is where you start
        __WEBPACK_IMPORTED_MODULE_2_pdfjs_dist_webpack__["getDocument"](this.param.datatext.URL_toolbox).then(function (pdf) {
            //Set PDFJS global object (so we can easily access in our page functions
            _this.thePDF = pdf;
            //How many pages it has
            _this.numPages = pdf.numPages;
            //Start with first page
            pdf.getPage(1).then(function (page) { _this.handlePages(page); });
        });
    };
    TutoPage.prototype.handlePages = function (page) {
        var _this = this;
        //This gives us the page's dimensions at full scale
        var viewport = page.getViewport(2);
        //We'll create a canvas for each page to draw it on
        var canvas = document.createElement("canvas");
        canvas.style.display = "block";
        var context = canvas.getContext('2d');
        canvas.height = viewport.height;
        canvas.width = viewport.width;
        //Draw it on the canvas
        page.render({ canvasContext: context, viewport: viewport });
        //Add it to the web page
        //document.body.appendChild( canvas );
        //console.log(document.getElementById("ionbody"));
        var elem = document.getElementById("ionbody");
        if (elem != null) {
            elem.appendChild(canvas);
            var line = document.createElement("hr");
            elem.appendChild(line);
        }
        //console.log(this.currPage);
        //Move to next page
        this.currPageAdd();
        if (this.thePDF !== null && this.currPage <= this.numPages) {
            this.thePDF.getPage(this.currPage).then(function (page) { _this.handlePages(page); });
        }
    };
    TutoPage.prototype.currPageAdd = function () {
        this.currPage = this.currPage + 1;
    };
    TutoPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-tuto',template:/*ion-inline-start:"C:\Users\laele\Documents\KomApp\toolbox\src\pages\tuto\tuto.html"*/'<!-- home page html template -->\n\n<ion-header no-border>\n\n  <headpage pageName="tuto"></headpage>\n\n</ion-header>\n\n\n\n<ion-content padding >\n\n  <ion-scroll scrollY="true" style="height:100%;width:80%;left:50%;transform:translateX(-50%); " >\n\n    <div class="center" style="height:100%;">\n\n      <div id="ionbody" style="display: flex;flex-grow: 1;  flex-direction: column;background: #ddd;overflow-y: auto;" width="60%" height="100%">\n\n\n\n      </div>\n\n    </div>\n\n  </ion-scroll>\n\n</ion-content>\n\n\n\n\n\n'/*ion-inline-end:"C:\Users\laele\Documents\KomApp\toolbox\src\pages\tuto\tuto.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__services_param_service__["a" /* ParamService */]])
    ], TutoPage);
    return TutoPage;
}());

//# sourceMappingURL=tuto.js.map

/***/ }),

/***/ 287:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ParamPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_api_service__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__paramuv_paramuv__ = __webpack_require__(288);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__langue_langue__ = __webpack_require__(289);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__services_param_service__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__password_password__ = __webpack_require__(290);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var ParamPage = /** @class */ (function () {
    function ParamPage(navCtrl, api, param) {
        this.navCtrl = navCtrl;
        this.api = api;
        this.param = param;
        this.languePage = __WEBPACK_IMPORTED_MODULE_4__langue_langue__["a" /* LanguePage */];
        this.paramuvPage = __WEBPACK_IMPORTED_MODULE_3__paramuv_paramuv__["a" /* ParamUVPage */];
        this.passwordPage = __WEBPACK_IMPORTED_MODULE_6__password_password__["a" /* PasswordPage */];
    }
    ParamPage.prototype.ngOnInit = function () { };
    ParamPage.prototype.ionViewDidEnter = function () {
        this.api.textHeadBand = this.param.datatext.param;
    };
    ParamPage.prototype.goLangue = function (ev) {
        ev.preventDefault();
        this.navCtrl.push(this.languePage);
    };
    ParamPage.prototype.goParamUV = function () {
        this.param.getUVparam();
        this.navCtrl.push(this.paramuvPage);
    };
    ParamPage.prototype.goPassword = function (ev) {
        ev.preventDefault();
        this.navCtrl.push(this.passwordPage);
    };
    ParamPage.prototype.activateAdminMode = function () {
        this.api.adminMode = !this.api.adminMode;
    };
    ParamPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: "page-param",template:/*ion-inline-start:"C:\Users\laele\Documents\KomApp\toolbox\src\pages\param\param.html"*/'<!-- param page html template -->\n\n<ion-header no-border>\n\n  <headpage pageName="param"></headpage>\n\n</ion-header>\n\n\n\n<ion-content padding>\n\n  <ion-card text-center class="big_card">\n\n    <ion-grid class="heightstyle">\n\n      <ion-list>\n\n        <!-- <button ion-item class="btnscss" (click)="goParamUV()">\n\n          <ion-icon class="iconscss" name="settings" item-start></ion-icon\n\n          >{{this.param.datatext.uvsettings}}<ion-icon\n\n            class="iconscss"\n\n            name="settings"\n\n            item-end\n\n          ></ion-icon>\n\n        </button> -->\n\n        <button ion-item class="btnscss" (mouseup)="goLangue($event)">\n\n          <ion-icon class="iconscss" name="globe" item-start></ion-icon\n\n          >{{param.datatext.langage}}<ion-icon\n\n            class="iconscss"\n\n            name="globe"\n\n            item-end\n\n          ></ion-icon>\n\n        </button>\n\n        <button ion-item class="btnscss" (mouseup)="goPassword($event)"><ion-icon class="iconscss" name="key" item-start></ion-icon>{{param.datatext.editpswd1}}<ion-icon class="iconscss" name="key" item-end></ion-icon></button>\n\n        <ion-row  ion-item class="adminscss">\n\n          \n\n            <ion-label style="color:black;">{{param.datatext.admin}}</ion-label>>\n\n          \n\n            <ion-toggle\n\n                    [checked]="api.adminMode"\n\n                    (ionChange)="activateAdminMode()"\n\n                    item-end\n\n                ></ion-toggle>\n\n         </ion-row>\n\n        \n\n      </ion-list>\n\n    </ion-grid>\n\n  </ion-card>\n\n</ion-content>\n\n'/*ion-inline-end:"C:\Users\laele\Documents\KomApp\toolbox\src\pages\param\param.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_2__services_api_service__["a" /* ApiService */],
            __WEBPACK_IMPORTED_MODULE_5__services_param_service__["a" /* ParamService */]])
    ], ParamPage);
    return ParamPage;
}());

//# sourceMappingURL=param.js.map

/***/ }),

/***/ 288:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ParamUVPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_api_service__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_param_service__ = __webpack_require__(14);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var ParamUVPage = /** @class */ (function () {
    function ParamUVPage(navCtrl, toastCtrl, api, param, alertCtrl) {
        this.navCtrl = navCtrl;
        this.toastCtrl = toastCtrl;
        this.api = api;
        this.param = param;
        this.alertCtrl = alertCtrl;
        this.t_evac = this.param.paramuv.evacuation_time;
        this.t_preheat = this.param.paramuv.preheating_time;
        this.threshold_lamp = this.param.paramuv.lamp_threshold;
        this.threshold_uv = this.param.paramuv.uv_threshold;
        this.threshold_battery = this.param.paramuv.battery_threshold;
        this.detection = this.param.paramuv.person_detection == 1;
        console.log(this.detection);
    }
    ParamUVPage.prototype.ngOnInit = function () { };
    ParamUVPage.prototype.editparamuv = function () {
        if (Math.abs(this.threshold_battery) > 90) {
            this.threshold_battery = 90;
        }
        this.param.paramuv.evacuation_time = Math.abs(this.t_evac);
        this.param.paramuv.preheating_time = Math.abs(this.t_preheat);
        this.param.paramuv.lamp_threshold = Math.abs(this.threshold_lamp);
        this.param.paramuv.uv_threshold = Math.abs(this.threshold_uv);
        this.param.paramuv.battery_threshold = Math.abs(this.threshold_battery);
        if (this.detection) {
            this.param.paramuv.person_detection = 1;
        }
        else {
            this.param.paramuv.person_detection = 0;
        }
        this.okToast(this.param.datatext.saveDone);
        this.param.updateUVparam();
        this.navCtrl.pop();
    };
    ParamUVPage.prototype.savePassword = function (ev) {
        var _this = this;
        ev.preventDefault();
        var alert = this.alertCtrl.create({
            title: this.param.datatext.password,
            message: this.param.datatext.enterPassword,
            cssClass: "alertstyle",
            enableBackdropDismiss: true,
            inputs: [
                {
                    name: "password",
                    placeholder: this.param.datatext.password,
                    type: "password",
                },
            ],
            buttons: [
                {
                    text: this.param.datatext.btn_cancel,
                    role: "cancel",
                    handler: function (data) {
                        console.log("Cancel clicked");
                    },
                },
                {
                    text: this.param.datatext.btn_save,
                    role: "backdrop",
                    handler: function (data) {
                        if (data.password === "Kompai64") {
                            _this.editparamuv();
                        }
                        else {
                            _this.wrongPassword();
                        }
                    },
                },
            ],
        });
        alert.present();
    };
    ParamUVPage.prototype.wrongPassword = function () {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: this.param.datatext.password,
            message: this.param.datatext.wrongPassword,
            cssClass: "alertstyle_wrongpass",
            enableBackdropDismiss: true,
            inputs: [
                {
                    name: "password",
                    placeholder: this.param.datatext.password,
                    type: "password",
                },
            ],
            buttons: [
                {
                    text: this.param.datatext.btn_cancel,
                    role: "cancel",
                    handler: function (data) {
                        console.log("Cancel clicked");
                    },
                },
                {
                    text: "Save",
                    role: "backdrop",
                    handler: function (data) {
                        if (data.password === "Kompai64") {
                            _this.editparamuv();
                        }
                        else {
                            _this.wrongPassword();
                        }
                    },
                },
            ],
        });
        alert.present();
    };
    ParamUVPage.prototype.btnsave = function () {
        return (this.param.paramuv.evacuation_time != Math.abs(this.t_evac) ||
            this.param.paramuv.preheating_time != Math.abs(this.t_preheat) ||
            this.param.paramuv.lamp_threshold != Math.abs(this.threshold_lamp) ||
            this.param.paramuv.uv_threshold != Math.abs(this.threshold_uv) ||
            this.param.paramuv.battery_threshold !=
                Math.abs(this.threshold_battery) ||
            (this.param.paramuv.person_detection == 1) != this.detection);
    };
    ParamUVPage.prototype.onCancel = function (ev) {
        ev.preventDefault();
        this.navCtrl.pop();
    };
    ParamUVPage.prototype.onToggleChange = function () {
        this.detection = !this.detection;
    };
    ParamUVPage.prototype.okToast = function (m) {
        var toast = this.toastCtrl.create({
            message: m,
            duration: 3000,
            position: "middle",
            cssClass: "toastok",
        });
        toast.present();
    };
    ParamUVPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: "page-paramuv",template:/*ion-inline-start:"C:\Users\laele\Documents\KomApp\toolbox\src\pages\paramuv\paramuv.html"*/'<!-- param page html template -->\n\n<ion-header no-border>\n\n  <headpage pageName="paramuv"></headpage>\n\n</ion-header>\n\n\n\n<ion-content padding>\n\n  <ion-card text-center class="big_card">\n\n    <ion-grid class="wstyle70">\n\n      <ion-row class="heightstyle90">\n\n        <ion-col col-12>\n\n          <ion-item>\n\n            <ion-label text-wrap>{{param.datatext.evacuationTime}}</ion-label>\n\n            <ion-input\n\n              type="number"\n\n              min="0"\n\n              max="1000"\n\n              step="10"\n\n              [(ngModel)]="t_evac"\n\n            ></ion-input>\n\n          </ion-item>\n\n          <ion-item>\n\n            <ion-label text-wrap>{{param.datatext.preheatingTime}}</ion-label>\n\n            <ion-input\n\n              type="number"\n\n              min="0"\n\n              max="1000"\n\n              step="10"\n\n              [(ngModel)]="t_preheat"\n\n            ></ion-input>\n\n          </ion-item>\n\n          <ion-item>\n\n            <ion-label text-wrap>{{param.datatext.lampThreshold}}</ion-label>\n\n            <ion-input\n\n              type="number"\n\n              min="0"\n\n              max="20000"\n\n              step="100"\n\n              [(ngModel)]="threshold_lamp"\n\n            ></ion-input>\n\n          </ion-item>\n\n          <ion-item>\n\n            <ion-label text-wrap>{{param.datatext.uvThreshold}}</ion-label>\n\n            <ion-input\n\n              type="number"\n\n              min="0"\n\n              max="1000"\n\n              step="10"\n\n              [(ngModel)]="threshold_uv"\n\n            ></ion-input>\n\n          </ion-item>\n\n          <ion-item>\n\n            <ion-label text-wrap>{{param.datatext.batteryThreshold}}</ion-label>\n\n            <ion-input\n\n              type="number"\n\n              min="10"\n\n              max="90"\n\n              step="2"\n\n              [(ngModel)]="threshold_battery"\n\n            ></ion-input>\n\n          </ion-item>\n\n          <ion-item>\n\n            <ion-label text-wrap>{{param.datatext.personDetection}}</ion-label>\n\n            <ion-toggle\n\n              [checked]="detection"\n\n              (ionChange)="onToggleChange()"\n\n            ></ion-toggle>\n\n          </ion-item>\n\n        </ion-col>\n\n      </ion-row>\n\n      <ion-row class="heightstyle10">\n\n        <ion-col col-6>\n\n          <ion-buttons class="div_btn_param">\n\n            <button\n\n              class="btn_param"\n\n              (mouseup)="onCancel($event)"\n\n              style="color: rgb(26, 156, 195); text-decoration: underline"\n\n              ion-button\n\n              clear\n\n            >\n\n            {{param.datatext.btn_cancel}}\n\n            </button></ion-buttons\n\n          >\n\n        </ion-col>\n\n        <ion-col col-6>\n\n          <ion-buttons class="div_btn_param">\n\n            <button\n\n              class="btn_param"\n\n              [disabled]="!btnsave()"\n\n              ion-button\n\n              color="primary"\n\n              (mouseup)="savePassword($event)"\n\n            >\n\n            {{param.datatext.btn_save}}\n\n            </button></ion-buttons\n\n          >\n\n        </ion-col>\n\n      </ion-row>\n\n    </ion-grid>\n\n  </ion-card>\n\n</ion-content>\n\n'/*ion-inline-end:"C:\Users\laele\Documents\KomApp\toolbox\src\pages\paramuv\paramuv.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_2__services_api_service__["a" /* ApiService */],
            __WEBPACK_IMPORTED_MODULE_3__services_param_service__["a" /* ParamService */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */]])
    ], ParamUVPage);
    return ParamUVPage;
}());

//# sourceMappingURL=paramuv.js.map

/***/ }),

/***/ 289:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LanguePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_param_service__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(5);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var LanguePage = /** @class */ (function () {
    function LanguePage(param, loadingCtrl) {
        this.param = param;
        this.loadingCtrl = loadingCtrl;
        this.monitor = this.param.langage;
    }
    LanguePage.prototype.ngOnInit = function () {
    };
    LanguePage.prototype.monitorHandler = function () {
        this.loading = this.loadingCtrl.create({});
        this.loading.present(); //loading animation display
        this.param.updateRobot();
        setTimeout(function () {
            document.location.reload();
        }, 1000);
    };
    LanguePage.prototype.onSliderRelease = function (ev, id) {
        ev.preventDefault();
        id.open();
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])("select1"),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["j" /* Select */])
    ], LanguePage.prototype, "select1", void 0);
    LanguePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-langue',template:/*ion-inline-start:"C:\Users\laele\Documents\KomApp\toolbox\src\pages\langue\langue.html"*/'<!-- langue page html template -->\n\n<ion-header no-border>\n\n  <headpage pageName="langue"></headpage>\n\n</ion-header>\n\n<ion-content padding >\n\n  <ion-grid class="main_ion_grid">\n\n    <div style="background-color: rgb(255, 255, 255); border-radius: 15px; height: 40%; width: 60%; ">\n\n      <ion-row style="height: 25%; color: rgb(0, 0, 0);justify-content: center!important; align-items: center!important; font-size: 4em;">\n\n        {{param.datatext.langage}}\n\n      </ion-row>\n\n      <ion-row style="height: 75%;vertical-align: middle;\n\n      justify-content: center;\n\n      display: flex!important;\n\n      align-items: center!important">\n\n       \n\n        <ion-select #select1 [(ngModel)]="param.robot.langage" (ionChange)="monitorHandler()" style="width: 70% !important; font-size: x-large;background-color:rgba(26, 156, 195, 0.199);" interface="popover" (mouseup)="onSliderRelease($event,select1)">\n\n          <ion-option value="de-DE">Deutsch</ion-option>\n\n          <ion-option value="en-GB">English</ion-option>\n\n          <ion-option value="el-GR">Ελληνικά</ion-option>\n\n          <ion-option value="fr-FR">Français</ion-option>\n\n          <ion-option value="it-IT">Italiano</ion-option>\n\n          <ion-option value="es-ES">Spanish</ion-option>\n\n          \n\n        </ion-select>\n\n     \n\n      </ion-row>\n\n    </div>\n\n   </ion-grid>\n\n\n\n</ion-content>\n\n\n\n'/*ion-inline-end:"C:\Users\laele\Documents\KomApp\toolbox\src\pages\langue\langue.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__services_param_service__["a" /* ParamService */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["g" /* LoadingController */]])
    ], LanguePage);
    return LanguePage;
}());

//# sourceMappingURL=langue.js.map

/***/ }),

/***/ 290:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PasswordPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_param_service__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_popup_service__ = __webpack_require__(35);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var PasswordPage = /** @class */ (function () {
    function PasswordPage(navCtrl, popup, param) {
        this.navCtrl = navCtrl;
        this.popup = popup;
        this.param = param;
        this.newpassword = "";
        this.warn = false;
    }
    PasswordPage.prototype.ngOnInit = function () {
    };
    PasswordPage.prototype.editpassword = function (ev) {
        ev.preventDefault();
        if (this.password === atob(this.param.robot.password)) {
            if (this.newpassword.length >= 2) {
                this.popup.showToastGreen(this.param.datatext.pswdsaved, 3000, "middle");
                this.param.robot.password = btoa(this.newpassword);
                this.param.updateRobot();
                this.password = "";
                this.newpassword = "";
                this.navCtrl.pop();
            }
        }
        else {
            this.warn = true;
        }
    };
    PasswordPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-password',template:/*ion-inline-start:"C:\Users\laele\Documents\KomApp\toolbox\src\pages\password\password.html"*/'<!-- password page html template -->\n\n<ion-header no-border>\n\n    <headpage pageName="password"></headpage>\n\n  </ion-header>\n\n  \n\n  \n\n  <ion-content padding >\n\n    <ion-grid class="main_ion_grid">\n\n        <div style="background-color: rgb(255, 255, 255); border-radius: 15px; height: 60%; width: 60%; ">\n\n    \n\n        <ion-row style="height: 25%;justify-content: center!important; align-items: center!important;">\n\n          <ion-item style="width: 90%;">\n\n          <ion-label style="font-size: x-large; min-width: 30% !important;\n\n          max-width: 30% !important;">{{param.datatext.currentpswd}}</ion-label>\n\n          <ion-input minlength=2 maxlength=15 [(ngModel)]="password" style="font-size: xx-large; color: rgb(26, 156, 195);"  required></ion-input>\n\n       </ion-item>\n\n          \n\n        </ion-row>\n\n        \n\n    \n\n        <ion-row style="height: 25%;justify-content: center!important; align-items: center!important;">\n\n          <ion-item style="width: 90%;">\n\n          <ion-label style="font-size: x-large; min-width: 30% !important;\n\n          max-width: 30% !important;">{{param.datatext.newpswd}}</ion-label>\n\n          <ion-input [(ngModel)]="newpassword" minlength=2 maxlength=15 style="font-size: xx-large; color: rgb(26, 156, 195);" required></ion-input>\n\n          </ion-item>\n\n          \n\n        </ion-row>\n\n       <ion-row style="height: 20%; justify-content: center!important; ">\n\n          <p *ngIf="warn" style="color: red; font-size: large;">** {{param.datatext.wrongpass}} **</p>\n\n        </ion-row>\n\n        <ion-row style="height: 25%;justify-content: center!important; align-items: center!important;">\n\n          <button [disabled]="2>this.newpassword.length" ion-button solid large class="btn_login" (mouseup)="editpassword($event)"> \n\n            <!-- the text of the button depends on the page you are on -->\n\n            <font class="font_menu_size" >{{param.datatext.save}}</font>\n\n          </button>\n\n     \n\n        </ion-row>\n\n        </div>\n\n       </ion-grid>\n\n  \n\n  </ion-content>\n\n  '/*ion-inline-end:"C:\Users\laele\Documents\KomApp\toolbox\src\pages\password\password.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_3__services_popup_service__["a" /* PopupService */], __WEBPACK_IMPORTED_MODULE_1__services_param_service__["a" /* ParamService */]])
    ], PasswordPage);
    return PasswordPage;
}());

//# sourceMappingURL=password.js.map

/***/ }),

/***/ 291:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(292);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(299);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 299:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(45);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(241);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__ = __webpack_require__(243);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__components_components_module__ = __webpack_require__(345);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__app_component__ = __webpack_require__(244);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_poi_poi__ = __webpack_require__(141);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__components_headpage_headpage__ = __webpack_require__(269);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__services_api_service__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__services_alert_service__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__angular_common_http__ = __webpack_require__(75);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__ionic_native_background_mode__ = __webpack_require__(504);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__pages_tuto_tuto__ = __webpack_require__(270);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__services_popup_service__ = __webpack_require__(35);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__services_param_service__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__services_speech_service__ = __webpack_require__(54);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__pages_param_param__ = __webpack_require__(287);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__pages_langue_langue__ = __webpack_require__(289);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__pages_qrcode_qrcode__ = __webpack_require__(266);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__pages_paramuv_paramuv__ = __webpack_require__(288);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__pages_first_first__ = __webpack_require__(262);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__pages_rounddisplay_rounddisplay__ = __webpack_require__(264);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__pages_roundcreation_roundcreation__ = __webpack_require__(142);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__pages_roundedit_roundedit__ = __webpack_require__(265);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__pages_password_password__ = __webpack_require__(290);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_26__pages_docking_docking__ = __webpack_require__(267);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_27__pages_lift_lift__ = __webpack_require__(268);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

























//import { File } from "@ionic-native/file";



var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_6__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_21__pages_first_first__["a" /* FirstPage */],
                __WEBPACK_IMPORTED_MODULE_7__pages_poi_poi__["a" /* PoiPage */],
                __WEBPACK_IMPORTED_MODULE_13__pages_tuto_tuto__["a" /* TutoPage */],
                __WEBPACK_IMPORTED_MODULE_18__pages_langue_langue__["a" /* LanguePage */],
                __WEBPACK_IMPORTED_MODULE_22__pages_rounddisplay_rounddisplay__["a" /* RoundDisplayPage */],
                __WEBPACK_IMPORTED_MODULE_23__pages_roundcreation_roundcreation__["a" /* RoundCreationPage */],
                __WEBPACK_IMPORTED_MODULE_24__pages_roundedit_roundedit__["a" /* RoundEditPage */],
                __WEBPACK_IMPORTED_MODULE_19__pages_qrcode_qrcode__["a" /* QRCodePage */],
                __WEBPACK_IMPORTED_MODULE_27__pages_lift_lift__["a" /* LiftPage */],
                __WEBPACK_IMPORTED_MODULE_25__pages_password_password__["a" /* PasswordPage */],
                __WEBPACK_IMPORTED_MODULE_20__pages_paramuv_paramuv__["a" /* ParamUVPage */],
                __WEBPACK_IMPORTED_MODULE_17__pages_param_param__["a" /* ParamPage */],
                __WEBPACK_IMPORTED_MODULE_26__pages_docking_docking__["a" /* DockingPage */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_5__components_components_module__["a" /* ComponentsModule */],
                __WEBPACK_IMPORTED_MODULE_11__angular_common_http__["b" /* HttpClientModule */],
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["f" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_6__app_component__["a" /* MyApp */], {}, {
                    links: []
                }),
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["d" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_6__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_21__pages_first_first__["a" /* FirstPage */],
                __WEBPACK_IMPORTED_MODULE_7__pages_poi_poi__["a" /* PoiPage */],
                __WEBPACK_IMPORTED_MODULE_13__pages_tuto_tuto__["a" /* TutoPage */],
                __WEBPACK_IMPORTED_MODULE_18__pages_langue_langue__["a" /* LanguePage */],
                __WEBPACK_IMPORTED_MODULE_22__pages_rounddisplay_rounddisplay__["a" /* RoundDisplayPage */],
                __WEBPACK_IMPORTED_MODULE_23__pages_roundcreation_roundcreation__["a" /* RoundCreationPage */],
                __WEBPACK_IMPORTED_MODULE_24__pages_roundedit_roundedit__["a" /* RoundEditPage */],
                __WEBPACK_IMPORTED_MODULE_19__pages_qrcode_qrcode__["a" /* QRCodePage */],
                __WEBPACK_IMPORTED_MODULE_27__pages_lift_lift__["a" /* LiftPage */],
                __WEBPACK_IMPORTED_MODULE_25__pages_password_password__["a" /* PasswordPage */],
                __WEBPACK_IMPORTED_MODULE_20__pages_paramuv_paramuv__["a" /* ParamUVPage */],
                __WEBPACK_IMPORTED_MODULE_17__pages_param_param__["a" /* ParamPage */],
                __WEBPACK_IMPORTED_MODULE_26__pages_docking_docking__["a" /* DockingPage */],
                __WEBPACK_IMPORTED_MODULE_8__components_headpage_headpage__["a" /* HeadpageComponent */],
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_12__ionic_native_background_mode__["a" /* BackgroundMode */],
                __WEBPACK_IMPORTED_MODULE_9__services_api_service__["a" /* ApiService */],
                __WEBPACK_IMPORTED_MODULE_10__services_alert_service__["a" /* AlertService */],
                __WEBPACK_IMPORTED_MODULE_14__services_popup_service__["a" /* PopupService */],
                __WEBPACK_IMPORTED_MODULE_15__services_param_service__["a" /* ParamService */],
                __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */],
                __WEBPACK_IMPORTED_MODULE_16__services_speech_service__["a" /* SpeechService */],
                //File,
                { provide: __WEBPACK_IMPORTED_MODULE_1__angular_core__["u" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["e" /* IonicErrorHandler */] },
                { provide: Window, useValue: window },
            ],
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 345:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ComponentsModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_component__ = __webpack_require__(244);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__headpage_headpage__ = __webpack_require__(269);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var ComponentsModule = /** @class */ (function () {
    function ComponentsModule() {
    }
    ComponentsModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [__WEBPACK_IMPORTED_MODULE_3__headpage_headpage__["a" /* HeadpageComponent */]],
            imports: [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_2__app_app_component__["a" /* MyApp */])],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_2__app_app_component__["a" /* MyApp */]
            ],
            exports: [__WEBPACK_IMPORTED_MODULE_3__headpage_headpage__["a" /* HeadpageComponent */]]
        })
    ], ComponentsModule);
    return ComponentsModule;
}());

//# sourceMappingURL=components.module.js.map

/***/ }),

/***/ 35:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PopupService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__alert_service__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__api_service__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__param_service__ = __webpack_require__(14);
// to send mail and sms alert
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var PopupService = /** @class */ (function () {
    function PopupService(app, alert, param, api, alertCtrl, toastCtrl) {
        this.app = app;
        this.alert = alert;
        this.param = param;
        this.api = api;
        this.alertCtrl = alertCtrl;
        this.toastCtrl = toastCtrl;
    }
    PopupService.prototype.onSomethingHappened1 = function (fn) {
        this.deleteMap = fn;
    };
    PopupService.prototype.onSomethingHappened2 = function (fn) {
        this.deletePOI = fn;
    };
    PopupService.prototype.onSomethingHappened3 = function (fn) {
        this.deleteRound = fn;
    };
    PopupService.prototype.onSomethingHappened4 = function (fn) {
        this.deleteqr = fn;
    };
    PopupService.prototype.onSomethingHappened5 = function (fn) {
        this.accessparam = fn;
    };
    PopupService.prototype.onSomethingHappened6 = function (fn) {
        this.removePOIdocking = fn;
    };
    PopupService.prototype.onSomethingHappened7 = function (fn) {
        this.configDocking = fn;
    };
    PopupService.prototype.ngOnInit = function () { };
    PopupService.prototype.blockedAlert = function () {
        this.alert_blocked = this.alertCtrl.create({
            title: this.param.datatext.blockedAlert_title,
            message: this.param.datatext.blockedAlert_message,
            cssClass: "alertstyle",
            enableBackdropDismiss: true,
            buttons: [
                {
                    text: this.param.datatext.btn_ok,
                    handler: function () {
                        console.log("Oui clicked");
                    },
                },
            ],
        });
        this.alert_blocked.present();
    };
    PopupService.prototype.startFailedAlert = function () {
        this.alert_blocked = this.alertCtrl.create({
            title: this.param.datatext.error,
            message: this.param.datatext.cantstart,
            cssClass: "alertstyle",
            enableBackdropDismiss: false,
            buttons: [
                {
                    text: this.param.datatext.btn_ok,
                    handler: function () {
                        console.log("Oui clicked");
                        window.close();
                    },
                },
            ],
        });
        this.alert_blocked.present();
    };
    PopupService.prototype.lostAlert = function () {
        this.alert_blocked = this.alertCtrl.create({
            title: this.param.datatext.lostAlert_title,
            message: this.param.datatext.lostAlert_message,
            cssClass: "alertstyle",
            enableBackdropDismiss: true,
            buttons: [
                {
                    text: this.param.datatext.btn_ok,
                    handler: function () {
                        console.log("Oui clicked");
                    },
                },
            ],
        });
        this.alert_blocked.present();
    };
    PopupService.prototype.errorlaunchAlert = function () {
        this.alert_blocked = this.alertCtrl.create({
            title: this.param.datatext.errorlaunchAlert_title,
            message: this.param.datatext.errorlaunchAlert_message,
            cssClass: "alertstyle",
            enableBackdropDismiss: true,
            buttons: [
                {
                    text: this.param.datatext.btn_ok,
                    handler: function () {
                        console.log("Oui clicked");
                    },
                },
            ],
        });
        this.alert_blocked.present();
    };
    PopupService.prototype.errorNavAlert = function () {
        this.alert_blocked = this.alertCtrl.create({
            title: this.param.datatext.errorNavAlert_title,
            message: this.param.datatext.errorNavAlert_message,
            cssClass: "alertstyle",
            enableBackdropDismiss: true,
            buttons: [
                {
                    text: this.param.datatext.btn_ok,
                    handler: function () {
                        console.log("Oui clicked");
                    },
                },
            ],
        });
        this.alert_blocked.present();
    };
    PopupService.prototype.robotmuststayondocking = function () {
        // pop up if the robot is in the docking when you start the round
        this.alert_blocked = this.alertCtrl.create({
            title: this.param.datatext.robotmuststayondocking_title,
            message: this.param.datatext.robotmuststayondocking_message,
            cssClass: "alertstyle",
            enableBackdropDismiss: true,
            buttons: [
                {
                    text: this.param.datatext.btn_ok,
                    handler: function () {
                        console.log("Oui clicked");
                    },
                },
            ],
        });
        this.alert_blocked.present();
    };
    PopupService.prototype.quitConfirm = function () {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: this.param.datatext.quitConfirm_title,
            cssClass: "alertstyle",
            enableBackdropDismiss: false,
            buttons: [
                {
                    text: this.param.datatext.btn_no,
                    role: "cancel",
                    handler: function () {
                        console.log("Non clicked");
                    },
                },
                {
                    text: this.param.datatext.btn_yes,
                    handler: function () {
                        console.log("Oui clicked");
                        _this.api.close_app = true;
                        _this.alert.appClosed(_this.api.mailAddInformationBasic());
                        //stop the round and quit the app
                        _this.api.abortNavHttp();
                        setTimeout(function () {
                            window.close();
                        }, 1000);
                    },
                },
            ],
        });
        alert.present();
    };
    PopupService.prototype.errorBlocked = function () {
        var alert = this.alertCtrl.create({
            title: this.param.datatext.errorBlocked_title,
            message: this.param.datatext.errorBlocked_message,
            cssClass: "alertstyle",
            enableBackdropDismiss: true,
            buttons: [
                {
                    text: this.param.datatext.btn_ok,
                    handler: function () {
                        console.log("OK clicked");
                    },
                },
            ],
        });
        alert.present();
    };
    PopupService.prototype.statusRedPresent = function () {
        var alert = this.alertCtrl.create({
            title: this.param.datatext.statusRedPresent_title,
            message: this.param.datatext.statusRedPresent_message,
            cssClass: "alertstyle",
            enableBackdropDismiss: true,
            buttons: [
                {
                    text: this.param.datatext.btn_ok,
                    handler: function () {
                        console.log("OK clicked");
                    },
                },
            ],
        });
        alert.present();
    };
    PopupService.prototype.statusGreenPresent = function () {
        var alert = this.alertCtrl.create({
            title: this.param.datatext.statusGreenPresent_title,
            message: this.param.datatext.statusGreenPresent_message,
            cssClass: "alertstyle",
            enableBackdropDismiss: true,
            buttons: [
                {
                    text: this.param.datatext.btn_ok,
                    handler: function () {
                        console.log("OK clicked");
                    },
                },
            ],
        });
        alert.present();
    };
    PopupService.prototype.DeleteMessage = function (type, id) {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: this.param.datatext.deletion,
            message: this.param.datatext.deleteMessage,
            cssClass: "alertstyle",
            enableBackdropDismiss: true,
            buttons: [
                {
                    text: this.param.datatext.btn_cancel,
                    role: "cancel",
                    handler: function () {
                        console.log("No clicked");
                    },
                },
                {
                    text: this.param.datatext.btn_yes,
                    handler: function () {
                        console.log("Yes clicked");
                        if (type === "Map") {
                            _this.deleteMap();
                        }
                        else if (type === "POI") {
                            _this.deletePOI(id);
                        }
                        else if (type === "Round") {
                            _this.deleteRound(id);
                        }
                        else {
                            _this.deleteqr(type, id);
                        }
                    },
                },
            ],
        });
        alert.present();
    };
    PopupService.prototype.askpswd = function () {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: this.param.datatext.password,
            message: this.param.datatext.enterPassword,
            cssClass: "alertstyle",
            enableBackdropDismiss: true,
            inputs: [
                {
                    name: "password",
                    placeholder: this.param.datatext.password,
                    type: "password",
                },
            ],
            buttons: [
                {
                    text: this.param.datatext.btn_cancel,
                    role: "cancel",
                    handler: function (data) {
                        console.log("Cancel clicked");
                    },
                },
                {
                    text: this.param.datatext.btn_save,
                    role: "backdrop",
                    handler: function (data) {
                        if (data.password === atob(_this.param.robot.password)) {
                            _this.accessparam();
                        }
                        else {
                            _this.wrongPassword();
                        }
                    },
                },
            ],
        });
        alert.present();
    };
    PopupService.prototype.wrongPassword = function () {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: this.param.datatext.password,
            message: this.param.datatext.wrongPassword,
            cssClass: "alertstyle_wrongpass",
            enableBackdropDismiss: true,
            inputs: [
                {
                    name: "password",
                    placeholder: this.param.datatext.password,
                    type: "password",
                },
            ],
            buttons: [
                {
                    text: this.param.datatext.btn_cancel,
                    role: "cancel",
                    handler: function (data) {
                        console.log("Cancel clicked");
                    },
                },
                {
                    text: this.param.datatext.btn_save,
                    role: "backdrop",
                    handler: function (data) {
                        if (data.password === atob(_this.param.robot.password)) {
                            _this.accessparam();
                        }
                        else {
                            _this.wrongPassword();
                        }
                    },
                },
            ],
        });
        alert.present();
    };
    PopupService.prototype.askRemoveDataDocking = function () {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: this.param.datatext.dataRemove,
            message: this.param.datatext.dataRemoveInfo,
            cssClass: "alertstyle",
            enableBackdropDismiss: true,
            buttons: [
                {
                    text: this.param.datatext.btn_cancel,
                    role: "cancel",
                    handler: function (data) {
                        console.log("Cancel clicked");
                    },
                },
                {
                    text: this.param.datatext.continue,
                    role: "backdrop",
                    handler: function (data) {
                        _this.removePOIdocking();
                    },
                },
            ],
        });
        alert.present();
    };
    PopupService.prototype.askConfigDocking = function (ev) {
        var _this = this;
        ev.preventDefault();
        var alert = this.alertCtrl.create({
            title: this.param.datatext.configDocking,
            message: this.param.datatext.configDockingConfirm,
            cssClass: "alertstyle",
            enableBackdropDismiss: true,
            buttons: [
                {
                    text: this.param.datatext.btn_cancel,
                    role: "cancel",
                    handler: function (data) {
                        console.log("Cancel clicked");
                    },
                },
                {
                    text: this.param.datatext.btn_yes,
                    role: "backdrop",
                    handler: function (data) {
                        _this.configDocking();
                    },
                },
            ],
        });
        alert.present();
    };
    PopupService.prototype.showToastGreen = function (msg, duration, position) {
        var toast = this.toastCtrl.create({
            message: msg,
            duration: duration,
            position: position,
            cssClass: "toastok",
        });
        toast.present();
    };
    PopupService.prototype.showToastRed = function (msg, duration, position) {
        var toast = this.toastCtrl.create({
            message: msg,
            duration: duration,
            position: position,
            cssClass: "toast",
        });
        toast.present();
    };
    PopupService.prototype.showToastBlue = function (m, n) {
        var toast = this.toastCtrl.create({
            message: m,
            duration: n,
            position: "middle",
            cssClass: "toastam",
        });
        toast.present();
    };
    PopupService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* App */],
            __WEBPACK_IMPORTED_MODULE_2__alert_service__["a" /* AlertService */],
            __WEBPACK_IMPORTED_MODULE_4__param_service__["a" /* ParamService */],
            __WEBPACK_IMPORTED_MODULE_3__api_service__["a" /* ApiService */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* ToastController */]])
    ], PopupService);
    return PopupService;
}());

//# sourceMappingURL=popup.service.js.map

/***/ }),

/***/ 446:
/***/ (function(module, exports) {

module.exports = {"quit":"QUITTER","battery":"BATTERIE","return":"RETOUR","tutorial":"TUTORIEL","param":"PARAMÈTRES","toolbox":"BOÎTE À OUTILS","receivesms":"RECEVOIR DES ALERTES SMS","receivemail":"RECEVOIR DES ALERTES PAR MAIL","changelangage":"CHANGER LA LANGUE","godocking":"EN ROUTE VERS LA STATION DE CHARGE","round":"TOURNÉE DE DIVERTISSEMENT","patrol":"Patrouille","patrolInProgress":"PATROUILLE EN COURS","roundInProgress":"TOURNÉE DE DIVERTISSEMENT EN COURS","moving":"DÉPLACEMENT EN COURS VERS :","charging_remaining":"ROBOT EN CHARGE - BATTERIE: ","walkInProgress":"MARCHEZ A VOTRE RYTHME SANS POUSSER","notpush":"INSTALLEZ VOUS COMME SUR LA PHOTO ET APPUYEZ SUR «ALLONS-Y»","mails":"Mails","sms":"SMS","langage":"Langue","distance_covered":"Distance parcourue","goto":"Se rendre au :","inProgress":"EN COURS","post":"Poste","sentinel":"Sentinelle","morningRound":"Tournée du matin","eveningRound":"Tournée du soir","btn_go":"GO","btn_help":"Aide","btn_walk":"Allons-y !","btn_stop":"STOP","btn_charge":"CHARGER","btn_cancel":"Annuler","btn_ok":"OK","btn_yes":"OUI","btn_no":"NON","numexist":"Ce numéro existe déjà !","deletenum":"Veuillez supprimer un numéro d'abord","numadd":"Numéro ajouté","numincorrect":"Numéro invalide","newnum":"Nouveau numéro","mailexist":"Cette adresse existe déjà ! ","deletemail":"Veuillez supprimer une adresse d'abord","mailadd":"Adresse mail ajoutée !","mailincorrect":"Adresse mail invalide","newmail":"Nouvelle adresse","maps":"Cartes","roundlist":"Liste des tournées","roundcreation":"CRÉATION DE TOURNÉES","headBand_rounds":"LES TOURNÉES","rounds_edit":"ÉDITION DE TOURNÉE","wait":"Veuillez patienter ...","error":"Erreur","cantstart":"Impossible de démarrer l'application. Vérifiez que le robot est allumé correctement.","nointernet":"Pas d'internet","robotConnectedToI":"Le robot est connecté à internet","editpswd":"CHANGER LE MOT DE PASSE","currentpswd":"Mot de passe actuel :","newpswd":"Nouveau mot de passe :","save":"Sauvegarder","success":"C'est Fait !","pswdsaved":"Mot de passe sauvegardé","langage1":"Changer la langue","editpswd1":"Changer le mot de passe","wrongpass":"Mauvais mot de passe","gabarit":"Profil anticollision","smsSOS":"Quelqu'un a appuyé sur le bouton SOS du robot","mailSOS_suj":"SOS","mailSOS_body":"<br> Une personne a besoin d'aide","mailBattery_suj":"Batterie Faible","mailBattery_body":"<br> Le robot a besoin d'etre chargé. Veuillez le mettre sur sa station de charge.","mailBlocked_suj":"Robot bloqué","mailBlocked_body":"<br> Le robot est bloqué <br>","mailFall_body":"Le robot détecte une personne à terre <br>","mailFall_suj":"CHUTE détectée","mailPerson_suj":"Personne détectée","mailPerson_body":"Le robot a détecté une personne <br>","presentAlert_title":"Robot en charge","presentAlert_message":"Attention je vais reculer !","presentConfirm_title":"Ronde suspendue","presentConfirm_message":"Reprendre la ronde ?","FallConfirm_title":"Chute détectée !","FallConfirm_message":"Reprendre la patrouille ?","RemoteConfirm_title":"Ronde arrêtée à distance","RemoteConfirm_message":"Reprendre la ronde ?","blockedAlert_title":"Robot bloqué !","blockedAlert_message":"Veuillez enlever l'obstacle ou me déplacer","goDockingConfirm_title":"Batterie: ","goDockingConfirm_message":"Aller en station de charge ?","lowBattery_title":"Batterie faible","lowBattery_message":"Aller en station de charge ?","errorlaunchAlert_title":"Une erreur est survenue","errorlaunchAlert_message":"Veuillez rententer de lancer la ronde","robotmuststayondocking_title":"Batterie faible","robotmuststayondocking_message":"Le robot doit rester sur la docking","errorNavAlert_title":"Une erreur est survenue","errorNavAlert_message":"Veuillez appeler le support technique si le problème persiste","lostAlert_title":"Robot perdu !","lostAlert_message":"Veuillez appeler le support technique","quitConfirm_title":"Quitter l'application ?","errorBlocked_title":"Robot bloqué","errorBlocked_message":"Veuillez vérifier que le robot est bien dégagé","statusRedPresent_title":"Erreur","statusRedPresent_message":"Veuillez me redémarrer OU appeler mon fabricant si le problème persiste","statusGreenPresent_title":"Statut vert","statusGreenPresent_message":"Le robot est en bonne santé !","leaveDockingConfirm_title":"Batterie: ","leaveDockingConfirm_message":"Sortir de la station de charge ?","leaveDockingAlert_title":"Robot en charge","leaveDockingAlert_message":"Veuillez me retirer de la station de charge","askHelp_title":"Aide demandée","askHelp_message":"Quelqu'un va venir vous aider","uvsettings":"Paramètres UV","evacuationTime":"Temps d'évacuation (s) :","preheatingTime":"Temps de préchauffage (s) : ","lampThreshold":"Seuil horaire des lampes (h) : ","uvThreshold":"Seuil UV (W/m²) : ","batteryThreshold":"Seuil batterie lampes (%) : ","personDetection":"Detection de personne : ","btn_save":"Sauver","saveDone":"Sauvegarde terminée","POI":"Les POI :","addPOI":"Ajouter un POI","editPOI":"Modifier un POI","addMap":"Ajouter une carte","editMap":"Modifier la carte","goToPOI":"Aller au POI :","reloc":"RELOC","confirm":"VALIDER","delete":"Supprimer","edit":"Éditer","POIname":"Nom du POI :","indicationAddPOI":"Positionnez et orientez le robot à l’endroit souhaité puis cliquez sur “Sauver”.","mapName":"Nom de la carte :","extensionIndication":"Le format est incorrect. Un .png est attendu.","colorIndication":"Erreur. La carte ne respecte pas le code couleur attendu.","relocate":"Repositionner","speedOption":"Vitesse (%) : ","speed":"Vitesse : ","directionOption":"Sens : ","frontOption":"Avant","backOption":"Arrière","avoidanceOption":"Évitement : ","cartography":"CARTOGRAPHIE","qrcodes":"QR CODES","rounds":"TOURNÉES","qrcodes_list":"QR codes :","qrcodeIndication":"Placez le robot en face du QR code (~ 50 cm).","qrcodeIndication1":"Puis cliquez sur 'Scanner'.","qrcodeScan":"Scanner","qrcodeDetected1":"QR code","qrcodeDetected2":"détecté","tieRound":"Lier à une tournée","round_qrcodesPage":"Tournée : ","qrcodeUsed1":"Le QR code '","qrcodeUsed2":"' est deja utilisé","name":"Nom :","map":"Carte","addPOIfunction":"Ajout de POI","POIselected":"POI :","breakOption":"Pause (s) : ","break":"Pause : ","options":"Options : ","option":"Option : ","none":"Aucune","lampon":"Lampes allumées","lampoff":"Lampes éteintes","lampoff1":"Sans détection","add":"+","POI_list":"Liste des POI : ","rounds_list":"Les tournées :","copy":"Copier","POI_edit":"Tournée :","deletion":"Suppression","deleteMessage":"Êtes-vous sûr de vouloir supprimer cet élément ?","map_selection":"Choisir une carte","nofile":"Aucun fichier","map_loaded":"Carte chargée.","configDocking":"Configuration de la docking","continue":"Continuer","configDockingInfo1":"Veuillez éteindre la station de charge lors de sa configuration.","configDockingInfo2":"Positionnez le robot sur la docking puis cliquez sur “Sauver”.","dataRemove":"Écrasement des données","dataRemoveInfo":"Les anciennes données vont être écrasées afin de reconfigurer la docking","configDockingConfirm":"Êtes-vous sûr de vouloir configurer la docking à cet endroit ?","password":"Mot de passe","enterPassword":"Entrez le mot de passe pour sauvegarder les modifications.","wrongPassword":"Mot de passe incorrect. Veuillez réessayer.","URL_toolbox":"assets/pdf/tuto_FR.pdf","manualreloc":"Reloc manuelle","qrcode":"Choisir un QR code","unknownqrcode":"QR code non reconnu","relocalizing":"Relocalisation en cours ...","alertReloc":"Prière de ne pas déplacer le robot pendant la relocalisation","localizationDone":"Robot localisé sur map : ","noqrdetected":"Aucun QR code détecté","admin":"Mode administrateur","cabinet_open":"Armoire ouverte"}

/***/ }),

/***/ 447:
/***/ (function(module, exports) {

module.exports = {"quit":"LEAVE","battery":"BATTERY","return":"RETURN","tutorial":"TUTORIAL","param":"SETTINGS","toolbox":"TOOLBOX","receivesms":"RECEIVE ALERTS BY SMS","receivemail":"RECEIVE ALERTS BY EMAIL","changelangage":"CHANGE THE LANGUAGE","godocking":"ON THE WAY TO THE CHARGING STATION","round":"ENTERTAINMENT TOUR","patrol":"Patrol","patrolInProgress":"PATROLLING","roundInProgress":"ENTERTAINMENT TOUR IN PROGRESS","moving":"TRAVELING IN PROGRESS TO :","charging_remaining":"ROBOT IN CHARGE - BATTERY: ","walkInProgress":"WALK AT YOUR OWN PACE WITHOUT PUSHING","notpush":"INSTALL YOURSELF AS IN THE PHOTO AND PRESS «LET'S GO»","mails":"Emails","sms":"SMS","langage":"Language","distance_covered":"Distance travelled","goto":"Go to :","inProgress":"IN PROGRESS","post":"Position","sentinel":"Sentry","morningRound":"Morning tour","eveningRound":"Evening tour","btn_go":"GO","btn_help":"Help","btn_walk":"Let's go !","btn_stop":"STOP","btn_charge":"LOAD","btn_cancel":"Cancel","btn_ok":"OK","btn_yes":"YES","btn_no":"NO","numexist":"This phone number already exists !","deletenum":"Please delete a phone number first","numadd":"Phone number added","numincorrect":"Invalid phone number","newnum":"New phone number","mailexist":"This address already exists ! ","deletemail":"Please delete an address first","mailadd":"Email address added!","mailincorrect":"Invalid email address","newmail":"New address","maps":"Maps","roundlist":"Rounds list","roundcreation":"ROUNDS CREATION","headBand_rounds":"ROUNDS","rounds_edit":"ROUND EDITING","wait":"Please wait ...","error":"Error","cantstart":"Application launching failed. Check that the robot is correctly switched on.","nointernet":"No internet access","robotConnectedToI":"The robot is connected to internet","editpswd":"EDIT PASSWORD","currentpswd":"Current password :","newpswd":"New password :","save":"Save","success":"Success","pswdsaved":"Password saved","langage1":"Langage","editpswd1":"Edit password","wrongpass":"Wrong password","gabarit":"Anti-collision profile","smsSOS":"Someone pressed the robot's SOS button","mailSOS_suj":"SOS","mailSOS_body":"<br> A person needs help","mailBattery_suj":"Low Battery","mailBattery_body":"<br> The robot needs to be charged. Please put it on its charging station.","mailBlocked_suj":"Robot blocked","mailBlocked_body":"<br> The robot is blocked <br>","mailFall_body":"The robot detects a person ashore <br>","mailFall_suj":"FALL detected","mailPerson_suj":"Person detected","mailPerson_body":"The robot detected a person <br>","presentAlert_title":"Robot in charge","presentAlert_message":"I'm going to back up !","presentConfirm_title":"Round Suspended","presentConfirm_message":"Continue the round ?","FallConfirm_title":"Fall detected !","FallConfirm_message":"Continue the patrol ?","RemoteConfirm_title":"Remote stop round","RemoteConfirm_message":"Continue the round ?","blockedAlert_title":"Robot blocked !","blockedAlert_message":"Please remove the obstacle or move me","goDockingConfirm_title":"Battery: ","goDockingConfirm_message":"Go to the charging station ?","lowBattery_title":"Low Battery","lowBattery_message":"Go to the charging station ?","errorlaunchAlert_title":"A mistake has occured","errorlaunchAlert_message":"Please try again to start the round","robotmuststayondocking_title":"Low Battery","robotmuststayondocking_message":"The robot must stay on the docking","errorNavAlert_title":"A mistake has occured","errorNavAlert_message":"Please call technical support if the problem persists","lostAlert_title":"Robot lost !","lostAlert_message":"Please call technical support","quitConfirm_title":"Leave the application ?","errorBlocked_title":"Robot blocked","errorBlocked_message":"Please check that the robot is clear","statusRedPresent_title":"Error","statusRedPresent_message":"Please restart me OR call my manufacturer if the problem persists","statusGreenPresent_title":"Green status","statusGreenPresent_message":"The robot is healthy !","leaveDockingConfirm_title":"Battery: ","leaveDockingConfirm_message":"Get out of the charging station ?","leaveDockingAlert_title":"Robot in charge","leaveDockingAlert_message":"Please, remove me from the charging station","askHelp_title":"Help requested","askHelp_message":"Someone is going to come and help you","uvsettings":"UV settings","evacuationTime":"Evacuation time (s) :","preheatingTime":"Preheating time (s) : ","lampThreshold":"Lamps hourly threshold (h) : ","uvThreshold":"UV threshold (W/m²) : ","batteryThreshold":"Lamps battery threshold (%) : ","personDetection":"Person detection : ","btn_save":"Save","saveDone":"Save done","POI":"POI list :","addPOI":"Add a POI","editPOI":"Edit a POI","addMap":"Add a map","editMap":"Edit a map","goToPOI":"Go to the POI :","reloc":"RELOC","confirm":"VALIDATE","delete":"Delete","edit":"Edit","POIname":"POI name :","indicationAddPOI":"Position and orient the robot at the desired place then click on “Save”.","mapName":"Map name :","extensionIndication":"Incorrect file extension. A .png is required.","colorIndication":"Error. The map does not respect the color code required.","relocate":"Reposition","speedOption":"Speed (%) : ","speed":"Speed : ","directionOption":"Direction : ","frontOption":"Forward","backOption":"Reverse","avoidanceOption":"Avoidance : ","cartography":"CARTOGRAPHY","qrcodes":"QR CODES","rounds":"ROUNDS","qrcodes_list":"QR codes :","qrcodeIndication":"Place the robot in front of the QR code (~ 50 cm).","qrcodeIndication1":"Then click on 'Scan'.","qrcodeScan":"Scan","qrcodeDetected1":"QR code","qrcodeDetected2":"detected","tieRound":"Tie to a round","round_qrcodesPage":"Round : ","qrcodeUsed1":"QR code '","qrcodeUsed2":"' is already used","name":"Name :","map":"Map","addPOIfunction":"POI addition","POIselected":"POI :","breakOption":"Wait (s) : ","break":"Wait : ","options":"Options : ","option":"Option : ","none":"None","lampon":"Lamps switched on","lampoff":"Lamps switched off","lampoff1":"Without detection","add":"+","POI_list":"POI list : ","rounds_list":"Rounds :","copy":"Copy","POI_edit":"Round :","deletion":"Deletion","deleteMessage":"Are you sure you want to remove this element ?","map_selection":"Select a map","nofile":"No file","map_loaded":"Map loaded.","configDocking":"Docking setup","continue":"Continue","configDockingInfo1":"Please, switch off the charging station when setting it up.","configDockingInfo2":"Place the robot on the charging station then click on “Save”.","dataRemove":"Data overwrite","dataRemoveInfo":"Old data will be overwritten in order to reconfigure the charging station","configDockingConfirm":"Are you sure you want to set up the charging station there ?","password":"Password","enterPassword":"Enter the password to save changes.","wrongPassword":"Wrong password. Please try again with another one.","URL_toolbox":"assets/pdf/tuto_EN.pdf","manualreloc":"Manual reloc","qrcode":"Select a QR code","unknownqrcode":"Unknown QR code","relocalizing":"Relocalizing ...","alertReloc":"Please do not move the robot during the relocalization","localizationDone":"Robot located on map : ","noqrdetected":"No QR detected","admin":"Admin Mode","cabinet_open":"Cabinet is open"}

/***/ }),

/***/ 448:
/***/ (function(module, exports) {

module.exports = {"quit":"SALIR","battery":"BATERÍA","return":"REGRESAR","tutorial":"TUTORIAL","param":"PARÁMETROS","toolbox":"CAJA DE HERRAMIENTAS","receivesms":"RECIBIR ALERTAS POR SMS","receivemail":"RECIBIR ALERTAS POR CORREO ELECTRÓNICO","changelangage":"CAMBIAR EL IDIOMA","godocking":"EN CAMINO A LA ESTACÍON DE CARGA","round":"RECORRIDO DE ENTRETENIMIENTO","patrol":"Patrulla","patrolInProgress":"PATRULLANDO","roundInProgress":"RECORRIDO DE ENTRETENIMIENTO EN CURSO","moving":"EN MOVIMIENTO PARA :","charging_remaining":"ROBOT EN CARGA - BATERÍA: ","walkInProgress":"CAMINA A TU PROPIO RITMO SIN EMPUJAR","notpush":"COLÓQUESE COMO EN LA FOTO Y PRESIONE «EMPEZAR»","mails":"Correos electrónicos","sms":"SMS","langage":"Idioma","distance_covered":"Distancia recorrida","goto":"Ir a :","inProgress":"EN CURSO","post":"Posición","sentinel":"Centinela","morningRound":"Ronda de la mañana","eveningRound":"Ronda de la noche","btn_go":"GO","btn_help":"Ayuda","btn_walk":"Empezar","btn_stop":"ALTO","btn_charge":"CARGAR","btn_cancel":"Cancelar","btn_ok":"OK","btn_yes":"SÍ","btn_no":"NO","numexist":"¡ Este número ya existe !","deletenum":"Por favor, borre un número primero","numadd":"Número añadido","numincorrect":"Número inválido","newnum":"Nuevo número","mailexist":"¡ Esta dirección de correo electrónico ya existe ! ","deletemail":"Por favor, elimine primero una dirección de correo electrónico","mailadd":"¡ Dirección de correo electrónico añadida !","mailincorrect":"Dirección de correo electrónico inválida","newmail":"Nueva dirección de correo electrónico","maps":"Mapas","roundlist":"Lista de las rondas","roundcreation":"CREACIÓN DE RONDAS","headBand_rounds":"LAS RONDAS","rounds_edit":"EDICIÓN DE RONDAS","wait":"Espere un momento ...","error":"Error","cantstart":"No se puede iniciar la aplicación. Compruebe si el robot está bien encendido.","nointernet":"Sin acceso a internet","robotConnectedToI":"El robot está conectado a internet","editpswd":"EDITAR CONTRASEÑA","currentpswd":"Contraseña actual :","newpswd":"Nueva contraseña :","save":"Guardar","success":"Está hecho !","pswdsaved":"Contraseña guardada","langage1":"Idioma","editpswd1":"Editar contraseña","wrongpass":"Contraseña incorrecta","gabarit":"Perfil anticolisión","smsSOS":"Alguien ha pulsado el botón SOS del robot","mailSOS_suj":"SOS","mailSOS_body":"<br> Una persona necesita ayuda","mailBattery_suj":"Batería Baja","mailBattery_body":"<br> Hay que cargar al robot. Por favor, pongalo en su estación de carga.","mailBlocked_suj":"Robot bloqueado","mailBlocked_body":"<br> El robot está bloqueado <br>","mailFall_body":"El robot detectó a una persona en el suelo <br>","mailFall_suj":"CAÍDA detectada","mailPerson_suj":"Persona detectada","mailPerson_body":"El robot detectó a una persona <br>","presentAlert_title":"Robot en carga","presentAlert_message":"¡ Voy a ir hacia atrás !","presentConfirm_title":"Ronda suspendida","presentConfirm_message":"¿ Volver a la ronda ?","FallConfirm_title":"¡ Caída detectada !","FallConfirm_message":"¿ Volver a la patrulla ?","RemoteConfirm_title":"Ronda parada a distancia","RemoteConfirm_message":"¿ Volver a la ronda ?","blockedAlert_title":"¡ Robot bloqueado !","blockedAlert_message":"Por favor, quite el obstáculo o muevame","goDockingConfirm_title":"Batería: ","goDockingConfirm_message":"¿ Ir a la estación de carga ?","lowBattery_title":"Batería Baja","lowBattery_message":"¿ Ir a la estación de carga ?","errorlaunchAlert_title":"Se ha producido un error","errorlaunchAlert_message":"Intente volver a empezar la ronda, por favor","robotmuststayondocking_title":"Batería Baja","robotmuststayondocking_message":"El robot debe quedarse en el atraque","errorNavAlert_title":"Se ha producido un error","errorNavAlert_message":"Por favor, llame al soporte técnico si el problema persiste","lostAlert_title":"¡ Robot perdido !","lostAlert_message":"Por favor, llame al soporte técnico","quitConfirm_title":"¿ Salir de la aplicación ?","errorBlocked_title":"Robot bloqueado","errorBlocked_message":"Compruebe si el robot está desbloqueado","statusRedPresent_title":"Error","statusRedPresent_message":"Por favor, reinicie me o llame a mi fabricante si el problema persiste","statusGreenPresent_title":"Situación verde","statusGreenPresent_message":"¡ El robot está sano !","leaveDockingConfirm_title":"Batería: ","leaveDockingConfirm_message":"¿ Salir de la estación de carga ?","leaveDockingAlert_title":"Robot cargando","leaveDockingAlert_message":"Por favor, retireme de la estación de carga","askHelp_title":"Ayuda solicitada","askHelp_message":"Alguien vendrá a ayudarte","uvsettings":"Parámetros UV","evacuationTime":"Duración de evacuación (s) :","preheatingTime":"Duración del precalentamiento (s) : ","lampThreshold":"Umbral horario de las lámparas (h) : ","uvThreshold":"Umbral UV (W/m²) : ","batteryThreshold":"Umbral batería lámparas (%) : ","personDetection":"Detección de persona : ","btn_save":"Guardar","saveDone":"Cambios memorizados","POI":"Los POI :","addPOI":"Añadir un POI","editPOI":"Modificar un POI","addMap":"Añadir una mapa","editMap":"Modificar la mapa","goToPOI":"Ir al POI :","reloc":"RELOC","confirm":"VALIDAR","delete":"Eliminar","edit":"Editar","POIname":"Nombre del POI :","indicationAddPOI":"Coloque e oriente el robot al sitio que quiere y haga clic en “Guardar”.","mapName":"Nombre de la mapa :","extensionIndication":"El formato es incorrecto. Un fichero .png es esperado.","colorIndication":"Error. La mapa no respeta el código de colores esperado.","relocate":"Volver a poner en su sitio","speedOption":"Velocidad (%) : ","speed":"Velocidad : ","directionOption":"Sentido : ","frontOption":"Hacia adelante","backOption":"Hacia atrás","avoidanceOption":"Evitación : ","cartography":"CARTOGRAFÍA","qrcodes":"Códigos QR","rounds":"RONDAS","qrcodes_list":"Los códigos QR :","qrcodeIndication":"Coloque el robot enfrente del código QR (~ 50 cm).","qrcodeIndication1":"Depués haga clic en “Escanear”.","qrcodeScan":"Escanear","qrcodeDetected1":"Código QR","qrcodeDetected2":"detectado","tieRound":"Enlace a una ronda","round_qrcodesPage":"Ronda : ","qrcodeUsed1":"El código QR '","qrcodeUsed2":"' ya está en uso","name":"Nombre :","map":"Mapa","addPOIfunction":"Añadidura de POI","POIselected":"POI :","breakOption":"Pausa (s) : ","break":"Pausa : ","options":"Opciones : ","option":"Opción : ","none":"Ninguna","lampon":"Lámparas encendidas","lampoff":"Lámparas apagadas","lampoff1":"Sin detección","add":"+","POI_list":"Lista de los POI : ","rounds_list":"Las rondas :","copy":"Copiar","POI_edit":"Ronda :","deletion":"Supresión","deleteMessage":"¿ Está seguro de que desea eliminar este elemento ?","map_selection":"Elegir una mapa","nofile":"Ningún archivo","map_loaded":"Mapa cargada.","configDocking":"Configuración de la docking","continue":"Continuar","configDockingInfo1":"Por favor, apague la estación de carga durante su configuración.","configDockingInfo2":"Coloque el robot encima de la docking y haga clic en “Guardar”.","dataRemove":"Sobrescritura de datos","dataRemoveInfo":"Los datos antiguos se sobrescribirán para reconfigurar la docking","configDockingConfirm":"¿ Está seguro de que quiere configurar la docking en este sitio ?","password":"Contraseña","enterPassword":"Introduzca la contraseña para guardar los cambios.","wrongPassword":"Contraseña incorrecta. Por favor, intente de nuevo con otra contraseña.","URL_toolbox":"assets/pdf/tuto_EN.pdf","manualreloc":"Manual reloc","qrcode":"Elegir un código QR","unknownqrcode":"Código QR no reconocido","relocalizing":"Relocalizando ...","alertReloc":"Por favor, no mueva el robot durante la relocalización","localizationDone":"Robot localizado en el mapa : ","noqrdetected":"Ningún QR detectado","admin":"Modo de administrador","cabinet_open":"Armario abierto"}

/***/ }),

/***/ 449:
/***/ (function(module, exports) {

module.exports = {"quit":"BEENDEN","battery":"BATTERIE","return":"ZURÜCK","tutorial":"ANLEITUNG","param":"EINSTELLUNGEN","toolbox":"FUNKTIONEN","receivesms":"BENACHRICHTIGUNG ALS SMS ERHALTEN","receivemail":"BENACHRICHTIGUNG ALS E-MAIL ERHALTEN","changelangage":"SPRACHE ÄNDERN","godocking":"AUF DEM WEG ZUR LADESTATION","round":"ENTERTAINMENT RUNDE","patrol":"Kontrollfahrt","patrolInProgress":"KONTROLLFAHRT WIRD DURCHGEFÜHRT","roundInProgress":"ENTERTAINMENT RUNDE  WIRD DURCHGEFÜHRT","moving":"AUF DEM WEG ZU :","charging_remaining":"ROBOTER LÄDT AUF - BATTERIE: ","walkInProgress":"GEHE IN DEINEM EIGENEN TEMPO OHNE ANSCHIEBEN","notpush":"STELLE DICH AUF, WIE AUF DEM FOTO ZU SEHEN, UND DRÜCKE «LOS GEHT'S»","mails":"E-Mails","sms":"SMS","langage":"Sprache","distance_covered":"Zurückgelegte Strecke","goto":"Gehe zu :","inProgress":"WIRD DURCHGEFÜHRT","post":"Position","sentinel":"Überwachung","morningRound":"Morgen-Runde","eveningRound":"Abend-Runde","btn_go":"LOS","btn_help":"Hilfe","btn_walk":"Los geht's!","btn_stop":"STOP","btn_charge":"LADEN","btn_cancel":"Abbrechen","btn_ok":"OK","btn_yes":"JA","btn_no":"NEIN","numexist":"Diese Telefonnummer existiert bereits!","deletenum":"Bitte lösche zuerst eine Telefonnummer","numadd":"Telefonnummer hinzugefügt","numincorrect":"Ungültige Telefonnummer","newnum":"Neue Telefonnummer","mailexist":"Diese Adresse  existiert bereits ! ","deletemail":"Bitte lösche zuerst eine Adresse","mailadd":"E-Mail Adresse hinzugefügt","mailincorrect":"Ungültige E-Mail Adresse","newmail":"Neue Adresse","maps":"Karten","roundlist":"Liste der Runden","roundcreation":"RUNDE ERSTELLEN","headBand_rounds":"RUNDEN","rounds_edit":"RUNDE BEARBEITEN","wait":"Bitte warten ...","error":"Fehler","cantstart":"Start der Anwendung ist fehlgeschlagen. Prüfe, ob der Roboter korrekt eingeschaltet ist.","nointernet":"Keine Internetverbindung","robotConnectedToI":"Der Roboter ist mit dem Internet verbunden","editpswd":"PASSWORT BEARBEITEN","currentpswd":"Aktuelles Passwort :","newpswd":"Neues Kennwort :","save":"Speichern","success":"Erfolg","pswdsaved":"Passwort gespeichert","langage1":"Sprache","editpswd1":"Passwort bearbeiten","wrongpass":"Falsches Passwort","gabarit":"Antikollisionsprofil","smsSOS":"Der SOS Knopf wurde gedrückt","mailSOS_suj":"SOS","mailSOS_body":"<br> Eine Person benötigt Hilfe","mailBattery_suj":"Batterie schwach","mailBattery_body":"<br> Der Roboter muss aufgeladen werden. Bitte stelle ihn auf die Ladestation.","mailBlocked_suj":"Roboter ist blockiert","mailBlocked_body":"<br> Der Roboter ist blockiert <br>","mailFall_body":"Der Roboter erkennt eine Person <br>","mailFall_suj":"STURZ erkannt","mailPerson_suj":"Person erkannt","mailPerson_body":"Der Roboter hat eine Person erkannt <br>","presentAlert_title":"Roboter lädt auf","presentAlert_message":"Ich gehe zurück !","presentConfirm_title":"Runde unterbrochen","presentConfirm_message":"Runde fortführen ?","FallConfirm_title":"Sturz erkannt !","FallConfirm_message":"Kontrollfahrt fortführen ?","RemoteConfirm_title":"Ferngesteuert Runde stoppen","RemoteConfirm_message":"Runde fortführen ?","blockedAlert_title":"Roboter ist blockiert !","blockedAlert_message":"Bitte entferne das Hindernis oder bewege mich","goDockingConfirm_title":"Batterie: ","goDockingConfirm_message":"Zur Ladestation gehen ?","lowBattery_title":"Batterie schwach","lowBattery_message":"Zur Ladestation gehen?","errorlaunchAlert_title":"Ein Fehler ist aufgetreten","errorlaunchAlert_message":"Bitte versuche erneut die Runde zu starten","robotmuststayondocking_title":"Batterie schwach","robotmuststayondocking_message":"Der Roboter muss auf der Ladestation bleiben","errorNavAlert_title":"Ein Fehler ist aufgetreten","errorNavAlert_message":"Bitte kontaktiere den technischen Support wenn das Problem weiterhin besteht","lostAlert_title":"Roboter hat sich verirrt !","lostAlert_message":"Bitte kontaktiere den technischen Support","quitConfirm_title":"Die Anwendung beenden ?","errorBlocked_title":"Roboter ist blockiert","errorBlocked_message":"Bitte prüfe, ob der Roboter frei ist","statusRedPresent_title":"Fehler","statusRedPresent_message":"Bitte starte mich neu ODER kontaktiere meinen Hersteller wenn das Problem weiterhin besteht","statusGreenPresent_title":"Grüner Status","statusGreenPresent_message":"Der Roboter ist in Ordnung !","leaveDockingConfirm_title":"Batterie: ","leaveDockingConfirm_message":"Die Ladestation verlassen ?","leaveDockingAlert_title":"Roboter lädt auf","leaveDockingAlert_message":"Bitte entferne mich von der Ladestation","askHelp_title":"Hilfe ist angefordert","askHelp_message":"Jemand kommt um zu helfen","uvsettings":"UV Einstellungen","evacuationTime":"Zeit zur Evakuation (s) :","preheatingTime":"Zeit zum Vorwärmen(s) : ","lampThreshold":"Lampen Grenzwert in Stunden (h) : ","uvThreshold":"UV Grenzwert (W/m²) : ","batteryThreshold":"LampenBatterie Grenzwert (%) : ","personDetection":"Personenerkennung : ","btn_save":"Sichern","saveDone":"Sichern erledigt","POI":"POI list :","addPOI":"Füge einen POI hinzu","editPOI":"Bearbeite einen POI","addMap":"Füge eine Karte hinzu","editMap":"Bearbeite eine Karte","goToPOI":"Gehe zum POI :","reloc":"RELOKALISIERUNG","confirm":"VALIDIERUNG","delete":"Löschen","edit":"Bearbeiten","POIname":"POI Name :","indicationAddPOI":"Positioniere und richte den Roboter am gewünschten Ort aus und drücke dann 'Sichern' .","mapName":"Name der Karte:","extensionIndication":"Datei Auswahl ist nicht korrekt. Ein .png wird benötigt.","colorIndication":"Fehler. Die Farbkodierung wird in der Karte nicht berücksichtigt.","relocate":"Reposition","speedOption":"Geschwindigkeit (%) : ","speed":"Geschwindigkeit : ","directionOption":"Richtung : ","frontOption":"Vorwärts","backOption":"Rückwärts","avoidanceOption":"Ausweichen : ","cartography":"KARTOGRAFIE","qrcodes":"QR CODES","rounds":"RUNDEN","qrcodes_list":"QR codes :","qrcodeIndication":"Stelle den Roboter vor den QR code (~ 50 cm).","qrcodeIndication1":"Drück dann auf 'Scan'.","qrcodeScan":"Scan","qrcodeDetected1":"QR code","qrcodeDetected2":"erkannt","tieRound":"Verknüpfe mit einer Runde","round_qrcodesPage":"Runde : ","qrcodeUsed1":"QR code '","qrcodeUsed2":"' wird schon verwendet","name":"Name :","map":"Karte","addPOIfunction":"POI hinzufügen","POIselected":"POI :","breakOption":"Warten (s) : ","break":"Warten : ","options":"Optionen : ","option":"Option : ","none":"Keine","lampon":"Lampen sind eingeschaltet","lampoff":"Lampen sind ausgeschaltet","lampoff1":"Ohne Erkennung","add":"+","POI_list":"POI Liste : ","rounds_list":"Runden :","copy":"Kopieren","POI_edit":"Runde :","deletion":"Löschen","deleteMessage":"Bist du sicher, dass du dieses Element entfernen möchtest ?","map_selection":"Karte auswählen","nofile":"Keine Datei","map_loaded":"Karte geladen.","configDocking":"Docking-Einrichtung","continue":"Weiter","configDockingInfo1":"Bitte schalten Sie die Ladestation aus, wenn Sie sie aufstellen.","configDockingInfo2":"Stellen Sie den Roboter auf die Ladestation und klicken Sie auf ”Speichern”.","dataRemove":"Daten überschreiben","dataRemoveInfo":"Alte Daten werden überschrieben, um die Ladestation neu zu konfigurieren","configDockingConfirm":"Sind Sie sicher, dass Sie die Ladestation dort aufstellen wollen?","password":"Passwort","enterPassword":"Trage das Passwort ein um Änderungen zu sichern.","wrongPassword":"Falsches Passwort. Bitte versuche es mit einem anderen.","URL_toolbox":"assets/pdf/tuto_EN.pdf","manualreloc":"Manuelle Relokalisierung","qrcode":"Wähle einen QR-Code","unknownqrcode":"Unbekannter QR-Code","relocalizing":"Relokalisierung ...","alertReloc":"Bitte bewege den Roboter nicht während der Relokalisierung","localizationDone":"Roboter ist auf der Karte lokalisiert: ","noqrdetected":"Kein QR erkannt","admin":"Admin-Modus","cabinet_open":"Kabinett ist geöffnet"}

/***/ }),

/***/ 450:
/***/ (function(module, exports) {

module.exports = {"quit":"ESCI","battery":"BATTERIA","return":"INDIETRO","tutorial":"TUTORIAL","param":"IMPOSTAZIONI","toolbox":"TOOLBOX","receivesms":"RICEVI AVVISI VIA SMS","receivemail":"RICEVI AVVISI VIA EMAIL","changelangage":"CAMBIA LA LINGUA","godocking":"STO ANDANDO ALLA STAZIONE DI RICARICA...","round":"TOUR DI INTRATTENIMENTO","patrol":"Ricognizione","patrolInProgress":"In ricognizione...","roundInProgress":"TOUR DI INTRATTENIMENTO IN CORSO","moving":"SPOSTAMENTO VERSO :","charging_remaining":"ROBOT IN CARICA - BATTERIA: ","walkInProgress":"CAMMINA AL TUO RITMO SENZA ACCELERARE","notpush":"METTITI COME NELLA FOTO E PRIMA 'VIA'","mails":"E-mails","sms":"SMS","langage":"Lingua","distance_covered":"Distanza percorsa","goto":"Vai a :","inProgress":"IN CORSO","post":"Posizione","sentinel":"Sentinella","morningRound":"Tour mattutino","eveningRound":"Tour serale","btn_go":"VAI","btn_help":"AIUTO","btn_walk":"Andiamo!","btn_stop":"STOP","btn_charge":"CARICAMENTO","btn_cancel":"ANNULLA","btn_ok":"OK","btn_yes":"YES","btn_no":"NO","numexist":"Questo numero di telefono è già esistente!","deletenum":"Perfavore cancella prima un numero di telefono","numadd":"Numero di telefono salvato","numincorrect":"Numero di telefono non valido","newnum":"Nuovo numero di telefono","mailexist":"Indirizzo già esistente! ","deletemail":"Perfavore cancella prima un indirizzo","mailadd":"Indirizzo e-mail aggiunto!","mailincorrect":"Indirizzo e-mail non valido","newmail":"Nuovo indirizzo","maps":"Mappe","roundlist":"Lista dei giri","roundcreation":"CREAZIONI DEI PERCORSI","headBand_rounds":"PERCORSI","rounds_edit":"MODIFICA DEI PERCORSI","wait":"Attendere ...","error":"Errore","cantstart":"Avvio dell'applicazione fallito. Controlla che il robot sia acceso...","nointernet":"Nessun accesso a internet","robotConnectedToI":"Il robot è connesso a internet","editpswd":"CAMBIA PASSWORD","currentpswd":"Password attuale :","newpswd":"Nuova password :","save":"Salva","success":"Successo","pswdsaved":"Password salvata","langage1":"Lingua","editpswd1":"Modifica password","wrongpass":"Password errata","gabarit":"Profilo anti-collisione","smsSOS":"Qualcuno ha premuto il bottone di SOS sul robot","mailSOS_suj":"SOS","mailSOS_body":"<br> Una persona ha bisogno di assistenza","mailBattery_suj":"Batteria scarica","mailBattery_body":"<br> Il robot ha bisogno di essere ricaricato. Collegarlo alla stazione di ricarica.","mailBlocked_suj":"Robot bloccato","mailBlocked_body":"<br> Il robot è bloccato <br>","mailFall_body":"Il robot ha rilevato una persona a terra <br>","mailFall_suj":"Caduta rilevata","mailPerson_suj":"Persona rilevata","mailPerson_body":"Persona rilevata <br>","presentAlert_title":"Robot in carica","presentAlert_message":"I'm going to back up !","presentConfirm_title":"Giro sospeso","presentConfirm_message":"Continuare il giro ?","FallConfirm_title":"Caduta rilevata !","FallConfirm_message":"Il robot ha rilevato una caduta","RemoteConfirm_title":"Giro fermato da remoto","RemoteConfirm_message":"Continuare il giro ?","blockedAlert_title":"Robot bloccato !","blockedAlert_message":"Perfavore rimuovere l'ostacolo oppure spostami","goDockingConfirm_title":"Batteria: ","goDockingConfirm_message":"Andare alla stazione di ricarica ?","lowBattery_title":"Batteria scarica","lowBattery_message":"Andare alla stazione di ricarica ?","errorlaunchAlert_title":"C'è stato un errore","errorlaunchAlert_message":"Prova a riavviare il giro","robotmuststayondocking_title":"Batteria scarica","robotmuststayondocking_message":"Il robot deve essere connesso alla docking","errorNavAlert_title":"C'è stato un errore","errorNavAlert_message":"Contattare il supporto tecnico se il problema persiste","lostAlert_title":"Robot perso !","lostAlert_message":"Contattare il supporto tecnico","quitConfirm_title":"Chiudere l'applicazione ?","errorBlocked_title":"Robot bloccato","errorBlocked_message":"Controllare che il robot sia pulito","statusRedPresent_title":"Errore","statusRedPresent_message":"Riavviami o chiama il produttore se il problema persiste","statusGreenPresent_title":"Verde","statusGreenPresent_message":"The robot è in salute !","leaveDockingConfirm_title":"Batteria: ","leaveDockingConfirm_message":"Allontanarsi dalla stazione di ricarica ?","leaveDockingAlert_title":"Robot in carica","leaveDockingAlert_message":"Perfavore, rimuovimi dalla stazione di ricarica","askHelp_title":"Richiesta di assistenza","askHelp_message":"Qualcuno arriverà ad aiutarti","uvsettings":"Impostazioni UV","evacuationTime":"Tempo di evacuazione (s) :","preheatingTime":"Tempo di preriscaldamento (s) : ","lampThreshold":"Soglia oraria delle lampade (h) : ","uvThreshold":"Soglia UV (W/m²) : ","batteryThreshold":"Soglia della batteria della lampada (%) : ","personDetection":"Rilevamento persone : ","btn_save":"Salva","saveDone":"Salva completato","POI":"Lista POI :","addPOI":"Aggiungi POI","editPOI":"Modifica POI","addMap":"Aggiungi mappa","editMap":"Modifica mappa","goToPOI":"Vai a un POI :","reloc":"RILOCALIZZA","confirm":"VALIDA","delete":"Cancella","edit":"Modifica","POIname":"Nome POI :","indicationAddPOI":"Posiziona ed orienta il robot nel punto desiderato e poi clicca su “Salva”.","mapName":"Nome mappa :","extensionIndication":"Estensione del file non riconosciuta. E' richiesto un .png.","colorIndication":"Errore. La mappa non rispetta i codici colori ammessi.","relocate":"Riposiziona","speedOption":"Velocità (%) : ","speed":"Velocità : ","directionOption":"Direzione : ","frontOption":"Avanti","backOption":"Indietro","avoidanceOption":"Evita : ","cartography":"CARTOGRAFIA","qrcodes":"CODICI QR","rounds":"GIRI","qrcodes_list":"Codici QR :","qrcodeIndication":"Posiziona il robot difronte al codice QR (~ 50 cm).","qrcodeIndication1":"Poi premi su 'Scansiona'.","qrcodeScan":"Scansiona","qrcodeDetected1":"Codice QR","qrcodeDetected2":"rilevato","tieRound":"Associa a un percorso","round_qrcodesPage":"Percorso : ","qrcodeUsed1":"Codice QR '","qrcodeUsed2":"' già in uso","name":"Nome :","map":"Mappa","addPOIfunction":"Aggiungi POI","POIselected":"POI :","breakOption":"Attesa (s) : ","break":"Attesa : ","options":"Opzioni : ","option":"Opzioni : ","none":"Nessuno","lampon":"Lampada accesa","lampoff":"Lampada spenta","lampoff1":"Senza rilevamento","add":"+","POI_list":"Lista POI : ","rounds_list":"Percorsi :","copy":"Copia","POI_edit":"Percorso :","deletion":"Cancellazione","deleteMessage":"Se sicuro di voler rimuovere questo elemento ?","map_selection":"Seleziona mappa","nofile":"Nessun file","map_loaded":"Mappa caricata.","configDocking":"Impostazioni della docking","continue":"Continua","configDockingInfo1":"Perfavore, spegni la stazione di ricarica mentre la imposti.","configDockingInfo2":"Posiziona il robot sulla stazione di ricarica e premi “Salva”.","dataRemove":"Sovrascrivi i dati","dataRemoveInfo":"I vecchi dati verrano sovrascritti per impostare la nuova stazione di ricarica","configDockingConfirm":"Sei sicuro di voler impostare la stazione di ricarica qui ?","password":"Password","enterPassword":"Inserisci la password per salvare le modifiche.","wrongPassword":"Password errata. Perfavore riprova.","URL_toolbox":"assets/pdf/tuto_EN.pdf","manualreloc":"Riposizionamento manuale","qrcode":"Seleziona un codice QR","unknownqrcode":"Codice QR sconosciuto","relocalizing":"Rilocalizzazione ...","alertReloc":"Non muovere il robot durante la rilocalizzazione","localizationDone":"Robot trovato sulla mappa : ","noqrdetected":"Nessun codice QR rilevato","admin":"Modalità amministratore","cabinet_open":"Involucro aperto"}

/***/ }),

/***/ 451:
/***/ (function(module, exports) {

module.exports = {"quit":"ΕΞΟΔΟΣ","battery":"ΜΠΑΤΑΡΙΑ","return":"ΕΠΙΣΤΡΟΦΗ","tutorial":"TUTORIAL","param":"ΡΥΘΜΙΣΕΙΣ","toolbox":"ΕΡΓΑΛΕΙΟΘΗΚΗ","receivesms":"ΠΑΡΑΛΑΒΗ ΕΙΔΟΠΟΙΗΣΕΩΝ ΜΕΣΩ SMS","receivemail":"ΠΑΡΑΛΑΒΗ ΕΙΔΟΠΟΙΗΣΕΩΝ ΜΕΣΩ EMAIL","changelangage":"ΑΛΛΑΓΗ ΓΛΩΣΣΑΣ","godocking":"ΚΑΘ'ΟΔΟΝ ΠΡΟΣ ΤΟ ΣΤΑΘΜΟ ΦΟΡΤΙΣΗΣ","round":"ΠΕΡΙΗΓΗΣΗ ΨΥΧΑΓΩΓΙΑΣ","patrol":"ΠΕΡΙΠΟΛΙΑ","patrolInProgress":"ΠΕΡΙΠΟΛΙΑ ΣΕ ΕΞΕΛΙΞΗ","roundInProgress":"ΠΕΡΙΗΓΗΣΗ ΨΥΧΑΓΩΓΙΑΣ ΣΕ ΕΞΕΛΙΞΗ","moving":"ΣΕ ΕΞΕΛΙΞΗ ΜΕΤΑΚΙΝΗΣΗ ΣΕ :","charging_remaining":"ΤΟ ΡΟΜΠΟΤ ΦΟΡΤΙΖΕΤΑΙ - ΜΠΑΤΑΡΙΑ: ","walkInProgress":"ΠΕΡΠΑΤΗΣΤΕ ΜΕ ΤΟ ΡΥΘΜΟ ΣΑΣ ΧΩΡΙΣ ΝΑ ΣΠΡΩΧΝΕΤΕ","notpush":"ΤΟΠΟΘΕΤΕΙΣΤΕ ΤΟΝ ΕΑΥΤΟ ΣΑΣ ΟΠΩΣ ΣΤΗ ΦΩΤΟΓΡΑΦΙΑ ΚΑΙ ΠΙΕΣΤΕ  «ΠΑΜΕ»","mails":"E-mails","sms":"SMS","langage":"Γλώσσα","distance_covered":"Απόσταση που διανήθηκε","goto":"Πήγαινε σε :","inProgress":"ΣΕ ΕΞΕΛΙΞΗ","post":"Θέση","sentinel":"Φρουρός","morningRound":"Πρωινή περιήγηση","eveningRound":"Απογευματινή περιήγηση","btn_go":"ΠΗΓΑΙΝΕ","btn_help":"Βοήθεια","btn_walk":"Πάμε !","btn_stop":"ΣΤΑΜΑΤΑ","btn_charge":"ΦΟΡΤΙΣΗ","btn_cancel":"Ακύρωση","btn_ok":"OK","btn_yes":"ΝΑΙ","btn_no":"ΟΧΙ","numexist":"Αυτός ο τηλεφωνικός αριθμός υπάρχει ήδη !","deletenum":"Παρακαλώ πρώτα διαγράψτε ένα τηλεφωνικό αριθμό","numadd":"Ο τηλεφωνικός αριθμός προστέθηκε","numincorrect":"Μη-έγκυρος τηλεφωνικός αριθμός","newnum":"Νέος τηλεφωνικός αριθμός","mailexist":"Αυτή η διεύθυνση υπάρχει ήδη ! ","deletemail":"Παρακαλώ διαγράψτε μία διεύθυνση πρώτα","mailadd":"Η διεύθυνση ηλεκτρονικού ταχυδρομείου προστέθηκε!","mailincorrect":"Μη-έγκυρη διεύθυνση ηλεκτρονικού ταχυδρομείου","newmail":"Νέα διεύθυνση","maps":"Χάρτες","roundlist":"Λίστα γύρων","roundcreation":"ΔΗΜΙΟΥΡΓΙΑ ΓΥΡΩΝ","headBand_rounds":"ΓΥΡΟΙ","rounds_edit":"ΕΠΕΞΕΡΓΑΣΙΑ ΓΥΡΟΥ","wait":"Παρακαλώ περιμένετε ...","error":"Σφάλμα","cantstart":"Η έναρξη της εφαρμογής απέτυχε. Ελέγξτε ότι το ρομπότ είναι σωστά σε λειτουργία.","nointernet":"Δεν υπάρχει σύνδεση στο διαδίκτυο","robotConnectedToI":"Το ρομπότ είναι συνδεδεμένο στο διαδίκτυο","editpswd":"ΕΠΕΞΕΡΓΑΣΙΑ ΚΩΔΙΚΟΥ","currentpswd":"Τρέχον κωδικός :","newpswd":"Νέος κωδικός :","save":"Αποθήκευση","success":"Επιτυχία","pswdsaved":"Ο κωδικός αποθηκεύτηκε","langage1":"Γλώσσα","editpswd1":"Επεξεργασία κωδικού","wrongpass":"Λάθος κωδικός","gabarit":"Προφίλ προστασίας από σύγκρουση","smsSOS":"Κάποιος πίεσε το κουμπί έκτακτης ανάγκης του ρομπότ","mailSOS_suj":"SOS","mailSOS_body":"<br> Κάποιος/α χρειάζεται βοήθεια","mailBattery_suj":"Χαμηλή μπαταρία","mailBattery_body":"<br> Το ρομπότ χρειάζεται φόρτιση. Παρακαλώ τοποθετήστε το στο σταθμό φόρτισης.","mailBlocked_suj":"Το ρομπότ παρεμποδίζεται","mailBlocked_body":"<br> Το ρομπότ παρεμποδίζεται <br>","mailFall_body":"The robot detects a person ashore <br>","mailFall_suj":"Ανιχνεύθηκε ΠΤΩΣΗ","mailPerson_suj":"Ανιχνεύθηκε άτομο","mailPerson_body":"Το ρομπότ ανίχνευσε άτομο <br>","presentAlert_title":"Το ρομπότ βρίσκεται σε φόρτιση","presentAlert_message":"Θα κάνω πίσω !","presentConfirm_title":"Ο γύρος αναβλήθηκε","presentConfirm_message":"Συνέχεια του γύρου ?","FallConfirm_title":"Ανιχνεύθηκε Πτώση !","FallConfirm_message":"Συνέχεια της περιπολίας ?","RemoteConfirm_title":"Διακοπή γύρου από απόσταση","RemoteConfirm_message":"Συνέχεια του γύρου ?","blockedAlert_title":"Το ρομπότ παρεμποδίζεται !","blockedAlert_message":"Παρακαλώ αφαιρέστε το εμπόδιο ή μετακινήστε με","goDockingConfirm_title":"Μπαταρία: ","goDockingConfirm_message":"Μετακίνηση προς το σταθμό φόρτισης? ?","lowBattery_title":"Χαμηλή μπαταρία","lowBattery_message":"Μετακίνηση προς το σταθμό φόρτισης ?","errorlaunchAlert_title":"Προέκυψε σφάλμα","errorlaunchAlert_message":"Παρακαλώ προσπαθήστε ξανά για εκκίνηση του γύρου","robotmuststayondocking_title":"Χαμηλή μπαταρία","robotmuststayondocking_message":"Το ρομπότ πρέπει να παραμείνει στο σταθμό","errorNavAlert_title":"Προέκυψε ένα σφάλμα","errorNavAlert_message":"Παρακαλώ καλέστε την τεχνική υποστήριξη αν το πρόβλημα παραμένει","lostAlert_title":"Το ρομπότ χάθηκε !","lostAlert_message":"Παρακαλώ καλέστε την τεχνική υποστήριξη","quitConfirm_title":"Έξοδος από την εφαρμογή ?","errorBlocked_title":"Το ρομπότ παρεμποδίζεται","errorBlocked_message":"Παρακαλώ ελέγξτε ότι το ρομπότ είναι ελεύθερο και δεν παρεμποδίζεται","statusRedPresent_title":"Σφάλμα","statusRedPresent_message":"Παρακαλώ κάντε επανεκκίνηση Ή καλέστε τον κατασκευαστή μου αν το πρόβλημα παραμένει","statusGreenPresent_title":"Πράσινη κατάσταση","statusGreenPresent_message":"Το ρομπότ είναι υγιές!","leaveDockingConfirm_title":"Μπαταρία: ","leaveDockingConfirm_message":"Έξοδος από το σταθμό φόρτισης ?","leaveDockingAlert_title":"Το ρομπότ βρίσκεται σε φόρτιση","leaveDockingAlert_message":"Παρακαλώ αφαιρέστε με από το σταθμό φόρτισης","askHelp_title":"Ζητήθηκε βοήθεια","askHelp_message":"Κάποιος/α έρχεται να σας βοηθήσει","uvsettings":"ρυθμίσεις UV","evacuationTime":"Χρόνος εκκένωσης (s) :","preheatingTime":"Χρόνος προθέρμανσης (s) : ","lampThreshold":"Ωριαίο όριο λαμπτήρα (h) : ","uvThreshold":"όριο UV (W/m²) : ","batteryThreshold":"Όριο μπαταρίας λαμπτήρα (%) : ","personDetection":"Εντοπισμός ατόμου : ","btn_save":"Αποθήκευση","saveDone":"Αποθηκεύτηκε επιτυχώς","POI":"λίστα POI :","addPOI":"Προσθήκη POI","editPOI":"Επεξεργασία POI","addMap":"Προσθήκη χάρτη","editMap":"EΕπεξεργασία χάρτη","goToPOI":"Μετακίνηση στο POI :","reloc":"Επανατοποθέτηση","confirm":"ΕΠΙΒΕΒΑΙΩΣΗ","delete":"Διαγραφή","edit":"Επεξεργασία","POIname":"όνομα POI :","indicationAddPOI":"Τοποθετήστε το ρομπότ στο επιλεγμένο σημείο και πατήστε “Αποθήκευση”.","mapName":"Όνομα χάρτη :","extensionIndication":"Λάθος τύπος αρχείου. Απαιτείται .png.","colorIndication":"Σφάλμα. Ο χάρτης δεν ακολουθεί τον απαιτούμενο χρωματικό κωδικό.","relocate":"Επανατοποθέτηση","speedOption":"Ταχύτητα (%) : ","speed":"Ταχύτητα : ","directionOption":"Κατεύθυνση : ","frontOption":"Εμπρός","backOption":"Πίσω","avoidanceOption":"Αποφυγή : ","cartography":"ΧΑΡΤΟΓΡΑΦΗΣΗ","qrcodes":"ΚΩΔΙΚΟΙ QR","rounds":"ΓΥΡΟΙ","qrcodes_list":"κωδικοί QR :","qrcodeIndication":"Τοποθετήστε το ρομπότ μπροστά από τον κωδικό QR (~ 50 εκ).","qrcodeIndication1":"Επειτε πιέστε 'Σάρωση'.","qrcodeScan":"Σάρωση","qrcodeDetected1":"κωδικός QR","qrcodeDetected2":"ανιχνεύθηκε","tieRound":"Δέσμευση σε γύρο","round_qrcodesPage":"Γύρος : ","qrcodeUsed1":"κωδικός QR '","qrcodeUsed2":"' χρησιμοποιείται ήδη","name":"Όνομα :","map":"Χάρτης","addPOIfunction":"Προσθήκη POI","POIselected":"POI :","breakOption":"Αναμονή (s) : ","break":"Αναμονή : ","options":"Ρυθμίσεις : ","option":"Ρύθμιση : ","none":"Κανένα","lampon":"Οι λάμπες άναψαν","lampoff":"Οι λάμπες έσβησαν","lampoff1":"Χωρίς ανίχνευση","add":"+","POI_list":"λίστα POI : ","rounds_list":"Γύροι :","copy":"Αντιγραφή","POI_edit":"Γύρος :","deletion":"Διαγραφή","deleteMessage":"Είστε σίγουροι ότι θέλετε να αφαιρέστε αυτό το στοιχείο ?","map_selection":"Επιλέξτε χάρτη","nofile":"Κανένα αρχείο","map_loaded":"Ο χάρτης φορτώθηκε.","configDocking":"Ρύθμιση σύνδεσης","continue":"Επόμενο","configDockingInfo1":"Παρακαλώ απενεργοποιήστε τον σταθμό φόρτισης ενώ τον ρυθμίζετε.","configDockingInfo2":"Τοποθετήστε το ρομπότ στον σταθμό φόρτισης και πιέστε “Αποθήκευση”.","dataRemove":"Αντικατάσταση δεδομένων","dataRemoveInfo":"Τα παλιά δεδομένα θα αντικατασταθούν ώστε να επαναρυθμιστεί ο σταθμός φόρτισης","configDockingConfirm":"Είστε σίγουροι ότι θέλετε να ρυθμίσετε τον σταθμό φόρτισης εκεί ?","password":"Κωδικός","enterPassword":"Εισάγετε τον κωδικό για να αποθηκευτούν οι αλλαγές.","wrongPassword":"Λάθος κωδικός. Παρακαλώ προσπαθήστε ξανά.","URL_toolbox":"assets/pdf/tuto_EN.pdf","manualreloc":"Χειροκίνητη επανατοποθέτηση","qrcode":"Επιλέξτε έναν κωδικό QR","unknownqrcode":"Άγνωστος κωδικός QR","relocalizing":"Μετεγκατάσταση ...","alertReloc":"Παρακαλώ μη μετακινείτε το ρομπότ κατά τη διάρκεια της μετεγκατάστασης","localizationDone":"Το ρομπότ βρίσκεται στον χάρτη : ","noqrdetected":"Δεν ανιχνεύθηκε QR","admin":"Λειτουργία Διαχειριστή","cabinet_open":"Το ντουλάπι είναι ανοιχτό"}

/***/ }),

/***/ 459:
/***/ (function(module, exports) {

/* (ignored) */

/***/ }),

/***/ 461:
/***/ (function(module, exports) {

/* (ignored) */

/***/ }),

/***/ 491:
/***/ (function(module, exports) {

/* (ignored) */

/***/ }),

/***/ 492:
/***/ (function(module, exports) {

/* (ignored) */

/***/ }),

/***/ 493:
/***/ (function(module, exports) {

/* (ignored) */

/***/ }),

/***/ 501:
/***/ (function(module, exports) {

/* (ignored) */

/***/ }),

/***/ 54:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SpeechService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__param_service__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var SpeechService = /** @class */ (function () {
    function SpeechService(param) {
        this.param = param;
        this.msg = new SpeechSynthesisUtterance();
        this.msg.volume = parseFloat("1");
        this.msg.rate = parseFloat("0.9");
        this.msg.pitch = parseFloat("1");
        //this.msg.voice = speechSynthesis.getVoices().filter(function(voice) { return voice.name ; })[10];
    }
    SpeechService.prototype.getVoice = function () {
        var _this = this;
        var voices = speechSynthesis
            .getVoices()
            .filter(function (voice) {
            return voice.localService == true && voice.lang == _this.param.langage;
        });
        if (voices.length > 1) {
            this.msg.voice = voices[1];
        }
        else {
            this.msg.voice = voices[0];
        }
    };
    // Create a new utterance for the specified text and add it to
    // the queue.
    SpeechService.prototype.speak = function (text) {
        this.msg.lang = this.param.langage;
        // Create a new instance of SpeechSynthesisUtterance.
        // Set the text.
        this.getVoice();
        this.msg.text = text;
        //this.msg.voice = speechSynthesis.getVoices().filter(function(voice) { return voice.name ; })[10];
        //console.log(msg);
        // Queue this utterance.
        window.speechSynthesis.speak(this.msg);
    };
    SpeechService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__param_service__["a" /* ParamService */]])
    ], SpeechService);
    return SpeechService;
}());

//# sourceMappingURL=speech.service.js.map

/***/ })

},[291]);
//# sourceMappingURL=main.js.map