**** v2
improve qr code scan
improve wifi internet detection
add https switch
***v1.6
add logistic duration in mail
belharra page for lift 
more exploitation mails
mail duration check if mail has been sent
fix bug map not display if you add a new and no one was charged before
add greek
add italiano
add cabinet open popup
add compressed ros
add admin mode
add reloc qrcode
fix bug when the robot just reboot and has no map charged
fix bug map display
*******v1.5
add tuto pdf
conf docking auto
scrolldown
improve all buttons sensibility
get battery table
display round id (for autolaunch config)
change spanish words
add gabarit profil
change plugin qrdode scanner
****** v1.4
show number version in welcom page
open default eyes at start
open eyes at start
let more time for the sockets to open
more zoom
****** v1.3
update tutorial
add german translation
add possibility to change password in settings
add german language 
resolve bug go to poi (if space in the name) #437
resolve bug in round edition #419
****** v1.2